package alvin.unitils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.unitils.UnitilsJUnit4TestClassRunner;
import org.unitils.inject.annotation.InjectInto;
import org.unitils.inject.annotation.TestedObject;
import org.unitils.mock.Mock;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;


@RunWith(UnitilsJUnit4TestClassRunner.class)
public class InjectTest {

    @InjectInto(property = "userService")
    private Mock<UserService> mockUserService;

    @TestedObject
    private UserController userController;

    @Before
    public void setUp() {
        mockUserService.returns(Optional.of(new User(1, "Alvin"))).findByName("Alvin");
    }

    @Test
    public void test_inject() throws Exception {
        String json = userController.getUser("Alvin");
        assertThat(json, is("{\"id\":1,\"name\":\"Alvin\"}"));
    }

    static class User {
        private int id;
        private String name;

        User(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

    public interface UserService {
        Optional<User> findByName(String name);
    }

    static class UserController {
        private UserService userService;

        String getUser(String name) throws JsonProcessingException {
            Optional<User> userOptional = userService.findByName(name);
            if (!userOptional.isPresent()) {
                return "";
            }
            return new ObjectMapper().writeValueAsString(userOptional.get());
        }
    }
}
