package alvin.unitils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.unitils.UnitilsJUnit4TestClassRunner;
import org.unitils.mock.Mock;
import org.unitils.mock.annotation.Dummy;

import java.util.List;
import java.util.Optional;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;


@RunWith(UnitilsJUnit4TestClassRunner.class)
public class MockTest {

    private UserService userService;
    private List<User> users;

    private Mock<UserService> mockUserService;

    @Dummy
    private User user1, user2;

    @Before
    public void setUp() {
        userService = mockUserService.getMock();
        users = newArrayList(user1, user2);
    }

    @Test
    public void test_mock_find_all() {
        mockUserService.returns(users).findAll();

        List<User> users = userService.findAll();
        assertThat(users, contains(user1, user2));
    }

    @Test
    public void test_mock_find_by_name() {
        mockUserService.returns(Optional.of(user1)).findByName("Alvin");
        mockUserService.returns(Optional.of(user2)).findByName("Emma");
        mockUserService.returns(Optional.empty()).findByName("Lucy");

        Optional<User> userOptional = userService.findByName("Alvin");
        assertThat(userOptional.isPresent(), is(true));
        assertThat(userOptional.get(), is(user1));

        userOptional = userService.findByName("Emma");
        assertThat(userOptional.isPresent(), is(true));
        assertThat(userOptional.get(), is(user2));

        userOptional = userService.findByName("Lucy");
        assertThat(userOptional.isPresent(), is(false));
    }

    static class User {
        private int id;
        private String name;

        User(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

    interface UserService {
        List<User> findAll();

        Optional<User> findByName(String name);
    }
}
