package alvin.unitils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.unitils.UnitilsJUnit4TestClassRunner;
import org.unitils.inject.annotation.InjectIntoByType;
import org.unitils.inject.annotation.TestedObject;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@SuppressWarnings("WeakerAccess")
@RunWith(UnitilsJUnit4TestClassRunner.class)
public class InjectTestWithMockito {

    @Mock
    @InjectIntoByType
    private UserService mockUserService = mock(UserService.class);

    @TestedObject
    protected UserController userController;

    @Before
    public void setUp() {
        when(mockUserService.findByName("Alvin")).thenReturn(Optional.of(new User(1, "Alvin")));
    }

    @Test
    public void test_inject() throws Exception {
        String json = userController.getUser("Alvin");
        assertThat(json, is("{\"id\":1,\"name\":\"Alvin\"}"));
    }

    static class User {
        private int id;
        private String name;

        User(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }

    public interface UserService {
        List<User> findAll();

        Optional<User> findByName(String name);
    }

    public static class UserController {
        private UserService userService;

        public String getUser(String name) throws JsonProcessingException {
            Optional<User> userOptional = userService.findByName(name);
            if (!userOptional.isPresent()) {
                return "";
            }
            return new ObjectMapper().writeValueAsString(userOptional.get());
        }
    }
}
