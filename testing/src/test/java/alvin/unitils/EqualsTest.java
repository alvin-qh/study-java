package alvin.unitils;

import junit.framework.AssertionFailedError;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.unitils.UnitilsJUnit4TestClassRunner;

import static com.google.common.collect.Lists.newArrayList;
import static org.unitils.reflectionassert.ReflectionAssert.assertLenientEquals;
import static org.unitils.reflectionassert.ReflectionAssert.assertPropertyLenientEquals;
import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

@RunWith(UnitilsJUnit4TestClassRunner.class)
public class EqualsTest {

    @Test
    public void test_reflection_equals() {
        User user1 = new User(1, "Alvin");
        User user2 = new User(1, "Alvin");
        assertReflectionEquals(user1, user2);
    }

    @Test
    public void test_reflection_equals_with_collections_in_order() {
        assertReflectionEquals(newArrayList(
                new User(1, "Alvin"),
                new User(2, "Emma")
        ), newArrayList(
                new User(1, "Alvin"),
                new User(2, "Emma")
        ));
    }

    @Test(expected = AssertionFailedError.class)
    public void test_reflection_equals_failed() {
        User user1 = new User(1, "Alvin");
        User user2 = new User(2, "Emma");
        assertReflectionEquals(user1, user2);
    }

    @Test
    public void test_lenient_equals_ignore_null_or_zero_property() {
        assertLenientEquals(newArrayList(
                new User(1, null),
                new User(0, "Emma"),
                null
        ), newArrayList(
                new User(1, "Alvin"),
                new User(2, "Emma"),
                new User(3, "Lucy")
        ));
    }

    @Test(expected = AssertionFailedError.class)
    public void test_lenient_equals_ignore_null_or_zero_property_failed() {
        assertLenientEquals(newArrayList(
                new User(1, null),
                new User(3, "Emma")
        ), newArrayList(
                new User(1, "Alvin"),
                new User(2, "Emma")
        ));
    }

    @Test
    public void test_lenient_equals_with_certain_property() {
        assertPropertyLenientEquals("name", "Alvin", new User(1, "Alvin"));
    }

    @Test
    public void test_lenient_equals_with_certain_property_multi_object() {
        assertPropertyLenientEquals(
                "name",
                newArrayList("Alvin", "Emma"),
                newArrayList(new User(1, "Alvin"), new User(2, "Emma"))
        );
    }

    static class User {
        private int id;
        private String name;

        User(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public String getName() {
            return name;
        }
    }
}
