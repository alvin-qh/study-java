package alvin.drools.loop;

import alvin.drools.common.RuleEngine;
import alvin.drools.loop.domain.Entity1;
import alvin.drools.loop.domain.Entity2;
import alvin.drools.loop.domain.LoopSolution;
import alvin.drools.test.BaseTest;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SuppressWarnings("unchecked")
class RuleTest extends BaseTest {
    private RuleEngine ruleEngine = new RuleEngine(makeResourceFile("drools/loop/rules.drl"));

    @Test
    void test_loop() {
        LoopSolution solution = new LoopSolution(newArrayList(
                new Entity1(11),
                new Entity1(12)
        ), newArrayList(
                new Entity2(21),
                new Entity2(22)
        ));

        ruleEngine.execute(solution);
        List<List<Object>> rows = solution.getRows();
        assertThat(rows.size(), is(16));

        assertThat(rows.get(0).toString(), is("[E1(value: 12), E1(value: 11), E1(value: 12), E1(value: 11)]"));
        assertThat(rows.get(1).toString(), is("[E1(value: 12), E1(value: 11), E1(value: 12), E1(value: 12)]"));
        assertThat(rows.get(2).toString(), is("[E1(value: 12), E1(value: 11), E1(value: 11), E1(value: 11)]"));
        assertThat(rows.get(3).toString(), is("[E1(value: 12), E1(value: 11), E1(value: 11), E1(value: 12)]"));
        // ...
        assertThat(rows.get(15).toString(), is("[E1(value: 11), E1(value: 12), E1(value: 11), E1(value: 12)]"));
    }
}
