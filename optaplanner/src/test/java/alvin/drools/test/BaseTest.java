package alvin.drools.test;

import java.io.File;
import java.util.Objects;

public abstract class BaseTest {

    protected File makeResourceFile(String resource) {
        return new File(Objects.requireNonNull(BaseTest.class.getClassLoader().getResource(resource)).getFile());
    }
}
