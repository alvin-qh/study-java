package alvin.drools.constraint;

import alvin.drools.common.RuleEngine;
import alvin.drools.constraint.domain.ConstraintSolution;
import alvin.drools.constraint.domain.Person;
import alvin.drools.test.BaseTest;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;

class RuleTest extends BaseTest {
    private RuleEngine ruleEngine = new RuleEngine(makeResourceFile("drools/constraint/rule.drl"));

    @Test
    void test_rule() {
        List<Person> persons = newArrayList(
                new Person("Alvin", 30),
                new Person("Emma", 31),
                new Person("Lucy", 32)
        );
        ConstraintSolution solution = new ConstraintSolution(persons);

        persons = newArrayList(
                new Person("老李", 30),
                new Person("老王", 31),
                new Person("老张", 32)
        );
        ruleEngine.addGlobalVariable("persons", persons);

        Map<String, Person> personMap = new HashMap<>();
        personMap.put("Simon", new Person("Simon", 30));
        personMap.put("Emma", new Person("Emma", 31));
        personMap.put("Lucy", new Person("Lucy", 32));
        ruleEngine.addGlobalVariable("personMap", personMap);

        ruleEngine.execute(solution);

        // name is 'Alvin'
        assertThat(solution.getResult1().toString(), is("Person{name='Emma', age=31}"));

        // from persons and age is Alvin's age
        assertThat(solution.getResult2().toString(), is("Person{name='老李', age=30}"));

        // age > 30 from all persons
        assertThat(solution.getResults3().toString(), is("[Person{name='Lucy', age=32}, Person{name='Emma', age=31}]"));

        // age is Alvin's age from global value
        assertThat(solution.getResults4().toString(), is("[Person{name='老李', age=30}]"));

        // match name include 'L' (or 'l')
        assertThat(solution.getResults5().toString(), is("[Person{name='Lucy', age=32}, Person{name='Alvin', age=30}]"));

        // match name start with 'Al' or length of name is 4
        assertThat(solution.getResults6().toString(), is("[Person{name='Lucy', age=32}, Person{name='Emma', age=31}, Person{name='Alvin', age=30}]"));

        // person's name contains 'vin'
        assertThat(solution.getResults7().toString(), is("[Person{name='Alvin', age=30}]"));

        // person's name in ("Alvin", "Lucy")
        assertThat(solution.getResults8().toString(), is("[Person{name='Lucy', age=32}, Person{name='Alvin', age=30}]"));

        // match age is equals than persons[1]'s age
        assertThat(solution.getResult9().toString(), is("Person{name='Emma', age=31}"));

        // find persons which age > 30 into a map
        assertThat(solution.getResults10().keySet(), containsInAnyOrder("Lucy", "Emma"));

        // find person named Lucy from global map, and find other person based on Lucy's age from global person list
        assertThat(solution.getResult11().toString(), is("Person{name='老张', age=32}"));

        // find person which age > 30 from global persons, and get count of them
        assertThat(solution.getResult12(), is(2));

        // find person which age > 30 from global persons, and get total sum value of them
        assertThat(solution.getResult13(), is(63));

        // find person which age > 30 from global persons, and get average value of them
        assertThat(solution.getResult14(), is(315D));
    }
}
