package alvin.drools.hello;

import alvin.drools.common.RuleEngine;
import alvin.drools.hello.domain.HelloSolution;
import alvin.drools.test.BaseTest;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;

@SuppressWarnings("unchecked")
class RuleTest extends BaseTest {

    private RuleEngine ruleEngine = new RuleEngine(makeResourceFile("drools/hello/rules.drl"));

    @Test
    void test_rule_ok() {
        HelloSolution solution = new HelloSolution(true);
        ruleEngine.execute(solution);
        assertThat(solution.getContent(), is("Hello World"));
    }

    @Test
    void test_rule_not_ok() {
        HelloSolution solution = new HelloSolution(false);
        ruleEngine.execute(solution);
        assertThat(solution.getContent(), is(nullValue()));
    }
}
