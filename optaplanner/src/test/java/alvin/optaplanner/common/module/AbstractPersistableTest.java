package alvin.optaplanner.common.module;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class AbstractPersistableTest {

    @Test
    void test_compare_to() {
        assertThat(new TestClass(1L).compareTo(new TestClass(1L)), is(0));
    }

    @Test
    void test_to_string() {
        assertThat(new TestClass(1L).toString(), is("AbstractPersistableTest$TestClass-1"));
    }

    static class TestClass extends AbstractPersistable {
        TestClass(long id) {
            super(id);
        }
    }
}
