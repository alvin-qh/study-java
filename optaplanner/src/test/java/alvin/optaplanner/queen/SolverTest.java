package alvin.optaplanner.queen;

import alvin.optaplanner.queen.domain.NQueens;
import alvin.optaplanner.queen.domain.Queen;
import alvin.optaplanner.queen.persistence.NQueensGenerator;
import alvin.optaplanner.queen.solver.score.NQueensEasyScoreCalculator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;
import org.optaplanner.core.config.phase.PhaseConfig;
import org.optaplanner.core.config.score.director.ScoreDirectorFactoryConfig;
import org.optaplanner.core.config.score.trend.InitializingScoreTrendLevel;
import org.optaplanner.core.config.solver.SolverConfig;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class SolverTest {

    private SolverFactory<NQueens> solverFactory;

    @BeforeEach
    void setUp() {
        solverFactory = SolverFactory.createFromXmlResource("optaplanner/queen/config.xml");
    }

    private ScoreDirectorFactoryConfig makeJavaScoreDirectorFactoryConfig() {
        ScoreDirectorFactoryConfig scoreConfig = new ScoreDirectorFactoryConfig();
        scoreConfig.setEasyScoreCalculatorClass(NQueensEasyScoreCalculator.class);
//        scoreConfig.setScoreDefinitionType(ScoreDefinitionType.SIMPLE);
        scoreConfig.setInitializingScoreTrend(InitializingScoreTrendLevel.ONLY_DOWN.name());
        return scoreConfig;
    }

    private List<PhaseConfig> makePhaseConfigs(List<PhaseConfig> existPhaseConfigs) {
        if (existPhaseConfigs == null) {
            existPhaseConfigs = Collections.emptyList();
        }

//        ConstructionHeuristicPhaseConfig constructionHeuristicPhaseConfig = new ConstructionHeuristicPhaseConfig();
//        constructionHeuristicPhaseConfig.setConstructionHeuristicType(
//                ConstructionHeuristicType.STRONGEST_FIT_DECREASING);
//        phaseConfigs.add(constructionHeuristicPhaseConfig);
//
//        ChangeMoveSelectorConfig changeMoveSelectorConfig = new ChangeMoveSelectorConfig();
//        changeMoveSelectorConfig.setSelectionOrder(SelectionOrder.ORIGINAL);
//
//        LocalSearchPhaseConfig localSearchPhaseConfig = new LocalSearchPhaseConfig();
//        localSearchPhaseConfig.setMoveSelectorConfig(changeMoveSelectorConfig);
//
//        AcceptorConfig acceptorConfig = new AcceptorConfig();
//        acceptorConfig.setEntityTabuSize(5);
//        localSearchPhaseConfig.setAcceptorConfig(acceptorConfig);
//
//        LocalSearchForagerConfig localSearchForagerConfig = new LocalSearchForagerConfig();
//        localSearchForagerConfig.setAcceptedCountLimit(2000);
//        localSearchForagerConfig.setPickEarlyType(LocalSearchPickEarlyType.FIRST_BEST_SCORE_IMPROVING);
//        localSearchPhaseConfig.setForagerConfig(localSearchForagerConfig);
//
//        phaseConfigs.add(localSearchPhaseConfig);
        return new ArrayList<>(existPhaseConfigs);
    }

    @Test
    void test_with_java() {
        SolverConfig solverConfig = solverFactory.getSolverConfig();
        solverConfig.setScoreDirectorFactoryConfig(makeJavaScoreDirectorFactoryConfig());
        solverConfig.setPhaseConfigList(makePhaseConfigs(solverConfig.getPhaseConfigList()));

        Solver<NQueens> solver = solverFactory.buildSolver();

        NQueens nQueens = solver.solve(new NQueensGenerator().createNQueens(64));
        assertThat(checkResult(nQueens), is(true));
    }

    private ScoreDirectorFactoryConfig makeDrlScoreDirectorFactoryConfig() {
        ScoreDirectorFactoryConfig scoreConfig = new ScoreDirectorFactoryConfig();
        scoreConfig.setScoreDrlList(newArrayList("optaplanner/queen/rules.drl"));
        scoreConfig.setInitializingScoreTrend(InitializingScoreTrendLevel.ONLY_DOWN.name());
        return scoreConfig;
    }

    @Test
    void test_with_drl() {
        SolverConfig solverConfig = solverFactory.getSolverConfig();

        solverConfig.setScoreDirectorFactoryConfig(makeDrlScoreDirectorFactoryConfig());
        solverConfig.setPhaseConfigList(makePhaseConfigs(solverConfig.getPhaseConfigList()));

        Solver<NQueens> solver = solverFactory.buildSolver();

        NQueens nQueens = solver.solve(new NQueensGenerator().createNQueens(64));
        assertThat(checkResult(nQueens), is(true));
    }

    private boolean checkResult(NQueens nQueens) {
        for (int i = 0; i < nQueens.getN(); i++) {
            Queen left = nQueens.getQueenList().get(i);
            for (int j = 0; j < nQueens.getN(); j++) {
                Queen right = nQueens.getQueenList().get(j);
                if (left.getId() != right.getId()) {
                    if (left.getRow().equals(right.getRow())) {
                        return false;
                    }
                    if (left.getAscendingDiagonalIndex() == right.getAscendingDiagonalIndex()) {
                        return false;
                    }
                    if (left.getDescendingDiagonalIndex() == right.getDescendingDiagonalIndex()) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
