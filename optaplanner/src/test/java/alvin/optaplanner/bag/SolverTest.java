package alvin.optaplanner.bag;

import alvin.optaplanner.bag.domain.Bag;
import alvin.optaplanner.bag.domain.BagSolution;
import alvin.optaplanner.bag.domain.Goods;
import alvin.optaplanner.bag.domain.InputData;
import alvin.optaplanner.bag.persistence.BagSolutionGenerator;
import alvin.optaplanner.bag.persistence.BagSolutionGeneratorTest;
import com.google.gson.GsonBuilder;
import org.junit.jupiter.api.Test;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

class SolverTest {

    @Test
    void test_solver() throws Exception {
        SolverFactory<BagSolution> solverFactory = SolverFactory.createFromXmlResource("optaplanner/bag/config.xml");
        Solver<BagSolution> solver = solverFactory.buildSolver();

        File file = new File(BagSolutionGeneratorTest.class.getResource("/optaplanner/bag/input-data.json").getFile());
        BagSolution solve = solver.solve(new BagSolutionGenerator().createSolution(file));
        System.out.println(solve);
    }

    static void main(String[] args) {
        final Random random = new Random();

        int bags = 5;
        int goods = 50;
        int totalSize = 30 * goods;

        List<Bag> bagList = new ArrayList<>();
        int leftSize = totalSize;
        for (int i = 0; i < bags; i++) {
            int size = random.nextInt(totalSize / bags - 1) + 1;
            size = Math.min(size, leftSize);
            bagList.add(new Bag(i, size));
            leftSize -= size;
        }

        List<Goods> goodsList = new ArrayList<>();
        leftSize = totalSize;
        for (int i = 0; i < goods; i++) {
            int size = random.nextInt(totalSize / goods - 1) + 1;
            size = Math.min(size, leftSize);
            goodsList.add(new Goods(i, size,
                    BigDecimal.valueOf(random.nextDouble() * 100).setScale(2, RoundingMode.DOWN)));
            leftSize -= size;
        }

        InputData data = new InputData(bagList, goodsList);
        String s = new GsonBuilder().create().toJson(data);
        System.out.println(s);
    }
}
