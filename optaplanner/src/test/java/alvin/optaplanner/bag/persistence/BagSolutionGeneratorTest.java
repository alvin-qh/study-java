package alvin.optaplanner.bag.persistence;

import alvin.optaplanner.bag.domain.BagSolution;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class BagSolutionGeneratorTest {

    @Test
    void test_create_solution() throws Exception {
        File file = new File(BagSolutionGeneratorTest.class.getResource("/optaplanner/bag/input-data.json").getFile());
        BagSolution solution = new BagSolutionGenerator().createSolution(file);

        assertThat(solution.getBags().size(), is(5));
        assertThat(solution.getGoodses().size(), is(50));
        assertThat(solution.getAvgWorth().doubleValue(), is(612.97));
    }
}
