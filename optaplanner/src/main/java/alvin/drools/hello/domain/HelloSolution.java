package alvin.drools.hello.domain;

import alvin.drools.common.BaseSolution;

public class HelloSolution extends BaseSolution {

    private boolean ok;
    private String content;

    public HelloSolution(boolean ok) {
        this.ok = ok;
    }

    public boolean isOk() {
        return ok;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
