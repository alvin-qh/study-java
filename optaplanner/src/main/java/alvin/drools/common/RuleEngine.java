package alvin.drools.common;

import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.internal.io.ResourceFactory;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RuleEngine {

    private final KieServices kieServices;
    private Map<String, Object> globalMap;

    public RuleEngine(File drlFile) {
        kieServices = KieServices.Factory.get();
        KieFileSystem kfs = kieServices.newKieFileSystem();
        kfs.write(ResourceFactory.newFileResource(drlFile));

        KieBuilder kieBuilder = kieServices.newKieBuilder(kfs).buildAll();
        if (kieBuilder.getResults().hasMessages(Message.Level.ERROR)) {
            List<Message> errors = kieBuilder.getResults().getMessages(Message.Level.ERROR);
            StringBuilder sb = new StringBuilder("Errors:");
            for (Message msg : errors) {
                sb.append("\n\t").append(msg);
            }
            throw new RuntimeException(sb.toString());
        }
    }

    public RuleEngine addGlobalVariable(String name, Object variable) {
        if (globalMap == null) {
            globalMap = new HashMap<>();
        }
        globalMap.put(name, variable);
        return this;
    }

    public <T> KieSession execute(BaseSolution<T> solution) {
        final KieContainer kieContainer = kieServices.newKieContainer(
                kieServices.getRepository().getDefaultReleaseId());
        final KieSession kieSession = kieContainer.newKieSession();

        kieSession.insert(solution);
        if (globalMap != null) {
            globalMap.forEach(kieSession::setGlobal);
        }
        solution.getProblemFacts().forEach(kieSession::insert);
        kieSession.fireAllRules();
        return kieSession;
    }
}
