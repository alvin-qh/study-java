package alvin.drools.common;

import java.util.Collection;
import java.util.Collections;

public abstract class BaseSolution<T> {
    public Collection<?> getProblemFacts() {
        return Collections.emptyList();
    }
}
