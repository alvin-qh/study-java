package alvin.drools.constraint.domain;

import alvin.drools.common.BaseSolution;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ConstraintSolution extends BaseSolution<Person> {
    private List<Person> persons;

    private Person result1;
    private Person result2;
    private List<Person> results3;
    private List<Person> results4;
    private List<Person> results5;
    private List<Person> results6;
    private List<Person> results7;
    private List<Person> results8;
    private Person result9;
    private Map<String, Person> results10;
    private Person result11;
    private int result12;
    private int result13;
    private Double result14;

    public ConstraintSolution(List<Person> persons) {
        this.persons = persons;
    }

    @Override
    public Collection<?> getProblemFacts() {
        List<Object> facts = new ArrayList<>();
        facts.addAll(persons);
        return facts;
    }

    public void setResult1(Person result1) {
        this.result1 = result1;
    }

    public void setResult2(Person result2) {
        this.result2 = result2;
    }

    public void setResults3(Collection<Person> result3) {
        results3 = new ArrayList<>(result3);
        Collections.sort(results3);
    }

    public void setResults4(Collection<Person> result4) {
        this.results4 = new ArrayList<>(result4);
        Collections.sort(results4);
    }

    public void setResults5(Collection<Person> result5) {
        this.results5 = new ArrayList<>(result5);
        Collections.sort(results5);
    }

    public void setResults6(Collection<Person> result6) {
        this.results6 = new ArrayList<>(result6);
        Collections.sort(results6);
    }

    public void setResults7(Collection<Person> result7) {
        this.results7 = new ArrayList<>(result7);
        Collections.sort(results7);
    }

    public void setResults8(Collection<Person> result8) {
        this.results8 = new ArrayList<>(result8);
        Collections.sort(results8);
    }

    public void setResult9(Person result9) {
        this.result9 = result9;
    }

    public void setResults10(Map<String, Person> result10) {
        this.results10 = result10;
    }

    public void setResult12(int result12) {
        this.result12 = result12;
    }

    public void setResult11(Person result11) {
        this.result11 = result11;
    }

    public Person getResult1() {
        return result1;
    }

    public Person getResult2() {
        return result2;
    }

    public List<Person> getResults3() {
        return results3 == null ? Collections.emptyList() : results3;
    }

    public List<Person> getResults4() {
        return results4 == null ? Collections.emptyList() : results4;
    }

    public List<Person> getResults5() {
        return results5;
    }

    public List<Person> getResults6() {
        return results6;
    }

    public List<Person> getResults7() {
        return results7 == null ? Collections.emptyList() : results7;
    }

    public List<Person> getResults8() {
        return results8 == null ? Collections.emptyList() : results8;
    }

    public Person getResult9() {
        return result9;
    }

    public Map<String, Person> getResults10() {
        return results10;
    }

    public Person getResult11() {
        return result11;
    }

    public int getResult12() {
        return result12;
    }

    public void setResult13(int result13) {
        this.result13 = result13;
    }

    public int getResult13() {
        return result13;
    }

    public void setResult14(double result14) {
        this.result14 = result14;
    }

    public Double getResult14() {
        return result14;
    }
}
