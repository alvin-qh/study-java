package alvin.drools.loop.domain;

public class Entity1 {
    private int value;

    public Entity1(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "E1(value: " + value + ")";
    }
}
