package alvin.drools.loop.domain;

import alvin.drools.common.BaseSolution;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class LoopSolution extends BaseSolution {
    private List<Entity1> entity1s;
    private List<Entity2> entity2s;
    private List<List<Object>> loopRows = new ArrayList<>();

    public LoopSolution(List<Entity1> entity1s, List<Entity2> entity2s) {
        this.entity1s = entity1s;
        this.entity2s = entity2s;
    }

    @Override
    public Collection<?> getProblemFacts() {
        List<Object> facts = new ArrayList<>();
        facts.addAll(entity1s);
        facts.addAll(entity2s);
        return facts;
    }

    public void addRow(List<Object> row) {
        loopRows.add(row);
    }

    public List<List<Object>> getRows() {
        return loopRows;
    }
}
