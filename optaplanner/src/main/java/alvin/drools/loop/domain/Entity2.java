package alvin.drools.loop.domain;

public class Entity2 {
    private int value;

    public Entity2(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "E2(value: " + value + ")";
    }
}
