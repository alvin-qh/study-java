package alvin.optaplanner.bag.solver.score;

import alvin.optaplanner.bag.domain.Bag;
import alvin.optaplanner.bag.domain.BagSolution;
import alvin.optaplanner.bag.domain.Goods;
import org.optaplanner.core.api.score.Score;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;
import org.optaplanner.core.impl.score.director.easy.EasyScoreCalculator;

import java.math.BigDecimal;
import java.util.List;

public class BagSolutionEasyScoreCalculator implements EasyScoreCalculator<BagSolution> {

    /**
     * Calculate the score of each queen.
     * <p>
     * if two queen in same row, score plus -1;
     * if one queen at top right of other queen, score plus -1
     * if one queen at left down of other queen, score plus -1
     */
    @Override
    public Score calculateScore(BagSolution bagSolution) {
        List<Bag> bags = bagSolution.getBags();
        List<Goods> goodses = bagSolution.getGoodses();

        int hardScore = 0;
        int softScore = 0;
        for (Bag bag : bags) {
            int totalSize = 0;
            BigDecimal totalWorth = BigDecimal.ZERO;

            for (Goods goods : goodses) {
                if (goods.getBag() != null) {
                    if (goods.getBag().getId() == bag.getId()) {
                        totalSize += goods.getSize();
                        totalWorth = totalWorth.add(goods.getWorth());
                    }
                }
            }
            if (totalSize > bag.getSize()) {
                hardScore--;
            }
            if (totalWorth.intValue() >= bagSolution.getAvgWorth().intValue()) {
                softScore++;
            }
        }
        return HardSoftScore.valueOf(hardScore, softScore);
    }
}
