package alvin.optaplanner.bag.move;

import alvin.optaplanner.bag.domain.Bag;
import alvin.optaplanner.bag.domain.BagSolution;
import alvin.optaplanner.bag.domain.Goods;
import com.google.common.base.Objects;
import org.optaplanner.core.impl.heuristic.move.AbstractMove;
import org.optaplanner.core.impl.heuristic.move.Move;
import org.optaplanner.core.impl.heuristic.selector.move.factory.MoveListFactory;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class BagMoveFactory implements MoveListFactory<BagSolution> {

    @Override
    public List<? extends Move<BagSolution>> createMoveList(BagSolution solution) {
        List<Move<BagSolution>> moves = new ArrayList<>();
        for (Bag bag : solution.getBags()) {
            for (Goods goods : solution.getGoodses()) {
                moves.add(new BagMove(goods, bag));
            }
        }
        return moves;
    }

    private static class BagMove extends AbstractMove<BagSolution> {

        private Goods goods;
        private Bag moveTo;

        BagMove(Goods goods, Bag moveTo) {
            this.goods = goods;
            this.moveTo = moveTo;
        }

        @Override
        protected void doMoveOnGenuineVariables(ScoreDirector scoreDirector) {
            scoreDirector.beforeVariableChanged(goods, "bag"); // before changes are made
            goods.setBag(moveTo);
            scoreDirector.afterVariableChanged(goods, "bag"); // after changes are made
        }

        @Override
        public boolean isMoveDoable(ScoreDirector scoreDirector) {
            return goods.getBag().getId() != moveTo.getId();
        }

        @Override
        public AbstractMove<BagSolution> createUndoMove(ScoreDirector scoreDirector) {
            return new BagMove(goods, goods.getBag());
        }

        @Override
        public Collection<Object> getPlanningEntities() {
            return Collections.singletonList(goods);
        }

        @Override
        public Collection<Object> getPlanningValues() {
            return Collections.singletonList(moveTo);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            BagMove bagMove = (BagMove) o;
            return Objects.equal(goods, bagMove.goods) &&
                    Objects.equal(moveTo, bagMove.moveTo);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(goods, moveTo);
        }
    }
}
