package alvin.optaplanner.bag.domain.solution;

import alvin.optaplanner.bag.domain.Bag;

import java.util.Comparator;

public class BagStrengthWeight implements Comparator<Bag> {

    @Override
    public int compare(Bag o1, Bag o2) {
        int n = o1.getSize() - o2.getSize();
        if (n == 0) {
            n = (int) (o1.getId() - o2.getId());
        }
        return n;
    }
}
