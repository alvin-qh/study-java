package alvin.optaplanner.bag.domain;

import alvin.optaplanner.common.module.AbstractPersistable;

public class Bag extends AbstractPersistable {
    private int size;

    public Bag(long id, int size) {
        super(id);
        this.size = size;
    }

    public int getSize() {
        return size;
    }

    @Override
    public String toString() {
        return "Bag-" + id + "(size=" + size + ")";
    }
}
