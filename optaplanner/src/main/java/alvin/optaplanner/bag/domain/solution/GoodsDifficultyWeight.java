package alvin.optaplanner.bag.domain.solution;

import alvin.optaplanner.bag.domain.Goods;

import java.util.Comparator;

public class GoodsDifficultyWeight implements Comparator<Goods> {

    @Override
    public int compare(Goods o1, Goods o2) {
        int n = o2.getSize() - o1.getSize();
        if (n == 0) {
            n = (int) (o1.getId() - o2.getId());
        }
        return n;
    }
}
