package alvin.optaplanner.bag.domain;

import java.util.List;

public class InputData {
    private List<Bag> bags;
    private List<Goods> goodses;

    InputData() {
    }

    public InputData(List<Bag> bags, List<Goods> goodses) {
        this.bags = bags;
        this.goodses = goodses;
    }

    public List<Bag> getBags() {
        return bags;
    }

    public List<Goods> getGoodses() {
        return goodses;
    }
}
