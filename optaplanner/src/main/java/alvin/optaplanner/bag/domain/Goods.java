package alvin.optaplanner.bag.domain;

import alvin.optaplanner.bag.domain.solution.BagStrengthWeight;
import alvin.optaplanner.bag.domain.solution.GoodsDifficultyWeight;
import alvin.optaplanner.common.module.AbstractPersistable;
import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

import java.math.BigDecimal;

@PlanningEntity(difficultyComparatorClass = GoodsDifficultyWeight.class)
public class Goods extends AbstractPersistable {

    private int size;
    private BigDecimal worth;

    private Bag bag;

    Goods() {
    }

    public Goods(int id, int size, BigDecimal worth) {
        super(id);
        this.size = size;
        this.worth = worth;
    }

    public long getBagId() {
        return bag.getId();
    }

    public int getSize() {
        return size;
    }

    public BigDecimal getWorth() {
        return worth;
    }

    public void setBag(Bag bag) {
        this.bag = bag;
    }

    @PlanningVariable(valueRangeProviderRefs = {"bagRange"},
            strengthComparatorClass = BagStrengthWeight.class)
    public Bag getBag() {
        return bag;
    }

    @Override
    public String toString() {
        return "Goods-" + id + "(size=" + size + ",worth=" + worth + ")";
    }
}
