package alvin.optaplanner.bag.domain;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactProperty;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
@PlanningSolution
public class BagSolution {

    private List<Bag> bags;
    private List<Goods> goodses;
    private HardSoftScore hardSoftScore;
    private BigDecimal avgWorth = BigDecimal.ZERO;

    BagSolution() {
    }

    public BagSolution(List<Bag> bags, List<Goods> goodses) {
        this.bags = bags;
        this.goodses = goodses;
        this.avgWorth = goodses.stream()
                .map(Goods::getWorth)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO)
                .divide(BigDecimal.valueOf(bags.size()), RoundingMode.UP);
    }

    @PlanningEntityCollectionProperty
    public List<Goods> getGoodses() {
        return goodses;
    }

    @ValueRangeProvider(id = "bagRange")
    @ProblemFactCollectionProperty
    public List<Bag> getBags() {
        return bags;
    }

    @PlanningScore
    public HardSoftScore getHardSoftScore() {
        return hardSoftScore;
    }

    public void setHardSoftScore(HardSoftScore hardSoftScore) {
        this.hardSoftScore = hardSoftScore;
    }

    public BigDecimal getAvgWorth() {
        return avgWorth;
    }

    @ProblemFactProperty
    public BagSolution getBagSolution() {
        return this;
    }

    @Override
    public String toString() {
        Map<Long, Bag> bagMap = bags.stream().collect(Collectors.toMap(Bag::getId, Function.identity()));
        Map<Long, List<Goods>> goodsMap = goodses.stream().collect(Collectors.groupingBy(g -> g.getBag().getId()));

        final StringBuilder sb = new StringBuilder();
        goodsMap.forEach((bagId, goods) -> {
            Bag bag = bagMap.get(bagId);
            sb.append(bag.toString()).append(":\n");

            int totalSize = 0;
            BigDecimal totalWorth = BigDecimal.ZERO;
            for (Goods g : goods) {
                sb.append("\t").append(g.toString()).append("\n");
                totalSize += g.getSize();
                totalWorth = totalWorth.add(g.getWorth());
            }
            sb.append("\t").append("total size: ")
                    .append(totalSize)
                    .append(", total worth: ")
                    .append(totalWorth)
                    .append("\n");
        });
        return sb.toString();
    }
}
