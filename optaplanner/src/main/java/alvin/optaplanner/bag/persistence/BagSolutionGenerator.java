package alvin.optaplanner.bag.persistence;

import alvin.optaplanner.bag.domain.BagSolution;
import alvin.optaplanner.bag.domain.InputData;
import com.google.gson.GsonBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class BagSolutionGenerator {

    public BagSolution createSolution(File dataFile) throws IOException {
        InputData data = readJson(dataFile);
        return new BagSolution(data.getBags(), data.getGoodses());
    }

    private InputData readJson(File dataFile) throws IOException {
        try (InputStream in = new FileInputStream(dataFile)) {
            try (Reader reader = new BufferedReader(new InputStreamReader(in))) {
                return new GsonBuilder().create().fromJson(reader, InputData.class);
            }
        }
    }
}
