package alvin.optaplanner.queen.move;

import alvin.optaplanner.queen.domain.NQueens;
import alvin.optaplanner.queen.domain.Queen;
import org.optaplanner.core.api.domain.valuerange.CountableValueRange;
import org.optaplanner.core.impl.heuristic.move.AbstractMove;
import org.optaplanner.core.impl.heuristic.move.Move;
import org.optaplanner.core.impl.heuristic.selector.move.factory.MoveListFactory;
import org.optaplanner.core.impl.score.director.ScoreDirector;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class RowChangeMoveFactory implements MoveListFactory<NQueens> {

    private List<Move<NQueens>> movesCache = null;

    @Override
    public List<Move<NQueens>> createMoveList(NQueens nQueens) {
        if (movesCache == null) {
            movesCache = new ArrayList<>();
            for (Queen queen : nQueens.getQueenList()) {
                CountableValueRange<Integer> rowRange = nQueens.getRowRange();
                for (int i = 0; i < rowRange.getSize(); i++) {
                    movesCache.add(new RowMove(queen, rowRange.get(i)));
                }
            }
        }
        return movesCache;
    }

    private static class RowMove extends AbstractMove<NQueens> {

        private Queen queen;
        private int toRow;

        RowMove(Queen queen, int toRow) {
            this.queen = queen;
            this.toRow = toRow;
        }

        @Override
        public boolean isMoveDoable(ScoreDirector scoreDirector) {
            return !Objects.equals(queen.getRow(), toRow);
        }

        @Override
        public AbstractMove createUndoMove(ScoreDirector scoreDirector) {
            return new RowMove(queen, queen.getRow());
        }

        @Override
        protected void doMoveOnGenuineVariables(ScoreDirector scoreDirector) {
            scoreDirector.beforeVariableChanged(queen, "row"); // before changes are made
            queen.setRow(toRow);
            scoreDirector.afterVariableChanged(queen, "row"); // after changes are made
        }

        @Override
        public Collection<?> getPlanningEntities() {
            return Collections.singletonList(queen);
        }

        @Override
        public Collection<?> getPlanningValues() {
            return Collections.singletonList(toRow);
        }

        public String toString() {
            return queen + " {" + queen.getRow() + " -> " + toRow + "}";
        }
    }
}
