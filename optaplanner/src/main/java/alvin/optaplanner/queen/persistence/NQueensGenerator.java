package alvin.optaplanner.queen.persistence;

import alvin.optaplanner.queen.domain.NQueens;
import alvin.optaplanner.queen.domain.Queen;
import com.google.common.math.BigIntegerMath;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class NQueensGenerator {

    private static final Logger LOGGER = LoggerFactory.getLogger(NQueensGenerator.class);

    public NQueens createNQueens(int n) {
//        NQueens nQueens = new NQueens(n, createColumnList(n), createRowList(n), createQueenList(n));
        NQueens nQueens = new NQueens(n, createQueenList(n));
        BigInteger possibleSolutionSize = BigInteger.valueOf(nQueens.getN()).pow(nQueens.getN());
        LOGGER.info("NQueens {} has {} queens with a search space of {}.", n,
                nQueens.getN(), getFlooredPossibleSolutionSize(possibleSolutionSize));
        return nQueens;
    }

    /**
     * Create column with each column index (0 - n).
     */
//    private List<Integer> createColumnList(int n) {
//        List<Integer> columnList = new ArrayList<>(n);
//        for (int i = 0; i < n; i++) {
//            columnList.add(i);
//        }
//        return columnList;
//    }

    /**
     * Create row with each row index (0 - n).
     * <p>
     * If any row is impossible, skip that index
     */
    private List<Integer> createRowList(int n) {
        List<Integer> rowList = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            rowList.add(i);
        }
        return rowList;
    }

    /**
     * Initialize all queens, the ordinal queens in each column and without row.
     */
    private List<Queen> createQueenList(int n) {
        List<Queen> queenList = new ArrayList<>(n);
        long id = 0;
        for (int i = 0; i < n; i++) {
            queenList.add(new Queen(id++, i));
        }
        return queenList;
    }

    private String getFlooredPossibleSolutionSize(BigInteger possibleSolutionSize) {
        if (possibleSolutionSize.compareTo(BigInteger.valueOf(1000L)) < 0) {    // SUPPRESS
            return possibleSolutionSize.toString();
        }
        return "10^" + (BigIntegerMath.log10(possibleSolutionSize, RoundingMode.FLOOR));
    }
}
