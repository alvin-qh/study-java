package alvin.optaplanner.queen.solver.score;

import alvin.optaplanner.queen.domain.NQueens;
import alvin.optaplanner.queen.domain.Queen;
import org.optaplanner.core.api.score.Score;
import org.optaplanner.core.api.score.buildin.simple.SimpleScore;
import org.optaplanner.core.impl.score.director.easy.EasyScoreCalculator;

import java.util.List;

public class NQueensEasyScoreCalculator implements EasyScoreCalculator<NQueens> {

    /**
     * Calculate the score of each queen.
     * <p>
     * if two queen in same row, score plus -1;
     * if one queen at top right of other queen, score plus -1
     * if one queen at left down of other queen, score plus -1
     */
    @Override
    public Score calculateScore(NQueens nQueens) {
        int n = nQueens.getN();
        List<Queen> queenList = nQueens.getQueenList();

        int score = 0;
        for (int i = 0; i < n; i++) {
            for (int j = i + 1; j < n; j++) {
                Queen leftQueen = queenList.get(i);
                Queen rightQueen = queenList.get(j);
                if (leftQueen.getRow() != null && rightQueen.getRow() != null) {
                    if (leftQueen.getRow().equals(rightQueen.getRow())) {
                        score--;
                    }
                    if (leftQueen.getAscendingDiagonalIndex() == rightQueen.getAscendingDiagonalIndex()) {
                        score--;
                    }
                    if (leftQueen.getDescendingDiagonalIndex() == rightQueen.getDescendingDiagonalIndex()) {
                        score--;
                    }
                }
            }
        }
        return SimpleScore.valueOf(score);
    }
}
