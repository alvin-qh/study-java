package alvin.optaplanner.queen.domain;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactProperty;
import org.optaplanner.core.api.domain.valuerange.CountableValueRange;
import org.optaplanner.core.api.domain.valuerange.ValueRangeFactory;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.simple.SimpleScore;

import java.util.List;

@PlanningSolution
public class NQueens {

    // problem size
    private int n;

    // plain problem properties, for '@ProblemFactCollectionProperty' annotation
//    private List<Integer> rowList;

    // plain entities
    private List<Queen> queenList;

    // score of solve result (must be 0 at end of)
    private SimpleScore score;

    /**
     * Default constructor for object clone.
     */
    NQueens() {
    }

    /**
     * Constructor to initialize plain with problem properties.
     *
     * plain problem properties
     */
//    public NQueens(int n, List<Integer> rowList, List<Queen> queenList) {
//        super(0L);
//        this.n = n;
//        this.rowList = rowList;
//        this.queenList = queenList;
//    }

    /**
     * Constructor to initialize plain with problem size.
     *
     * plain problem properties
     */
    public NQueens(int n, List<Queen> queenList) {
        this.n = n;
        this.queenList = queenList;
    }

    public int getN() {
        return n;
    }

    // each of plain problem properties list and problem range

    // get all initialized problem properties list
//    @ValueRangeProvider(id = "rowRange")
//    @ProblemFactCollectionProperty
//    public List<Integer> getRowList() {
//        return rowList;
//    }


    // get plain problem range
    @ValueRangeProvider(id = "rowRange")
    @ProblemFactProperty
    public CountableValueRange<Integer> getRowRange() {
        return ValueRangeFactory.createIntValueRange(0, n);
    }


    // get all initialized plain entities
    @PlanningEntityCollectionProperty
    public List<Queen> getQueenList() {
        return queenList;
    }


    /////////////////////////
    // Score calculate
    @PlanningScore
    public SimpleScore getScore() {
        return score;
    }

    public void setScore(SimpleScore score) {
        this.score = score;
    }
    /////////////////////////
}
