package alvin.optaplanner.queen.domain.solution;

import alvin.optaplanner.queen.domain.NQueens;
import org.optaplanner.core.impl.heuristic.selector.common.decorator.SelectionSorterWeightFactory;


/**
 * Plain were more difficultly when column of queen near by middle column (n / 2).
 *
 * So weight of certain queen would be smaller
 */
public class RowStrengthWeightFactory implements SelectionSorterWeightFactory<NQueens, Integer> {

    @Override
    public Comparable createSorterWeight(NQueens nQueens, Integer row) {
        int middle = nQueens.getN() / 2;
        int distance = Math.abs(row - middle);
        if (nQueens.getN() % 2 == 0 && row - middle < 0) {
            distance--;
        }
        return new RowStrengthWeight(row, distance);
    }

    /**
     * Compare queen to sorted them.
     */
    static class RowStrengthWeight implements Comparable<RowStrengthWeight> {
        private final Integer row;
        private final int distanceFromMiddle;

        RowStrengthWeight(Integer row, int distanceFromMiddle) {
            this.row = row;
            this.distanceFromMiddle = distanceFromMiddle;
        }

        @Override
        public int compareTo(RowStrengthWeight other) {
            int n = other.distanceFromMiddle - distanceFromMiddle;
            if (n == 0) {
                n = row - other.row;
            }
            return n;
        }
    }
}
