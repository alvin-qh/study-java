package alvin.optaplanner.queen.domain.solution;

import alvin.optaplanner.queen.domain.NQueens;
import alvin.optaplanner.queen.domain.Queen;
import org.optaplanner.core.impl.heuristic.selector.common.decorator.SelectionSorterWeightFactory;


/**
 * Plain were more difficultly when column of queen near by middle column (n / 2).
 *
 * So weight of certain queen would be smaller
 */
public class QueenDifficultyWeightFactory implements SelectionSorterWeightFactory<NQueens, Queen> {

    @Override
    public Comparable createSorterWeight(NQueens nQueens, Queen queen) {
        int middle = nQueens.getN() / 2;
        int distance = Math.abs(queen.getColumn() - middle);
        if (nQueens.getN() % 2 == 0 && queen.getColumn() - middle < 0) {
            distance--;
        }
        return new QueenDifficultyWeight(queen, distance);
    }

    /**
     * Compare queen to sorted them.
     */
    static class QueenDifficultyWeight implements Comparable<QueenDifficultyWeight> {
        private final Queen queen;
        private final int distanceFromMiddle;

        QueenDifficultyWeight(Queen queen, int distanceFromMiddle) {
            this.queen = queen;
            this.distanceFromMiddle = distanceFromMiddle;
        }

        @Override
        public int compareTo(QueenDifficultyWeight other) {
            int n = other.distanceFromMiddle - distanceFromMiddle;
            if (n == 0) {
                n = queen.getColumn() - other.queen.getColumn();
            }
            return n;
        }
    }
}
