package alvin.optaplanner.queen.domain;

import alvin.optaplanner.common.module.AbstractPersistable;
import alvin.optaplanner.queen.domain.solution.QueenDifficultyWeightFactory;
import alvin.optaplanner.queen.domain.solution.RowStrengthWeightFactory;
import org.optaplanner.core.api.domain.entity.PlanningEntity;
import org.optaplanner.core.api.domain.variable.PlanningVariable;

@PlanningEntity(difficultyWeightFactoryClass = QueenDifficultyWeightFactory.class)
public class Queen extends AbstractPersistable {

    private Integer column;
    private Integer row = Integer.MIN_VALUE;

    Queen() {
    }

    public Queen(long id, Integer column) {
        super(id);
        this.column = column;
    }

    public Integer getColumn() {
        return column;
    }

    @PlanningVariable(valueRangeProviderRefs = {"rowRange"},
            strengthWeightFactoryClass = RowStrengthWeightFactory.class)
    public Integer getRow() {
        return row;
    }

    public void setRow(Integer row) {
        this.row = row;
    }

    public int getAscendingDiagonalIndex() {
        return getColumn() + getRow();
    }

    public int getDescendingDiagonalIndex() {
        return getColumn() - getRow();
    }
}
