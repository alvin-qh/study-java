package alvin.optaplanner.common.helper;

import org.drools.core.spi.KnowledgeHelper;

public final class DroolsHelper {

    private DroolsHelper() {
    }

    public static void print(final KnowledgeHelper drools, String format, Object... args) {
        System.out.printf("%nrule triggered: %s%n", drools.getRule().getName());
        System.out.printf(format + "%n", args);
    }
}
