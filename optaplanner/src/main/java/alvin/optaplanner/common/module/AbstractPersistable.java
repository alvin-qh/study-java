package alvin.optaplanner.common.module;

import org.apache.commons.lang3.builder.CompareToBuilder;

import java.io.Serializable;

public abstract class AbstractPersistable implements Serializable, Comparable<AbstractPersistable> {

    protected long id;

    protected AbstractPersistable() {
    }

    protected AbstractPersistable(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    @Override
    public int compareTo(AbstractPersistable other) {
        return new CompareToBuilder()
                .append(getClass(), other.getClass())
                .append(id, other.id)
                .toComparison();
    }

    public String toString() {
        return getClass().getName().replaceAll(".*\\.", "") + "-" + id;
    }
}
