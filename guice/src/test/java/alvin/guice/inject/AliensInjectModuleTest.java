package alvin.guice.inject;

import com.google.inject.Guice;
import com.google.inject.Provider;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.inject.Named;

import static alvin.guice.inject.AliensInjectModule.InjectByConstructor;
import static alvin.guice.inject.AliensInjectModule.InjectByField;
import static alvin.guice.inject.AliensInjectModule.InjectByMethod;
import static alvin.guice.inject.AliensInjectModule.VALUE_A;
import static alvin.guice.inject.AliensInjectModule.VALUE_B;
import static alvin.guice.inject.AliensInjectModule.VALUE_C;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

@Slf4j
class AliensInjectModuleTest {

    @Inject
    private InjectByField injectByField;

    @Inject
    private InjectByConstructor injectByConstructor;

    @Inject
    private InjectByMethod injectByMethod;

    @Inject
    @Named(AliensInjectModule.VALUE_C)
    private Provider<String> stringProvider;

    @BeforeEach
    void setup() {
        Guice.createInjector(new AliensInjectModule()).injectMembers(this);
    }

    /**
     * Test inject by field of class, with alien name 'A'.
     */
    @Test
    void test_injectByField() {
        // Make sure the instance is injected into current object
        assertThat(injectByField, is(notNullValue()));

        String value = injectByField.getValue();
        // Make sure the VALUE is certain VALUE
        assertThat(value, is(VALUE_A));
        log.debug(value);
    }

    /**
     * Test inject by constructor of class, with alien name 'B'.
     */
    @Test
    void test_injectByConstructor() {
        // Make sure the instance is injected into current object
        assertThat(injectByConstructor, is(notNullValue()));

        String value = injectByConstructor.getValue();
        // Make sure the VALUE is certain VALUE
        assertThat(value, is(VALUE_B));
        log.debug(value);
    }

    /**
     * Test inject by set method of class, with alien name 'B'.
     */
    @Test
    void test_injectByMethod() {
        // Make sure the instance is injected into current object
        assertThat(injectByMethod, is(notNullValue()));

        String value = injectByMethod.getValue();
        // Make sure the VALUE is certain VALUE
        assertThat(value, is(VALUE_C));
        log.debug(value);
    }

    @Test
    void test_stringProvider() {
        assertThat(stringProvider.get(), is(AliensInjectModule.VALUE_C));
    }
}
