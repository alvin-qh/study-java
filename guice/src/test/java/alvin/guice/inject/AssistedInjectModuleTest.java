package alvin.guice.inject;

import com.google.inject.Guice;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class AssistedInjectModuleTest {

    @Inject
    private AssistedInjectModule.ConnectionFactory connectionFactory;

    @BeforeEach
    void setUp() {
        Guice.createInjector(new AssistedInjectModule()).injectMembers(this);
    }

    @Test
    void test_connection_factory() {
        AssistedInjectModule.Connection connection = connectionFactory.create("alvin", "123123");
        assertThat(connection.toString(), is("alvin@alvin.guice.edu?timeout=1000&password=123123"));
    }

    @Test
    void test_connection_factory_anonymous() {
        AssistedInjectModule.Connection connection = connectionFactory.createAnonymous();
        assertThat(connection.toString(), is("anonymous@alvin.guice.edu?timeout=1000"));
    }
}
