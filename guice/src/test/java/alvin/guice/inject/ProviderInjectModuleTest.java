package alvin.guice.inject;

import com.google.inject.Guice;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static alvin.guice.inject.ProviderInjectModule.InjectByConstructor;
import static alvin.guice.inject.ProviderInjectModule.InjectByField;
import static alvin.guice.inject.ProviderInjectModule.InjectByMethod;
import static alvin.guice.inject.ProviderInjectModule.VALUE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Test inject the VALUE by provider method in module.
 */
@Slf4j
class ProviderInjectModuleTest {

    @Inject
    private InjectByField injectByField;

    @Inject
    private InjectByConstructor injectByConstructor;

    @Inject
    private InjectByMethod injectByMethod;

    @BeforeEach
    void setup() {
        // Create the injector instance and inject somethings into current object
        Guice.createInjector(new ProviderInjectModule()).injectMembers(this);
    }

    /**
     * Test inject by field.
     */
    @Test
    void test_injectByField() {
        // Make sure the instance is injected into current object
        assertThat(injectByField, is(notNullValue()));

        String value = injectByField.getValue();
        // Make sure the certain VALUE is injected into the instance
        assertThat(value, is(VALUE));
        log.debug(value);
    }

    /**
     * Test inject by constructor.
     */
    @Test
    void test_injectByConstructor() {
        // Make sure the instance is injected into current object
        assertThat(injectByConstructor, is(notNullValue()));

        String value = injectByConstructor.getValue();
        // Make sure the certain VALUE is injected into the instance
        assertThat(value, is(VALUE));
        log.debug(value);
    }

    /**
     * Test inject by method.
     */
    @Test
    void test_injectByMethod() {
        // Make sure the instance is injected into current object
        assertThat(injectByMethod, is(notNullValue()));

        String value = injectByMethod.getValue();
        // Make sure the certain VALUE is injected into the instance
        assertThat(value, is(VALUE));
        log.debug(value);
    }
}
