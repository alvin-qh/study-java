package alvin.guice.inject;

import com.google.inject.Guice;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Test inject different values by different annotations.
 */
@Slf4j
class AnnotationInjectModuleTest {

    @Inject
    private AnnotationInjectModule.InjectByField injectByField;

    @Inject
    private AnnotationInjectModule.InjectByConstructor injectByConstructor;

    @Inject
    private AnnotationInjectModule.InjectByMethod injectByMethod;

    @BeforeEach
    void setup() {
        Guice.createInjector(new AnnotationInjectModule()).injectMembers(this);
    }

    /**
     * Test inject VALUE by field, the VALUE was injected with annotation 'A'.
     */
    @Test
    void test_injectByField() {
        // Make sure the instance has been injected into current object
        assertThat(injectByField, is(notNullValue()));

        // Get the VALUE from injected instance
        String value = injectByField.getValue();
        // Make sure the VALUE is certain VALUE
        assertThat(value, Matchers.is(AnnotationInjectModule.VALUE_A));
        log.debug(value);
    }

    /**
     * Test inject VALUE by constructor, the VALUE was injected with annotation 'B'.
     */
    @Test
    void test_injectByConstructor() {
        // Make sure the instance has been injected into current object
        assertThat(injectByConstructor, is(notNullValue()));

        // Get the VALUE from injected instance
        String value = injectByConstructor.getValue();
        // Make sure the VALUE is certain VALUE
        assertThat(value, Matchers.is(AnnotationInjectModule.VALUE_B));
        log.debug(value);
    }

    /**
     * Test inject VALUE by set method, the VALUE was injected with annotation 'C'.
     */
    @Test
    void test3_injectByMethod() {
        // Make sure the instance has been injected into current object
        assertThat(injectByMethod, is(notNullValue()));

        // Get the VALUE from injected instance
        String value = injectByMethod.getValue();
        // Make sure the VALUE is certain VALUE
        assertThat(value, Matchers.is(AnnotationInjectModule.VALUE_C));
        log.debug(value);
    }
}
