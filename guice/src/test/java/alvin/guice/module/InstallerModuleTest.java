package alvin.guice.module;

import alvin.guice.interfaces.BindDemo;
import com.google.inject.Guice;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Random;

import static alvin.guice.module.InstallerModule.BindDemoImplA;
import static alvin.guice.module.InstallerModule.BindDemoImplB;
import static alvin.guice.module.InstallerModule.MODULE_A;
import static alvin.guice.module.InstallerModule.MODULE_B;
import static alvin.guice.module.InstallerModule.MODULE_NAME;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.typeCompatibleWith;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Test inject instance by different modules.
 */
@Slf4j
class InstallerModuleTest {

    // Inject the instance of interface
    @Inject
    private BindDemo bindDemo;

    // Inject the module name by alien name 'MODULE_NAME'
    @Inject
    @Named(MODULE_NAME)
    private String moduleName;

    @BeforeEach
    void setup() {
        // Get module name by random
        String moduleName = new Random().nextBoolean() ? MODULE_A : MODULE_B;
        // Get injector instance and inject things into current object with different module name
        Guice.createInjector(new InstallerModule(moduleName)).injectMembers(this);
    }

    /**
     * Test inject VALUE by different modules.
     */
    @Test
    void test() {
        // Check the different instance by module name
        switch (moduleName) {
        case MODULE_A:
            // Make sure the type of instance is class 'BindDemoImplA'
            assertThat(bindDemo.getClass(), is(typeCompatibleWith(BindDemoImplA.class)));
            // Make sure the VALUE of instance is 'MODULE_A'
            assertThat(bindDemo.test(), is(MODULE_A));
            break;
        case MODULE_B:
            // Make sure the type of instance is class 'BindDemoImplB'
            assertThat(bindDemo.getClass(), is(typeCompatibleWith(BindDemoImplB.class)));
            // Make sure the VALUE of instance is 'MODULE_B'
            assertThat(bindDemo.test(), is(MODULE_B));
            break;
        default:
            fail();
            break;
        }
        log.debug(bindDemo.getClass().getCanonicalName());
        log.debug(bindDemo.test());
    }
}
