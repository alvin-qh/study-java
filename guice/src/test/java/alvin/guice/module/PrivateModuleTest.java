package alvin.guice.module;

import alvin.guice.interfaces.BindDemo;
import com.google.inject.ConfigurationException;
import com.google.inject.Guice;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import javax.inject.Inject;
import javax.inject.Named;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SuppressWarnings("unused")
class PrivateModuleTest {

    @Test
    void test_binding_a() {
        Executable executable = () -> {
            TestForA testForA = new TestForA();
            Guice.createInjector(new PrivateModule()).injectMembers(testForA);
        };
        assertThrows(ConfigurationException.class, executable);
    }

    @Test
    void test_binding_b() {
        TestForB testForB = new TestForB();
        Guice.createInjector(new PrivateModule()).injectMembers(testForB);

        assertThat(testForB.bindDemo.test(), is("This is BindDemoImplB"));
    }

    static class TestForA {
        @Inject
        @Named(PrivateModule.NAMED_A)
        private BindDemo bindDemo;

        BindDemo getBindDemo() {
            return bindDemo;
        }
    }

    static class TestForB {
        @Inject
        @Named(PrivateModule.NAMED_B)
        private BindDemo bindDemo;

        BindDemo getBindDemo() {
            return bindDemo;
        }
    }
}
