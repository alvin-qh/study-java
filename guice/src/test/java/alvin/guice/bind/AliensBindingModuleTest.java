package alvin.guice.bind;

import alvin.guice.interfaces.BindDemo;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.typeCompatibleWith;

/**
 * Test binding different classes to one interface with different alien names.
 */
@Slf4j
class AliensBindingModuleTest {
    private Injector injector;

    @BeforeEach
    void setup() {
        // create new injector instance.
        injector = Guice.createInjector(new AliensBindingModule());
    }

    /**
     * Test get instance which is bind with 'BindDemo' interface with alien name 'A'.
     */
    @Test
    void test_getInstanceByAlienNameA() {
        // Get instance of interface by alien name 'A'
        BindDemo bindDemo = injector.getInstance(Key.get(BindDemo.class, Names.named("A")));

        // Make sure the type of instance is 'BindDemoImplA'
        assertThat(bindDemo.getClass(), typeCompatibleWith(AliensBindingModule.BindDemoImplA.class));
        log.debug(bindDemo.getClass().getCanonicalName());
    }

    /**
     * Test get instance which is bind with 'BindDemo' interface with alien name 'B'.
     */
    @Test
    void test_getInstanceByAlienNameB() {
        // Get instance of interface by alien name 'B'
        BindDemo bindDemo = injector.getInstance(Key.get(BindDemo.class, Names.named("B")));

        // Make sure the type of instance is 'BindDemoImplB'
        assertThat(bindDemo.getClass(), typeCompatibleWith(AliensBindingModule.BindDemoImplB.class));
        log.debug(bindDemo.getClass().getCanonicalName());
    }
}
