package alvin.guice.bind;

import alvin.guice.annotations.PropertyBind;
import alvin.guice.bind.MultiBinderModule.PersonNames;
import com.google.common.base.Joiner;
import com.google.inject.Guice;
import com.google.inject.name.Named;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;

class MultiBinderModuleTest {

    @Inject
    @Named("integer_set")
    private Set<Integer> numberSet;

    @Inject
    @Named("string_map")
    private Map<String, String> stringMap;

    @Inject
    @PropertyBind
    private Map<String, Object> propertyMap;

    @BeforeEach
    void setUp() {
        MultiBinderModule.NameProvider.setGender("M");
        Guice.createInjector(new MultiBinderModule()).injectMembers(this);
    }

    @Test
    void test_numberSet_injection() {
        assertThat(numberSet.size(), is(3));
        assertThat(numberSet, contains(100, 200, 300));
    }

    @Test
    void test_stringMap_injection() {
        assertThat(stringMap.get("A"), is("Alvin"));
        assertThat(stringMap.get("B"), is("Alvin"));
        assertThat(stringMap.get("C"), is("Emma"));
    }

    @Test
    void test_propertyMap_injection() {
        final PersonNames personNames = (PersonNames) propertyMap.get("names");
        final String names = Joiner.on(",").join(personNames.getNames());
        assertThat(names, is("Alvin,Tom,Lily,Author"));
    }
}
