package alvin.guice.bind;

import alvin.guice.annotations.A;
import alvin.guice.annotations.B;
import alvin.guice.interfaces.BindDemo;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static alvin.guice.bind.AnnotationBindingModule.BindDemoImplA;
import static alvin.guice.bind.AnnotationBindingModule.BindDemoImplB;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.typeCompatibleWith;

@Slf4j
class AnnotationBindingModuleTest {

    private Injector injector;

    @BeforeEach
    void setup() {
        // create new injector instance.
        injector = Guice.createInjector(new AnnotationBindingModule());
    }

    /**
     * Test get instance of 'BindDemo' interface with annotation 'A'.
     */
    @Test
    void test_getInstanceByAnnotationA() {
        // Get instance by annotation A
        BindDemo bindDemo = injector.getInstance(Key.get(BindDemo.class, A.class));

        // Make sure the type of instance is 'BindDemoImplA'
        assertThat(bindDemo.getClass(), typeCompatibleWith(BindDemoImplA.class));
        log.debug(bindDemo.getClass().getCanonicalName());
    }

    /**
     * Test get instance of 'BindDemo' interface with annotation 'B'.
     */
    @Test
    void test_getInstanceByAnnotationB() {
        // Get instance by annotation B
        BindDemo bindDemo = injector.getInstance(Key.get(BindDemo.class, B.class));

        // Make sure the type of instance is 'BindDemoImplB'
        assertThat(bindDemo.getClass(), typeCompatibleWith(BindDemoImplB.class));
        log.debug(bindDemo.getClass().getCanonicalName());
    }
}
