package alvin.guice.bind;

import alvin.guice.interfaces.BindDemo;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provider;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static alvin.guice.bind.ProviderBindingModule.BindDemoImpl;
import static alvin.guice.bind.ProviderBindingModule.VALUE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.typeCompatibleWith;

/**
 * Test bind interface to a provider callback method.
 */
@Slf4j
class ProviderBindingModuleTest {

    private Injector injector;

    @BeforeEach
    void setup() {
        // Create the injector instance
        injector = Guice.createInjector(new ProviderBindingModule());
    }

    /**
     * Test get class instance with binding a provider.
     */
    @Test
    void test_getBindDemoInstanceByProviderBinding() {
        // Get the instance of 'BindDemo' type
        BindDemo instance = injector.getInstance(BindDemo.class);
        assertThat(instance.getClass(), typeCompatibleWith(BindDemoImpl.class));
        log.debug(instance.getClass().getCanonicalName());

        // Get the VALUE from instance
        String value = instance.test();
        // Make sure the VALUE is a certain VALUE
        assertThat(value, is(VALUE));
        log.debug(value);
    }

    /**
     * Test get provider with binding a provider.
     */
    @Test
    void test_getBindDemoProviderByProviderBinding() {
        // Get the provider
        Provider<BindDemo> provider = injector.getProvider(BindDemo.class);
        // Get the instance of 'BindDemo' type by the provider
        BindDemo instance = provider.get();
        // Make sure the type of instance is 'BindDemoImpl'
        assertThat(instance.getClass(), typeCompatibleWith(BindDemoImpl.class));
        log.debug(instance.getClass().getCanonicalName());

        // Get the VALUE from instance
        String value = instance.test();
        // Make sure the VALUE is a certain VALUE
        assertThat(value, is(VALUE));
        log.debug(value);
    }
}
