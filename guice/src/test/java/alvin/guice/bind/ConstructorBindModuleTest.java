package alvin.guice.bind;

import alvin.guice.interfaces.BindDemo;
import com.google.inject.Guice;
import com.google.inject.Injector;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static alvin.guice.bind.ConstructorBindingModule.BindDemoImpl;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.typeCompatibleWith;

/**
 * Test binding a constructor of class to an interface.
 */
@Slf4j
class ConstructorBindModuleTest {

    private Injector injector;

    @BeforeEach
    void setup() {
        // Create injector instance
        injector = Guice.createInjector(new ConstructorBindingModule());
    }

    @Test
    void test() {
        // Get instance of interface
        BindDemo bindDemo = injector.getInstance(BindDemo.class);
        // Make sure the type of instance is 'BindDemoImpl'
        assertThat(bindDemo.getClass(), typeCompatibleWith(BindDemoImpl.class));
        log.debug(bindDemo.getClass().getCanonicalName());

        // Get VALUE from instance
        String expected = bindDemo.test();
        // Make sure the VALUE is equals to the expected VALUE
        assertThat(expected, is(ConstructorBindingModule.VALUE));
        log.debug(expected);
    }
}
