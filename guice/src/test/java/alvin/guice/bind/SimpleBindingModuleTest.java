package alvin.guice.bind;

import alvin.guice.interfaces.BindDemo;
import com.google.inject.Guice;
import com.google.inject.Injector;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.typeCompatibleWith;

/**
 * Test the simple binding, binding a interface to it's implemented class.
 */
@Slf4j
class SimpleBindingModuleTest {

    private Injector injector;

    @BeforeEach
    void setup() {
        // Create a injector instance
        injector = Guice.createInjector(new SimpleBindingModule());
    }

    /**
     * Test the binding.
     */
    @Test
    void test() {
        // Get the instance by 'BindDemo' type
        BindDemo instance = injector.getInstance(BindDemo.class);

        // Make sure the instance is match the certain type
        assertThat(instance.getClass(), typeCompatibleWith(SimpleBindingModule.BindDemoImpl.class));
        log.debug(instance.getClass().getCanonicalName());
    }
}
