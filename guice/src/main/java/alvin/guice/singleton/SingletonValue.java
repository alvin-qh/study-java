package alvin.guice.singleton;


import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

@Singleton
@Slf4j
public class SingletonValue {
    private String value;

    @Inject
    public SingletonValue(@Named("Value") String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void showValue() {
        log.debug("The VALUE is " + value);
    }
}
