package alvin.guice.singleton;

import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.inject.Named;

@Slf4j
public class SingletonInterfaceImpl implements SingletonInterface {
    private String value;

    @Inject
    public SingletonInterfaceImpl(@Named("Value") String value) {
        this.value = value;
    }

    @Override
    public void showValue() {
        log.debug("The VALUE is " + value);
    }
}
