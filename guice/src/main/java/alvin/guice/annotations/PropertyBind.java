package alvin.guice.annotations;

import com.google.inject.BindingAnnotation;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Documented
@BindingAnnotation
@Retention(RetentionPolicy.RUNTIME)
public @interface PropertyBind {
}
