package alvin.guice.inject;

import com.google.common.base.Strings;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.assistedinject.Assisted;
import com.google.inject.assistedinject.AssistedInject;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.google.inject.name.Named;

import javax.inject.Singleton;

public class AssistedInjectModule extends AbstractModule {

    @Override
    protected void configure() {
        install(
                // implement(...) can be ignore if no interface was implemented, just bind builder class.
                new FactoryModuleBuilder()
                        .implement(Connection.class, ConnectionImpl.class)
                        .build(ConnectionFactory.class)
        );
    }

    @Provides
    @Singleton
    @Named("url")
    public String provideUrl() {
        return "alvin.guice.edu";
    }

    @Provides
    @Singleton
    @Named("timeout")
    public int provideTimeout() {
        return 1000;    // SUPPRESS
    }

    public interface Connection {
        String toString();
    }

    public static class ConnectionImpl implements Connection {
        private String url;
        private int timeout;
        private String account;
        private String password;

        @AssistedInject
        public ConnectionImpl(@Named("url") String url,
                              @Named("timeout") int timeout,
                              @Assisted("account") String account,
                              @Assisted("password") String password) {
            this.url = url;
            this.timeout = timeout;
            this.account = account;
            this.password = password;
        }

        @AssistedInject
        public ConnectionImpl(@Named("url") String url,
                              @Named("timeout") int timeout) {
            this.url = url;
            this.timeout = timeout;
        }

        @Override
        public String toString() {
            if (Strings.isNullOrEmpty(account)) {
                return String.format("anonymous@%s?timeout=%s", url, timeout);
            }
            return String.format("%s@%s?timeout=%s&password=%s", account, url, timeout, password);
        }
    }

    public interface ConnectionFactory {
        Connection create(@Assisted("account") String account,
                          @Assisted("password") String password);

        Connection createAnonymous();
    }
}
