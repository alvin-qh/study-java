package alvin.guice.inject;

import com.google.inject.AbstractModule;

import javax.inject.Inject;

/**
 * A module bind the String instance to a provider.
 */
public class SimpleInjectModule extends AbstractModule {
    public static final String VALUE = "SimpleInjectModule";

    @Override
    protected void configure() {
        // To provide the String VALUE for inject
        bind(String.class).toProvider(() -> VALUE);
    }

    /**
     * Inject the VALUE by field.
     */
    public static class InjectByField {
        @Inject
        private String value;

        public String getValue() {
            return value;
        }
    }

    /**
     * Inject the VALUE by constructor.
     */
    public static class InjectByConstructor {
        private String value;

        @Inject
        public InjectByConstructor(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    /**
     * Inject the VALUE by set method.
     */
    public static class InjectByMethod {
        private String value;

        public String getValue() {
            return value;
        }

        @Inject
        public void setValue(String value) {
            this.value = value;
        }
    }
}
