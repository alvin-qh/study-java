package alvin.guice.module;

import alvin.guice.interfaces.BindDemo;
import com.google.inject.Scopes;
import com.google.inject.name.Names;

public class PrivateModule extends com.google.inject.PrivateModule {

    public static final String NAMED_A = "A";
    public static final String NAMED_B = "B";

    @Override
    protected void configure() {
        bind(BindDemo.class).annotatedWith(Names.named(NAMED_A)).to(BindDemoImplA.class).in(Scopes.SINGLETON);
        bind(BindDemo.class).annotatedWith(Names.named(NAMED_B)).to(BindDemoImplB.class).in(Scopes.SINGLETON);
        expose(BindDemo.class).annotatedWith(Names.named(NAMED_B));
    }

    public static class BindDemoImplA implements BindDemo {
        private String value;

        @Override
        public String test() {
            return "This is BindDemoImplA";
        }
    }

    public static class BindDemoImplB implements BindDemo {
        @Override
        public String test() {
            return "This is BindDemoImplB";
        }
    }
}
