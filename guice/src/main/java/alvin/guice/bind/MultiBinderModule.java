package alvin.guice.bind;

import alvin.guice.annotations.PropertyBind;
import com.google.inject.AbstractModule;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.name.Names;

import javax.inject.Provider;
import javax.inject.Singleton;


public class MultiBinderModule extends AbstractModule {

    @Override
    protected void configure() {
        Multibinder<Integer> numberSet = Multibinder.newSetBinder(binder(), Integer.class, Names.named("integer_set"));
        numberSet.addBinding().toInstance(100); // SUPPRESS
        numberSet.addBinding().toInstance(200); // SUPPRESS
        numberSet.addBinding().toInstance(300); // SUPPRESS

        MapBinder<String, String> stringMapBinder = MapBinder.newMapBinder(binder(),
                String.class, String.class, Names.named("string_map"));

        stringMapBinder.addBinding("A").toProvider(NameProvider.class);
        stringMapBinder.addBinding("B").toInstance("Alvin");
        stringMapBinder.addBinding("C").toInstance("Emma");


        MapBinder<String, Object> propertyMapBinder = MapBinder.newMapBinder(binder(),
                String.class, Object.class, PropertyBind.class);
        propertyMapBinder.addBinding("names").to(PersonNames.class);
    }

    @Singleton
    public static class NameProvider implements Provider<String> {

        private static String gender;

        @Override
        public String get() {
            if ("M".equals(gender)) {
                return "Alvin";
            } else {
                return "Emma";
            }
        }

        static void setGender(String gender) {
            NameProvider.gender = gender;
        }
    }

    @Singleton
    public static class PersonNames {
        private String[] names = {"Alvin", "Tom", "Lily", "Author"};

        public String[] getNames() {
            return names;
        }
    }
}
