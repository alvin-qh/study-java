package alvin.common.db;

import org.junit.jupiter.api.Test;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class MigrationTest {
    private static final String CONNECT_URL = "jdbc:derby:memory:test_db;create=true";

    @Test
    void test_migration() throws Exception {
        new Migration.Builder(CONNECT_URL)
                .account("APP")
                .password("APP")
                .addLocation(makeResourceFile("/migrate"))
                .build()
                .reset();

        try (Connection conn = DriverManager.getConnection(CONNECT_URL)) {
            final String sql = "insert into test_user(name, telephone)values(?, ?)";
            try (PreparedStatement stat = conn.prepareStatement(sql)) {
                stat.setString(1, "Alvin");
                stat.setString(2, "13000012345");
                stat.executeUpdate();
            }
        }

        List<Map<String, Object>> rows = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(CONNECT_URL)) {
            final String sql = "select name, telephone from test_user";
            try (PreparedStatement stat = conn.prepareStatement(sql)) {
                try (ResultSet rs = stat.executeQuery()) {
                    if (rs.next()) {
                        do {
                            Map<String, Object> row = new LinkedHashMap<>();
                            row.put("name", rs.getString("name"));
                            row.put("telephone", rs.getString("telephone"));
                            rows.add(row);
                        } while (rs.next());
                    }
                }
            }
        }
        assertThat(rows.size(), is(1));
        assertThat(rows.get(0).get("name"), is("Alvin"));
        assertThat(rows.get(0).get("telephone"), is("13000012345"));
    }

    private String makeResourceFile(String path) {
        URL resource = MigrationTest.class.getResource(path);
        if (resource == null) {
            throw new IllegalArgumentException(String.format("path '%s' is not exist", path));
        }
        return "filesystem:" + resource.getFile();
    }
}
