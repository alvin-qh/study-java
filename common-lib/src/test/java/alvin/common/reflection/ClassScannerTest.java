package alvin.common.reflection;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Slf4j
class ClassScannerTest {

    @Test
    void test_matching() {
        final Matcher<Class> matcher = Matchers.subclassesOf(TestClass.class)
                .and(Matchers.annotatedWith(TestAnnotation.class))
                .and(Matchers.not(Matchers.is(TestClass.class)));

        log.debug("matcher is: {}", matcher);

        Set<Class<?>> classes = ClassScanner.matching(matcher)
                .in(ClassScannerTest.class.getPackage());

        assertThat(classes.size(), is(1));
        assertThat(classes.contains(TestSubClass.class), is(true));
    }

    @Documented
    @Retention(RetentionPolicy.RUNTIME)
    private @interface TestAnnotation {
    }

    @TestAnnotation
    private static class TestClass {

        @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
        @Override
        public boolean equals(Object obj) {
            return true;
        }

        @Override
        public int hashCode() {
            return 0;
        }
    }

    @TestAnnotation
    private static class TestSubClass extends TestClass {
    }
}
