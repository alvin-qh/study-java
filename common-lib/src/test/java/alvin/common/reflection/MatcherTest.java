package alvin.common.reflection;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class MatcherTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(MatcherTest.class);

    @Test
    void test_any() {
        Matcher<Object> matcher = Matchers.any();
        LOGGER.debug("matcher is: {}", matcher);

        assertThat(matcher.matches(TestClass.class), is(true));
    }

    @Test
    void test_not() {
        Matcher<Object> matcher = Matchers.not(Matchers.any());
        LOGGER.debug("matcher is: {}", matcher);

        assertThat(matcher.matches(TestClass.class), is(false));
    }

    @Test
    void test_annotatedWith() {
        Matcher<AnnotatedElement> matcher = Matchers.annotatedWith(TestAnnotation.class);
        LOGGER.debug("matcher is: {}", matcher);

        assertThat(matcher.matches(TestClass.class), is(true));
    }

    @Test
    void test_annotatedWith_annotation() {
        TestAnnotation annotation = TestClass.class.getAnnotation(TestAnnotation.class);
        Matcher<AnnotatedElement> matcher = Matchers.annotatedWith(annotation);
        LOGGER.debug("matcher is: {}", matcher);

        assertThat(matcher.matches(TestClass.class), is(true));
    }

    @Test
    void test_methodWith_annotation() throws Exception {
        Matcher<AnnotatedElement> matcher = Matchers.annotatedWith(TestAnnotation.class);
        LOGGER.debug("matcher is: {}", matcher);

        assertThat(matcher.matches(TestClass.class.getMethod("equals", Object.class)), is(true));
    }

    @Test
    void test_subclassOf() {
        Matcher<Class> matcher = Matchers.subclassesOf(TestClass.class);
        LOGGER.debug("matcher is: {}", matcher);

        assertThat(matcher.matches(TestSubClass.class), is(true));
    }

    @Test
    void test_only() {
        Matcher<Object> matcher = Matchers.only(new TestClass());
        LOGGER.debug("matcher is: {}", matcher);

        assertThat(matcher.matches(new TestClass()), is(true));
    }

    @Test
    void test_subPackage() {
        Matcher<Class> matcher = Matchers.inSubpackage("alvin.common.reflection");
        LOGGER.debug("matcher is: {}", matcher);

        assertThat(matcher.matches(TestClass.class), is(true));
    }

    @Test
    void test_identicalTo() {
        TestClass testClass = new TestClass();
        Matcher<Object> matcher = Matchers.identicalTo(testClass);
        LOGGER.debug("matcher is: {}", matcher);

        assertThat(matcher.matches(testClass), is(true));
    }

    @Test
    void test_identicalTo_failed() {
        Matcher<Object> matcher = Matchers.identicalTo(new TestClass());
        LOGGER.debug("matcher is: {}", matcher);

        assertThat(matcher.matches(new TestClass()), is(false));
    }

    @Test
    void test_inPackage() {
        Package pack = getClass().getClassLoader().getDefinedPackage("alvin.common.reflection");
        Matcher<Class> matcher = Matchers.inPackage(pack);
        LOGGER.debug("matcher is: {}", matcher);

        assertThat(matcher.matches(TestClass.class), is(true));
    }

    @Test
    void test_and() {
        Package pack = getClass().getClassLoader().getDefinedPackage("alvin.common.reflection");
        Matcher<Class> matcher = Matchers.inPackage(pack);
        matcher.and(Matchers.annotatedWith(TestAnnotation.class));
        LOGGER.debug("matcher is: {}", matcher);

        assertThat(matcher.matches(TestClass.class), is(true));
    }

    @Test
    void test_is() {
        Matcher<Class> matcher = Matchers.is(TestClass.class);
        LOGGER.debug("matcher is: {}", matcher);

        assertThat(matcher.matches(TestClass.class), is(true));
    }

    @Test
    void test_returns() throws Exception {
        Matcher<Method> matcher = Matchers.returns(new AbstractMatcher<>() {
            @Override
            public boolean matches(Class<?> clazz) {
                return clazz == boolean.class;
            }
        });
        LOGGER.debug("matcher is: {}", matcher);

        assertThat(matcher.matches(TestClass.class.getMethod("equals", Object.class)), is(true));
    }

    @Documented
    @Retention(RetentionPolicy.RUNTIME)
    private @interface TestAnnotation {
    }

    @TestAnnotation
    private static class TestClass {

        @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
        @Override
        @TestAnnotation
        public boolean equals(Object obj) {
            return true;
        }

        @Override
        public int hashCode() {
            return 0;
        }
    }

    private static class TestSubClass extends TestClass {
    }
}
