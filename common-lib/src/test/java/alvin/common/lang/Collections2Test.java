package alvin.common.lang;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

class Collections2Test {
    private static final Logger LOGGER = LoggerFactory.getLogger(Collections2Test.class);

    @SuppressWarnings("ConstantConditions")
    @Test
    void test_isNullOrEmpty_with_null_argument() {
        final List list = null;
        LOGGER.debug("list is: {}", list);

        boolean actual = Collections2.isNullOrEmpty(list);
        assertThat(actual, is(true));
    }

    @Test
    void test_isNullOrEmpty_with_empty_argument() {
        final List list = new ArrayList();
        LOGGER.debug("list is: {}", list);

        boolean actual = Collections2.isNullOrEmpty(list);
        assertThat(actual, is(true));
    }

    @Test
    void test_isNullOrEmpty_with_nonempty_argument() {
        final List<Integer> list = newArrayList(1, 2, 3);
        LOGGER.debug("list is: {}", list);

        boolean actual = Collections2.isNullOrEmpty(list);
        assertThat(actual, is(false));
    }
}
