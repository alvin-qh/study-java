package alvin.common.io;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class DirectoryTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(DirectoryTest.class);

    @Test
    void test_combine() {
        String path = Directory.join("abc", "def");
        LOGGER.debug("join(\"abc\", \"def\") is: {}", path);

        assertThat(path, is("abc/def"));
    }

    @Test
    void test_assetPath() {
        String path = "/User/me/work/asset";
        String assetPath = Directory.join(path, "{fileName: .*}");
        LOGGER.debug("join(\"{}\", \"{fileName: .*}\") is: {}", path, assetPath);

        assertThat(assetPath, is("/User/me/work/asset/{fileName: .*}"));
    }

    @Test
    void test_abs() {
        String path = Directory.abs("abc");
        LOGGER.debug("abs(\"abc\") is: {}", path);

        String pwd = Directory.pwd();
        LOGGER.debug("pwd() is: {}", pwd);

        String expected = Directory.join(pwd, "abc");
        LOGGER.debug("join(\"{}\", \"abc\") is: {}", pwd, expected);

        assertThat(path, is(expected));
    }

    @Test
    void test_file_name() {
        String filename = Directory.filename("test/abc.txt");
        LOGGER.debug("filename(\"test/abc.txt\") is {}", filename);

        assertThat(filename, is("abc.txt"));

        filename = Directory.filename("abc.txt");
        LOGGER.debug("filename(\"abc.txt\") is {}", filename);

        assertThat(filename, is("abc.txt"));
    }
}
