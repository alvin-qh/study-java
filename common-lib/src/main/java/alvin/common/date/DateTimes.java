package alvin.common.date;

import javax.inject.Singleton;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Singleton
public class DateTimes {

    public LocalDateTime utcNow() {
        return LocalDateTime.now(ZoneOffset.UTC);
    }

    public LocalDateTime localNow() {
        return LocalDateTime.now();
    }
}
