package alvin.common.utils;

import java.util.UUID;

public class IdGenerator {

    public String randomUUID(String prefix, String splitter) {
        String uuid = UUID.randomUUID().toString();
        if (splitter != null && !splitter.equals("-")) {
            uuid = uuid.replace("-", splitter);
        }
        return prefix + uuid;
    }

    public String randomUUID(String prefix) {
        return randomUUID(prefix, "");
    }
}
