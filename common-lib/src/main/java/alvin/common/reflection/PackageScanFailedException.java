package alvin.common.reflection;

public class PackageScanFailedException extends RuntimeException {
    public PackageScanFailedException(String message, Throwable cause) {
        super(message, cause);
    }
}
