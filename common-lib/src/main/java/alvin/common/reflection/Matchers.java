package alvin.common.reflection;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;

/**
 * Matcher implementations. Supports matching classes and methods.
 *
 * @author crazybob@google.com (Bob Lee)
 */
public final class Matchers {
    private static final Matcher<Object> ANY = new Any();

    private Matchers() {
    }

    /**
     * Returns a matcher which matches any input.
     */
    public static Matcher<Object> any() {
        return ANY;
    }

    /**
     * Inverts the given matcher.
     */
    public static <T> Matcher<T> not(final Matcher<? super T> p) {
        return new Not<>(p);
    }

    /**
     * Returns a matcher which matches elements (methods, classes, etc.)
     * with a given annotation.
     */
    public static Matcher<AnnotatedElement> annotatedWith(final Annotation annotation) {
        return new AnnotatedWith(annotation);
    }

    /**
     * Returns a matcher which matches subclasses of the given type (as well as
     * the given type).
     */
    public static Matcher<Class> subclassesOf(final Class<?> superclass) {
        return new SubclassesOf(superclass);
    }

    private static void checkForRuntimeRetention(final Class<? extends Annotation> annotationType) {
        Retention retention = annotationType.getAnnotation(Retention.class);
        if (retention == null || retention.value() != RetentionPolicy.RUNTIME) {
            throw new IllegalArgumentException(
                    String.format("Annotation %s is missing RUNTIME retention", annotationType.getSimpleName())
            );
        }
    }

    /**
     * Returns a matcher which matches elements (methods, classes, etc.)
     * with a given annotation.
     */
    public static Matcher<AnnotatedElement> annotatedWith(final Class<? extends Annotation> annotationType) {
        return new AnnotatedWithType(annotationType);
    }

    /**
     * Returns a matcher which matches objects equal to the given object.
     */
    public static Matcher<Object> only(Object value) {
        return new Only(value);
    }

    /**
     * Returns a matcher which matches classes in the given package and its subpackages. Unlike
     * {@link #inPackage(Package) inPackage()}, this matches classes from any classloader.
     *
     * @since 2.0
     */
    public static Matcher<Class> inSubpackage(final String targetPackageName) {
        return new InSubpackage(targetPackageName);
    }

    /**
     * Returns a matcher which matches only the given object.
     */
    public static Matcher<Object> identicalTo(final Object value) {
        return new IdenticalTo(value);
    }

    /**
     * Returns a matcher which matches classes in the given package. Packages are specific to their
     * classloader, so classes with the same package name may not have the same package at runtime.
     */
    public static Matcher<Class> inPackage(final Package targetPackage) {
        return new InPackage(targetPackage);
    }

    /**
     * Returns a matcher which matches methods with matching return types.
     */
    public static Matcher<Method> returns(final Matcher<? super Class<?>> returnType) {
        return new Returns(returnType);
    }

    public static Matcher<Class> is(final Class<?> type) {
        return new Is(type);
    }

    private static final class Any extends AbstractMatcher<Object> implements Serializable {
        public boolean matches(Object o) {
            return true;
        }

        @Override
        public String toString() {
            return "any()";
        }

        public Object readResolve() {
            return any();
        }

        private static final long serialVersionUID = 0;
    }

    private static final class Not<T> extends AbstractMatcher<T> implements Serializable {
        private final Matcher<? super T> delegate;

        private Not(Matcher<? super T> delegate) {
            if (delegate == null) {
                throw new IllegalArgumentException("delegate");
            }
            this.delegate = delegate;
        }

        public boolean matches(T t) {
            return !delegate.matches(t);
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof Not && ((Not) other).delegate.equals(delegate);
        }

        @Override
        public int hashCode() {
            return -delegate.hashCode();
        }

        @Override
        public String toString() {
            return "not(" + delegate + ")";
        }
    }

    private static final class AnnotatedWithType extends AbstractMatcher<AnnotatedElement> implements Serializable {
        private final Class<? extends Annotation> annotationType;

        AnnotatedWithType(Class<? extends Annotation> annotationType) {
            if (annotationType == null) {
                throw new IllegalArgumentException("annotationType");
            }
            this.annotationType = annotationType;
            checkForRuntimeRetention(annotationType);
        }

        public boolean matches(AnnotatedElement element) {
            return element.isAnnotationPresent(annotationType);
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof AnnotatedWithType
                    && ((AnnotatedWithType) other).annotationType.equals(annotationType);
        }

        @Override
        public int hashCode() {
            return 37 * annotationType.hashCode();  // SUPPRESS
        }

        @Override
        public String toString() {
            return "annotatedWith(" + annotationType.getSimpleName() + ".class)";
        }
    }

    private static final class AnnotatedWith extends AbstractMatcher<AnnotatedElement> implements Serializable {
        private final Annotation annotation;

        AnnotatedWith(Annotation annotation) {
            if (annotation == null) {
                throw new IllegalArgumentException("annotation");
            }
            this.annotation = annotation;
            checkForRuntimeRetention(annotation.annotationType());
        }

        public boolean matches(AnnotatedElement element) {
            Annotation fromElement = element.getAnnotation(annotation.annotationType());
            return fromElement != null && annotation.equals(fromElement);
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof AnnotatedWith
                    && ((AnnotatedWith) other).annotation.equals(annotation);
        }

        @Override
        public int hashCode() {
            return 37 * annotation.hashCode();  // SUPPRESS
        }

        @Override
        public String toString() {
            return "annotatedWith(" + annotation + ")";
        }
    }

    private static final class SubclassesOf extends AbstractMatcher<Class> implements Serializable {
        private final Class<?> superclass;

        SubclassesOf(Class<?> superclass) {
            if (superclass == null) {
                throw new IllegalArgumentException("superclass");
            }
            this.superclass = superclass;
        }

        public boolean matches(Class subclass) {
            return superclass.isAssignableFrom(subclass);
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof SubclassesOf
                    && ((SubclassesOf) other).superclass.equals(superclass);
        }

        @Override
        public int hashCode() {
            return 37 * superclass.hashCode();  // SUPPRESS
        }

        @Override
        public String toString() {
            return "subclassesOf(" + superclass.getSimpleName() + ".class)";
        }
    }

    private static final class Only extends AbstractMatcher<Object> implements Serializable {
        private final Object value;

        Only(Object value) {
            if (value == null) {
                throw new IllegalArgumentException("value");
            }
            this.value = value;
        }

        public boolean matches(Object other) {
            return value.equals(other);
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof Only
                    && ((Only) other).value.equals(value);
        }

        @Override
        public int hashCode() {
            return 37 * value.hashCode();   // SUPPRESS
        }

        @Override
        public String toString() {
            return "only(" + value + ")";
        }
    }

    private static final class IdenticalTo extends AbstractMatcher<Object> implements Serializable {
        private final Object value;

        IdenticalTo(Object value) {
            if (value == null) {
                throw new IllegalArgumentException("value");
            }
            this.value = value;
        }

        public boolean matches(Object other) {
            return value == other;
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof IdenticalTo
                    && ((IdenticalTo) other).value == value;
        }

        @Override
        public int hashCode() {
            return 37 * System.identityHashCode(value); // SUPPRESS
        }

        @Override
        public String toString() {
            return "identicalTo(" + value + ")";
        }
    }

    private static final class InPackage extends AbstractMatcher<Class> implements Serializable {
        private final transient Package targetPackage;
        private final String packageName;

        InPackage(Package targetPackage) {
            if (targetPackage == null) {
                throw new IllegalArgumentException("targetPackage");
            }
            this.targetPackage = targetPackage;
            this.packageName = targetPackage.getName();
        }

        public boolean matches(Class c) {
            return c.getPackage().equals(targetPackage);
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof InPackage
                    && ((InPackage) other).targetPackage.equals(targetPackage);
        }

        @Override
        public int hashCode() {
            return 37 * targetPackage.hashCode();   // SUPPRESS
        }

        @Override
        public String toString() {
            return "inPackage(" + targetPackage.getName() + ")";
        }

        public Object readResolve() {
            return inPackage(getClass().getClassLoader().getDefinedPackage(packageName));
        }
    }

    private static final class InSubpackage extends AbstractMatcher<Class> implements Serializable {
        private final String targetPackageName;

        InSubpackage(String targetPackageName) {
            this.targetPackageName = targetPackageName;
        }

        public boolean matches(Class c) {
            String classPackageName = c.getPackage().getName();
            return classPackageName.equals(targetPackageName)
                    || classPackageName.startsWith(targetPackageName + ".");
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof InSubpackage
                    && ((InSubpackage) other).targetPackageName.equals(targetPackageName);
        }

        @Override
        public int hashCode() {
            return 37 * targetPackageName.hashCode();   // SUPPRESS
        }

        @Override
        public String toString() {
            return "inSubpackage(" + targetPackageName + ")";
        }
    }

    private static final class Returns extends AbstractMatcher<Method> implements Serializable {
        private final Matcher<? super Class<?>> returnType;

        Returns(Matcher<? super Class<?>> returnType) {
            if (returnType == null) {
                throw new IllegalArgumentException("returnType");
            }
            this.returnType = returnType;
        }

        public boolean matches(Method m) {
            return returnType.matches(m.getReturnType());
        }

        @Override
        public boolean equals(Object other) {
            return other instanceof Returns
                    && ((Returns) other).returnType.equals(returnType);
        }

        @Override
        public int hashCode() {
            return 37 * returnType.hashCode();  // SUPPRESS
        }

        @Override
        public String toString() {
            return "returns(" + returnType + ")";
        }
    }

    private static final class Is extends AbstractMatcher<Class> implements Serializable {
        private final Class type;

        Is(Class type) {
            this.type = type;
        }

        @Override
        public boolean matches(Class other) {
            return type == other;
        }

        @Override
        public String toString() {
            return "is(" + type.getSimpleName() + ")";
        }
    }
}
