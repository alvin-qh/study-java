package alvin.common.reflection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.Enumeration;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * Utility class that finds all the classes in a given package.
 * (based on a similar utility in TestNG)
 * <p>
 * Created on Feb 24, 2006
 *
 * @author <a href="mailto:cedric@beust.com">Cedric Beust</a>
 * @author Dhanji R. Prasanna (dhanji@gmail.com)
 */
public final class ClassScanner {

    private static final int EXTENSION_LENGTH = 6;
    private static final Logger LOGGER = LoggerFactory.getLogger(ClassScanner.class);

    private final Matcher<? super Class<?>> matcher;

    private ClassScanner(final Matcher<? super Class<?>> matcher) {
        this.matcher = matcher;
    }

    public static ClassScanner matching(Matcher<? super Class<?>> matcher) {
        return new ClassScanner(matcher);
    }

    /**
     * @param pack A list of packages to scan, recursively.
     * @return A set of all the classes inside this package
     * @throws PackageScanFailedException Thrown when error reading from disk or jar.
     * @throws IllegalStateException      Thrown when something very odd is
     *                                    happening with classloaders.
     */
    public Set<Class<?>> in(Package pack) {
        return in(pack.getName());
    }

    public Set<Class<?>> in(String packageName) {
        final Set<Class<?>> classes = new LinkedHashSet<>();
        final String packageDirName = packageName.replace('.', '/');

        final Enumeration<URL> dirs;
        try {
            dirs = Thread.currentThread().getContextClassLoader().getResources(packageDirName);
        } catch (IOException e) {
            throw new PackageScanFailedException("Could not read from package directory: " + packageDirName, e);
        }

        while (dirs.hasMoreElements()) {
            URL url = dirs.nextElement();
            String protocol = url.getProtocol();

            if ("file".equals(protocol)) {
                findClassesInDirPackage(packageName, toPath(url), true, classes);
            } else if ("jar".equals(protocol)) {
                JarFile jar;
                try {
                    jar = ((JarURLConnection) url.openConnection()).getJarFile();
                } catch (IOException e) {
                    throw new PackageScanFailedException("Could not read from jar url: " + url, e);
                }

                Enumeration<JarEntry> entries = jar.entries();
                while (entries.hasMoreElements()) {
                    JarEntry entry = entries.nextElement();
                    String name = entry.getName();
                    if (name.charAt(0) == '/') {
                        name = name.substring(1);
                    }
                    if (name.startsWith(packageDirName)) {
                        int idx = name.lastIndexOf('/');
                        if (idx != -1) {
                            packageName = name.substring(0, idx).replace('/', '.');
                        }
                        if (name.endsWith(".class") && !entry.isDirectory()) {
                            String className = name.substring(idx + 1, name.length() - EXTENSION_LENGTH);
                            if (!"package-info".equalsIgnoreCase(className)) {
                                add(packageName, classes, className);
                            }
                        }
                    }
                }
            }
        }
        return classes;
    }

    private void add(final String packageName, final Set<Class<?>> classes, final String className) {
        Class<?> clazz;
        try {
            clazz = Class.forName(packageName + '.' + className);
        } catch (ClassNotFoundException e) {
            LOGGER.info("A class discovered by the scanner could not be found by the ClassLoader, " +
                    "something very odd has happened with the classloading (see root cause): " + e.toString());
            throw new IllegalStateException(
                    "A class discovered by the scanner could not be found by the ClassLoader", e);
        }
        if (matcher.matches(clazz)) {
            classes.add(clazz);
        }
    }

    private void findClassesInDirPackage(final String packageName,
                                         final String packagePath,
                                         final boolean recursive,
                                         final Set<Class<?>> classes) {
        File dir = new File(packagePath);
        if (!dir.exists() || !dir.isDirectory()) {
            return;
        }
        File[] files = dir.listFiles(file -> (recursive && file.isDirectory()) || file.getName().endsWith(".class"));
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    findClassesInDirPackage(packageName + "." + file.getName(),
                            file.getAbsolutePath(), recursive, classes);
                } else {
                    String className = file.getName().substring(0, file.getName().length() - EXTENSION_LENGTH);
                    add(packageName, classes, className);
                }
            }
        }
    }

    private static String toPath(final URL url) {
        String path = url.getPath();
        StringBuilder buf = new StringBuilder();
        for (int i = 0, length = path.length(); i < length; i++) {
            char c = path.charAt(i);
            if ('/' == c) {
                buf.append(File.separatorChar);
            } else if ('%' == c && i < length - 2) {
                buf.append((char) Integer.parseInt(path.substring(++i, ++i + 1), 16)); // SUPPRESS
            } else {
                buf.append(c);
            }
        }
        return buf.toString();
    }
}
