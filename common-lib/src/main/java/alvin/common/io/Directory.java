package alvin.common.io;

import java.nio.file.Paths;

public final class Directory {

    private Directory() {
    }

    public static String join(String path1, String... path2) {
        return Paths.get(path1, path2).toString();
    }

    public static String abs(String path1, String... path2) {
        return Paths.get(path1, path2).toAbsolutePath().toString();
    }

    public static String pwd() {
        return abs("");
    }

    public static String filename(String path) {
        return Paths.get(path).getFileName().toString();
    }
}
