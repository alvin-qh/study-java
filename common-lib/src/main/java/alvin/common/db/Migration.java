package alvin.common.db;

import alvin.common.lang.Collections2;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.configuration.FluentConfiguration;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("WeakerAccess")
public final class Migration {

    private Flyway flyway;

    private Migration(Builder builder) {
        FluentConfiguration conf = Flyway.configure();
        if (builder.dataSource == null) {
            conf.dataSource(builder.connectionUrl, builder.account, builder.password);
        } else {
            conf.dataSource(builder.dataSource);
        }
        if (Collections2.notNullAndEmpty(builder.locations)) {
            conf.locations(builder.locations.toArray(new String[0]));
        }
        conf.table("schema_versions");
        flyway = conf.load();
    }

    public void clean() {
        flyway.clean();
    }

    public void migrate() {
        flyway.migrate();
    }

    public void reset() {
        clean();
        migrate();
    }

    public static class Builder {
        private String connectionUrl;
        private String account;
        private String password;
        private List<String> locations = new ArrayList<>();
        private DataSource dataSource;

        public Builder(String connectionUrl) {
            this.connectionUrl = connectionUrl;
        }

        public Builder(DataSource dataSource) {
            this.dataSource = dataSource;
        }

        public Builder account(String account) {
            this.account = account;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public Builder addLocation(String location) {
            this.locations.add(location);
            return this;
        }

        public Migration build() {
            return new Migration(this);
        }
    }
}

