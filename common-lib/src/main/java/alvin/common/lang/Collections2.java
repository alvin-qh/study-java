package alvin.common.lang;

import java.util.Collection;

public final class Collections2 {

    private Collections2() {
    }

    public static boolean isNullOrEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean notNullAndEmpty(Collection<?> collection) {
        return !isNullOrEmpty(collection);
    }
}
