package alvin.rx.basic.model;

public class ConsumerData {
    private final int index;
    private final long threadId;

    public ConsumerData(int index, long threadId) {
        this.index = index;
        this.threadId = threadId;
    }

    public int getIndex() {
        return index;
    }

    public long getThreadId() {
        return threadId;
    }

    @Override
    public String toString() {
        return "ConsumerData{" +
                "index=" + index +
                ", threadId=" + threadId +
                '}';
    }
}
