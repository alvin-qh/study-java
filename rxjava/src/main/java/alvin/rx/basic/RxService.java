package alvin.rx.basic;

import alvin.rx.basic.model.ConsumerData;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Singleton;
import java.util.concurrent.TimeUnit;

@Singleton
public class RxService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RxService.class);

    public Disposable subscribe(final int count,
                                final Consumer<ConsumerData> onNext,
                                final Action onComplete) {
        return Observable.create(emitter -> {
            LOGGER.info("subscribe thread is {}", Thread.currentThread().getId());

            for (int i = 0; i < count; i++) {
                emitter.onNext(new ConsumerData(i + 1, Thread.currentThread().getId()));
            }
            emitter.onComplete();
        }).cast(ConsumerData.class)
                .subscribeOn(Schedulers.computation())
                .observeOn(Schedulers.computation())
                .subscribe(onNext,
                        throwable -> LOGGER.error("Cause error in observe thread", throwable),
                        onComplete);
    }

    public Disposable single(final int waitingMillis,
                             final Consumer<String> onSuccess,
                             final Consumer<Throwable> onError) {
        return Single.create(emitter -> {
            LOGGER.info("subscribe thread is {}", Thread.currentThread().getId());

            try {
                Thread.sleep(waitingMillis);
                emitter.onSuccess("OK");
            } catch (InterruptedException e) {
                emitter.onError(e);
            }
        }).cast(String.class)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .subscribe(onSuccess, onError);
    }

    public Disposable timeout(final int timeoutMillis,
                              Consumer<String> onSuccess,
                              Consumer<Throwable> onError) {
        return Single.create(emitter -> {
            LOGGER.info("subscribe thread is {}", Thread.currentThread().getId());
            try {
                Thread.sleep((long) Integer.MAX_VALUE);
                emitter.onSuccess("OK");
            } catch (InterruptedException e) {
                emitter.onError(e);
            }
        }).timeout(timeoutMillis, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .cast(String.class)
                .subscribe(onSuccess, onError);
    }
}
