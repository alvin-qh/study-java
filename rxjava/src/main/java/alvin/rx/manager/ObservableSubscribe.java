package alvin.rx.manager;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.annotations.SchedulerSupport;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.functions.Functions;

public class ObservableSubscribe<T> {
    private final RxManager rxManager;
    private final Observable<T> observable;

    ObservableSubscribe(final RxManager rxManager,
                        final Observable<T> observable) {
        this.rxManager = rxManager;
        this.observable = observable;
    }

    @CheckReturnValue
    @SchedulerSupport(SchedulerSupport.NONE)
    public final Disposable subscribe() {
        return subscribe(Functions.emptyConsumer(),
                Functions.ON_ERROR_MISSING,
                Functions.EMPTY_ACTION,
                Functions.emptyConsumer());
    }

    @CheckReturnValue
    @SchedulerSupport(SchedulerSupport.NONE)
    public final Disposable subscribe(final Consumer<? super T> onNext) {
        return subscribe(onNext, Functions.ON_ERROR_MISSING, Functions.EMPTY_ACTION, Functions.emptyConsumer());
    }

    @CheckReturnValue
    @SchedulerSupport(SchedulerSupport.NONE)
    public final Disposable subscribe(final Consumer<? super T> onNext,
                                      final Consumer<? super Throwable> onError) {
        return subscribe(onNext, onError, Functions.EMPTY_ACTION, Functions.emptyConsumer());
    }

    @CheckReturnValue
    @SchedulerSupport(SchedulerSupport.NONE)
    public final Disposable subscribe(final Consumer<? super T> onNext,
                                      final Consumer<? super Throwable> onError,
                                      final Action onComplete) {
        return subscribe(onNext, onError, onComplete, Functions.emptyConsumer());
    }

    @CheckReturnValue
    @SchedulerSupport(SchedulerSupport.NONE)
    public final Disposable subscribe(final Consumer<? super T> onNext,
                                      final Consumer<? super Throwable> onError,
                                      final Action onComplete,
                                      final Consumer<? super Disposable> onSubscribe) {
        final DisposableBinder binder = rxManager.makeDisposableBinder();

        observable.subscribe(
                onNext,
                throwable -> {
                    rxManager.unbindDisposable(binder);
                    onError.accept(throwable);
                }, () -> {
                    rxManager.unbindDisposable(binder);
                    onComplete.run();
                }, disposable -> {
                    binder.bind(disposable);
                    onSubscribe.accept(disposable);
                }
        );
        return binder;
    }

    @SchedulerSupport(SchedulerSupport.NONE)
    public final void subscribe(final Observer<? super T> observer) {
        final DisposableBinder binder = rxManager.makeDisposableBinder();

        observable.subscribe(new Observer<T>() {
            @Override
            public void onSubscribe(Disposable d) {
                binder.bind(d);
                observer.onSubscribe(d);
            }

            @Override
            public void onNext(T t) {
                observer.onNext(t);
            }

            @Override
            public void onError(Throwable e) {
                rxManager.unbindDisposable(binder);
                observer.onError(e);
            }

            @Override
            public void onComplete() {
                rxManager.unbindDisposable(binder);
                observer.onComplete();
            }
        });
    }
}
