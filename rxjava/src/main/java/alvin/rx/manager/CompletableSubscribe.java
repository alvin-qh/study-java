package alvin.rx.manager;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.annotations.SchedulerSupport;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.functions.Functions;

public class CompletableSubscribe {
    private final RxManager rxManager;
    private final Completable completable;

    CompletableSubscribe(final RxManager rxManager,
                         final Completable completable) {
        this.rxManager = rxManager;
        this.completable = completable;
    }

    @CheckReturnValue
    @SchedulerSupport(SchedulerSupport.NONE)
    public final Disposable subscribe() {
        return subscribe(Functions.EMPTY_ACTION);
    }

    @CheckReturnValue
    @SchedulerSupport(SchedulerSupport.NONE)
    public final Disposable subscribe(final Action onComplete) {
        return subscribe(onComplete, Functions.ON_ERROR_MISSING);
    }

    @CheckReturnValue
    @SchedulerSupport(SchedulerSupport.NONE)
    public final Disposable subscribe(final Action onComplete,
                                      final Consumer<? super Throwable> onError) {
        final DisposableBinder binder = rxManager.makeDisposableBinder();

        binder.bind(completable.subscribe(
                () -> {
                    rxManager.unbindDisposable(binder);
                    onComplete.run();
                },
                throwable -> {
                    rxManager.unbindDisposable(binder);
                    onError.accept(throwable);
                }
        ));
        return binder;
    }

    @SchedulerSupport(SchedulerSupport.NONE)
    public final void subscribe(final CompletableObserver s) {
        final DisposableBinder binder = rxManager.makeDisposableBinder();

        completable.subscribe(new CompletableObserver() {
            @Override
            public void onSubscribe(Disposable d) {
                binder.bind(d);
                s.onSubscribe(d);
            }

            @Override
            public void onComplete() {
                rxManager.unbindDisposable(binder);
                s.onComplete();
            }

            @Override
            public void onError(Throwable e) {
                rxManager.unbindDisposable(binder);
                s.onError(e);
            }
        });
    }
}
