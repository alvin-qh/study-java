package alvin.rx.manager;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.annotations.SchedulerSupport;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.functions.Functions;

public class SingleSubscribe<T> {
    private final RxManager rxManager;
    private final Single<T> single;

    SingleSubscribe(final RxManager rxManager,
                    final Single<T> single) {
        this.rxManager = rxManager;
        this.single = single;
    }

    @CheckReturnValue
    @SchedulerSupport(SchedulerSupport.NONE)
    public final Disposable subscribe() {
        return subscribe(Functions.emptyConsumer());
    }

    @CheckReturnValue
    @SchedulerSupport(SchedulerSupport.NONE)
    public final Disposable subscribe(final BiConsumer<? super T, ? super Throwable> onCallback) {
        final DisposableBinder binder = rxManager.makeDisposableBinder();

        binder.bind(single.subscribe((t, throwable) -> {
            rxManager.unbindDisposable(binder);
            onCallback.accept(t, throwable);
        }));

        return binder;
    }

    @CheckReturnValue
    @SchedulerSupport(SchedulerSupport.NONE)
    public final Disposable subscribe(final Consumer<? super T> onSuccess) {
        return subscribe(onSuccess, Functions.ON_ERROR_MISSING);
    }

    @CheckReturnValue
    @SchedulerSupport(SchedulerSupport.NONE)
    public final Disposable subscribe(final Consumer<? super T> onSuccess,
                                      final Consumer<? super Throwable> onError) {
        final DisposableBinder binder = rxManager.makeDisposableBinder();

        binder.bind(single.subscribe(t -> {
            rxManager.unbindDisposable(binder);
            onSuccess.accept(t);
        }, throwable -> {
            rxManager.unbindDisposable(binder);
            onError.accept(throwable);
        }));

        return binder;
    }

    @SchedulerSupport(SchedulerSupport.NONE)
    public final void subscribe(SingleObserver<? super T> subscriber) {
        final DisposableBinder binder = rxManager.makeDisposableBinder();
        single.subscribe(new SingleObserver<T>() {
            @Override
            public void onSubscribe(Disposable d) {
                binder.bind(d);
                subscriber.onSubscribe(d);
            }

            @Override
            public void onSuccess(T t) {
                rxManager.unbindDisposable(binder);
                subscriber.onSuccess(t);
            }

            @Override
            public void onError(Throwable e) {
                rxManager.unbindDisposable(binder);
                subscriber.onError(e);
            }
        });
    }
}
