package alvin.rx.manager;

import io.reactivex.Flowable;
import io.reactivex.FlowableSubscriber;
import io.reactivex.annotations.BackpressureKind;
import io.reactivex.annotations.BackpressureSupport;
import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.annotations.SchedulerSupport;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.functions.Functions;
import io.reactivex.internal.operators.flowable.FlowableInternalHelper;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public class FlowableSubscribe<T> {
    private final RxManager rxManager;
    private final Flowable<T> flowable;

    FlowableSubscribe(final RxManager rxManager,
                      final Flowable<T> flowable) {
        this.rxManager = rxManager;
        this.flowable = flowable;
    }

    @BackpressureSupport(BackpressureKind.UNBOUNDED_IN)
    @SchedulerSupport(SchedulerSupport.NONE)
    public final Disposable subscribe() {
        return subscribe(Functions.emptyConsumer(), Functions.ON_ERROR_MISSING,
                Functions.EMPTY_ACTION, FlowableInternalHelper.RequestMax.INSTANCE);
    }

    @CheckReturnValue
    @BackpressureSupport(BackpressureKind.UNBOUNDED_IN)
    @SchedulerSupport(SchedulerSupport.NONE)
    public final Disposable subscribe(final Consumer<? super T> onNext) {
        return subscribe(onNext, Functions.ON_ERROR_MISSING,
                Functions.EMPTY_ACTION, FlowableInternalHelper.RequestMax.INSTANCE);
    }

    @CheckReturnValue
    @BackpressureSupport(BackpressureKind.UNBOUNDED_IN)
    @SchedulerSupport(SchedulerSupport.NONE)
    public final Disposable subscribe(final Consumer<? super T> onNext,
                                      final Consumer<? super Throwable> onError) {
        return subscribe(onNext, onError, Functions.EMPTY_ACTION, FlowableInternalHelper.RequestMax.INSTANCE);
    }

    @CheckReturnValue
    @BackpressureSupport(BackpressureKind.SPECIAL)
    @SchedulerSupport(SchedulerSupport.NONE)
    public final Disposable subscribe(final Consumer<? super T> onNext,
                                      final Consumer<? super Throwable> onError,
                                      final Action onComplete) {
        return subscribe(onNext, onError, onComplete, FlowableInternalHelper.RequestMax.INSTANCE);
    }

    @CheckReturnValue
    @BackpressureSupport(BackpressureKind.SPECIAL)
    @SchedulerSupport(SchedulerSupport.NONE)
    public final Disposable subscribe(final Consumer<? super T> onNext,
                                      final Consumer<? super Throwable> onError,
                                      final Action onComplete,
                                      final Consumer<? super Subscription> onSubscribe) {
        final DisposableBinder binder = rxManager.makeDisposableBinder();
        binder.bind(flowable.subscribe(
                onNext,
                throwable -> {
                    rxManager.unbindDisposable(binder);
                    onError.accept(throwable);
                },
                () -> {
                    rxManager.unbindDisposable(binder);
                    onComplete.run();
                },
                onSubscribe
        ));
        return binder;
    }

    @BackpressureSupport(BackpressureKind.SPECIAL)
    @SchedulerSupport(SchedulerSupport.NONE)
    public final void subscribe(final Subscriber<? super T> s) {
        flowable.subscribe(s);
    }

    @BackpressureSupport(BackpressureKind.SPECIAL)
    @SchedulerSupport(SchedulerSupport.NONE)
    public final void subscribe(FlowableSubscriber<? super T> s) {
        flowable.subscribe(s);
    }
}
