package alvin.rx.manager;

import io.reactivex.disposables.Disposable;

import java.util.concurrent.atomic.AtomicReference;

public final class DisposableBinder implements Disposable {

    private AtomicReference<Disposable> disposableRef;

    private DisposableBinder() {
    }

    private Disposable get() {
        if (disposableRef == null) {
            return null;
        }
        return disposableRef.get();
    }

    void bind(Disposable disposable) {
        disposableRef = new AtomicReference<>(disposable);
    }

    @Override
    public void dispose() {
        final Disposable disposable = get();
        if (disposable != null) {
            disposable.dispose();
        }
    }

    @Override
    public boolean isDisposed() {
        final Disposable disposable = get();
        return disposable == null || disposable.isDisposed();
    }

    static DisposableBinder newInstance() {
        return new DisposableBinder();
    }
}
