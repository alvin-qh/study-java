package alvin.rx.manager;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

import java.util.function.Supplier;

public final class RxManager {

    private final Supplier<Scheduler> subscribeOnSupplier;
    private final Supplier<Scheduler> observeOnSupplier;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();

    private RxManager(final Supplier<Scheduler> subscribeOnSupplier,
                      final Supplier<Scheduler> observeOnSupplier) {
        this.subscribeOnSupplier = subscribeOnSupplier;
        this.observeOnSupplier = observeOnSupplier;
    }

    public <T> ObservableSubscribe<T> with(Observable<T> observable) {
        if (subscribeOnSupplier != null) {
            observable = observable.subscribeOn(subscribeOnSupplier.get());
        }
        if (observeOnSupplier != null) {
            observable = observable.observeOn(observeOnSupplier.get());
        }
        return new ObservableSubscribe<>(this, observable);
    }

    public <T> SingleSubscribe<T> with(Single<T> single) {
        if (subscribeOnSupplier != null) {
            single = single.subscribeOn(subscribeOnSupplier.get());
        }
        if (observeOnSupplier != null) {
            single = single.observeOn(observeOnSupplier.get());
        }
        return new SingleSubscribe<>(this, single);
    }

    public CompletableSubscribe with(Completable completable) {
        if (subscribeOnSupplier != null) {
            completable = completable.subscribeOn(subscribeOnSupplier.get());
        }
        if (observeOnSupplier != null) {
            completable = completable.observeOn(observeOnSupplier.get());
        }
        return new CompletableSubscribe(this, completable);
    }

    public <T> FlowableSubscribe<T> with(Flowable<T> flowable) {
        if (subscribeOnSupplier != null) {
            flowable = flowable.subscribeOn(subscribeOnSupplier.get());
        }
        if (observeOnSupplier != null) {
            flowable = flowable.observeOn(observeOnSupplier.get());
        }
        return new FlowableSubscribe<>(this, flowable);
    }

    public void clear() {
        compositeDisposable.clear();
    }

    void unbindDisposable(final DisposableBinder binder) {
        compositeDisposable.remove(binder);
    }

    DisposableBinder makeDisposableBinder() {
        DisposableBinder binder = DisposableBinder.newInstance();
        compositeDisposable.add(binder);
        return binder;
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public static class Builder {
        private Supplier<Scheduler> subscribeOnSupplier;
        private Supplier<Scheduler> observeOnSupplier;

        public Builder subscribeOn(final Supplier<Scheduler> supplier) {
            this.subscribeOnSupplier = supplier;
            return this;
        }

        public Builder observeOn(final Supplier<Scheduler> supplier) {
            this.observeOnSupplier = supplier;
            return this;
        }

        public RxManager build() {
            return new RxManager(
                    subscribeOnSupplier,
                    observeOnSupplier
            );
        }
    }
}
