package alvin.rx;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import org.junit.jupiter.api.Test;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class RxDisposeTest {

    @Test
    void test_dispose() {
        final Lock lock = new ReentrantLock();
        lock.lock();

        final CompositeDisposable compositeDisposable = new CompositeDisposable();
        Disposable subscribe = Observable.just("Hello").subscribe(
                s -> assertThat(s, is("Hello")),
                throwable -> {
                },
                lock::unlock,
                compositeDisposable::add);

        lock.lock();
        assertThat(compositeDisposable.size(), is(1));
        assertThat(subscribe.isDisposed(), is(true));
    }
}
