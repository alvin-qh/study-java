package alvin.rx;

import alvin.rx.basic.RxService;
import alvin.testing.BaseTest;
import io.reactivex.disposables.Disposable;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

@Slf4j
class RxServiceTest extends BaseTest {

    @Inject
    private RxService service;

    private synchronized void notifyMe() {
        this.notify();
    }

    private synchronized void waitMe() {
        try {
            this.wait();
        } catch (InterruptedException ignore) {
        }
    }

    @Test
    void test_subscript() {
        final long tId = Thread.currentThread().getId();
        final Counter counter = new Counter();

        final Disposable disposable = service.subscribe(5,
                data -> {    // next
                    log.debug("Consumer data is {}", data);

                    assertThat(data.getIndex(), is(counter.count));
                    counter.increment();
                    assertThat(tId, not(data.getThreadId()));
                },
                this::notifyMe);

        waitMe();
        assertThat(disposable.isDisposed(), is(true));
        assertThat(counter.count, is(6));
    }

    private static class Counter {
        private int count = 1;

        void increment() {
            this.count++;
        }
    }

    @Test
    void test_single_on_success() {
        log.debug("Main thread is {}", Thread.currentThread().getId());

        long timer = System.currentTimeMillis();
        StringBuilder result = new StringBuilder();

        Disposable disposable = service.single(100,
                s -> {
                    log.debug("Observe thread is {}", Thread.currentThread().getId());
                    result.append(s);

                    notifyMe();
                },
                throwable -> fail(throwable.getMessage()));

        waitMe();
        assertThat(disposable.isDisposed(), is(true));
        assertThat(System.currentTimeMillis() - timer, greaterThanOrEqualTo(100L));
        assertThat(result.toString(), is("OK"));
    }

    @Test
    void test_single_on_error() {
        log.debug("Main thread is {}", Thread.currentThread().getId());

        final long timer = System.currentTimeMillis();
        final StringBuilder result = new StringBuilder();

        Disposable disposable = service.single(100,
                s -> {
                    log.debug("Observe thread is {}", Thread.currentThread().getId());
                    result.append(s);

                    notifyMe();
                },
                throwable -> fail(throwable.getMessage()));

        waitMe();
        assertThat(disposable.isDisposed(), is(true));
        assertThat(System.currentTimeMillis() - timer, greaterThanOrEqualTo(100L));
        assertThat(result.toString(), is("OK"));
    }

    @Test
    void test_timeout() {
        log.debug("Main thread is {}", Thread.currentThread().getId());

        Executable exec = () -> {
            List<Throwable> exceptions = new ArrayList<>();

            Disposable disposable = service.timeout(100,
                    s -> fail(),
                    throwable -> {
                        log.debug("Observe thread is {}", Thread.currentThread().getId());
                        exceptions.add(throwable);

                        notifyMe();
                    });

            waitMe();
            assertThat(disposable.isDisposed(), is(true));
            assertThat(exceptions.size(), is(1));
            throw exceptions.get(0);
        };

        assertThrows(TimeoutException.class, exec);
    }
}
