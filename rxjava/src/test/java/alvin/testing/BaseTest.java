package alvin.testing;

import com.google.inject.Guice;
import org.junit.jupiter.api.BeforeEach;

public abstract class BaseTest {

    @BeforeEach
    public void setUp() {
        Guice.createInjector().injectMembers(this);
    }
}
