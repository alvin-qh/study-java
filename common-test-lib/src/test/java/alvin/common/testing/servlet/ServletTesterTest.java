package alvin.common.testing.servlet;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class ServletTesterTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServletTesterTest.class);
    private static final TestServlet TEST_SERVLET = new TestServlet();

    @Test
    void test_ServletTester_for_writer() throws Exception {
        ServletTester tester = new ServletTester("GET", "UTF-8");
        LOGGER.debug("tester is: {}", tester);

        TEST_SERVLET.doGet(tester.getRequest(), tester.getResponse());
        final String body = tester.getResponseBody();

        LOGGER.debug("response body: {}", body);
        assertThat(body, is("Hello World"));
    }

    @Test
    void test_ServletTester_for_context() throws Exception {
        ServletTester tester = new ServletTester("POST", "UTF-8");
        LOGGER.debug("tester is: {}", tester);

        TEST_SERVLET.doPost(tester.getRequest(), tester.getResponse());

        LOGGER.debug("redirect url is: {}", tester.getRedirectUrl());
        assertThat(tester.getRedirectUrl(), is("/login"));
    }

    static class TestServlet extends HttpServlet {

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
            resp.setCharacterEncoding(req.getCharacterEncoding());
            resp.getWriter().print("Hello World");
        }

        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
            resp.setCharacterEncoding(req.getCharacterEncoding());
            resp.sendRedirect("/login");
        }
    }
}
