package alvin.common.testing.guice;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.inject.Named;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@GuiceModules({GuiceTesterTest.TestModule.class})
class GuiceTesterTest {
    @Inject
    @Named("for_test")
    private String testString;

    @BeforeEach
    void setUp() {
        GuiceTester.create(this);
    }

    @Test
    void test_runner() {
        assertThat(testString, is("HelloWorld"));
    }

    public static class TestModule extends AbstractModule {

        @Override
        protected void configure() {
        }

        @Named("for_test")
        @Provides
        public String provideTestString() {
            return "HelloWorld";
        }
    }
}
