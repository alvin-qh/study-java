package alvin.common.testing.util;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Supplier;

public final class TestContext {
    private final Map<String, Object> valueMap;

    private TestContext() {
        valueMap = new ConcurrentHashMap<>();
    }

    public static TestContext create() {
        return new TestContext();
    }

    public void put(String key, Object val) {
        valueMap.put(key, val);
    }

    public <T> T get(String key, Supplier<T> defaultCompute) {
        final Object val = valueMap.get(key);
        return val == null ? defaultCompute.get() : (T) val;
    }

    public int get(String key, int def) {
        return get(key, () -> def);
    }

    public double get(String key, double def) {
        return get(key, () -> def);
    }

    public boolean get(String key, boolean def) {
        return get(key, () -> def);
    }

    public boolean has(String key) {
        return valueMap.containsKey(key);
    }

    public boolean none(String key) {
        return !valueMap.containsKey(key);
    }

    public <T> T compute(String key, Function<T, T> mappingFn) {
        if (none(key)) {
            throw new IllegalArgumentException("Key=" + key + " dose not exist");
        }
        return (T) valueMap.compute(key, (k, v) -> mappingFn.apply((T) v));
    }

    public int addAndGet(String key, int addition) {
        return compute(key, (val) -> val + addition);
    }
}
