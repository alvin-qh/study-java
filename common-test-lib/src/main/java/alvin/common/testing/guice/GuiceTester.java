package alvin.common.testing.guice;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public final class GuiceTester {
    private static final Logger LOGGER = LoggerFactory.getLogger(GuiceTester.class);

    private final transient Injector injector;
    private final Object testInstance;

    private GuiceTester(Object testInstance) {
        this.testInstance = testInstance;
        this.injector = createInjectorFor(moduleFor(testInstance.getClass()));
    }

    public static GuiceTester create(Object test) {
        final GuiceTester tester = new GuiceTester(test);
        tester.init();
        return tester;
    }

    private void init() {
        injector.injectMembers(testInstance);
        LOGGER.debug("inject into {}", testInstance.getClass().getName());
    }

    private Injector createInjectorFor(final Class<?>[] classes) {
        final List<Module> modules = new ArrayList<>(classes.length);
        for (final Class<?> module : classes) {
            try {
                modules.add((Module) module.getConstructor().newInstance());
            } catch (final ReflectiveOperationException exception) {
                throw new RuntimeException(exception);
            }
        }
        return Guice.createInjector(modules);
    }

    private Class<?>[] moduleFor(final Class<?> clazz) {
        GuiceModules annotation = clazz.getAnnotation(GuiceModules.class);
        if (annotation == null) {
            throw new RuntimeException("Miss GuiceModules annotation");
        }
        return annotation.value();
    }
}
