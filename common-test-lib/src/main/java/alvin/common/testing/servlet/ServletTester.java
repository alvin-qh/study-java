package alvin.common.testing.servlet;

import org.apache.commons.io.output.ByteArrayOutputStream;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public final class ServletTester implements AutoCloseable {
    public static final String GET = "GET", POST = "POST", DELETE = "DELETE", PUT = "PUT";

    private final HttpServletRequest request = mock(HttpServletRequest.class);
    private final HttpServletResponse response = mock(HttpServletResponse.class);
    private final ServletConfig servletConfig = mock(ServletConfig.class);
    private final ServletContext servletContext = mock(ServletContext.class);
    private final RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);

    private final ByteArrayOutputStream output = new ByteArrayOutputStream();
    private final PrintWriter writer = new PrintWriter(output);
    private String charsetName;
    private String method;
    private String contextPath = "/";
    private final Map<String, String[]> requestParametersMap = new LinkedHashMap<>();
    private final Map<String, Object> requestAttributesMap = new LinkedHashMap<>();
    private final Map<String, String> initializeParametersMap = new LinkedHashMap<>();

    private String requestDispatchPath = "";
    private String redirectUrl = "";

    public ServletTester(String method, String charsetName) throws IOException {
        this.method = method;
        this.charsetName = charsetName;

        doMock(request);
        doMock(response);
        doMock(servletConfig);
    }

    public ServletTester() throws IOException {
        this("", "UTF-8");
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void setCharsetName(String charsetName) {
        this.charsetName = charsetName;
    }

    private void doMock(ServletConfig servletConfig) {
        when(servletConfig.getServletContext()).thenReturn(servletContext);
        when(servletConfig.getInitParameter(anyString())).thenAnswer(invocation ->
                initializeParametersMap.get(invocation.getArgumentAt(0, String.class))
        );
        when(servletConfig.getInitParameterNames()).thenReturn(
                Collections.enumeration(initializeParametersMap.keySet())
        );
    }

    public void addRequestParameter(String name, Object val) {
        if (!val.getClass().isArray()) {
            requestParametersMap.put(name, new String[]{val.toString()});
        } else {
            requestParametersMap.put(name, (String[]) val);
        }
    }

    public void addInitParameter(String name, String value) {
        initializeParametersMap.put(name, value);
    }

    public ServletConfig getServletConfig() {
        return servletConfig;
    }

    private void doMock(HttpServletRequest request) {
        when(request.getServletContext()).thenReturn(servletContext);
        when(request.getMethod()).thenAnswer(invocation -> method);
        when(request.getCharacterEncoding()).thenAnswer(invocation -> charsetName);
        when(request.getParameterMap()).thenReturn(requestParametersMap);
        when(request.getRequestDispatcher(anyString())).thenAnswer(invocation -> {
            requestDispatchPath = invocation.getArgumentAt(0, String.class);
            return requestDispatcher;
        });
        try {
            doNothing().when(requestDispatcher).forward(anyObject(), anyObject());
        } catch (ServletException | IOException ignore) {
        }
        when(request.getParameter(anyString())).thenAnswer(invocation ->
                requestParametersMap.get(invocation.getArgumentAt(0, String.class))[0]
        );
        when(request.getParameterValues(anyString())).thenAnswer(invocation ->
                requestParametersMap.get(invocation.getArgumentAt(0, String.class)));
        when(request.getParameterNames()).thenReturn(Collections.enumeration(requestParametersMap.keySet()));

        doAnswer(invocation ->
                requestAttributesMap.put(
                        invocation.getArgumentAt(0, String.class),
                        invocation.getArgumentAt(1, Object.class)
                )
        ).when(request).setAttribute(anyString(), anyObject());

        when(request.getAttribute(anyString())).thenAnswer(invocation ->
                requestAttributesMap.get(invocation.getArgumentAt(0, String.class))
        );

        when(request.getPathInfo()).thenAnswer(invocation -> contextPath + requestDispatchPath);

        when(request.getContextPath()).thenAnswer(invocation -> contextPath);
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    public byte[] getResponseRaw() throws IOException {
        writer.flush();
        output.flush();
        return output.toByteArray();
    }

    public String getResponseBody() throws IOException {
        return new String(getResponseRaw(), charsetName);
    }

    private void doMock(HttpServletResponse response) throws IOException {
        when(response.getOutputStream()).thenReturn(new ServletOutputStream() {
            @Override
            public boolean isReady() {
                return true;
            }

            @Override
            public void setWriteListener(WriteListener writeListener) {
            }

            @Override
            public void write(int b) throws IOException {
                output.write(b);
            }
        });

        when(response.getWriter()).thenReturn(writer);
        doAnswer(invocation -> redirectUrl = invocation.getArgumentAt(0, String.class))
                .when(response).sendRedirect(anyString());
    }

    public String getRequestDispatchPath() {
        return requestDispatchPath;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    @Override
    public void close() {
        try {
            output.close();
        } catch (IOException ignore) {
        }
    }

    @Override
    public String toString() {
        return "ServletTester{" +
                "charsetName='" + charsetName + '\'' +
                ", method='" + method + '\'' +
                ", contextPath='" + contextPath + '\'' +
                ", requestParametersMap=" + requestParametersMap +
                ", requestAttributesMap=" + requestAttributesMap +
                ", initializeParametersMap=" + initializeParametersMap +
                '}';
    }
}
