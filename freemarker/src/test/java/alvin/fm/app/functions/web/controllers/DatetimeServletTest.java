package alvin.fm.app.functions.web.controllers;

import alvin.common.testing.servlet.ServletTester;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class DatetimeServletTest {

    private static final DatetimeServlet DATETIME_SERVLET = new DatetimeServlet();

    @Test
    void test_DatetimeServlet_doGet() throws Exception {
        ServletTester tester = new ServletTester(ServletTester.GET, "UTF-8");

        DATETIME_SERVLET.doGet(tester.getRequest(), tester.getResponse());
        Document doc = Jsoup.parse(tester.getResponseBody());

        assertThat(doc.select("html>head>title").text(), is("Datetime"));
    }
}
