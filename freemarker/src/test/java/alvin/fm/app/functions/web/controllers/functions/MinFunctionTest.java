package alvin.fm.app.functions.web.controllers.functions;

import freemarker.template.SimpleNumber;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class MinFunctionTest {

    @Test
    void test_exec() throws Exception {
        MinFunction function = new MinFunction();

        List<SimpleNumber> args = Arrays.asList(
                new SimpleNumber(1),
                new SimpleNumber(2),
                new SimpleNumber(3),
                new SimpleNumber(2),
                new SimpleNumber(1));

        Object min = function.exec(args);
        assertThat(min, is(1));
    }
}
