package alvin.fm.app.hello.web.controller;

import alvin.common.testing.servlet.ServletTester;
import alvin.fm.app.hello.domain.model.Hello;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class HelloServletTest {

    private static final HelloServlet HELLO_SERVLET = new HelloServlet();

    @Test
    void test_HelloServlet_get() throws Exception {
        ServletTester tester = new ServletTester("GET", "UTF-8");

        HELLO_SERVLET.doGet(tester.getRequest(), tester.getResponse());
        Hello module = (Hello) tester.getRequest().getAttribute("model");
        assertThat(module.getName(), is("Alvin"));
        assertThat(module.getGender(), is("M"));
        assertThat(tester.getRequestDispatchPath(), is("hello/index.ftl"));
    }
}
