package alvin.fm.app.functions.web.controllers;


import alvin.fm.core.servlet.BaseServlet;
import com.google.common.collect.ImmutableMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/functions/number")
public class NumberServlet extends BaseServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        template("index", ImmutableMap.of("num", 1234.5), req, resp);   // SUPPRESS
    }
}
