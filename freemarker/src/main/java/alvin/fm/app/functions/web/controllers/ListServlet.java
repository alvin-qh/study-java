package alvin.fm.app.functions.web.controllers;

import alvin.fm.core.servlet.BaseServlet;
import com.google.common.collect.ImmutableMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/functions/list")
public class ListServlet extends BaseServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<String> list = new ArrayList<>();
        for (char c : "hello world".toCharArray()) {
            list.add(String.valueOf(c));
        }
        template("index", ImmutableMap.of("list", list), req, resp);
    }
}
