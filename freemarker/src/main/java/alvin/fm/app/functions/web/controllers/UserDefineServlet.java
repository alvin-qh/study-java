package alvin.fm.app.functions.web.controllers;

import alvin.fm.app.functions.web.controllers.functions.MinFunction;
import alvin.fm.core.servlet.BaseServlet;
import com.google.common.collect.ImmutableMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@WebServlet("/functions/user_define")
public class UserDefineServlet extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // Configuration#setSharedVariable("min", new MinFunction());
        // Configuration#setSharedVariable("select", new SelectDirective());
        Map<String, Object> args = ImmutableMap.of(
                "min", new MinFunction(),
                "items", makeItems(),
                "select", new SelectDirective()
        );
        template("index", args, req, resp);
    }

    private List<Item> makeItems() {
        List<Item> items = new ArrayList<>();
        for (int i = 0; i < 3; i++) {   // SUPPRESS
            items.add(Item.of(i + 1, String.valueOf((char) ('A' + i))));
        }
        return items;
    }

    public static class Item {
        private Integer id;
        private String name;

        public Integer getId() {
            return id;
        }

        void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        static Item of(Integer id, String name) {
            Item item = new Item();
            item.setId(id);
            item.setName(name);
            return item;
        }
    }
}
