package alvin.fm.app.mvc.web.controllers;

import alvin.fm.app.mvc.domain.models.Person;
import alvin.fm.core.models.ErrorMessages;
import alvin.fm.core.servlet.BaseServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@WebServlet("/persons")
public class PersonsServlet extends BaseServlet {

    private static AtomicInteger atomicInteger = new AtomicInteger(0);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Person> persons = getPersonList(req.getServletContext());

        // Save the model instance and variables into a map object.
        Map<String, Object> arguments = new HashMap<>();
        arguments.put("persons", persons);

        // Call the method to process the freemarker template to html, and write the result into response scope
        template("index", arguments, req, resp);
    }

    private boolean checkParameters(Person person, HttpServletRequest req, HttpServletResponse resp)
            throws IOException, ServletException {
        final Map<String, Object> arguments = new HashMap<>();
        final ErrorMessages messages = new ErrorMessages();
        final boolean result = person.check(messages);
        if (!result) {
            arguments.put(ERROR_MESSAGE, messages);
            arguments.put("person", person);
            render("/alvin/fm/app/mvc/web/views/PersonsNewServlet/index.ftl", arguments, req, resp);
        }
        return result;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Person person = Person.populate(req);
        if (!checkParameters(person, req, resp)) {
            return;
        }
        person.setId(atomicInteger.incrementAndGet());
        getPersonList(req.getServletContext()).add(person);
        resp.sendRedirect("/persons");
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Person person = Person.populate(req);
        ErrorMessages messages = new ErrorMessages();
        Map<String, Object> arguments = new HashMap<>();
        if (!checkParameters(person, req, resp)) {
            return;
        }
        List<Person> persons = getPersonList(req.getServletContext());
        Optional<Person> personOptional = persons.stream().filter(item ->
                person.getId().equals(item.getId())).findAny();

        if (!personOptional.isPresent()) {
            messages.addMessage("id", "无效的用户ID");
            arguments.put(ERROR_MESSAGE, messages);
            arguments.put("person", person);
            template("index", arguments, req, resp);
            return;
        }
        Person savedPerson = personOptional.get();
        savedPerson.setName(person.getName());
        savedPerson.setBirthday(person.getBirthday());
        savedPerson.setEmail(person.getEmail());
        savedPerson.setGender(person.getGender());
        savedPerson.setTelephone(person.getTelephone());

        resp.sendRedirect("/persons");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        Integer id = Integer.parseInt(req.getParameter("id"));
        List<Person> persons = getPersonList(req.getServletContext());
        persons.removeIf(item -> id.equals(item.getId()));

        resp.sendRedirect("/persons");
    }

    @SuppressWarnings("unchecked")
    private static List<Person> getPersonList(ServletContext app) {
        List<Person> persons = (List<Person>) app.getAttribute("persons");
        if (persons == null) {
            persons = new LinkedList<>();
            app.setAttribute("persons", persons);
        }
        return persons;
    }
}
