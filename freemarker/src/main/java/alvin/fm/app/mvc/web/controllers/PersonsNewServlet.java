package alvin.fm.app.mvc.web.controllers;

import alvin.fm.core.servlet.BaseServlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/persons/new")
public class PersonsNewServlet extends BaseServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // Call the method to process the freemarker template to html, and write the result into response scope
        template("index", null, req, resp);
    }
}
