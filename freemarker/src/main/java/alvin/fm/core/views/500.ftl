<#import "/alvin/fm/core/views/layout.ftl" as l>

<@l.layout title="500">

<div class="panel">
    <div class="panel-title">500 Caused</div>
    <div class="panel-body">
        <div class="error">${err.message}</div>
    </div>
</div>
</@l.layout>
