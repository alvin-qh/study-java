package alvin.fm.core.servlet;

import alvin.fm.core.freemarker.Configuration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

public class BaseServlet extends HttpServlet {
    protected static final String ERROR_MESSAGE = "ERROR_MESSAGE";

    private final Configuration configuration = Configuration.getInstance();

    protected void template(final String templateName,
                            final Map<String, Object> params,
                            final HttpServletRequest req,
                            final HttpServletResponse resp) throws IOException, ServletException {
        configuration.template(this, templateName, params, req, resp);
    }

    protected void render(final String templateFile,
                          final Map<String, Object> params,
                          final HttpServletRequest req,
                          final HttpServletResponse resp) throws IOException, ServletException {
        configuration.render(templateFile, params, req, resp);
    }
}
