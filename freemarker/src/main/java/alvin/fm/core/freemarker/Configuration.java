package alvin.fm.core.freemarker;

import alvin.fm.core.servlet.BaseServlet;
import alvin.fm.core.wrappers.ExtObjectWrapper;
import freemarker.cache.FileTemplateLoader;
import freemarker.cache.MruCacheStorage;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.security.ProtectionDomain;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class Configuration {
    private static final freemarker.template.Configuration CONFIGURATION =
            new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_21);
    private static final Map<String, String> TEMPLATES_MAP = new ConcurrentHashMap<>();
    private static final Configuration INSTANCE = new Configuration();

    static {
        CONFIGURATION.setDefaultEncoding("UTF-8");
        CONFIGURATION.setURLEscapingCharset("UTF-8");
        CONFIGURATION.setNumberFormat("#.##");
        CONFIGURATION.setCacheStorage(new MruCacheStorage(1024, 1024)); // SUPPRESS
        CONFIGURATION.setObjectWrapper(new ExtObjectWrapper());

        // Get the root of resource path
        ProtectionDomain domain = BaseServlet.class.getProtectionDomain();
        URL root = domain.getCodeSource().getLocation();
        if (root == null) {
            throw new IllegalAccessError("Can not get root path.");
        }
        File rootPath = new File(root.getFile());

        try {
            // Set the root of resource path as freemarker template path.
            CONFIGURATION.setTemplateLoader(new FileTemplateLoader(rootPath));
        } catch (IOException e) {
            throw new IllegalAccessError("Can not find root path.");
        }
    }

    private Configuration() {
    }

    public void template(final Object servletInstance,
                         final String templateName,
                         final Map<String, Object> params,
                         final ServletRequest req,
                         final ServletResponse resp) throws IOException, ServletException {
        final String fullName = servletInstance.getClass().getName() + "#" + templateName;
        final String templateFile = TEMPLATES_MAP.computeIfAbsent(fullName, name -> {
            // Fetch first times
            final Class<?> clazz = servletInstance.getClass();
            final String packageName = clazz.getPackage().getName().replace(".", "/");
            return "/" + packageName.substring(0, packageName.lastIndexOf("/") + 1) +
                    "views/" + clazz.getSimpleName() + "/" + templateName + ".ftl";
        });
        render(templateFile, params, req, resp);
    }

    public void render(final String templateFile,
                       final Map<String, Object> params,
                       final ServletRequest req,
                       final ServletResponse resp) throws IOException, ServletException {
        try {
            Writer writer = new BufferedWriter(
                    new OutputStreamWriter(resp.getOutputStream(), req.getCharacterEncoding()));
            CONFIGURATION.getTemplate(templateFile).process(params, writer);
        } catch (TemplateException e) {
            throw new ServletException(e);
        }
    }

    public static Configuration getInstance() {
        return INSTANCE;
    }
}
