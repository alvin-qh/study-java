package alvin.fm.core.filters;

import alvin.fm.core.freemarker.Configuration;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.io.IOException;

@WebFilter(urlPatterns = {"/*"}, dispatcherTypes = {DispatcherType.REQUEST})
public class HttpMethodFilter implements Filter {
    private static final String ENCODING = "UTF-8";
    private final Configuration configuration = Configuration.getInstance();
    private static final Logger LOGGER = LoggerFactory.getLogger(HttpMethodFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        request.setCharacterEncoding(ENCODING);
        response.setCharacterEncoding(ENCODING);

        String method = request.getParameter("_method");
        if (!Strings.isNullOrEmpty(method)) {
            request = new HttpMethodServletRequest((HttpServletRequest) request, method);
        }
        try {
            chain.doFilter(request, response);
        } catch (Exception e) {
            LOGGER.error("Error caused in servlet", e);
            configuration.render("/alvin/fm/core/views/500.ftl",
                    ImmutableMap.of("err", e), request, response);
        }
    }

    @Override
    public void destroy() {
    }

    private static class HttpMethodServletRequest extends HttpServletRequestWrapper {

        private String method;

        HttpMethodServletRequest(HttpServletRequest request, String method) {
            super(request);
            if (method != null) {
                this.method = method.toUpperCase();
            }
        }

        @Override
        public String getMethod() {
            if (Strings.isNullOrEmpty(method)) {
                return super.getMethod();
            }
            return method;
        }
    }
}
