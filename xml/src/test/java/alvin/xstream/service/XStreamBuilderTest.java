package alvin.xstream.service;

import alvin.xstream.converter.LocalDateTimeConverter;
import alvin.xstream.model.Person;
import alvin.xstream.model.PhoneNumber;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class XStreamBuilderTest {

    private static final String XML_SOURCE = "<person>\n" +
            "  <firstName>Alvin</firstName>\n" +
            "  <lastName>Qu</lastName>\n" +
            "  <phone>\n" +
            "    <code>1</code>\n" +
            "    <number>1234567</number>\n" +
            "  </phone>\n" +
            "  <fax>\n" +
            "    <code>2</code>\n" +
            "    <number>7654321</number>\n" +
            "  </fax>\n" +
            "  <createdAt>2016-01-01T12:01:01</createdAt>\n" +
            "</person>";

    private static final Person PERSON = new Person("Alvin", "Qu",
            new PhoneNumber(1, "1234567"),
            new PhoneNumber(2, "7654321"),
            LocalDateTime.of(2016, 1, 1, 12, 1, 1));

    @Test
    void test_marshal() {
        String xml = new XStreamBuilder()
                .addAlias("person", Person.class)
                .registConverters(new LocalDateTimeConverter())
                .buildMarshaller()
                .marshal(PERSON);

        assertThat(xml, is(XML_SOURCE));
    }

    @Test
    void test_unmarshal() {
        Person excepted = new Person("Alvin", "Qu",
                new PhoneNumber(1, "1234567"),
                new PhoneNumber(2, "7654321"),
                LocalDateTime.of(2016, 1, 1, 12, 1, 1));

        assertThat(excepted, is(PERSON));
    }

    @Test
    void test_marshal_with_annotation() {
        String xml = new XStreamBuilder()
                .processAnnotation(Person.class)
                .buildMarshaller()
                .marshal(PERSON);

        assertThat(xml, is(XML_SOURCE));
    }

    @Test
    void test_unmarshal_with_annotation() {
        Person person = new XStreamBuilder()
                .processAnnotation(Person.class)
                .buildUnmarshaller()
                .unmarshal(XML_SOURCE, Person.class);

        assertThat(person, is(PERSON));
    }
}
