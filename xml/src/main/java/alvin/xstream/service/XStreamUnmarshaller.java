package alvin.xstream.service;

import com.thoughtworks.xstream.XStream;

public class XStreamUnmarshaller {

    private XStream xStream;

    XStreamUnmarshaller(XStream xStream) {
        this.xStream = xStream;
    }

    public <T> T unmarshal(String xml, Class<T> type) {
        return type.cast(xStream.fromXML(xml));
    }
}
