package alvin.xstream.service;

import com.thoughtworks.xstream.XStream;

public class XStreamMarshaller {

    private XStream xStream;

    XStreamMarshaller(XStream xStream) {
        this.xStream = xStream;
    }

    public String marshal(Object obj) {
        return xStream.toXML(obj);
    }
}
