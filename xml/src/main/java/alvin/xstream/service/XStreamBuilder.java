package alvin.xstream.service;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.ConverterMatcher;
import com.thoughtworks.xstream.converters.SingleValueConverter;
import com.thoughtworks.xstream.io.HierarchicalStreamDriver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class XStreamBuilder {
    private HierarchicalStreamDriver driver = null;
    private Map<String, Class<?>> aliasMap = new HashMap<>();
    private List<Class<?>> annotationClasses = new ArrayList<>();
    private List<ConverterMatcher> converters = new ArrayList<>();

    public XStreamBuilder driver(HierarchicalStreamDriver driver) {
        this.driver = driver;
        return this;
    }

    public XStreamBuilder addAlias(String alias, Class<?> type) {
        aliasMap.put(alias, type);
        return this;
    }

    public XStreamBuilder processAnnotation(Class<?> type) {
        annotationClasses.add(type);
        return this;
    }

    public XStreamBuilder registConverters(ConverterMatcher converter) {
        converters.add(converter);
        return this;
    }

    private XStream build() {
        XStream xStream;
        if (driver == null) {
            xStream = new XStream();
        } else {
            xStream = new XStream(driver);
        }
        aliasMap.forEach(xStream::alias);
        if (!annotationClasses.isEmpty()) {
            xStream.processAnnotations(annotationClasses.toArray(new Class[annotationClasses.size()]));
        }
        converters.forEach(c -> {
            if (c instanceof SingleValueConverter) {
                xStream.registerConverter((SingleValueConverter) c);
            } else if (c instanceof Converter) {
                xStream.registerConverter((Converter) c);
            } else {
                throw new IllegalArgumentException("Invalid converter type");
            }
        });
        return xStream;
    }

    public XStreamMarshaller buildMarshaller() {
        return new XStreamMarshaller(build());
    }

    public XStreamUnmarshaller buildUnmarshaller() {
        return new XStreamUnmarshaller(build());
    }
}
