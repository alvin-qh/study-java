package alvin.xstream.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@XStreamAlias("phone")
@Data
@EqualsAndHashCode
@ToString(doNotUseGetters = true)
public class PhoneNumber {
    private int code;
    private String number;

    PhoneNumber() {
    }

    public PhoneNumber(int code, String number) {
        this.code = code;
        this.number = number;
    }
}
