package alvin.xstream.model;

import alvin.xstream.converter.LocalDateTimeConverter;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDateTime;

@XStreamAlias("person")
@Data
@EqualsAndHashCode
@ToString(doNotUseGetters = true)
public class Person {
    private String firstName;
    private String lastName;
    private PhoneNumber phone;
    private PhoneNumber fax;

    @XStreamConverter(LocalDateTimeConverter.class)
    private LocalDateTime createdAt;

    Person() {
    }

    public Person(String firstName, String lastName, PhoneNumber phone, PhoneNumber fax, LocalDateTime createdAt) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.fax = fax;
        this.createdAt = createdAt;
    }
}
