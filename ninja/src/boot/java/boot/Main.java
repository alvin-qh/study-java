package boot;

import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.RequestLogHandler;
import org.eclipse.jetty.server.handler.gzip.GzipHandler;
import org.eclipse.jetty.util.BlockingArrayQueue;
import org.eclipse.jetty.util.thread.QueuedThreadPool;
import org.eclipse.jetty.webapp.WebAppContext;

import java.lang.reflect.Method;
import java.net.URL;
import java.security.ProtectionDomain;
import java.util.Arrays;
import java.util.concurrent.BlockingQueue;

public final class Main {
    private static final String ACCESS_LOG_FILE = "jetty.access.log";
    private static final String PORT = "jetty.port";
    private static final String CONTEXT_PATH = "jetty.context.path";
    private static final String THREAD_POOL_MIN = "jetty.threadpool.min";
    private static final String THREAD_POOL_MAX = "jetty.threadpool.max";
    private static final String IDLE_TIME = "jetty.threadpool.idle";
    private static final String QUEUE_SIZE = "jetty.queue.size";
    private static final int RETAIN_DAYS = 90;

    private int defaultMaxThread = 200;     // SUPPRESS
    private int defaultMinThread = 8;       // SUPPRESS
    private int defaultIdleTime = 60000;    // SUPPRESS

    private Main() {
    }

    public static void main(final String[] args) throws Exception {
        new Main().startJetty();
    }

    private void run(final String[] args) throws Exception {
        if (args.length > 0) {
            invokeMain(args);
        } else {
            startJetty();
        }
    }

    private void invokeMain(final String[] args) throws Exception {
        WebAppContext context = createWebContext();
        context.setServer(new Server());
        context.preConfigure();
        context.configure();

        Thread.currentThread().setContextClassLoader(context.getClassLoader());
        try {
            Class<?> cls = context.getClassLoader().loadClass(args[0]);
            Object[] mainArgs = Arrays.copyOfRange(args, 1, args.length);

            Method method = cls.getMethod("main", String[].class);
            method.invoke(null, new Object[]{mainArgs});
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(2);
        }
    }

    private void startJetty() throws Exception {
        QueuedThreadPool threadPool = createQueuedThreadPool();
        Server server = new Server(threadPool);

        WebAppContext webCtx = createWebContext();

        RequestLogHandler requestLogHandler = new RequestLogHandler();
        NCSARequestLog requestLogger = createLogger();
        if (requestLogger != null) {
            requestLogHandler.setRequestLog(requestLogger);
        }
        webCtx.insertHandler(requestLogHandler);

        final GzipHandler handler = createZipHandler(webCtx);

        server.addConnector(createConnector(server));
        server.setHandler(handler);
        server.setRequestLog(requestLogger);
        server.start();
        server.join();
    }

    private GzipHandler createZipHandler(WebAppContext context) {
        final GzipHandler handler = new GzipHandler();
        handler.setHandler(context);
        handler.setIncludedMimeTypes("text/html",
                "text/plain",
                "text/css",
                "text/xml",
                "application/json",
                "application/javascript",
                "application/xhtml+xml",
                "application/xml",
                "image/svg+xml");
        return handler;
    }

    private QueuedThreadPool createQueuedThreadPool() {
        BlockingQueue<Runnable> queue;

        String threadPoolMax = System.getProperty(THREAD_POOL_MAX);
        if (threadPoolMax != null) {
            defaultMaxThread = Integer.valueOf(threadPoolMax);
        }

        String threadPoolMin = System.getProperty(THREAD_POOL_MIN);
        if (threadPoolMin != null) {
            defaultMinThread = Integer.valueOf(threadPoolMin);
        }

        final String idleTime = System.getProperty(IDLE_TIME);
        if (idleTime != null) {
            defaultIdleTime = Integer.valueOf(idleTime);
        }

        String queueSize = System.getProperty(QUEUE_SIZE);
        if (queueSize != null) {
            int size = Integer.valueOf(queueSize);
            queue = new BlockingArrayQueue<>(size, defaultMinThread);
        } else {
            queue = new BlockingArrayQueue<>(defaultMinThread, defaultMinThread);
        }

        return new QueuedThreadPool(defaultMaxThread, defaultMinThread, defaultIdleTime, queue);
    }

    private ServerConnector createConnector(final Server server) {
        ServerConnector connector = new ServerConnector(server);

        String sport = System.getProperty(PORT);
        if (sport == null) {
            connector.setPort(8080);    // SUPPRESS
        } else {
            connector.setPort(Integer.parseInt(sport));
        }
        return connector;
    }

    private WebAppContext createWebContext() {
        WebAppContext context = new WebAppContext();
        String contextPath = System.getProperty(CONTEXT_PATH);
        if (contextPath == null) {
            contextPath = "";
        }
        context.setContextPath(contextPath);
        context.setWar(getWarLocation());
        return context;
    }

    private NCSARequestLog createLogger() {
        String logfile = System.getProperty(ACCESS_LOG_FILE);
        if (logfile == null) {
            return null;
        }
        NCSARequestLog log = new NCSARequestLog(logfile);

        log.setPreferProxiedForAddress(true);
        log.setRetainDays(RETAIN_DAYS);
        log.setAppend(true);
        log.setExtended(false);
        log.setLogTimeZone("GMT");

        return log;
    }

    private String getWarLocation() {
        ProtectionDomain domain = Main.class.getProtectionDomain();
        URL location = domain.getCodeSource().getLocation();
        return location.toExternalForm();
    }
}
