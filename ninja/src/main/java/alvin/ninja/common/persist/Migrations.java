package alvin.ninja.common.persist;

import alvin.common.db.Migration;
import org.hibernate.engine.jdbc.connections.spi.ConnectionProvider;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.jpa.HibernateEntityManagerFactory;
import org.hibernate.jpa.internal.EntityManagerImpl;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.sql.DataSource;

public class Migrations {
    private final Provider<EntityManager> emProvider;

    @Inject
    public Migrations(Provider<EntityManager> emProvider) {
        this.emProvider = emProvider;
    }

    public void migrate() {
        final EntityManager em = emProvider.get();
        if (em != null) {
            final HibernateEntityManagerFactory emFactory = ((EntityManagerImpl) em).getFactory();
            final SessionFactoryImpl sessionFactory = (SessionFactoryImpl) emFactory.getSessionFactory();
            final ConnectionProvider connectionProvider = sessionFactory.getConnectionProvider();
            final DataSource dataSource = connectionProvider.unwrap(DataSource.class);

            Migration migration = new Migration.Builder(dataSource)
                    .addLocation("classpath:/migrations/sql")
                    .build();

            migration.migrate();
        }
    }
}
