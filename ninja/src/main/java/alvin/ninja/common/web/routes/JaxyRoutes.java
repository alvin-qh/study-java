package alvin.ninja.common.web.routes;

import com.google.common.base.Splitter;
import ninja.utils.NinjaProperties;
import org.reflections.util.ClasspathHelper;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

@Singleton
public class JaxyRoutes extends ninja.jaxy.JaxyRoutes {

    private final Set<String> enabledFeatureNames;

    @Inject
    public JaxyRoutes(NinjaProperties ninjaProperties) {
        super(ninjaProperties);
        this.enabledFeatureNames = new HashSet<>();

        processEnableFeatures(ninjaProperties);
    }

    private void processEnableFeatures(NinjaProperties ninjaProperties) {
        String appsPackage = ninjaProperties.get("application.modules.package") + ".apps.";

        Splitter.on(",").omitEmptyStrings().trimResults()
                .split(ninjaProperties.get("application.enables"))
                .forEach(name -> enabledFeatureNames.add(appsPackage + name));
    }

    @Override
    public Set<URL> getPackagesToScanForRoutes() {
        Set<URL> packagesToScanForRoutes = super.getPackagesToScanForRoutes();
        enabledFeatureNames.forEach(name ->
                packagesToScanForRoutes.addAll(ClasspathHelper.forPackage(name)));
        return packagesToScanForRoutes;
    }
}
