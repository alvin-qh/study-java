package alvin.ninja.common.web.assets;

import com.google.common.io.ByteStreams;
import com.google.inject.Inject;
import ninja.AssetsControllerHelper;
import ninja.Context;
import ninja.Renderable;
import ninja.Result;
import ninja.Results;
import ninja.utils.HttpCacheToolkit;
import ninja.utils.MimeTypes;
import ninja.utils.NinjaProperties;
import ninja.utils.ResponseStreams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class AssetsController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AssetsController.class);
    private static final String FILENAME_PATH_PARAM = "fileName";

    private final String assetsDir;
    private final MimeTypes mimeTypes;
    private final HttpCacheToolkit httpCacheToolkit;
    private final NinjaProperties ninjaProperties;
    private final AssetsControllerHelper assetsControllerHelper;

    @Inject
    public AssetsController(AssetsControllerHelper assetsControllerHelper,
                            HttpCacheToolkit httpCacheToolkit,
                            MimeTypes mimeTypes,
                            NinjaProperties ninjaProperties,
                            AssetsHelper assetsHelper) {
        this.assetsControllerHelper = assetsControllerHelper;
        this.httpCacheToolkit = httpCacheToolkit;
        this.mimeTypes = mimeTypes;
        this.ninjaProperties = ninjaProperties;
        this.assetsDir = assetsHelper.getAssetsDir();
    }

    public Result serveStatic() {
        Object renderable = (Renderable) (context, result) -> {
            String fileName = getFileNameFromPathOrReturnRequestPath(context);
            URL url = getStaticFileFromAssetsDir(fileName);
            streamOutUrlEntity(url, context, result);
        };
        return Results.ok().render(renderable);
    }

    public Result serveWebJars() {
        Object renderable = (Renderable) (context, result) -> {
            String fileName = getFileNameFromPathOrReturnRequestPath(context);
            URL url = getStaticFileFromMetaInfResourcesDir(fileName);
            streamOutUrlEntity(url, context, result);
        };
        return Results.ok().render(renderable);
    }

    @SuppressWarnings("UnstableApiUsage")
    private void streamOutUrlEntity(URL url, Context context, Result result) {
        if (url == null) {
            context.finalizeHeadersWithoutFlashAndSessionCookie(Results.notFound());
        } else if (assetsControllerHelper.isDirectoryURL(url)) {
            context.finalizeHeadersWithoutFlashAndSessionCookie(Results.notFound());
        } else {
            try {
                final URLConnection urlConnection = url.openConnection();
                final Long lastModified = urlConnection.getLastModified();
                httpCacheToolkit.addEtag(context, result, lastModified);

                if (result.getStatusCode() == Result.SC_304_NOT_MODIFIED) {
                    context.finalizeHeadersWithoutFlashAndSessionCookie(result);
                } else {
                    result.status(Result.SC_200_OK);

                    final String mimeType = mimeTypes.getContentType(context, url.getFile());
                    if (mimeType != null && !mimeType.isEmpty()) {
                        result.contentType(mimeType);
                    }

                    final ResponseStreams respStream = context.finalizeHeadersWithoutFlashAndSessionCookie(result);
                    try (InputStream is = urlConnection.getInputStream()) {
                        try (OutputStream os = respStream.getOutputStream()) {
                            ByteStreams.copy(is, os);
                        }
                    }
                }
            } catch (IOException e) {
                LOGGER.error("error streaming file", e);
            }
        }
    }

    private URL getStaticFileFromAssetsDir(String fileName) {
        final URL url;
        if ((ninjaProperties.isDev() || ninjaProperties.isTest()) &&
                new File(assetsDirInDevModeWithoutTrailingSlash()).exists()) {
            String finalNameWithoutLeadingSlash =
                    assetsControllerHelper.normalizePathWithoutLeadingSlash(fileName, false);
            File possibleFile =
                    new File(assetsDirInDevModeWithoutTrailingSlash() + File.separator + finalNameWithoutLeadingSlash);
            url = getUrlForFile(possibleFile);
        } else {
            String finalNameWithoutLeadingSlash =
                    assetsControllerHelper.normalizePathWithoutLeadingSlash(fileName, true);
            url = this.getClass().getClassLoader().getResource(assetsDir + "/" + finalNameWithoutLeadingSlash);
        }
        return url;
    }

    private URL getUrlForFile(File possibleFileInSrc) {
        if (possibleFileInSrc.exists() && !possibleFileInSrc.isDirectory()) {
            try {
                return possibleFileInSrc.toURI().toURL();
            } catch (MalformedURLException malformedURLException) {
                LOGGER.error("Error in dev mode while streaming files from src dir. ", malformedURLException);
            }
        }
        return null;
    }

    private URL getStaticFileFromMetaInfResourcesDir(String fileName) {
        String finalNameWithoutLeadingSlash
                = assetsControllerHelper.normalizePathWithoutLeadingSlash(fileName, true);

        final ClassLoader classLoader = this.getClass().getClassLoader();
        return classLoader.getResource("META-INF/resources/webjars/" + finalNameWithoutLeadingSlash);
    }

    private static String getFileNameFromPathOrReturnRequestPath(Context context) {
        String fileName = context.getPathParameter(FILENAME_PATH_PARAM);

        if (fileName == null) {
            fileName = context.getRequestPath();
        }
        return fileName;
    }

    private String assetsDirInDevModeWithoutTrailingSlash() {
        String srcDir = System.getProperty("user.dir") + File.separator +
                "src" + File.separator +
                "main" + File.separator + "java";
        return srcDir + File.separator + assetsDir;
    }
}
