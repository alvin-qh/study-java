package alvin.ninja.common.web.routes;

import alvin.common.io.Directory;
import alvin.ninja.common.web.assets.AssetsController;
import alvin.ninja.common.web.assets.AssetsHelper;
import ninja.Router;
import ninja.application.ApplicationRoutes;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class StaticRoutes implements ApplicationRoutes {

    private final String assetsDir;

    @Inject
    public StaticRoutes(AssetsHelper assetsHelper) {
        this.assetsDir = assetsHelper.getAssetsDir();
    }

    @Override
    public void init(Router router) {
        router.GET()
                .route(Directory.join(assetsDir, "{fileName: .*}"))
                .with(AssetsController.class, "serveStatic");

        router.GET()
                .route(Directory.join(assetsDir, "webjars/{fileName: .*}"))
                .with(AssetsController.class, "serveWebJars");
    }
}
