package alvin.ninja.common.web.assets;

import alvin.common.io.Directory;
import com.google.common.base.Strings;
import ninja.utils.NinjaProperties;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AssetsHelper {
    private final String assetsDir;

    @Inject
    public AssetsHelper(NinjaProperties ninjaProperties) {
        this.assetsDir = processAssetsDir(ninjaProperties);
    }

    private static String processAssetsDir(NinjaProperties ninjaProperties) {
        String assetsDir = ninjaProperties.get("application.assets.path");
        if (Strings.isNullOrEmpty(assetsDir)) {
            assetsDir = "/assets/";
        }
        return assetsDir;
    }

    public String getAssetsDir() {
        return assetsDir;
    }

    public String js(String fileName) {
        return Directory.join(assetsDir, "js", fileName);
    }

    public String css(String fileName) {
        return Directory.join(assetsDir, "css", fileName);
    }

    public String image(String fileName) {
        return Directory.join(assetsDir, "images", fileName);
    }
}
