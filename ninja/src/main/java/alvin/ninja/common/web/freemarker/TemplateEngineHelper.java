package alvin.ninja.common.web.freemarker;

import ninja.Result;
import ninja.Route;

import static ninja.utils.NinjaConstant.CONTROLLERS_DIR;
import static ninja.utils.NinjaConstant.VIEWS_DIR;

/**
 * This class can change ninja default package name from 'controllers, views' to others (like 'controller, view').
 *
 * @see alvin.ninja.conf.Module#configure()
 */
public class TemplateEngineHelper extends ninja.template.TemplateEngineHelper {

    @Override
    public String getTemplateForResult(Route route, Result result, String suffix) {
        if (result.getTemplate() == null) {
            Class controller = route.getControllerClass();

            String controllerPackageName = controller.getPackage().getName();
            String parentPackageOfController = controllerPackageName.replaceFirst(CONTROLLERS_DIR, VIEWS_DIR);

            String parentControllerPackageAsPath = parentPackageOfController.replaceAll("\\.", "/");

            return String.format("/%s/%s/%s%s", parentControllerPackageAsPath,
                    controller.getSimpleName(), route.getControllerMethod()
                            .getName(), suffix);
        } else {
            return result.getTemplate();
        }
    }
}
