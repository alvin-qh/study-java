package alvin.ninja.apps.intro.controllers;

import ninja.Result;
import ninja.Results;
import ninja.jaxy.GET;
import ninja.jaxy.Path;

import javax.inject.Singleton;

@Singleton
@Path("/intro")
public class HelloController {

    @Path("/hello")
    @GET
    public Result hello() {
        return Results.html();
    }
}
