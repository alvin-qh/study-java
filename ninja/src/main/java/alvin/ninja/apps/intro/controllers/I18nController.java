package alvin.ninja.apps.intro.controllers;

import com.google.common.base.Strings;
import ninja.Result;
import ninja.Results;
import ninja.i18n.Lang;
import ninja.jaxy.GET;
import ninja.jaxy.Path;
import ninja.params.Param;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
@Path("/intro")
public class I18nController {

    private final Lang i18nLang;

    @Inject
    public I18nController(Lang i18nLang) {
        this.i18nLang = i18nLang;
    }

    @Path("/i18n")
    @GET
    public Result i18n(@Param("lang") String lang) {
        if (Strings.isNullOrEmpty(lang)) {
            return Results.html();
        }
        return i18nLang.setLanguage(lang, Results.html());
    }
}
