package alvin.ninja.apps.intro.controllers;

import ninja.Result;
import ninja.Results;
import ninja.jaxy.GET;
import ninja.jaxy.Path;

import javax.inject.Singleton;

@Singleton
@Path("/intro")
public class AssetsController {

    @Path("/assets")
    @GET
    public Result assets() {
        return Results.html();
    }
}
