package alvin.ninja.apps.curd.web.controllers;

import ninja.Result;
import ninja.Results;
import ninja.jaxy.GET;
import ninja.jaxy.Path;

@Path("/curd")
public class ItemsController {

    @GET
    @Path("/items")
    public Result index() {
        return Results.html();
    }
}
