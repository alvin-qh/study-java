package alvin.ninja.conf;

import alvin.ninja.common.web.routes.JaxyRoutes;
import alvin.ninja.common.web.routes.StaticRoutes;
import ninja.Router;
import ninja.application.ApplicationRoutes;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class Routes implements ApplicationRoutes {

    private final JaxyRoutes jaxyRoutes;
    private final StaticRoutes staticRoutes;

    @Inject
    public Routes(JaxyRoutes jaxyRoutes, StaticRoutes staticRoutes) {
        this.jaxyRoutes = jaxyRoutes;
        this.staticRoutes = staticRoutes;
    }

    @Override
    public void init(Router router) {
        jaxyRoutes.init(router);
        staticRoutes.init(router);
    }
}
