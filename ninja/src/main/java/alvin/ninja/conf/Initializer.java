package alvin.ninja.conf;

import alvin.ninja.common.persist.Migrations;
import alvin.ninja.common.web.assets.AssetsHelper;
import alvin.ninja.common.web.freemarker.DefaultObjectWrapper;
import com.google.inject.persist.PersistService;
import freemarker.template.Configuration;
import freemarker.template.SimpleHash;
import freemarker.template.TemplateModelException;
import ninja.lifecycle.Start;
import ninja.template.TemplateEngineFreemarker;
import ninja.utils.NinjaProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@SuppressWarnings("unused")
@Singleton
public class Initializer {
    private static final Logger LOGGER = LoggerFactory.getLogger(Initializer.class);
    private static final String PERSISTENCE_UNIT_NAME = "persistenceUnitName";
    private static final String PERSISTENCE_PROPERTIES = "persistenceProperties";

    @Inject
    private PersistService persistService;

    @Inject
    private NinjaProperties ninjaProperties;

    @Inject
    private AssetsHelper assetsHelper;

    @Inject
    private TemplateEngineFreemarker engineFreemarker;

    @Inject
    private Migrations migrations;

    private Map<String, Object> getPropertiesWithPrefixOfName(String prefix) {
        final Map<String, Object> result = new HashMap<>();

        ninjaProperties.getAllCurrentNinjaProperties().forEach((key, val) -> {
            String sKey = key.toString();
            if (sKey.startsWith(prefix)) {
                result.put(sKey.substring(prefix.length()), val);
            }
        });
        return result;
    }

    private static <T> T getFieldValue(Object target, String fieldName, Class<? extends T> type) {
        try {
            Field field = target.getClass().getDeclaredField(fieldName);
            field.setAccessible(true);

            return type.cast(field.get(target));
        } catch (NoSuchFieldException | IllegalAccessException e) {
            LOGGER.error("Cannot load field \'" + fieldName + "\' from " + target);
            throw new RuntimeException(e);
        }
    }

    @Start(order = 1)
    public void jpaConfig() {
        String unitName = getFieldValue(persistService, PERSISTENCE_UNIT_NAME, String.class);
        Properties properties = getFieldValue(persistService, PERSISTENCE_PROPERTIES, Properties.class);
        properties.putAll(getPropertiesWithPrefixOfName(unitName + "."));
    }

    @Start(order = 90)
    public void freemarkerConfig() throws TemplateModelException {
        Configuration configuration = engineFreemarker.getConfiguration();

        SimpleHash tags = new SimpleHash(new DefaultObjectWrapper());
        tags.put("assets", assetsHelper);

        configuration.setAllSharedVariables(tags);
    }

    @Start(order = 90)
    public void databaseMigration() {
        migrations.migrate();
    }
}
