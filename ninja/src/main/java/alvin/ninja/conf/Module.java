package alvin.ninja.conf;

import com.google.inject.AbstractModule;
import ninja.template.TemplateEngineHelper;

public class Module extends AbstractModule {

    @Override
    protected void configure() {
        bind(TemplateEngineHelper.class).to(alvin.ninja.common.web.freemarker.TemplateEngineHelper.class);
        bind(Initializer.class);
    }
}
