CREATE TABLE `prod_stock`(
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `period` BIGINT NOT NULL,
    `period_unit` VARCHAR(50) NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT current_timestamp,
    PRIMARY KEY `pk_prod_stock`(`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `prod_item`(
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(100) NOT NULL,
    `model` VARCHAR(50) NOT NULL,
    `origin` VARCHAR(100) NOT NULL,
    PRIMARY KEY `pk_prod_item`(`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE `prod_stock_item`(
    `id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    `stock_id` VARCHAR(100) NOT NULL,
    `item_id` VARCHAR(50) NOT NULL,
    `created_at` TIMESTAMP NOT NULL DEFAULT current_timestamp,
    PRIMARY KEY `prod_stock_item`(`id`),
    UNIQUE KEY `uk_stock_item`(`stock_id`, `item_id`)
) DEFAULT CHARSET=utf8;
