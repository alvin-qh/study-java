;
(function (global) {

    function fixNumber(n) {
        if (n < 10) {
            return '0' + n;
        }
        return n;
    }

    global.intro = {};

    global.intro.assets = function () {
        function showTime($times) {
            var now = new Date();
            $times.innerText = now.getFullYear() + '-'
                + fixNumber(now.getMonth()) + '-'
                + fixNumber(now.getDate()) + ' '
                + fixNumber(now.getHours()) + ':'
                + fixNumber(now.getMinutes()) + ':'
                + fixNumber(now.getSeconds());
        }

        window.addEventListener("DOMContentLoaded", function () {
            var $times = document.getElementsByClassName("time")[0];
            showTime($times);

            setInterval(function () {
                showTime($times);
            }, 1000, this);
        });
    };

})(window);
