# Ninja Framework

## Jetty embedded servlet container

## Write Main class to start servlet container

The `boot.Main` class include main method, to start jetty servlet container service, and make current jar file as war.

Load location of current jar file:
```java
private String getWarLocation() {
    ProtectionDomain domain = Main.class.getProtectionDomain();
    URL location = domain.getCodeSource().getLocation();
    return location.toExternalForm();
}
```

Make jar location as war locaction:
```java
private WebAppContext createWebContext() {
    WebAppContext context = new WebAppContext();
    String contextPath = System.getProperty(CONTEXT_PATH);
    if (contextPath == null) {
        contextPath = "";
    }
    context.setContextPath(contextPath);
    context.setWar(getWarLocation());
    return context;
}
```

Start jetty:
```java
private void startJetty() throws Exception {
    QueuedThreadPool threadPool = createQueuedThreadPool();
    Server server = new Server(threadPool);

    WebAppContext webCtx = createWebContext();

    RequestLogHandler requestLogHandler = new RequestLogHandler();
    NCSARequestLog requestLogger = createLogger();
    if (requestLogger != null) {
        requestLogHandler.setRequestLog(requestLogger);
    }
    webCtx.insertHandler(requestLogHandler);

    final GzipHandler handler = createZipHandler(webCtx);

    server.addConnector(createConnector(server));
    server.setHandler(handler);
    server.setRequestLog(requestLogger);
    server.start();
    server.join();
}
```

### Make fat jar base war

In `build.gradle` file, add task to build fat jar:

```groovy
task capsule(type: Jar, dependsOn: [war, bootClasses]) {
    archiveName = "study-java-ninja-${version}.jar"

    from zipTree(war.archivePath)

    from fileTree(dir: new File(buildDir, "classes/java/boot"), include: '**/*.class')

    from {
        configurations.bootCompile.collect { it.isDirectory() ? it : zipTree(it) }
    } {
        exclude 'about.html'
    }

    manifest {
        attributes('Main-Class': 'boot.Main')
    }
}
```

This jar file based on war file, it copy all `.class` files from war file and other dependances jar file. The entrance of jar file is `boot.Main`