# study-java

## Logs

### 2018-11-10: Upgrade gradle wrapper

Update gradle version to `4.10.2` to support `open-jdk-11`

```bash
$ gradle wrapper
$ ./gradlew -v

------------------------------------------------------------
Gradle 4.10.2
------------------------------------------------------------

Build time:   2018-09-19 18:10:15 UTC
Revision:     b4d8d5d170bb4ba516e88d7fe5647e2323d791dd

Kotlin DSL:   1.0-rc-6
Kotlin:       1.2.61
Groovy:       2.4.15
Ant:          Apache Ant(TM) version 1.9.11 compiled on March 23 2018
JVM:          11.0.1 (Oracle Corporation 11.0.1+13)
OS:           Mac OS X 10.14.1 x86_64
```

Issues:

Some warning caused after update gradle
```text
WARNING: An illegal reflective access operation has occurred
WARNING: Illegal reflective access by org.codehaus.groovy.reflection.CachedClass (file:/Users/alvin/.gradle/wrapper/dists/gradle-4.10.2-all/9fahxiiecdb76a5g3aw9oi8rv/gradle-4.10.2/lib/groovy-all-2.4.15.jar) to method java.lang.Object.finalize()
WARNING: Please consider reporting this to the maintainers of org.codehaus.groovy.reflection.CachedClass
WARNING: Use --illegal-access=warn to enable warnings of further illegal reflective access operations
WARNING: All illegal access operations will be denied in a future release
```

### 2018-11-12: Upgrade gradle-gretty-plugin 

Gretty plugin `2.0.0` cannot support `Open JDK 11`, update to `2.2.0`;

- Classpath changed from `org.akhikhl.gretty:gretty:2.0.0` to `gradle.plugin.org.gretty:gretty:2.2.0`
- Plugin name changed from `org.akhikhl.gretty` to `org.gretty`
- Jetty9 cannot support JDK 11 yet, change servlet container to Tomcat8

Now, `build.gradle` would be changed like:

```groovy
buildscript {
    // ...
    dependencies {
        // ...
        classpath "gradle.plugin.org.gretty:gretty:${versions.grettyPlugin}"
    }
}

// ...

apply plugin: 'org.gretty'
gretty {
    servletContainer = 'tomcat8'
    contextPath = ''
    scanInterval = 2
}
```

### 2018-11-13: ASM package not support JDK 11

Gradle test, jetty 9.4, jacoco not support JDK 11 yet, cause the current version of package `org.ow2.asm`
not support JDK 11 yet. so any java lib dependence it can not work well now.

In `build.gradle` file, downgrade jdk compatibility like:
```groovy
sourceCompatibility = '10'
targetCompatibility = '10'
```

If use idea plugin, add more configs:

```groovy
idea {
    project {
        jdkName = '11'
        languageLevel = '10'
    }
}
```
can solve this problem temporary

### 2018-11-25: Update JUnit from version 4 to 5

- `@RunWith`    -> `@ExtendWith`
- `@Before`     -> `@BeforeEach`
- `@After`      -> `@AfterEach`
- `@BeforClass` -> `@BeforeAll`
- `@AfterClass` -> `@AfterAll`
- `@Test` annotation abandon `expected` argument, if need check exception, use `assertThrows` assertion. 
```java
final Executable exec = () -> {...};
assertThrows(Throwable.class, exec)
```
