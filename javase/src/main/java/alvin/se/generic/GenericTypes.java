package alvin.se.generic;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

@SuppressWarnings("ALL")
public final class GenericTypes {
    private GenericTypes() {
    }

    public static Class[] getType(Class<?> clazz) {
        for (Type type : clazz.getGenericInterfaces()) {
            if (type instanceof ParameterizedType) {
                final ParameterizedType pt = (ParameterizedType) type;
                final Type[] types = pt.getActualTypeArguments();
                if (types != null && types.length > 0) {
                    final Class[] results = new Class[types.length];
                    for (int i = 0; i < types.length; i++) {
                        final Type at = types[i];
                        if (at instanceof ParameterizedType) {
                            final ParameterizedType pat = (ParameterizedType) at;
                            results[i] = (Class) pat.getRawType();
                        } else {
                            results[i] = (Class) at;
                        }
                    }
                    return results;
                }
                return null;
            }
        }
        return null;
    }

    public static Class[] getType(Object obj) {
        return getType(obj.getClass());
    }
}
