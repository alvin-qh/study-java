package alvin.se.generic;

public interface GenericInterface<T1, T2> {
    default void test(T1 first, T2 second) {
    }
}
