package alvin.se.mail;

public interface Mail {
    void send() throws Exception;
}
