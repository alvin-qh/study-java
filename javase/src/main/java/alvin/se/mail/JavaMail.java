package alvin.se.mail;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.util.Date;
import java.util.Properties;

public class JavaMail {

    private static final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
    private static final boolean DEBUG = true;
    private static final String HTML_CONTENT_TYPE = "text/html; charset=UTF-8";

    private String smtpHost;
    private int smtpPort;
    private boolean auth = true;
    private boolean fallback = false;
    private boolean startTlsEnable = true;
    private String protocol = "smtp";
    private String account;
    private String password;

    public JavaMail(String smtpHost, int smtpPort, String account, String password) {
        this.smtpHost = smtpHost;
        this.smtpPort = smtpPort;
        this.account = account;
        this.password = password;
    }

    public JavaMail auth(boolean auth) {
        this.auth = auth;
        return this;
    }

    public JavaMail fallback(boolean fallback) {
        this.fallback = fallback;
        return this;
    }

    public JavaMail startTlsEnable(boolean startTlsEnable) {
        this.startTlsEnable = startTlsEnable;
        return this;
    }

    public JavaMail protocol(String protocol) {
        this.protocol = protocol;
        return this;
    }

    public Mail createSimpleMail(String from, String to, String subject, String content) throws MessagingException {
        Properties props = makeProperties();

        Session session = Session.getDefaultInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(account, password);
                    }
                });

        final MimeMessage message = new MimeMessage(session);

        // creates message part
        MimeBodyPart mimeBodyPart = new MimeBodyPart();
        mimeBodyPart.setContent(content, HTML_CONTENT_TYPE);

        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(mimeBodyPart);


        message.setFrom(new InternetAddress(from));
        message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
        message.setSentDate(new Date());
        message.setSubject(subject);
        message.setContent(multipart);

        // send the message
        return () -> Transport.send(message);
    }

    private Properties makeProperties() {
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", startTlsEnable);
        props.put("mail.smtp.host", smtpHost);
        props.put("mail.smtp.auth", auth);
        props.put("mail.debug", DEBUG);
        props.put("mail.smtp.port", smtpPort);
        props.put("mail.smtp.socketFactory.port", smtpPort);
        props.put("mail.smtp.socketFactory.class", SSL_FACTORY);
        props.put("mail.smtp.socketFactory.fallback", fallback);
        props.put("mail.transport.protocol", protocol);
        return props;
    }
}
