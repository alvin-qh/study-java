package alvin.se.binary;

@SuppressWarnings("ALL")
public final class Bytes {

    private Bytes() {
    }

    public static byte[] fill(byte[] dst, int dstOffset, byte[] src, int srcOffset, int length) {
        System.arraycopy(src, srcOffset, dst, dstOffset, length);
        return dst;
    }

    public static byte[] fill(byte[] dst, int dstOffset, byte[] src) {
        return fill(dst, dstOffset, src, 0, src.length);
    }

    public static byte[] fill(byte[] dst, int offset, short val) {
        dst[offset + 1] = (byte) (val & 0xFF);  // SUPPRESS
        dst[offset] = (byte) (val >> 8 & 0xFF); // SUPPRESS
        return dst;
    }

    public static byte[] fill(byte[] dst, int offset, int val) {
        fill(dst, offset, (short) ((val >> 16) & 0xFFFF));      // SUPPRESS
        fill(dst, offset + 2, (short) (val & 0xFFFF));   // SUPPRESS
        return dst;
    }

    public static byte[] fill(byte[] dst, int offset, long val) {
        fill(dst, offset, (int) ((val >> 32) & 0xFFFFFFFFL));       // SUPPRESS
        fill(dst, offset + 4, (int) (val & 0xFFFFFFFFL));    // SUPPRESS
        return dst;
    }

    public static byte[] concat(byte[] a, byte[] b) {
        byte[] c = new byte[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);
        return c;
    }

    public static byte[] concat(byte[] a, byte b) {
        byte[] c = new byte[a.length + 1];
        System.arraycopy(a, 0, c, 0, a.length);
        c[a.length] = b;
        return c;
    }

    public static byte[] concat(byte[] a, short b) {
        byte[] c = new byte[a.length + 2];
        System.arraycopy(a, 0, c, 0, a.length);
        fill(c, a.length, b);
        return c;
    }

    public static byte[] concat(byte[] a, int b) {
        byte[] c = new byte[a.length + 4];  // SUPPRESS
        System.arraycopy(a, 0, c, 0, a.length);
        fill(c, a.length, b);
        return c;
    }

    public static byte[] concat(byte[] a, long b) {
        byte[] c = new byte[a.length + 8];  // SUPPRESS
        System.arraycopy(a, 0, c, 0, a.length);
        fill(c, a.length, b);
        return c;
    }

    public static byte[] toBytes(byte b) {
        byte[] array = new byte[1];
        array[0] = b;
        return array;
    }

    public static byte[] toBytes(int i) {
        byte[] array = new byte[4];     // SUPPRESS

        array[3] = (byte) (i & 0xFF);           // SUPPRESS
        array[2] = (byte) ((i >> 8) & 0xFF);    // SUPPRESS
        array[1] = (byte) ((i >> 16) & 0xFF);   // SUPPRESS
        array[0] = (byte) ((i >> 24) & 0xFF);   // SUPPRESS

        return array;
    }

    public static byte[] toBytes(short i) {
        byte[] array = new byte[2];

        array[1] = (byte) (i & 0xFF);           // SUPPRESS
        array[0] = (byte) ((i >> 8) & 0xFF);    // SUPPRESS

        return array;
    }

    public static byte[] toBytes(char c) {
        return toBytes((byte) toByte(c));
    }

    private static int toByte(char c) {
        if (c >= '0' && c <= '9') {
            return (c - '0');
        }
        if (c >= 'A' && c <= 'F') {
            return (c - 'A' + 10);      // SUPPRESS
        }
        if (c >= 'a' && c <= 'f') {
            return (c - 'a' + 10);      // SUPPRESS
        }
        throw new RuntimeException("Invalid hex char '" + c + "'");
    }

    public static short toShort(byte[] data, int offset) {
        int hi = data[offset];
        int lo = data[offset + 1];
        return (short) ((hi << 8 & 0xFF00) | (lo & 0xFF));  // SUPPRESS
    }

    public static short toShort(byte[] data) {
        return toShort(data, 0);
    }

    public static int toInt(byte[] data, int offset) {
        int hi = toShort(data, offset);
        int lo = toShort(data, offset + 2);
        return (hi << 16 & 0xFFFF0000) | (lo & 0xFFFF); // SUPPRESS
    }

    public static int toInt(byte[] data) {
        return toInt(data, 0);
    }

    public static long toLong(byte[] data, int offset) {
        long hi = toInt(data, offset);
        long lo = toInt(data, offset + 4);  // SUPPRESS
        return (hi << 32 & 0xFFFFFFFF00000000L) | (lo & 0xFFFFFFFFL);   // SUPPRESS
    }

    public static long toLong(byte[] data) {
        return toLong(data, 0);
    }

    public static byte flip(byte data) {
        int hi = data >>> 4 & 0xF;  // SUPPRESS
        int lo = data & 0xF;        // SUPPRESS
        return (byte) ((lo << 4 & 0xF0) | (hi & 0xF));  // SUPPRESS
    }

    public static short flip(short data) {
        int hi = data >>> 8 & 0xFF;     // SUPPRESS
        int lo = data & 0xFF;       // SUPPRESS
        return (short) ((lo << 8 & 0xFF00) | (hi & 0xFF));  // SUPPRESS
    }

    public static int flip(int data) {
        int hi = data >>> 16 & 0xFFFF;  // SUPPRESS
        int lo = data & 0xFFFF;     // SUPPRESS
        return (lo << 16 & 0xFFFF0000) | (hi & 0xFFFF); // SUPPRESS
    }

    public static long flip(long data) {
        long hi = data >>> 32 & 0xFFFFFFFFL;    // SUPPRESS
        long lo = data & 0xFFFFFFFFL;   // SUPPRESS
        return (lo << 32 & 0xFFFFFFFF00000000L) | (hi & 0xFFFFFFFFL);   // SUPPRESS
    }

    public static int toInt(int b) {
        return 0xFF & b;    // SUPPRESS
    }

    public static int bcc(byte[] data, int offset, int length) {
        int sum = 0;
        for (int i = offset; i < offset + length; i++) {
            sum += toInt(data[i]);
        }
        return ~sum;
    }

    public static boolean compare(byte[] a, int offsetA, byte[] b, int offsetB, int length) {
        for (int i = 0; i < length; i++) {
            if (a[offsetA + i] != b[offsetB + i]) {
                return false;
            }
        }
        return true;
    }
}
