package alvin.se.binary;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.Character.digit;

@SuppressWarnings("ALL")
public final class Hex {
    private static final char[] HEX_DIGITS = "0123456789ABCDEF".toCharArray();

    private Hex() {
    }

    public static byte[] toBytes(String s, int offset, int length) {
        checkArgument(length > 1);

        final byte[] data = new byte[length / 2];
        for (int i = offset; i < offset + length; i += 2) {
            final int hi = (digit(s.charAt(i), 16) << 4) & 0xF0;    // SUPPRESS
            final int lo = digit(s.charAt(i + 1), 16) & 0xF;        // SUPPRESS

            data[i / 2] = (byte) (hi | lo);
        }
        return data;
    }

    public static byte[] toBytes(final String s) {
        return toBytes(s, 0, s.length());
    }

    public static String toString(byte[] data, int offset, int length) {
        char[] buf = new char[length * 2];

        int n = 0;
        for (int i = offset; i < offset + length; i++) {
            byte b = data[i];
            buf[n++] = HEX_DIGITS[(b >>> 4) & 0xF];     // SUPPRESS
            buf[n++] = HEX_DIGITS[b & 0xF];     // SUPPRESS
        }
        return new String(buf);
    }

    public static String toString(byte b) {
        return toString(Bytes.toBytes(b));
    }

    public static String toString(byte[] array) {
        return toString(array, 0, array.length);
    }

    public static String toString(int i) {
        return toString(Bytes.toBytes(i));
    }

    public static String toString(short i) {
        return toString(Bytes.toBytes(i));
    }

    public static String toStringWithWhiteSpace(byte[] array) {
        return toStringWithWhiteSpace(array, 0, array.length);
    }

    public static String toStringWithWhiteSpace(byte[] array, int offset, int length) {
        char[] buf = new char[length * 3];  // SUPPRESS

        int n = 0;
        for (int i = offset; i < offset + length; i++) {
            byte b = array[i];
            buf[n++] = HEX_DIGITS[(b >>> 4) & 0xF]; // SUPPRESS
            buf[n++] = HEX_DIGITS[b & 0xF]; // SUPPRESS

            buf[n++] = ' ';
        }
        return new String(buf, 0, buf.length - 1);
    }

    public static String dump(byte[] array) {
        return dump(array, 0, array.length);
    }

    public static String dump(byte[] array, int offset, int length) {
        StringBuilder result = new StringBuilder();

        byte[] line = new byte[16]; // SUPPRESS
        int lineIndex = 0;

        result.append("\n0x").append(toString(offset));

        for (int i = offset; i < offset + length; i++) {
            if (lineIndex == 16) {  // SUPPRESS
                result.append(' ');

                for (int j = 0; j < 16; j++) {  // SUPPRESS
                    if (line[j] > ' ' && line[j] < '~') {
                        result.append(new String(line, j, 1));
                    } else {
                        result.append('.');
                    }
                }

                result.append("\n0x");
                result.append(toString(i));
                lineIndex = 0;
            }

            byte b = array[i];
            result.append(' ')
                    .append(HEX_DIGITS[(b >>> 4) & 0xF])        // SUPPRESS
                    .append(HEX_DIGITS[b & 0xF]);       // SUPPRESS

            line[lineIndex++] = b;
        }

        if (lineIndex != 16) {      // SUPPRESS
            int count = (16 - lineIndex) * 3;       // SUPPRESS
            count++;

            for (int i = 0; i < count; i++) {
                result.append(' ');
            }

            for (int i = 0; i < lineIndex; i++) {
                if (line[i] > ' ' && line[i] < '~') {
                    result.append(new String(line, i, 1));
                } else {
                    result.append('.');
                }
            }
        }

        return result.toString();
    }
}
