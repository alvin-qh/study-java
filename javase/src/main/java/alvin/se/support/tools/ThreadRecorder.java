package alvin.se.support.tools;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class ThreadRecorder {
    private Map<Long, Thread> threadMap = new ConcurrentHashMap<>();

    public void recordCurrentThread() {
        if (!threadMap.containsKey(Thread.currentThread().getId())) {
            threadMap.put(Thread.currentThread().getId(), Thread.currentThread());
        }
    }

    public Set<Long> threadIds() {
        return threadMap.keySet();
    }
}
