package alvin.se.optional.model;

import java.util.Optional;

public class Car {
    private int id;
    private User driver;
    private String plateNumber;

    public Car(int id, User driver, String plateNumber) {
        this.id = id;
        this.driver = driver;
        this.plateNumber = plateNumber;
    }

    public int getId() {
        return id;
    }

    public Optional<User> getDriver() {
        return Optional.ofNullable(driver);
    }

    public String getPlateNumber() {
        return plateNumber;
    }
}
