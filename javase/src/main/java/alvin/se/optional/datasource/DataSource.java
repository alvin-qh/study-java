package alvin.se.optional.datasource;

import alvin.se.optional.model.Car;
import alvin.se.optional.model.User;

public interface DataSource {

    User queryUser(int id);

    Car queryCar(int id);
}
