package alvin.se.annotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * 测试"注解"的反射.
 * 注解也可以使用反射，获取注解中定义的方法(参考{@link alvin.se.annotation.intel.A} 和 {@link alvin.se.annotation.intel.B}注解)
 *
 * @see alvin.se.annotation.AnnotationValueTest
 */
public class AnnotationValue {

    /**
     * 获取注解中的指定值.
     *
     * @param annotationClass        注解类型
     * @param annotationMethod       注解的某个方法名
     * @param annotationMethodReturn 注解方法返回值类型
     * @param obj                    使用注解的对象
     * @return 返回指定注解方法的返回值
     */
    public <T> T getAnnotationValue(Class<? extends Annotation> annotationClass,
                                    String annotationMethod,
                                    Class<? extends T> annotationMethodReturn,
                                    Object obj) throws Exception {
        for (Method method : obj.getClass().getMethods()) {
            if (method.isAnnotationPresent(annotationClass)) {
                // 通过注解类型，获取指定的注解方法对象
                Object ret = annotationClass.getMethod(annotationMethod)
                        .invoke(method.getAnnotation(annotationClass));
                if (annotationMethodReturn.isInstance(ret)) {
                    return (T) ret;
                }
            }
        }
        return null;
    }
}
