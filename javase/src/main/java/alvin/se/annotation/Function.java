package alvin.se.annotation;

import alvin.se.annotation.intel.A;
import alvin.se.annotation.intel.B;

public class Function {

    @A("Annotation-A")
    public void functionA() {
    }

    @B({"Annotation-B1", "Annotation-B2"})
    public void functionB() {
    }
}
