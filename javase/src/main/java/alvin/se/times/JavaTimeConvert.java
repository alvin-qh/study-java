package alvin.se.times;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public final class JavaTimeConvert {

    private JavaTimeConvert() {
    }

    public static LocalDate toLocalDate(Date date, ZoneId zoneId) {
        return toLocalDateTime(date, zoneId).toLocalDate();
    }

    public static LocalDate toLocalDate(Date date) {
        return toLocalDateTime(date).toLocalDate();
    }

    public static LocalDateTime toLocalDateTime(Date date, ZoneId zoneId) {
        return LocalDateTime.ofInstant(date.toInstant(), zoneId);
    }

    public static LocalDateTime toLocalDateTime(Date date) {
        return toLocalDateTime(date, ZoneId.systemDefault());
    }

    public static Date toDate(LocalDateTime dateTime, ZoneId zoneId) {
        Instant instant = dateTime.atZone(zoneId).toInstant();
        return Date.from(instant);
    }

    public static Date toDate(LocalDateTime dateTime) {
        return toDate(dateTime, ZoneId.systemDefault());
    }

    public static Date toDate(LocalDate date, ZoneId zoneId) {
        Instant instant = date.atStartOfDay().atZone(zoneId).toInstant();
        return Date.from(instant);
    }

    public static Date toDate(LocalDate date) {
        return toDate(date, ZoneId.systemDefault());
    }
}
