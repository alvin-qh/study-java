package alvin.se.thread.recursive;

import alvin.se.support.tools.ThreadRecorder;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public final class SumRecursiveTask extends RecursiveTask<Long> {
    private static final int SEQUENTIAL_THRESHOLD = 5000;

    private static final ThreadRecorder THREAD_RECORDER = new ThreadRecorder();

    private Integer[] array;
    private int low;
    private int high;

    private SumRecursiveTask(Integer[] array, int low, int high) {
        this.array = array;
        this.low = low;
        this.high = high;
    }

    // 进行计算
    @Override
    protected Long compute() {
        THREAD_RECORDER.recordCurrentThread();

        if (high - low <= SEQUENTIAL_THRESHOLD) {
            long sum = 0;
            for (int i = low; i < high; ++i) {
                sum += array[i];
            }
            return sum;
        } else {
            int mid = low + (high - low) / 2;

            SumRecursiveTask left = new SumRecursiveTask(array, low, mid);
            long leftAns = left.compute();  // 在当前线程计算

            SumRecursiveTask right = new SumRecursiveTask(array, mid, high);
            right.fork();   // 在其它线程计算
            long rightAns = (Long) right.join();

            return leftAns + rightAns;
        }
    }

    public static long arraySum(Integer[] array) {
        return arraySum(array, -1);
    }

    public static long arraySum(Integer[] array, int maxThread) {
        ForkJoinPool forkJoinPool;

        if (maxThread <= 0) {
            forkJoinPool = new ForkJoinPool();
        } else {
            forkJoinPool = new ForkJoinPool(maxThread);
        }
        return forkJoinPool.invoke(new SumRecursiveTask(array, 0, array.length));
    }

    public static ThreadRecorder getThreadRecorder() {
        return THREAD_RECORDER;
    }
}
