package alvin.se.thread.locks;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class UseReentrantLock {
    private final Lock lock = new ReentrantLock();
    private final long tryLockTime;

    public UseReentrantLock(long tryLockTime) {
        this.tryLockTime = tryLockTime;
    }

    public void doLock() {
        try {
            this.lock.lockInterruptibly();
        } catch (InterruptedException ignore) {
        }
    }

    public boolean tryLock() {
        try {
            return this.lock.tryLock(this.tryLockTime, TimeUnit.MICROSECONDS);
        } catch (InterruptedException ignore) {
        }
        return false;
    }

    public void release() {
        this.lock.unlock();
    }
}
