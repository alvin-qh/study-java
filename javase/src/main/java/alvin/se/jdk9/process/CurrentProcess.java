package alvin.se.jdk9.process;

public final class CurrentProcess {
    private static final CurrentProcess CURRENT_PROCESS = new CurrentProcess();

    private final ProcessHandle processHandle;

    private CurrentProcess() {
        processHandle = ProcessHandle.current();
    }

    public long pid() {
        return processHandle.pid();
    }

    public ProcessHandle.Info processInfo() {
        return processHandle.info();
    }

    public boolean isAlive() {
        return processHandle.isAlive();
    }

    public void destroy() {
        if (isAlive()) {
            processHandle.destroy();
        }
    }

    public static CurrentProcess getInstance() {
        return CURRENT_PROCESS;
    }
}
