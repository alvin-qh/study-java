# Java module


## Declare module

Every `.jar` file can be declared as a module. just need put a `module-info.java` file at root of class path.

```java
module blog {
    exports com.pluralsight.blog;   // packages to export from this module
    exports ...
    
    requires cms;       // package required
}
```

![pic1](./pic.png)
