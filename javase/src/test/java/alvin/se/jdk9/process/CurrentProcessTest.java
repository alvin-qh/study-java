package alvin.se.jdk9.process;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;

class CurrentProcessTest {

    @Test
    void test_pid() {
        final CurrentProcess instance = CurrentProcess.getInstance();
        assertThat(instance.pid(), not(0L));
    }

    @Test
    void processInfo() {
    }

    @Test
    void isAlive() {
    }

    @Test
    void destroy() {
    }
}
