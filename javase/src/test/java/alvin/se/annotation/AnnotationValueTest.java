package alvin.se.annotation;

import alvin.se.annotation.intel.A;
import alvin.se.annotation.intel.B;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class AnnotationValueTest {

    private final AnnotationValue annotationValue = new AnnotationValue();

    @Test
    void test_getAnnotationValue() throws Exception {
        Function function = new Function();

        String value = annotationValue.getAnnotationValue(A.class, "value", String.class, function);
        assertThat(value, is("Annotation-A"));
    }

    @Test
    void test_getAnnotationValue_as_array() throws Exception {
        Function function = new Function();

        String[] values = annotationValue.getAnnotationValue(B.class, "value", String[].class, function);
        assertThat(values.length, is(2));
        assertThat(values[0], is("Annotation-B1"));
        assertThat(values[1], is("Annotation-B2"));
    }
}
