package alvin.se.thread.recursive;

import alvin.se.support.generator.Generators;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

class RecursiveTaskTest {

    @Test
    void test_sum_job() {
        Integer[] array = Generators.generateRandomIntArray(1, 20001);

        long result = SumRecursiveTask.arraySum(array);
        assertThat(result, is(200010000L));
        assertThat(SumRecursiveTask.getThreadRecorder().threadIds().size(), lessThanOrEqualTo(4));
    }

    @Test
    void test_sum_job_with_fix_thread() {
        Integer[] array = Generators.generateRandomIntArray(1, 20001);

        long result = SumRecursiveTask.arraySum(array, 1);
        assertThat(result, is(200010000L));
        assertThat(SumRecursiveTask.getThreadRecorder().threadIds().size(), is(1));
    }
}
