package alvin.se.thread.locks;

import org.junit.jupiter.api.Test;

import java.util.concurrent.Semaphore;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class UseReentrantLockTest {

    @Test
    void test_doLock_twice_in_same_thread() {
        UseReentrantLock lock = new UseReentrantLock(500);
        lock.doLock();
        assertThat(lock.tryLock(), is(true));
    }

    @Test
    void test_doLock_twice_in_difference_thread() throws Exception {
        final UseReentrantLock lock = new UseReentrantLock(500);
        final Semaphore sem = new Semaphore(1);

        sem.acquire();

        Thread thread = new Thread(() -> {
            lock.doLock();
            sem.release();
        });
        thread.start();

        sem.acquire();
        assertThat(lock.tryLock(), is(false));
    }
}
