package alvin.se.times;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class JavaTimeConvertTest {

    private static final Calendar CALENDAR = Calendar.getInstance();
    private static final ZoneId ZONE_ID = ZoneOffset.UTC;
    private static final LocalDate DATE = LocalDate.of(2017, 10, 1);
    private static final LocalDateTime DATE_TIME = LocalDateTime.of(2017, 10, 1, 2, 0, 0);

    @BeforeEach
    void setUp()  {
        CALENDAR.set(2017, Calendar.OCTOBER, 1, 2, 0, 0);
        CALENDAR.set(Calendar.MILLISECOND, 0);
    }

    @Test
    void test_toLocalDate()  {
        LocalDate date = JavaTimeConvert.toLocalDate(CALENDAR.getTime());
        assertThat(date.format(DateTimeFormatter.ISO_DATE), is("2017-10-01"));

        date = JavaTimeConvert.toLocalDate(CALENDAR.getTime(), ZONE_ID);
        assertThat(date.format(DateTimeFormatter.ISO_DATE), is("2017-09-30"));
    }

    @Test
    void test_toLocalDateTime()  {
        LocalDateTime dateTime = JavaTimeConvert.toLocalDateTime(CALENDAR.getTime());
        assertThat(dateTime.format(DateTimeFormatter.ISO_DATE_TIME), is("2017-10-01T02:00:00"));

        dateTime = JavaTimeConvert.toLocalDateTime(CALENDAR.getTime(), ZONE_ID);
        assertThat(dateTime.format(DateTimeFormatter.ISO_DATE_TIME), is("2017-09-30T18:00:00"));
    }

    @Test
    void test_toDate_with_out_time()  {
        Date date = JavaTimeConvert.toDate(DATE);
        assertThat(date.toString(), is("Sun Oct 01 00:00:00 CST 2017"));

        date = JavaTimeConvert.toDate(DATE, ZONE_ID);
        assertThat(date.toString(), is("Sun Oct 01 08:00:00 CST 2017"));
    }

    @Test
    void test_toDate_with_time()  {
        Date date = JavaTimeConvert.toDate(DATE_TIME);
        assertThat(date.toString(), is("Sun Oct 01 02:00:00 CST 2017"));

        date = JavaTimeConvert.toDate(DATE_TIME, ZONE_ID);
        assertThat(date.toString(), is("Sun Oct 01 10:00:00 CST 2017"));
    }
}
