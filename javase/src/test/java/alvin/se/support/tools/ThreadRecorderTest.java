package alvin.se.support.tools;

import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class ThreadRecorderTest {

    private ExecutorService executorService = Executors.newFixedThreadPool(3);

    @Test
    void test_record_thread() throws Exception {
        ThreadRecorder recorder = new ThreadRecorder();
        AtomicInteger count = new AtomicInteger(0);

        for (int i = 0; i < 100; i++) {
            executorService.submit(() -> {
                recorder.recordCurrentThread();
                if (count.incrementAndGet() == 100) {
                    synchronized (recorder) {
                        recorder.notify();
                    }
                }
            });
        }

        synchronized (recorder) {
            recorder.wait();
        }

        assertThat(recorder.threadIds().size(), is(3));
    }
}
