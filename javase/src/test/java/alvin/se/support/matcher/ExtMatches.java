package alvin.se.support.matcher;

import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;

import java.lang.reflect.Array;
import java.util.Comparator;

public final class ExtMatches {

    private ExtMatches() {
    }

    @SuppressWarnings("unchecked")
    public static <T> Matcher<T[]> ordered(Comparator<T> comparator) {
        return new BaseMatcher<T[]>() {
            @Override
            public boolean matches(Object obj) {
                for (int i = 1; i < Array.getLength(obj); i++) {
                    if (comparator.compare((T) Array.get(obj, i - 1), (T) Array.get(obj, i)) >= 0) {
                        return false;
                    }
                }
                return true;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("not in order");
            }
        };
    }
}
