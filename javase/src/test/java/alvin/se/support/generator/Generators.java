package alvin.se.support.generator;

import java.util.Random;

public final class Generators {

    private static final Random RANDOM = new Random();

    private Generators() {
    }

    public static Integer[] generateIntArray(int min, int max) {
        Integer[] array = new Integer[max - min];
        for (int i = min; i < max; i++) {
            array[i - min] = i;
        }
        return array;
    }

    public static Integer[] generateRandomIntArray(int min, int max) {
        Integer[] array = generateIntArray(min, max);

        for (int j = 0; j < array.length * 2; j++) {
            int a = RANDOM.nextInt(array.length);
            int b = RANDOM.nextInt(array.length);
            if (a != b) {
                swap(array, a, b);
            }
        }
        return array;
    }

    private static <T> void swap(T[] array, int pos1, int pos2) {
        T tmp = array[pos1];
        array[pos1] = array[pos2];
        array[pos2] = tmp;
    }
}
