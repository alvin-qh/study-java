package alvin.se.binary;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;

class HexTest {

    @Test
    void test_string_toBytes1() {
        final String hex = "0A";
        final byte[] bytes = Hex.toBytes(hex);

        assertThat(bytes[0], is((byte) 0x0A));
    }

    @Test
    void test_string_toBytes2() {
        final String hex = "ABCD";
        final byte[] bytes = Hex.toBytes(hex);

        assertThat(bytes[0], is((byte) 0xAB));
        assertThat(bytes[1], is((byte) 0xCD));
    }

    @Test
    void test_byte_toString() {
        assertThat(Hex.toString((byte) 0xAB), is("AB"));
    }

    @Test
    void test_bytes_toString() {
        final byte[] bytes = {(byte) 0xAB, (byte) 0xCD, (byte) 0xEF};
        assertThat(Hex.toString(bytes), is("ABCDEF"));
    }

    @Test
    void test_bytes_toStringWithWhiteSpace() {
        final byte[] bytes = {(byte) 0xAB, (byte) 0xCD, (byte) 0xEF};
        assertThat(Hex.toStringWithWhiteSpace(bytes), is("AB CD EF"));
    }

    @Test
    void test_dump() {
        final byte[] data = {0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 10, 0xA, 0xB, 0xC, 0xD, 0xE, 0xF};
        assertThat(Hex.dump(data), containsString("\n0x00000000 00 01 02 03 04 05 06 07 08 09 0A 0A 0B 0C 0D 0E ................\n0x00000010 0F"));
    }
}
