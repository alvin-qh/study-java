package alvin.se.binary;

import org.junit.jupiter.api.Test;

import static alvin.se.binary.Hex.toBytes;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class CrcTest {

    @Test
    void test_count() {
        final byte[] data = toBytes("0050410000FFFF00");
        final byte[] crc = toBytes("6EDF");

        assertThat(Bytes.toBytes(Crc.count(data)), is(crc));
        assertThat(Bytes.toBytes(Crc.crc16(data, 0, data.length)), is(crc));
    }

    @Test
    void test_verify() {
        final byte[] data = toBytes("0050410000FFFF00");
        final byte[] crc = toBytes("6EDF");

        assertThat(Crc.verify(data, crc), is(true));
    }
}
