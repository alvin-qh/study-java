package alvin.se.binary;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class BytesTest {

    @Test
    void test_fill() {
        byte[] dst = {0x1, 0x2, 0x3, 0x4, 0, 0, 0x7};
        byte[] src = {0x5, 0x6};

        byte[] expected = {0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7};

        src = Bytes.fill(dst, 4, src);
        assertThat(src, is(expected));
    }

    @Test
    void test_char_toBytes() {
        byte[] expected = {0xA};
        assertThat(Bytes.toBytes('A'), is(expected));
    }

    @Test
    void test_short_toBytes1() {
        byte[] expected = {(byte) 0xAB, (byte) 0xCD};
        assertThat(Bytes.toBytes((short) 0xABCD), is(expected));
    }

    @Test
    void test_short_toBytes2() {
        byte[] expected = {(byte) 0x0, (byte) 0xCD};
        assertThat(Bytes.toBytes((short) 0xCD), is(expected));
    }

    @Test
    void test_int_toBytes1() {
        byte[] expected = {(byte) 0x12, (byte) 0x34, (byte) 0x56, (byte) 0x78};
        assertThat(Bytes.toBytes(0x12345678), is(expected));
    }

    @Test
    void test_int_toBytes2() {
        byte[] expected = {0x0, (byte) 0x34, (byte) 0x56, (byte) 0x78};
        assertThat(Bytes.toBytes(0x00345678), is(expected));
    }

    @Test
    void test_int_toBytes3() {
        byte[] expected = {0x0, (byte) 0x0, (byte) 0x56, (byte) 0x78};
        assertThat(Bytes.toBytes(0x00005678), is(expected));
    }

    @Test
    void test_int_toBytes4() {
        byte[] expected = {0x0, (byte) 0x0, (byte) 0x0, (byte) 0x78};
        assertThat(Bytes.toBytes(0x00000078), is(expected));
    }

    @Test
    void test_to_short() {
        byte[] data = {0x12, 0x34};
        assertThat(Bytes.toShort(data), is((short) 0x1234));
    }

    @Test
    void test_to_int() {
        byte[] data = {0x12, 0x34, 0x56, 0x78};
        assertThat(Bytes.toInt(data), is(0x12345678));
    }

    @Test
    void test_to_long() {
        byte[] data = {0x12, 0x34, 0x56, 0x78, (byte) 0x90, (byte) 0xAB, (byte) 0xCD, (byte) 0xEF};
        assertThat(Bytes.toLong(data), is(0x1234567890ABCDEFL));
    }

    @Test
    void test_flip_byte() {
        assertThat(Bytes.flip((byte) 0xAB), is((byte) 0xBA));
    }

    @Test
    void test_flip_short() {
        assertThat(Bytes.flip((short) 0x1234), is((short) 0x3412));
    }

    @Test
    void test_flip_int() {
        assertThat(Bytes.flip(0x12345678), is(0x56781234));
    }

    @Test
    void test_flip_long() {
        assertThat(Bytes.flip(0x123456780000ABCDL), is(0x0000ABCD12345678L));
    }

    @Test
    void test_fill_short() {
        final byte[] data = {0x1, 0x2, 0x3, 0x4, 0, 0, 0x7};
        final byte[] expected = {0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7};

        byte[] src = Bytes.fill(data, 4, (short) 0x0506);
        assertThat(src, is(expected));
    }

    @Test
    void test_fill_int() {
        final byte[] data = {0x1, 0x2, 0x3, 0x4, 0, 0, 0, 0, 0x9};
        final byte[] expected = {0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9};

        byte[] src = Bytes.fill(data, 4, 0x05060708);
        assertThat(src, is(expected));
    }

    @Test
    void test_fill_long() {
        final byte[] data = {0x1, 0x2, 0x3, 0x4, 0, 0, 0, 0, 0, 0, 0, 0, (byte) 0xDD};
        final byte[] expected = {0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9,
                (byte) 0xAA, (byte) 0xBB, (byte) 0xCC, (byte) 0xDD};

        byte[] src = Bytes.fill(data, 4, 0x0506070809AABBCCL);
        assertThat(src, is(expected));
    }

    @Test
    void test_concat_bytes() {
        byte[] a = {0x1, 0x2, 0x3, 0x4};
        byte[] b = {0x5, 0x6, 0x7, 0x8};

        byte[] expect = {0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8};
        a = Bytes.concat(a, b);

        assertThat(a, is(expect));
    }

    @Test
    void test_concat_byte() {
        byte[] a = {0x1, 0x2, 0x3, 0x4};

        byte[] expect = {0x1, 0x2, 0x3, 0x4, 0x5};
        a = Bytes.concat(a, (byte) 0x5);

        assertThat(a, is(expect));
    }

    @Test
    void test_concat_short() {
        byte[] a = {0x1, 0x2, 0x3, 0x4};

        byte[] expect = {0x1, 0x2, 0x3, 0x4, 0x5, 0x6};
        a = Bytes.concat(a, (short) 0x506);

        assertThat(a, is(expect));
    }

    @Test
    void test_concat_int() {
        byte[] a = {0x1, 0x2, 0x3, 0x4};

        byte[] expect = {0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8};
        a = Bytes.concat(a, 0x5060708);

        assertThat(a, is(expect));
    }

    @Test
    void test_concat_long() {
        byte[] a = {0x1, 0x2, 0x3, 0x4};

        byte[] expect = {0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0xA, 0xB, 0xC, 0xD};
        a = Bytes.concat(a, 0x50607080A0B0C0DL);

        assertThat(a, is(expect));
    }

    @Test
    void test_bcc_check_sum() {
        final byte[][] data = {
                {0x07, 0x01, (byte) 0xC0, 0x00, 0x00, (byte) 0x90, (byte) 0xA7},
                {0x0F, 0x01, 0x19, 0x00, (byte) 0x90, 0x00, 0x48, (byte) 0x86, (byte) 0xA2, 0x23, 0x57, 0x26, 0x63, 0x61, 0x72},
                {(byte) 0xB5, 0x42, (byte) 0xE2, (byte) 0xC7, (byte) 0x5F}
        };

        for (byte[] datum : data) {
            final byte bcc = (byte) Bytes.bcc(datum, 0, datum.length - 1);
            assertThat(datum[datum.length - 1], is(bcc));
        }
    }
}
