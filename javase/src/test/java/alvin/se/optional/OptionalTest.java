package alvin.se.optional;

import alvin.se.optional.datasource.DataSource;
import alvin.se.optional.model.Car;
import alvin.se.optional.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class OptionalTest {

    private DataSource dataSource;

    @BeforeEach
    void setUp() {
        dataSource = mock(DataSource.class);
    }

    @Test
    void test_map() {
        when(dataSource.queryUser(1)).thenReturn(new User(1, "Alvin", 34));
        when(dataSource.queryUser(2)).thenReturn(null);

        Optional<User> userOptional = Optional.ofNullable(dataSource.queryUser(1));
        String name = userOptional.map(User::getName).orElse("");
        assertThat(name, is("Alvin"));

        userOptional = Optional.ofNullable(dataSource.queryUser(2));
        name = userOptional.map(User::getName).orElse("");
        assertThat(name, is(""));
    }

    @Test
    void test_flatMap() {
        when(dataSource.queryCar(1)).thenReturn(new Car(1, new User(1, "Alvin", 34), "陕AF181D"));
        when(dataSource.queryCar(2)).thenReturn(null);

        Optional<Car> carOptional = Optional.ofNullable(dataSource.queryCar(1));
        String name = carOptional
                .flatMap(Car::getDriver)
                .map(User::getName).orElse("");
        assertThat(name, is("Alvin"));

        carOptional = Optional.ofNullable(dataSource.queryCar(2));
        name = carOptional.flatMap(Car::getDriver).map(User::getName).orElse("");
        assertThat(name, is(""));
    }

    @Test
    void test_ifPresent() {
        when(dataSource.queryCar(1)).thenReturn(new Car(1, new User(1, "Alvin", 34), "陕AF181D"));
        when(dataSource.queryCar(2)).thenReturn(null);

        Optional<Car> carOptional = Optional.ofNullable(dataSource.queryCar(1));
        assertThat(carOptional.isPresent(), is(true));
        carOptional.ifPresent(car ->
                car.getDriver().ifPresent(user ->
                        assertThat(user.getName(), is("Alvin")))
        );

        carOptional = Optional.ofNullable(dataSource.queryCar(2));
        assertThat(carOptional.isPresent(), is(false));
        carOptional.ifPresent(car -> {
                    fail();  // 不会运行到这里
                    car.getDriver().ifPresent(d -> fail());  // 不会运行到这里
                }
        );
    }

    @Test
    void test_filter() {
        when(dataSource.queryCar(1)).thenReturn(new Car(1, new User(1, "Alvin", 34), "陕AF181D"));
        when(dataSource.queryCar(2)).thenReturn(null);

        Optional<Car> carOptional = Optional.ofNullable(dataSource.queryCar(1));
        int age = carOptional.flatMap(Car::getDriver)
                .filter(user -> "Alvin".equals(user.getName()))
                .map(User::getAge).orElse(0);
        assertThat(age, is(34));

        age = carOptional.flatMap(Car::getDriver)
                .filter(user -> "Emma".equals(user.getName()))
                .map(User::getAge).orElse(0);
        assertThat(age, is(0));

        carOptional = Optional.ofNullable(dataSource.queryCar(2));
        age = carOptional.flatMap(Car::getDriver)
                .filter(user -> {
                    fail();  // 不会运行到这里
                    return false;
                })
                .map(user -> {
                    fail();  // 不会运行到这里
                    return user.getAge();
                }).orElse(0);
        assertThat(age, is(0));
    }
}
