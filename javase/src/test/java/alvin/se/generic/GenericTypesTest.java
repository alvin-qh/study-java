package alvin.se.generic;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;

class GenericTypesTest {

    @Test
    void test_getType() {
        class TestClass implements GenericInterface<Boolean, List<String>> {
        }

        Class[] type = GenericTypes.getType(TestClass.class);

        assertNotNull(type);
        assertSame(type[0], Boolean.class);
        assertSame(type[1], List.class);
    }

    @Test
    void test_getType_anonymous_class() {
        Class[] type = GenericTypes.getType(new GenericInterface<Boolean, List<String>>() {
        });

        assertNotNull(type);
        assertSame(type[0], Boolean.class);
        assertSame(type[1], List.class);
    }
}
