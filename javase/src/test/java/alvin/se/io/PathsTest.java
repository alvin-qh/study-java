package alvin.se.io;

import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class PathsTest {

    @Test
    void test_path_join() throws Exception {
        Path path = Paths.get("/aaa", "bbb", "c.txt");
        assertThat(path.toString(), is("/aaa/bbb/c.txt"));
    }
}
