package alvin.se.lambda.stream;

import alvin.se.support.generator.Generators;
import alvin.se.support.matcher.ExtMatches;
import alvin.se.support.tools.ThreadRecorder;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;

class ParallelStreamTest {

    @Test
    void test_parallelStream() {
        ThreadRecorder recorder = new ThreadRecorder();
        List<Integer[]> numbers = makeRandomArrayList(100);

        numbers.parallelStream().forEach(array -> {
            Arrays.sort(array);
            recorder.recordCurrentThread();
        });

        assertThat(recorder.threadIds().size(), greaterThan(1));
        for (Integer[] number : numbers) {
            assertThat(number, ExtMatches.ordered(Comparator.comparingInt(o -> o)));
        }
    }

    private List<Integer[]> makeRandomArrayList(int size) {
        List<Integer[]> arrayList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            arrayList.add(Generators.generateRandomIntArray(1, 100));
        }
        return arrayList;
    }
}
