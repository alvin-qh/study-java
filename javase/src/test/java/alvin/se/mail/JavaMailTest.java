package alvin.se.mail;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

@Disabled
class JavaMailTest {

    @Test
    void test_create_simple_mail() throws Exception {
        JavaMail javaMail = new JavaMail("smtp.163.com",
                994, "alvin_test@163.com", "***");
        Mail mail = javaMail.createSimpleMail("alvin_test@163.com",
                "mousebaby8080@gmail.com", "Welcome Alvin",
                "<html><head><title>Test</title></head><body>Hello Alvin, This is a mail for testing</body></html>");
        mail.send();
    }
}
