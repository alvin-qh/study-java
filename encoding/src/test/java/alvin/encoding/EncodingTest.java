package alvin.encoding;

import org.apache.commons.codec.binary.Hex;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class EncodingTest {

    @Test
    void test_encode() {
        Encoding encoding = new Encoding("GBK");
        byte[] encode = encoding.encode("Hello, 大家好");
        assertThat(Hex.encodeHexString(encode), is("48656c6c6f2c20b4f3bcd2bac3"));
    }

    @Test
    void test_decode() throws Exception {
        Encoding encoding = new Encoding("GBK");
        String encode = encoding.decode(Hex.decodeHex("48656c6c6f2c20b4f3bcd2bac3".toCharArray()));
        assertThat(encode, is("Hello, 大家好"));
    }
}
