package alvin.encoding;

import org.apache.commons.codec.binary.Hex;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Paths;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class FileEncodingTest {

    @AfterEach
    void tearDown() throws Exception {
        Files.delete(Paths.get("test.txt"));
    }

    @Test
    void test_read_write() throws Exception {
        FileEncoding fileEncoding = new FileEncoding("UTF-8", "test.txt");
        fileEncoding.write("Hello, 大家好");

        fileEncoding = new FileEncoding("UTF-8", "test.txt");
        String content = fileEncoding.read();
        assertThat(content, is("Hello, 大家好"));
    }

    @Test
    void test_readRaw() throws Exception {
        FileEncoding fileEncoding = new FileEncoding("UTF-8", "test.txt");
        fileEncoding.write("Hello, 大家好");

        byte[] result = fileEncoding.readRaw();
        assertThat(Hex.encodeHexString(result), is("48656c6c6f2c20e5a4a7e5aeb6e5a5bd"));
        assertThat(new String(result, UTF_8), is("Hello, 大家好"));
    }
}
