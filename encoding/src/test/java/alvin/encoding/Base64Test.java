package alvin.encoding;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class Base64Test {

    @Test
    void test_encoding() {
        Base64 base64 = new Base64();
        String encode = base64.encode("Hello World".getBytes());
        assertThat(encode, is("SGVsbG8gV29ybGQ="));
    }

    @Test
    void test_decode() {
        Base64 base64 = new Base64();
        byte[] decode = base64.decode("SGVsbG8gV29ybGQ=");
        assertThat(new String(decode), is("Hello World"));
    }
}
