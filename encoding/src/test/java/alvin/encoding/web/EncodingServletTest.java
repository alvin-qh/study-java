package alvin.encoding.web;

import alvin.common.testing.servlet.ServletTester;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class EncodingServletTest {

    private static final EncodingServlet SERVLET = new EncodingServlet();

    @Test
    void test_EncodingServlet_get() throws Exception {
        ServletTester tester = new ServletTester(ServletTester.GET, "UTF-8");

        SERVLET.doGet(tester.getRequest(), tester.getResponse());

        Document doc = Jsoup.parse(tester.getResponseBody());

        assertThat(doc.select("input[name=name]").size(), is(1));
        assertThat(doc.select("button[type=submit]").size(), is(1));
    }

    @Test
    void test_EncodingServlet_post() throws Exception {
        ServletTester tester = new ServletTester(ServletTester.POST, "UTF-8");

        tester.addRequestParameter("name", "Alvin");
        SERVLET.doPost(tester.getRequest(), tester.getResponse());

        Document doc = Jsoup.parse(tester.getResponseBody());
        assertThat(doc.select("body div").text(), is("Hello, Alvin!"));
    }
}
