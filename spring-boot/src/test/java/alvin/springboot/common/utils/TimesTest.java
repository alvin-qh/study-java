package alvin.springboot.common.utils;

import alvin.springboot.test.UnitTestSupport;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.TimeZone;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class TimesTest extends UnitTestSupport {
    private final Times times = new Times();

    @Test
    void test_utcNow() {
        final LocalDateTime utcNow = times.utcNow();
        final Duration duration = Duration.between(LocalDateTime.now(), utcNow).abs();

        final long offset = TimeZone.getDefault().getRawOffset() / 3600000;
        assertThat(duration.toHours(), is(offset));
    }

    @Test
    void toUtcDate() {
        final Date date = times.utcToDate(times.utcNow());
        assertThat(date.getTime() / 10, is(new Date().getTime() / 10));
    }
}
