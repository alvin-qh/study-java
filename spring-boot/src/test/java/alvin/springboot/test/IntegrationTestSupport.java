package alvin.springboot.test;

import alvin.springboot.Application;
import alvin.springboot.app.domain.models.Role;
import alvin.springboot.app.domain.models.Tenant;
import alvin.springboot.app.domain.models.TenantBuilder;
import alvin.springboot.app.domain.models.User;
import alvin.springboot.app.domain.models.UserBuilder;
import alvin.springboot.test.builders.AbstractBuilder;
import alvin.springboot.test.builders.BuilderFactory;
import alvin.springboot.test.context.TenantHolder;
import alvin.springboot.test.mockers.RequestScopeMocker;
import alvin.springboot.test.persist.TableCleaner;
import alvin.springboot.test.persist.Transaction;
import alvin.springboot.test.persist.TransactionManager;
import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

import static alvin.springboot.test.IntegrationTestSupport.Config;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {Application.class, Config.class}, webEnvironment = RANDOM_PORT,
        properties = {
//                "spring.activiti.check-process-definitions=false",
                "spring.jpa.show-sql=true",
                "spring.jpa.open-in-view=false",
                "spring.jpa.hibernate.ddl-auto=none",
                "spring.jpa.properties.hibernate.enable_lazy_load_no_trans=true",
                "spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MariaDBDialect",
                "spring.flyway.table=schema_version",
                "spring.main.banner-mode=off"})
@ActiveProfiles("test")
@ContextConfiguration
public abstract class IntegrationTestSupport extends UnitTestSupport {
    private static boolean tableCleaned = false;
    private static Tenant manager = null;

    @PersistenceContext
    private EntityManager em;

    @Inject
    private TransactionManager tm;

    @Inject
    private TableCleaner tableCleaner;

    @Inject
    private BuilderFactory builderFactory;

    @Inject
    private RequestScopeMocker requestScopeMocker;

    @BeforeEach
    public void setUp() {
        synchronized (IntegrationTestSupport.class) {
            if (!tableCleaned) {
                try (Transaction ignore = beginTx()) {
                    tableCleaner.clearAllTables("study_java_springboot",
                            "schema_version");
                }
                tableCleaned = true;
            }
        }

        try (Transaction ignore = beginTx()) {
            synchronized (IntegrationTestSupport.class) {
                if (manager == null) {
                    manager = newBuilder(TenantBuilder.class)
                            .name("Top Manager")
                            .code("top-manager")
                            .create();
                } else {
                    manager = em.find(Tenant.class, manager.getId());
                }
            }
            TenantHolder.setTenant(newBuilder(TenantBuilder.class).manager(manager).create());
            TenantHolder.setUser(newBuilder(UserBuilder.class).role(Role.ADMIN).create());
        }
        requestScopeMocker.mockHeader();
    }

    @AfterEach
    public void tearDown() {
        TenantHolder.clear();
    }

    protected Transaction beginTx() {
        return tm.begin();
    }

    protected void clearJPASession() {
        em.clear();
    }

    protected <T extends AbstractBuilder> T newBuilder(Class<T> type) {
        return builderFactory.newBuilder(type);
    }

    protected Tenant currentTenant() {
        return TenantHolder.getTenant();
    }

    protected User currentUser() {
        return TenantHolder.getUser();
    }

    @SuppressWarnings("UnstableApiUsage")
    protected String loadResourceAsString(String resourceName) throws IOException {
        try (Reader reader = new InputStreamReader(getClass().getResourceAsStream(resourceName), Charsets.UTF_8)) {
            return CharStreams.toString(reader);
        }
    }

    @Configuration
    static class Config {
    }
}
