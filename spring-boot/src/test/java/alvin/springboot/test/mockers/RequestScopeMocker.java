package alvin.springboot.test.mockers;

import alvin.springboot.app.domain.Context;
import alvin.springboot.common.di.SingleScope;
import alvin.springboot.test.context.TenantHolder;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Component
@SingleScope
public class RequestScopeMocker {

    private final ServletRequestAttributes requestAttr = new ServletRequestAttributes(new MockHttpServletRequest());

    public void mockHeader() {
        final Context context = new Context();
        context.setTenant(TenantHolder.getTenant());
        context.setUser(TenantHolder.getUser());
        context.setTarget(Context.Target.API);
        context.setRequestPath("fake-request-path");

        requestAttr.setAttribute(Context.KEY, context, RequestAttributes.SCOPE_REQUEST);
        RequestContextHolder.setRequestAttributes(requestAttr);
    }
}
