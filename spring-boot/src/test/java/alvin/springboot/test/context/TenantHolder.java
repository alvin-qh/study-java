package alvin.springboot.test.context;

import alvin.springboot.app.domain.models.Tenant;
import alvin.springboot.app.domain.models.User;
import lombok.Data;

public final class TenantHolder {
    private static final ThreadLocal<TenantInfo> THREAD_LOCAL = new ThreadLocal<>();

    private TenantHolder() {
    }

    private static TenantInfo lazyLoad() {
        TenantInfo attr = THREAD_LOCAL.get();
        if (attr == null) {
            attr = new TenantInfo();
            THREAD_LOCAL.set(attr);
        }
        return attr;
    }

    public static Tenant getTenant() {
        return lazyLoad().getTenant();
    }

    public static void setTenant(Tenant tenant) {
        lazyLoad().setTenant(tenant);
    }

    public static User getUser() {
        return lazyLoad().getUser();
    }

    public static void setUser(User user) {
        lazyLoad().setUser(user);
    }

    public static void clear() {
        THREAD_LOCAL.remove();
    }

    @Data
    private static class TenantInfo {
        private Tenant tenant;
        private User user;
    }
}
