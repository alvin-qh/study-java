package alvin.springboot.test.persist;

import alvin.springboot.common.di.SingleScope;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.HashSet;
import java.util.List;

import static com.google.common.collect.Sets.newHashSet;

@Component
@SingleScope
public class TableCleaner {
    private final EntityManager em;

    @Inject
    public TableCleaner(EntityManager em) {
        this.em = em;
    }

    @SuppressWarnings({"unchecked", "SqlResolve"})
    private List<String> listAllTables(String schema) {
        return em.createNativeQuery("SELECT `table_name` " +
                "FROM `information_schema`.`tables` " +
                "WHERE `table_schema`=:schame AND `table_type`='base table'")
                .setParameter("schame", schema)
                .getResultList();
    }

    public synchronized void clearAllTables(String schema, String... exclude) {
        HashSet<String> excludeSet = newHashSet(exclude);
        em.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0").executeUpdate();
        listAllTables(schema).stream()
                .filter(t -> !excludeSet.contains(t))
                .forEach(t -> em.createNativeQuery("TRUNCATE TABLE " + t).executeUpdate());
        em.createNativeQuery("SET FOREIGN_KEY_CHECKS = 1").executeUpdate();
    }
}
