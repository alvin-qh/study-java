package alvin.springboot.test;

import alvin.springboot.app.domain.service.RegisterService;
import alvin.springboot.common.Constants;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.test.web.reactive.server.WebTestClient;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import java.time.Duration;

import static org.springframework.test.web.reactive.server.WebTestClient.RequestBodySpec;
import static org.springframework.test.web.reactive.server.WebTestClient.RequestHeadersSpec;
import static org.springframework.test.web.reactive.server.WebTestClient.RequestHeadersUriSpec;

@SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
@AutoConfigureWebTestClient
public abstract class WebTestSupport extends IntegrationTestSupport {

    @Inject
    private WebTestClient webClient;

    @Inject
    private ServletContext context;

    @Inject
    private RegisterService registerService;

    protected WebTestClient webClient() {
        return webClient.mutate().responseTimeout(Duration.ofMinutes(1)).build();
    }

    @SuppressWarnings("unchecked")
    private <T extends RequestHeadersSpec<?>, R extends RequestHeadersUriSpec<?>>
    T setup(R client, String url, Object... args) {
        final String token = Constants.AUTH_BEARER + " " + registerService.makeJwtToken(currentUser());
        return (T) client.uri(context.getContextPath() + url, args)
                .header(Constants.AUTH_HEADER_NAME, token);
    }

    protected RequestBodySpec post(String url, Object... args) {
        return ((RequestBodySpec) setup(webClient().post(), url, args))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .accept(MediaType.TEXT_HTML);
    }

    protected RequestBodySpec postJson(String url, Object... args) {
        return ((RequestBodySpec) setup(webClient().post(), url, args))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8);
    }

    protected RequestHeadersSpec<?> get(String url, Object... args) {
        return setup(webClient().get(), url, args)
                .accept(MediaType.TEXT_HTML);
    }

    protected WebTestClient.RequestHeadersSpec<?> getJson(String url, Object... args) {
        return setup(webClient().get(), url, args)
                .accept(MediaType.APPLICATION_JSON_UTF8);
    }

    protected RequestBodySpec put(String url, Object... args) {
        return ((RequestBodySpec) setup(webClient().put(), url, args))
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .accept(MediaType.TEXT_HTML);
    }

    protected RequestBodySpec putJson(String url, Object... args) {
        return ((RequestBodySpec) setup(webClient().put(), url, args))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .accept(MediaType.APPLICATION_JSON_UTF8);
    }

    protected WebTestClient.RequestHeadersSpec<?> delete(String url, Object... args) {
        return setup(webClient().delete(), url, args)
                .accept(MediaType.TEXT_HTML);
    }

    protected WebTestClient.RequestHeadersSpec<?> deleteJson(String url, Object... args) {
        return setup(webClient().delete(), url, args)
                .accept(MediaType.APPLICATION_JSON_UTF8);
    }
}
