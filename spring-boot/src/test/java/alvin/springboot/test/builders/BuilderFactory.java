package alvin.springboot.test.builders;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
public class BuilderFactory {
    private final ApplicationContext context;

    @Inject
    public BuilderFactory(ApplicationContext context) {
        this.context = context;
    }

    public <T extends AbstractBuilder> T newBuilder(Class<T> type) {
        return context.getBean(type);
    }
}
