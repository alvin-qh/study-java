package alvin.springboot.test.builders;

import alvin.springboot.common.persist.BaseEntity;
import alvin.springboot.common.persist.Tenanted;
import alvin.springboot.test.context.TenantHolder;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class AbstractBuilder<T extends BaseEntity> {
    @Inject
    private EntityManager em;

    private T entity;

    public AbstractBuilder() {
        Class<?> type = getClass();
        Type t = type.getGenericSuperclass();
        if (t instanceof ParameterizedType) {
            type = (Class<?>) ((ParameterizedType) t).getActualTypeArguments()[0];
            try {
                //noinspection unchecked
                entity = (T) type.getDeclaredConstructor().newInstance();
                if (entity instanceof Tenanted) {
                    ((Tenanted) entity).setTenantId(TenantHolder.getTenant().getId());
                }
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        } else {
            throw new RuntimeException("Cannot create entity object");
        }
    }

    protected void preBuild(T entity) {
    }

    protected void prePersist(T entity) {
    }

    protected T getEntity() {
        return entity;
    }

    @SuppressWarnings("unchecked")
    public T build() {
        final T entity = (T) this.entity.clone();
        preBuild(entity);
        return entity;
    }

    public T create() {
        T entity = build();
        prePersist(entity);
        em.persist(entity);
        return entity;
    }
}
