package alvin.springboot.app.login.controllers;

import alvin.springboot.app.domain.models.Tenant;
import alvin.springboot.app.domain.models.User;
import alvin.springboot.app.login.models.AuthForm;
import alvin.springboot.app.login.models.TokenDto;
import alvin.springboot.common.exceptions.ClientError;
import alvin.springboot.test.WebTestSupport;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static alvin.springboot.app.domain.models.UserBuilder.PASSWORD;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

class AuthApiControllerTest extends WebTestSupport {

    @Test
    void test_auth() {
        final Tenant tenant = currentTenant();
        final User user = currentUser();
        final AuthForm form = new AuthForm(tenant.getCode(), user.getAccount(), PASSWORD);

        final TokenDto dto = postJson("/api/auth").syncBody(form).exchange()
                .expectStatus().isOk()
                .expectBody(TokenDto.class).returnResult().getResponseBody();

        assertThat(dto, not(nullValue()));
        assertThat(dto.getToken(), not(nullValue()));
        assertThat(dto.getTimestamp(), not(nullValue()));
    }

    @Test
    void test_auth_invalid_form_fields() {
        final AuthForm form = new AuthForm("", null, "");

        final ClientError error = postJson("/api/auth").syncBody(form).exchange()
                .expectStatus().isBadRequest()
                .expectBody(ClientError.class).returnResult().getResponseBody();

        assertThat(error, not(nullValue()));
        assertThat(error.getStatus(), is(HttpStatus.BAD_REQUEST));
        assertThat(error.getErrorFields().size(), is(3));
    }

    @Test
    void test_auth_invalid_tenant() {
        final User user = currentUser();
        final AuthForm form = new AuthForm("bad-tenant", user.getAccount(), PASSWORD);

        postJson("/api/auth").syncBody(form).exchange()
                .expectStatus().isForbidden();
    }
}
