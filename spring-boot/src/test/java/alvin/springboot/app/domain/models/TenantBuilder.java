package alvin.springboot.app.domain.models;

import alvin.common.utils.IdGenerator;
import alvin.springboot.common.di.ProtoScope;
import alvin.springboot.test.builders.AbstractBuilder;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Component
@ProtoScope
public class TenantBuilder extends AbstractBuilder<Tenant> {

    @Inject
    private IdGenerator idGenerator;

    @Override
    protected void preBuild(Tenant entity) {
        if (Strings.isEmpty(entity.getCode())) {
            entity.setCode(idGenerator.randomUUID("O-"));
        }

        if (Strings.isEmpty(entity.getName())) {
            entity.setName("Test Tenant");
        }
    }

    public TenantBuilder code(String code) {
        getEntity().setCode(code);
        return this;
    }

    public TenantBuilder name(String name) {
        getEntity().setName(name);
        return this;
    }

    public TenantBuilder manager(Tenant manager) {
        getEntity().setManager(manager);
        return this;
    }

    public TenantBuilder addManaged(Tenant tenant) {
        getEntity().addManaged(tenant);
        return this;
    }
}
