package alvin.springboot.app.domain.service;

import alvin.springboot.app.domain.models.User;
import alvin.springboot.common.exceptions.HttpClientException;
import alvin.springboot.test.IntegrationTestSupport;
import alvin.springboot.test.context.TenantHolder;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import javax.inject.Inject;

import static alvin.springboot.app.domain.models.UserBuilder.PASSWORD;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RegisterServiceTest extends IntegrationTestSupport {

    @Inject
    private RegisterService service;

    @Test
    void test_login() {
        final User user = service.login(currentTenant().getCode(),
                currentUser().getAccount(), PASSWORD);
        assertThat(user.getAccount(), is(currentUser().getAccount()));
    }

    @Test
    void test_login_invalid_tenant() {
        final Executable exec = () ->
                service.login("bad-tenant",
                        currentUser().getAccount(), PASSWORD);
        assertThrows(HttpClientException.class, exec);
    }

    @Test
    void test_login_invalid_user() {
        final Executable exec = () ->
                service.login(currentTenant().getCode(),
                        "bad-user", PASSWORD);
        assertThrows(HttpClientException.class, exec);
    }

    @Test
    void test_login_invalid_password() {
        final Executable exec = () ->
                service.login(currentTenant().getCode(),
                        currentUser().getAccount(), "bad-passwd");
        assertThrows(HttpClientException.class, exec);
    }

    @Test
    void test_makeJwtToken() {
        final String token = service.makeJwtToken(TenantHolder.getUser());
        assertThat(token, Matchers.not(nullValue()));
    }
}
