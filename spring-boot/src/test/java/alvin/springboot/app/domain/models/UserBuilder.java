package alvin.springboot.app.domain.models;

import alvin.common.utils.IdGenerator;
import alvin.springboot.common.di.ProtoScope;
import alvin.springboot.common.secret.Hashs;
import alvin.springboot.test.builders.AbstractBuilder;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Component
@ProtoScope
public class UserBuilder extends AbstractBuilder<User> {
    public static final String PASSWORD = "123456";

    @Inject
    private IdGenerator idGenerator;

    @Inject
    private Hashs hashs;

    @Override
    protected void preBuild(User entity) {
        if (Strings.isEmpty(entity.getName())) {
            entity.setName("Test User");
        }
        if (Strings.isEmpty(entity.getAccount())) {
            entity.setAccount(idGenerator.randomUUID("U-"));
        }
        if (Strings.isEmpty(entity.getPassword())) {
            try {
                entity.setPassword(hashs.encode(PASSWORD));
            } catch (NoSuchAlgorithmException | InvalidKeyException e) {
                throw new RuntimeException(e);
            }
        }
        if (entity.getGender() == null) {
            entity.setGender(Gender.MALE);
        }
        if (entity.getRole() == null) {
            entity.setRole(Role.NORMAL);
        }
    }

    public UserBuilder name(String name) {
        getEntity().setName(name);
        return this;
    }

    public UserBuilder gender(Gender gender) {
        getEntity().setGender(gender);
        return this;
    }

    public UserBuilder account(String account) {
        getEntity().setAccount(account);
        return this;
    }

    public UserBuilder password(String password) {
        getEntity().setPassword(password);
        return this;
    }

    public UserBuilder email(String email) {
        getEntity().setEmail(email);
        return this;
    }

    public UserBuilder phoneNum(String phoneNum) {
        getEntity().setPhoneNum(phoneNum);
        return this;
    }

    public UserBuilder role(Role role) {
        getEntity().setRole(role);
        return this;
    }
}
