package alvin.springboot.app.misc.controllers;

import alvin.springboot.app.misc.models.ApplicationInfoDto;
import alvin.springboot.test.WebTestSupport;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

class SettingsControllerTest extends WebTestSupport {

    @Test
    void test_getApplicationInfo() {
        final ApplicationInfoDto dto = getJson("/d/info").exchange()
                .expectStatus().isOk()
                .expectBody(ApplicationInfoDto.class).returnResult().getResponseBody();

        assertThat(dto, not(nullValue()));
        assertThat(dto.getAuthor(), is("Alvin"));
    }
}
