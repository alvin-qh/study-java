package alvin.springboot.app.error.controllers;

import alvin.springboot.common.exceptions.ClientError;
import alvin.springboot.test.WebTestSupport;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;

class ErrorHandlerTest extends WebTestSupport {

    @Test
    void test_404_error_html() {
        final String html = get("/api/path-cause-404").exchange()
                .expectStatus().isNotFound()
                .expectBody(String.class).returnResult().getResponseBody();

        assertThat(html, containsString("<h2>NOT_FOUND</h2>"));
        assertThat(html, containsString("<p>The requested resource could not be found</p>"));
    }

    @Test
    void test_404_error_json() {
        final ClientError clientError = getJson("/api/path-cause-404").exchange()
                .expectStatus().isNotFound()
                .expectBody(ClientError.class).returnResult().getResponseBody();

        assertThat(clientError, not(nullValue()));
        assertThat(clientError.getStatus(), is(HttpStatus.NOT_FOUND));
    }
}
