create table `core_tenant`
(
    `id`         bigint       not null auto_increment,
    `code`       varchar(100) not null,
    `name`       varchar(100) not null,
    `managed_by` bigint,
    `deleted`    bigint       not null default 0,
    `created_at` timestamp    not null default current_timestamp,
    `created_by` bigint,
    primary key (`id`),
    unique key (`code`)
);
