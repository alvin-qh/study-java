create table `core_user` (
    `id` bigint not null auto_increment,
    `tenant_id` bigint not null,
    `name` varchar(100) not null,
    `gender` varchar(20) not null,
    `account` varchar(100) not null,
    `password` varchar(500) not null,
    `email` varchar(500),
    `phone_num` varchar(1000),
    `deleted` bigint not null default 0,
    `created_by` bigint,
    `created_at` timestamp default current_timestamp,
    `updated_by` bigint,
    `updated_at` timestamp default current_timestamp,
    primary key (`tenant_id`, `id`),
    unique key (`id`),
    unique key (`tenant_id`, `account`, `deleted`)
);
