package alvin.springboot.common.exceptions;

import lombok.Getter;

@Getter
public class HttpClientException extends RuntimeException {
    private final ClientError clientError;

    public HttpClientException(ClientError clientError) {
        super(String.format("status: %s, message: %s", clientError.getStatus(), clientError.getMessage()));
        this.clientError = clientError;
    }
}
