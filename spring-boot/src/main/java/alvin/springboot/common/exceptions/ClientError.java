package alvin.springboot.common.exceptions;

import alvin.springboot.app.domain.Context;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableMap;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static org.springframework.web.context.request.RequestAttributes.SCOPE_REQUEST;

@Getter
@EqualsAndHashCode
@ToString(doNotUseGetters = true)
@Slf4j
public class ClientError implements Cloneable {
    @JsonProperty("status")
    private HttpStatus status;

    @JsonProperty("message")
    private String message;

    @JsonProperty("error_parameters")
    private Map<String, String[]> errorParameters;

    @JsonProperty("error_fields")
    private Map<String, String[]> errorFields;

    @JsonProperty("timestamp")
    private LocalDateTime timestamp;

    @JsonProperty("path")
    private String path;

    ClientError() {
    }

    private ClientError(HttpStatus status) {
        RequestAttributes attrib = RequestContextHolder.getRequestAttributes();
        if (attrib != null) {
            Context context = (Context) attrib.getAttribute(Context.KEY, SCOPE_REQUEST);
            if (context != null) {
                this.path = context.getRequestPath();
            }
        }
        this.status = status;
        this.timestamp = LocalDateTime.now(ZoneOffset.UTC);
    }

    public int statusCode() {
        return status.value();
    }

    public Map<String, Object> toMap() {
        return ImmutableMap.of("status", status.name(), "message", message == null ? "" : message);
    }

    @Override
    public ClientError clone() {
        try {
            return (ClientError) super.clone();
        } catch (CloneNotSupportedException ignore) {
            return null;
        }
    }

    public static final class Builder {
        private ClientError clientError;

        private Builder(ClientError clientError) {
            this.clientError = clientError;
        }

        public Builder message(String message) {
            this.clientError.message = message;
            return this;
        }

        public Builder withRequest(WebRequest request) {
            this.clientError.path = request.getContextPath();
            return this;
        }

        public Builder addParameterError(String name, String[] errors) {
            if (this.clientError.errorParameters == null) {
                this.clientError.errorParameters = new LinkedHashMap<>();
            }
            this.clientError.errorParameters.put(name, errors);
            return this;
        }

        public Builder addErrorFields(String name, String[] errors) {
            if (this.clientError.errorFields == null) {
                this.clientError.errorFields = new LinkedHashMap<>();
            }
            this.clientError.errorFields.put(name, errors);
            return this;
        }

        public Builder errorFields(Collection<FieldError> errors) {
            this.clientError.errorFields = errors.stream()
                    .collect(Collectors.groupingBy(FieldError::getField))
                    .entrySet()
                    .stream()
                    .collect(Collectors.toMap(Map.Entry::getKey,
                            e -> e.getValue().stream()
                                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                                    .toArray(String[]::new)));
            return this;
        }

        public ClientError build() {
            return clientError.clone();
        }
    }

    public static Builder badRequest() {
        return status(HttpStatus.BAD_REQUEST);
    }

    public static Builder errorFields(Collection<FieldError> errors) {
        return badRequest().errorFields(errors);
    }

    public static Builder internalServerError() {
        return status(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    public static Builder forbidden() {
        return status(HttpStatus.FORBIDDEN);
    }

    public static Builder status(HttpStatus status) {
        final String message;
        switch (status) {
        case NOT_FOUND:
            message = "The requested resource could not be found";
            break;
        case FORBIDDEN:
            message = "The user might not have the necessary permissions for a resource, " +
                    "or may need an account of some sort";
            break;
        case METHOD_NOT_ALLOWED:
            message = "A request method is not supported for the requested resource";
            break;
        case INTERNAL_SERVER_ERROR:
            message = "An unexpected condition was encountered";
            break;
        case BAD_REQUEST:
            message = "The server cannot or will not process the request due to an apparent client error";
            break;
        default:
            message = "An unexpected condition was encountered";
        }
        return new Builder(new ClientError(status)).message(message);
    }

    public static ClientError fromException(Exception exception) {
        final ClientError clientError;

        if (exception instanceof HttpClientException) {
            HttpClientException exp = (HttpClientException) exception;
            clientError = exp.getClientError();
        } else {
            final ClientError.Builder builder;
            if (exception instanceof HttpClientErrorException) {
                HttpClientErrorException exp = (HttpClientErrorException) exception;
                builder = ClientError.status(exp.getStatusCode());
            } else {
                builder = ClientError.status(HttpStatus.INTERNAL_SERVER_ERROR);
            }
            if (Strings.isNotBlank(exception.getMessage())) {
                builder.message(exception.getMessage());
            }
            clientError = builder.build();
        }

        if (clientError.statusCode() >= HttpStatus.INTERNAL_SERVER_ERROR.value()) {
            log.error("INTERNAL_SERVER_ERROR caused when access url \"{}\", status is: {}, message is: \"{}\"",
                    clientError.getPath(), clientError.statusCode(), clientError.getMessage(), exception);
        } else {
            log.debug("{} caused when access url \"{}\", status is: {}, message is: \"{}\"",
                    clientError.getStatus().name(), clientError.getPath(), clientError.statusCode(),
                    clientError.getMessage(), exception);
        }

        return clientError;
    }
}
