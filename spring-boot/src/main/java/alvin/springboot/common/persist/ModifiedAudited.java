package alvin.springboot.common.persist;

import java.io.Serializable;
import java.time.LocalDateTime;

public interface ModifiedAudited extends Serializable {
    Long getUpdatedBy();

    LocalDateTime getUpdatedAt();
}
