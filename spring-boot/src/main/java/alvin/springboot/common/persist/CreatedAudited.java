package alvin.springboot.common.persist;

import java.io.Serializable;
import java.time.LocalDateTime;

public interface CreatedAudited extends Serializable {
    Long getCreatedBy();

    LocalDateTime getCreatedAt();
}
