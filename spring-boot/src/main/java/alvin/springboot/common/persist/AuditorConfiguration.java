package alvin.springboot.common.persist;

import alvin.springboot.app.domain.Context;
import alvin.springboot.app.domain.models.User;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.lang.NonNull;

import javax.inject.Inject;
import java.util.Optional;

@Configuration
public class AuditorConfiguration implements AuditorAware<Long> {

    private final Context context;

    @Inject
    public AuditorConfiguration(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public Optional<Long> getCurrentAuditor() {
        try {
            final User user = context.getUser();
            if (user == null) {
                return Optional.empty();
            }
            return Optional.of(user.getId());
        } catch (BeanCreationException ignore) {
            return Optional.empty();
        }
    }
}
