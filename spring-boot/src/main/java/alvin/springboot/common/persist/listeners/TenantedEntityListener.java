package alvin.springboot.common.persist.listeners;

import alvin.springboot.app.domain.Context;
import alvin.springboot.common.persist.Tenanted;
import org.springframework.beans.factory.annotation.Configurable;

import javax.inject.Inject;
import javax.persistence.PrePersist;

@Configurable
public class TenantedEntityListener {
    private final Context context;

    @Inject
    public TenantedEntityListener(Context context) {
        this.context = context;
    }

    @PrePersist
    public void touchForCreate(Object target) {
        if (target instanceof Tenanted) {
            Tenanted tenanted = (Tenanted) target;
            if (tenanted.getTenantId() == null && context.getTenant() != null) {
                tenanted.setTenantId(context.getTenant().getId());
            }
        }
    }
}
