package alvin.springboot.common.persist;

import java.io.Serializable;

public interface Tenanted extends Serializable {
    void setTenantId(Long tenantId);

    Long getTenantId();
}
