package alvin.springboot.common.persist;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@Data
@EqualsAndHashCode(callSuper = false)
@MappedSuperclass
public abstract class AuditedEntity extends BaseEntity implements CreatedAudited, ModifiedAudited, Tenanted {

    protected Long tenantId;

    @CreatedBy
    protected Long createdBy;

    @CreatedDate
    protected LocalDateTime createdAt;

    @LastModifiedBy
    protected Long updatedBy;

    @LastModifiedDate
    protected LocalDateTime updatedAt;
}
