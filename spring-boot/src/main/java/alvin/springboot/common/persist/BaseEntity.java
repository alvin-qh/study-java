package alvin.springboot.common.persist;

import alvin.springboot.common.persist.listeners.TenantedEntityListener;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@MappedSuperclass
@EntityListeners({
        AuditingEntityListener.class,
        TenantedEntityListener.class
})
public abstract class BaseEntity implements Serializable, Cloneable {

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException ignore) {
            return null;
        }
    }
}
