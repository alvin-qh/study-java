package alvin.springboot.common;

public interface Constants {
    String DISPATCH_URI_ERROR = "/error";

    String AUTH_HEADER_NAME = "Authorization";
    String AUTH_BEARER = "Bearer ";
}
