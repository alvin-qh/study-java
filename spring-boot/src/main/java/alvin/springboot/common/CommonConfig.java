package alvin.springboot.common;

import alvin.common.utils.IdGenerator;
import alvin.springboot.app.domain.Settings;
import alvin.springboot.common.di.SingleScope;
import alvin.springboot.common.di.VersionDateFormatter;
import alvin.springboot.common.secret.Hashs;
import alvin.springboot.common.utils.Times;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.format.DateTimeFormatter;

@Configuration
public class CommonConfig {

    @Bean
    @SingleScope
    public IdGenerator idGenerator() {
        return new IdGenerator();
    }

    @Bean
    @SingleScope
    public Times times() {
        return new Times();
    }

    @Bean
    @VersionDateFormatter
    @SingleScope
    public DateTimeFormatter versionDateFormatter() {
        return DateTimeFormatter.ofPattern("yyyyMMddHHmmssSSS");
    }

    @Bean
    @SingleScope
    public Hashs hashs(Settings settings) {
        return new Hashs(settings);
    }
}
