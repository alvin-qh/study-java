package alvin.springboot.common.utils;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

public class Times {

    public LocalDateTime utcNow() {
        return LocalDateTime.now(ZoneOffset.UTC);
    }

    public Date utcToDate(LocalDateTime dateTime) {
        return Date.from(dateTime.atZone(ZoneOffset.UTC).toInstant());
    }
}
