package alvin.springboot.common.utils;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static com.google.common.collect.Lists.newArrayList;

public final class Collections {
    private Collections() {
    }

    public static boolean isNullOrEmpty(Collection<?> collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isNotNullOrEmpty(Collection<?> collection) {
        return !isNullOrEmpty(collection);
    }

    public static <T> Optional<T> first(Collection<T> collection) {
        if (isNullOrEmpty(collection)) {
            return Optional.empty();
        }
        return Optional.of(collection.iterator().next());
    }

    public static <T> Optional<T> first(List<T> list) {
        if (isNullOrEmpty(list)) {
            return Optional.empty();
        }
        return Optional.of(list.get(0));
    }

    public static <T> List<T> toList(Iterator<T> iter) {
        return newArrayList(iter);
    }

    public static <T> List<T> toList(Iterable<T> iter) {
        return newArrayList(iter);
    }

    public static <T> Stream<T> toStream(Iterator<T> iter) {
        final Iterable<T> iterable = () -> iter;
        return StreamSupport.stream(iterable.spliterator(), false);
    }
}
