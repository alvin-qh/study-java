package alvin.springboot.common.secret;

import alvin.springboot.app.domain.Settings;
import org.apache.commons.codec.binary.Hex;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static java.nio.charset.StandardCharsets.UTF_8;

public class Hashs {
    private final SecretKeySpec secretKeySpec;
    private final Settings.Hash setting;

    public Hashs(Settings settings) {
        this.setting = settings.getHash();
        this.secretKeySpec = new SecretKeySpec(setting.getHmacKey().getBytes(UTF_8), setting.getName());
    }

    private Mac hmacSHA256() throws NoSuchAlgorithmException, InvalidKeyException {
        final Mac mac = Mac.getInstance(setting.getName());
        mac.init(secretKeySpec);
        return mac;
    }

    public String encode(String str) throws NoSuchAlgorithmException, InvalidKeyException {
        final Mac mac = hmacSHA256();
        final byte[] data = mac.doFinal(str.getBytes(UTF_8));
        return Hex.encodeHexString(data);
    }

    public boolean verify(String src, String secret) throws NoSuchAlgorithmException, InvalidKeyException {
        return secret.equalsIgnoreCase(encode(src));
    }
}
