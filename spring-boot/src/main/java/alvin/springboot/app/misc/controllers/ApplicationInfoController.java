package alvin.springboot.app.misc.controllers;

import alvin.springboot.app.domain.Settings;
import alvin.springboot.app.misc.mappers.ApplicationInfoMapper;
import alvin.springboot.app.misc.models.ApplicationInfoDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

@RestController
@RequestMapping("/d")
public class ApplicationInfoController {

    private final Settings settings;
    private final ApplicationInfoMapper applicationInfoMapper;

    @Inject
    public ApplicationInfoController(Settings settings,
                                     ApplicationInfoMapper applicationInfoMapper) {
        this.settings = settings;
        this.applicationInfoMapper = applicationInfoMapper;
    }

    @ResponseBody
    @GetMapping(value = "/info")
    ApplicationInfoDto applicationInfo() {
        return applicationInfoMapper.toDto(settings);
    }
}
