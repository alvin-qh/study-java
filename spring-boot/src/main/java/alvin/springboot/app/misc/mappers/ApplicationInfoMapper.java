package alvin.springboot.app.misc.mappers;

import alvin.springboot.app.domain.Settings;
import alvin.springboot.app.misc.models.ApplicationInfoDto;
import org.springframework.stereotype.Component;

@Component
public class ApplicationInfoMapper {

    public ApplicationInfoDto toDto(Settings settings) {
        final Settings.Info info = settings.getInfo();
        return new ApplicationInfoDto(info.getAuthor(), info.getVersion(), info.getDescription());
    }
}
