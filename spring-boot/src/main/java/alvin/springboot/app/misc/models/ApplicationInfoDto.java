package alvin.springboot.app.misc.models;

import lombok.Getter;

import java.io.Serializable;

@Getter
public class ApplicationInfoDto implements Serializable {
    private String author;
    private String version;
    private String description;

    ApplicationInfoDto() {
    }

    public ApplicationInfoDto(String author, String version, String description) {
        this.author = author;
        this.version = version;
        this.description = description;
    }
}
