package alvin.springboot.app.login.controllers;

import alvin.springboot.app.domain.models.User;
import alvin.springboot.app.domain.service.RegisterService;
import alvin.springboot.app.login.models.AuthForm;
import alvin.springboot.app.login.models.TokenDto;
import alvin.springboot.common.exceptions.ClientError;
import alvin.springboot.common.exceptions.HttpClientException;
import alvin.springboot.common.utils.Times;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.inject.Inject;
import javax.validation.Valid;

@Slf4j
@Controller
@RequestMapping("/api/auth")
public class AuthApiController {
    private final RegisterService registerService;
    private final Times times;

    @Inject
    public AuthApiController(RegisterService registerService,
                             Times times) {
        this.registerService = registerService;
        this.times = times;
    }

    @PostMapping
    @ResponseBody
    TokenDto auth(@RequestBody @Valid AuthForm form,
                  BindingResult br) {
        if (br.hasFieldErrors()) {
            throw new HttpClientException(
                    ClientError.badRequest().errorFields(br.getFieldErrors()).build());
        }

        final User user = registerService.login(
                form.getTenantCode(), form.getAccount(), form.getPassword());
        return new TokenDto(registerService.makeJwtToken(user), times.utcNow());
    }
}
