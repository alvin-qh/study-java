package alvin.springboot.app.login.models;

import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@EqualsAndHashCode
public class TokenDto {
    private String token;
    private LocalDateTime timestamp;

    TokenDto() {
    }

    public TokenDto(String token, LocalDateTime timestamp) {
        this.token = token;
        this.timestamp = timestamp;
    }
}
