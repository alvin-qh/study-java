package alvin.springboot.app.login.models;

import lombok.Getter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
public class AuthForm {
    @NotBlank
    private String tenantCode;

    @NotBlank
    private String account;

    @NotBlank
    @Size(min = 6, max = 20)
    private String password;

    AuthForm() {
    }

    public AuthForm(String tenantCode, String account, String password) {
        this.tenantCode = tenantCode;
        this.account = account;
        this.password = password;
    }
}
