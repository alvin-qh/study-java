package alvin.springboot.app.filters;

import alvin.springboot.app.domain.Context;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.regex.Pattern;

@Slf4j
@Component
public class HttpInterceptor implements HandlerInterceptor {
    private final Context context;

    private final Pattern apiPattern;

    @Inject
    public HttpInterceptor(Context context) {
        this.context = context;
        this.apiPattern = Pattern.compile("^(/[\\w-]+)?/api/.*");
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        log.debug("visiting {}", request.getRequestURI());

        context.setRequestPath(request.getRequestURI());

        if (apiPattern.matcher(request.getRequestURI()).matches()) {
            context.setTarget(Context.Target.API);
        } else {
            context.setTarget(Context.Target.WEB);
        }
        return true;
    }
}
