package alvin.springboot.app.filters;

import alvin.springboot.app.domain.Context;
import alvin.springboot.app.domain.service.RegisterService;
import alvin.springboot.common.Constants;
import alvin.springboot.common.exceptions.ClientError;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class AuthInterceptor implements HandlerInterceptor {
    private final Context context;
    private final ObjectMapper objectMapper;
    private final RegisterService registerService;

    @Inject
    public AuthInterceptor(Context context,
                           ObjectMapper objectMapper,
                           RegisterService registerService) {
        this.context = context;
        this.objectMapper = objectMapper;
        this.registerService = registerService;
    }

    private void sendForbiddenError(HttpServletRequest request, HttpServletResponse response) throws Exception {
        final ClientError error = ClientError.forbidden().build();
        response.setStatus(error.statusCode());

        if (context.getTarget() == Context.Target.API) {
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.getWriter().write(objectMapper.writeValueAsString(error));
        } else {
            error.toMap().forEach(request::setAttribute);
            request.getRequestDispatcher(Constants.DISPATCH_URI_ERROR)
                    .forward(request, response);
        }
    }

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response,
                             Object handler) throws Exception {
        log.debug("visiting {}", request.getRequestURI());

        if (registerService.tryAuthByRequestHeader(request, context)) {
            return true;
        }

        sendForbiddenError(request, response);
        return false;
    }
}
