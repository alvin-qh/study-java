package alvin.springboot.app;

import alvin.springboot.app.filters.AuthInterceptor;
import alvin.springboot.app.filters.HttpInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.inject.Inject;

@Slf4j
@Component
@Configuration
public class Configure implements WebMvcConfigurer, ApplicationListener<ApplicationReadyEvent> {

    private final HttpInterceptor httpInterceptor;
    private final AuthInterceptor authInterceptor;

    @Inject
    public Configure(HttpInterceptor httpInterceptor,
                     AuthInterceptor authInterceptor) {
        this.httpInterceptor = httpInterceptor;
        this.authInterceptor = authInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(httpInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns("/static/**");

        registry.addInterceptor(authInterceptor)
                .addPathPatterns("/**")
                .excludePathPatterns(
                        "/static/**",
                        "/error/**",
                        "/login/**",
                        "/api/auth/**");
    }

    @Override
    public void onApplicationEvent(@NonNull ApplicationReadyEvent event) {
        log.info("Application was startup already");
    }
}
