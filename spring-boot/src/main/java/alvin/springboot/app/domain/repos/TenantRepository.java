package alvin.springboot.app.domain.repos;

import alvin.springboot.app.domain.models.Tenant;
import alvin.springboot.common.di.SingleScope;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@SingleScope
public interface TenantRepository extends CrudRepository<Tenant, Long> {
    Optional<Tenant> findByCode(String code);
}
