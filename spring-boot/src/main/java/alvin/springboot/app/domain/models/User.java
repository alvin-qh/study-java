package alvin.springboot.app.domain.models;

import alvin.springboot.common.persist.AuditedEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "core_user")
@Where(clause = "deleted = 0")
@SQLDelete(sql = "UPDATE core_user SET deleted = id WHERE id = ?")
public class User extends AuditedEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(AccessType.FIELD)
    @ToString.Include
    private Long id;

    private String name;

    @Enumerated(EnumType.STRING)
    private Gender gender = Gender.MALE;

    private String account;

    private String password;

    private String email;

    private String phoneNum;

    @Enumerated(EnumType.STRING)
    private Role role = Role.NORMAL;
}
