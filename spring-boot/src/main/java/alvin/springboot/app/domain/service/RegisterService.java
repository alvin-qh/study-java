package alvin.springboot.app.domain.service;

import alvin.springboot.app.domain.Context;
import alvin.springboot.app.domain.Settings;
import alvin.springboot.app.domain.models.Tenant;
import alvin.springboot.app.domain.models.User;
import alvin.springboot.app.domain.repos.TenantRepository;
import alvin.springboot.app.domain.repos.UserRepository;
import alvin.springboot.common.Constants;
import alvin.springboot.common.di.SingleScope;
import alvin.springboot.common.exceptions.ClientError;
import alvin.springboot.common.exceptions.HttpClientException;
import alvin.springboot.common.secret.Hashs;
import alvin.springboot.common.utils.Times;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.google.common.primitives.Longs;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.Optional;

@SuppressWarnings("UnstableApiUsage")
@Service
@SingleScope
@Slf4j
public class RegisterService {


    private final TenantRepository tenantRepository;
    private final UserRepository userRepository;
    private final Settings settings;
    private final Times times;
    private final Hashs hashs;

    @Inject
    public RegisterService(TenantRepository tenantRepository,
                           UserRepository userRepository,
                           Settings settings,
                           Times times,
                           Hashs hashs) {
        this.tenantRepository = tenantRepository;
        this.userRepository = userRepository;
        this.settings = settings;
        this.times = times;
        this.hashs = hashs;
    }

    private JWTVerifier buildJwtVerifier() {
        final Settings.Jwt jwt = settings.getJwt();
        return JWT.require(jwt.getAlgorithm())
                .withIssuer(jwt.getIssuer())
                .withSubject(jwt.getSubject())
                .build();
    }

    public String makeJwtToken(User user) {
        final Settings.Jwt jwt = settings.getJwt();
        return JWT.create()
                .withIssuer(jwt.getIssuer())
                .withSubject(jwt.getSubject())
                .withJWTId(String.valueOf(user.getId()))
                .withExpiresAt(times.utcToDate(LocalDateTime.now().plusHours(1)))
                .sign(jwt.getAlgorithm());
    }

    public User login(String tenantCode, String account, String password) {
        final Optional<Tenant> mayTenant = tenantRepository.findByCode(tenantCode);
        if (!mayTenant.isPresent()) {
            log.warn("Cannot find tenant by code: \"{}\"", tenantCode);
            throw new HttpClientException(ClientError.forbidden().build());
        }
        final Tenant tenant = mayTenant.get();

        final Optional<User> mayUser = userRepository.findByTenantIdAndAccount(tenant.getId(), account);
        if (!mayUser.isPresent()) {
            throw new HttpClientException(ClientError.forbidden().build());
        }
        final User user = mayUser.get();

        try {
            if (!hashs.verify(password, user.getPassword())) {
                throw new HttpClientException(ClientError.forbidden().build());
            }
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            log.error("Cannot encoding password", e);
            throw new HttpClientException(ClientError.internalServerError().build());
        }

        return user;
    }

    public boolean tryAuthByRequestHeader(HttpServletRequest request, Context context) {
        String auth = request.getHeader(Constants.AUTH_HEADER_NAME);

        if (Strings.isBlank(auth)) {
            return false;
        }
        if (!auth.startsWith(Constants.AUTH_BEARER)) {
            return false;
        }
        auth = auth.substring(Constants.AUTH_BEARER.length());

        final DecodedJWT jwt;
        try {
            jwt = buildJwtVerifier().verify(auth.trim());
        } catch (JWTVerificationException e) {
            log.warn("Cannot verify jwt token", e);
            return false;
        }

        final Long id = Longs.tryParse(jwt.getId());
        if (id == null) {
            return false;
        }
        final Optional<User> mayUser = userRepository.findById(id);
        if (!mayUser.isPresent()) {
            log.warn("Cannot find user by id {}", id);
            return false;
        }
        final User user = mayUser.get();
        context.setUser(user);

        final Optional<Tenant> mayTenant = tenantRepository.findById(user.getTenantId());
        if (!mayTenant.isPresent()) {
            log.warn("Cannot find tenant by id {}", user.getTenantId());
            return false;
        }
        context.setTenant(mayTenant.get());
        return true;
    }
}
