package alvin.springboot.app.domain;

import com.auth0.jwt.algorithms.Algorithm;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@EqualsAndHashCode
@ToString(doNotUseGetters = true)
@Component
@ConfigurationProperties("application")
public class Settings {
    private final Info info = new Info();
    private final Jwt jwt = new Jwt();
    private final Hash hash = new Hash();

    @Data
    public static class Info {
        private String author;
        private String version;
        private String description;
    }

    @Data
    public static class Jwt {
        private String issuer;
        private String hmacKey;
        private String subject;

        private Algorithm algorithm;

        public Algorithm getAlgorithm() {
            if (algorithm == null) {
                algorithm = Algorithm.HMAC384(hmacKey);
            }
            return algorithm;
        }
    }

    @Data
    public static class Hash {
        private String name;
        private String hmacKey;
    }
}
