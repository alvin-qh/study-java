package alvin.springboot.app.domain.models;

import alvin.springboot.common.persist.BaseEntity;
import alvin.springboot.common.persist.CreatedAudited;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString(onlyExplicitlyIncluded = true)
@Entity
@Table(name = "core_tenant")
@Where(clause = "deleted = 0")
@SQLDelete(sql = "UPDATE core_tenant SET deleted = id WHERE id = ?")
public class Tenant extends BaseEntity implements CreatedAudited {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Access(AccessType.FIELD)
    @ToString.Include
    private Long id;

    private String code;

    private String name;

    @ManyToOne
    @JoinColumn(name = "managed_by")
    private Tenant manager;

    @OneToMany(mappedBy = "manager", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Tenant> managed = new ArrayList<>();

    @CreatedBy
    private Long createdBy;

    @CreatedDate
    private LocalDateTime createdAt;

    public void addManaged(Tenant subTenant) {
        subTenant.setManager(this);
        managed.add(subTenant);
    }
}
