package alvin.springboot.app.domain;

import alvin.springboot.app.domain.models.Tenant;
import alvin.springboot.app.domain.models.User;
import lombok.Data;
import lombok.ToString;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

@Data
@ToString(doNotUseGetters = true)
@Component
@RequestScope
public class Context {
    public static final String KEY = "scopedTarget.context";

    private Tenant tenant;
    private User user;
    private String requestPath;
    private Target target;

    public enum Target {
        WEB, API
    }
}
