package alvin.springboot.app.domain.models;

public enum Role {
    ADMIN, NORMAL
}
