package alvin.springboot.app.domain.models;

public enum Gender {
    MALE, FEMALE
}
