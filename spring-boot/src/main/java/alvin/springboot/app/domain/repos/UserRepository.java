package alvin.springboot.app.domain.repos;

import alvin.springboot.app.domain.models.User;
import alvin.springboot.common.di.SingleScope;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@SingleScope
public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByTenantIdAndAccount(Long tenantId, String account);
}
