package alvin.springboot.app.error.controllers;

import alvin.springboot.app.domain.Context;
import alvin.springboot.common.exceptions.ClientError;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import static javax.servlet.RequestDispatcher.ERROR_STATUS_CODE;

@Controller
@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler implements ErrorController {

    private final ObjectMapper objectMapper;
    private final Context context;


    @Inject
    public ErrorHandler(ObjectMapper objectMapper, Context context) {
        this.objectMapper = objectMapper;
        this.context = context;
    }

    private ModelAndView jsonResult(ClientError error) {
        final ModelAndView result = new ModelAndView(new MappingJackson2JsonView(objectMapper));
        result.addObject(error);
        return result;
    }

    @ExceptionHandler(Exception.class)
    public final Object handleAllExceptions(Exception exception) {
        final ClientError clientError = ClientError.fromException(exception);
        if (context.getTarget() == Context.Target.API) {
            return new ResponseEntity<>(clientError, clientError.getStatus());
        }
        return new ModelAndView("common/error_page", clientError.toMap(), clientError.getStatus());
    }

    private ClientError makeError(HttpServletRequest request) {
        Integer errorCode = (Integer) request.getAttribute(ERROR_STATUS_CODE);
        if (errorCode == null) {
            errorCode = HttpStatus.INTERNAL_SERVER_ERROR.value();
        }
        final HttpStatus status = HttpStatus.resolve(errorCode);
        return ClientError.status(status == null ? HttpStatus.INTERNAL_SERVER_ERROR : status).build();
    }

    @GetMapping(value = "/error", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ClientError errorAsJson(HttpServletRequest request) {
        return makeError(request);
    }

    @GetMapping(value = "/error", produces = MediaType.TEXT_HTML_VALUE)
    public ModelAndView errorAsHtml(HttpServletRequest request) {
        final ClientError error = makeError(request);
        return new ModelAndView(getErrorPath(), error.toMap(), error.getStatus());
    }

    @Override
    public String getErrorPath() {
        return "/common/error_page";
    }
}
