# Spring Boot

## Build standalone fat jar

### Edit build.gradle, add following statement

```groovy
bootJar {
    baseName = project.name + '-all'

    manifest {
        attributes 'Implementation-Title': 'Spring boot demo'
        attributes 'Implementation-Version': version
    }

    exclude 'META-INF/*.DSA'
    exclude 'META-INF/*.SF'
    exclude 'META-INF/*.RSA'
}
```

### Run gradle task to build Jar file

```bash
$ ./gradlew :project-name:bootJar
```

then a jar file can found in `**/build/libs` folder

### Run jar file

```bash
$ java -jar your-build-jar-file.jar --server.port=9999 --spring.config.location=classpath:/application.yml,./config-ext/application.yml
```

Use --name=value can set spring settings, for examples:

`--server.port` means `server.port` in `application.properties`

`--spring.config.location` can set config files location

