# JPA Demo

## Setup

### Environmental Requirement

MariaDB version > 10.1 or MySQL version > 5.6

Create database named `study_java_jpa` first

```mysql
CREATE DATABASE `study_java_jpa` DEFAULT CHARACTER SET utf8;
```

Database system would be started with the following config:

```conf
[mysqld]
# bind_address=0.0.0.0
default_time_zone=+00:00
transaction-isolation=READ-COMMITTED
thread_handling=pool-of-threads
sql_mode=STRICT_TRANS_TABLES,ONLY_FULL_GROUP_BY,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION
character_set_server=utf8
# general_log=1
# general_log_file=/usr/local/var/log/mysqld.log
# log-bin=master
# binlog_format=row
# slow_query_log=false
# slow_query_log_file=/usr/local/var/log/mysql-slow.log
# long_query_time=0
```
