# Hibernate过滤器#

## 查询过滤器##

### 使用查询过滤器的场合###

“查询过滤器”可以在Hibernate最终产生的SQL语句中，自动生成某个(或某几个)查询条件。例如：

- 根据当前日期查询“当天”的备忘录；
- 根据不同登录用户查询“所在学校”的信息；

**注意：标准JPA规范并没有定义Filter，这是Hibernate的功能**


### 定义过滤器###

可以通过`@FilterDef`注解来定义过滤器，例如：

**范例：定义过滤器**

    @FilterDef(name = "filter name",
            parameters = {@ParamDef(name = "param name", type = "param type")},
            defaultCondition = "column_name=:param_value")

- name参数：表示过滤器的名称；
- parameters参数：表示过滤器可接受的参数，是一个`@ParamDef`注解的数组；
    - name：参数名称；
    - type：参数类型
- defaultCondition：表示为了完成过滤，所需产生的SQL语句；

**注意：`@FilterDef`注解可以定义在某个类上，也可以定义在package-info.java上**

    
### 使用过滤器###

**范例：School实体定义**

    @Entity
    @Table(name = "school")
    public class School {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Integer id;
        private String name;
        ....
    }

在“定义过滤器”的基础上，可以使用该过滤器，例如：

**范例：使用过滤器**
    
    @MappedSuperclass
    @FilterDef(name = "school",
            parameters = {@ParamDef(name = "schoolId", type = "int")},
            defaultCondition = "school_id=:schoolId")
    @Filter(name = "school")
    public class BaseModel {
        @ManyToOne
        @JoinColumn(name = "school_id")
        private School school;
    
        public School getSchool() {
            return school;
        }
    
        public void setSchool(School school) {
            this.school = school;
        }
    }

例子中定义了“BaseModel”类，具备一个名为“school”的过滤器，具备参数“schoolId”，过滤条件是“school_id=:schoolId”，所以任何继承“BaseModel”类的类，都将获得这个过滤器定义，例如：

**范例：继承BaseModel**

    @Entity
    @Table(name = "student")
    public class Student extends BaseModel {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Integer id;
        private String studentNo;
        private String name;
        private String gender;
        private String telephone;
        ....
    }
    
接下来，需要为启用过滤器，并为其设置参数值，例如：

**范例：启用过滤器并设置参数**

    EntityManager em = ....;
    Filter filter = em.unwrap(Session.class).enableFilter("school");
    filter.setParameter("schoolId", 1);

此时，所有对“Student”对象的查询操作，都会自动加入`school_id=1`这个条件。 **注意，这里使用了JPA的“unwrap”功能，获得了Hibernate的session对象**


### 为中间表启用过滤器###

不太常用，参考“src/main/java/jpa/filter/models/Klass.java”中关于中间表和中间表过滤器的定义。
