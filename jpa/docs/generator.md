# 主键生成

## JAP主键生成器##

### AUTO主键生成器###
    
根据“数据表定义的规则”产生主键值，例如对于Oracle数据库，会使用序列产生主键值，而SQLServer则会使用自增字段产生主键值；
    
**范例：AUTO主键生成器**

    @Entity
    @Table(name = "generator_identity")
    public class Serializer implements Serializable {
    
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Integer id;
        private String business;
        private Integer value;
        ....
    }

### IDENTITY主键生成器###
    
根据数据表的“自增字段”规则来产生主键值，例如利用SQLServer的`IDENTITY(1,1)`类型字段或者MySQL的`auto_increment`类型字段；

**范例：IDENTITY主键生成器**
    
    @Entity
    @Table(name = "generator_identity")
    public class Serializer implements Serializable {
    
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Integer id;
        private String business;
        private Integer value;
        ....
    }

### SEQUENCE主键生成器###

利用数据表的“序列对象”产生主键，例如Oracle数据库的序列对象；

**范例：SEQUENCE主键生成器**

    @Entity
    @Table(name = "generator_product")
    @SequenceGenerator(name="productSeq", sequenceName="product_seq")  
    public class Product implements Serializable {
        @Id
        @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "productSeq")
        private Integer id;
        private String name;
        private String serializeCode;
        private BigDecimal price;
        ....
    }

### TABLE主键生成器###

建立一张特定的数据表来保存主键值，表中存储最后一次产生的主键值，并在下次产生主键时自行更新；类似于SEQUENCE方式，但数据库本身无需支持；

**范例：TABLE主键生成器 建表**

    create table generator_identity(
    	id int unsigned auto_increment,
    	business varchar(50) not null,
    	value int unsigned null null,
    	primary key(id)
    );
    
    create unique index idx_generator_identity_business on generator_identity(business);
    insert into generator_identity(business,value)values('PRODUCT',1);

**范例：TABLE主键生成器 模型**

    @Entity
    @Table(name = "generator_product")
    @TableGenerator(name = "productSerialize",
            table = "generator_identity",
            pkColumnName = "business",
            valueColumnName = "value",
            pkColumnValue = "PRODUCT",
            allocationSize = 1)
    public class Product implements Serializable {
        @Id
        @GeneratedValue(strategy = GenerationType.TABLE, generator = "productSerialize")
        private Integer id;
        private String name;
        private String serializeCode;
        private BigDecimal price;
        ....
    }
    
### 利用数据表模拟自增序列 ###

如果不仅仅是为了产生主键，而是需要一个自增序列，但数据库本身病不支持自增序列（例如MySQL），则可以模仿“TABLE主键生成器”的方式，利用数据表模拟自增序列。

**范例：模拟TABLE主键生成器，自增序列 模型**

    @Entity
    @Table(name = "generator_identity")
    public class Identity implements Serializable {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Integer id;
        private String business;
        private Integer value;
        ...
    }        

**范例：模拟TABLE主键生成器，自增序列 数据访问层**
    
    @Singleton
    public class IdentityRepository {
    
        @Inject
        private EntityManager em;
        
        public Integer nextValue(String business, int step) {
            Integer value = (Integer) em.createQuery("select value from Identity " +
                    "where business=:business")
                    .setHint("javax.persistence.lock.timeout", 5000)    // 设置超时时间
                    .setParameter("business", business)
                    .setLockMode(LockModeType.PESSIMISTIC_WRITE)
                    .getSingleResult();
            em.createQuery("update Identity set value=:newValue " +
                    "where value=:oldValue and business=:business")
                    .setParameter("newValue", value + step)
                    .setParameter("oldValue", value)
                    .setParameter("business", business)
                    .executeUpdate();
            return value;
        }
    }

代码中用到了“排它锁”（PESSIMISTIC_WRITE），以避免多个事务之间产生脏读；

**范例：模拟TABLE主键生成器，自增序列 服务层**

    @Singleton
    public class SerializerService {
        @Inject
        private IdentityRepository identityRepository;
    
        @Transactional
        public Integer nextValue(String business, int step) {
            return identityRepository.nextValue(business, step);
        }
    }
    
由于使用了排它锁，所以对数据表的读取和更新必须在一个事务内完成，以防止对其它事务长时间阻塞；
