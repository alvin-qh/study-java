
# 类型转换器和自定义类型#

## 属性类型转换器##

### 介绍###

属性类型转换器用于将“Java类型”转换为“SQL类型”，或者将“SQL类型”转换为“Java类型”。
JPA内建了一些基本类型的转换规则，例如：

- String<=>VARCHAR(n)
- int<=>INTEGER/NUMBER(n)
- double<=>DOUBLE/NUMBER(n, m)
- date<=>DATE
- timestamp<=>DATETIME/TIMESTAMP
- ……
    
但仍不可避免一些类型无法正确转换，例如：

- LocalDateTime<=>DATETIME/TIMESTAMP
- Object<=>VARCHAR(n)

这类特殊转换，就需要通过定义“属性转换器”来完成。

### 定义属性转换器###

一个“属性转换器”需要实现`AttributeConverter<E, T>`接口，该接口具有两个泛型参数，其中：

- E 表示要转化Java类型
- T 表示JDBC支持的Java类型

例如：`AttributeConverter<LocalDateTime, Timestamp>`表示在持久化实体时，可以将实体对象中的`LocalDateTime`类型的属性值转为`Timestamp`类型；反之，在获取持久化实体时，可以将`Timestamp`类型字段转为实体对象的`LocalDateTime`类型属性值。

**举例：LocalDateTime<=>Timestamp转换器**
```java
    public class LocalDateTimeConverter implements AttributeConverter<LocalDateTime, Timestamp> {
        @Override
        public Timestamp convertToDatabaseColumn(LocalDateTime attribute) {
            if (attribute == null) {
                return null;
            }
            return Timestamp.from((attribute).toInstant(ZoneOffset.UTC));
        }
    
        @Override
        public LocalDateTime convertToEntityAttribute(Timestamp dbData) {
            if (dbData == null) {
                return null;
            }
            Instant instant = Instant.ofEpochMilli(dbData.getTime());
            return LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
        }
    }
```
大部分JAP Provider还未支持Java 8新加入的时间日期类型(例如Hibernate 4之前的版本)，可以通过上述代码完成转换。例子中将一个UTC时区的`LocalDateTime`类型属性和JDBC内建`Timestamp`类型进行相互转换。

### 使用类型转换器###

可以在每个Entity中显式的配置类型转换器，也可以让类型转换器自动的应用在所需的场合。

#### 显式配置类型转换器####

通过`@Convert`注解即可指定Entity的某个属性所需的转换器，例如：
    
    @Entity
    @Table(name = "user_")
    public class User {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private int id;
        private String name;
        private String password;
        
        @Convert(converter = LocalDateTimeConverter.class)
        private LocalDateTime lastLoginTime;
        ....
    }
    
#### 自动配置类型转换器####

在类型转换器定义是加上`@Converter(autoApply = true)`注解，即可在Entity包含指定类型属性时，自动应用该转换器，例如：

    @Converter(autoApply = true)
    public class LocalDateTimeConverter implements AttributeConverter<LocalDateTime, Timestamp> {
        ....
    }

## 自定义类型##

### 介绍###

自定义类型是另一种解决JAP内建类型无法满足需求的方法，和转换器相比，这种方式更为灵活一些，可以直接定义在指定package声明上，对指定包下面的所有类型起作用。

**注意：自定义类型是 Hibernate 的功能，并不是所有的 JPA 提供者都能有效**

### 定义自定义类型###

定义一个实现了`UserType`接口的类，即完成了自定义类型的定义。该接口定义了若干方法，用于完成 Java 类型到 JDBC 类型的转换。

这些方法包括：

- `int[] sqlTypes()`：表示自定义类型所对应的SQL类型，一个自定义类型可以对应多个SQL类型。例如：自定义类型对应`VARCHAR`, 
`CLOB`, `LONG_VARCHAR`, `VARBINARY`等；
- `AdminClass returnedClass()`：返回自定义类型对应的Java类；
- `boolean equals(Object x, Object y)`：用于两个指定类型对象的比较；
- `int hashCode(Object value)`：用于获取指定类型对象的 hashcode；
- `Object deepCopy(Object value)`：用于复制指定类型对象；
- `boolean isMutable()`：表示returnedClass和sqlTypes方法返回的值是否固定，即自定义类型的转换规则是否固定；
- `Object assemble(Serializable cached, Object owner)`：将自定义类型(cached)对象转为JPA托管类型对象；
- `Serializable disassemble(Object value)`：将JPA托管类型对象(value)转为自定义类型对象；
- `Object replace(Object original, Object target, Object owner)`：将已存在的托管对象(target)替换为新对象(original)；
- `Object nullSafeGet(ResultSet resultSet, String[] names, SessionImplementor session, Object owner)`：从ResultSet对象中安全的获取指定字段的值；
- `void nullSafeSet(PreparedStatement preparedStatement, Object value, int index, SessionImplementor session)`：将指定的实体属性值安全设置到数据表中。

**范例：可以定一个常用的抽象类，实现`UserType`接口的大部分方法**

    public abstract class DefaultUserType implements UserType, Serializable {
        @Override
        public boolean equals(Object x, Object y) throws HibernateException {
            return Objects.equals(x, y);
        }
    
        @Override
        public int hashCode(Object object) throws HibernateException {
            return Objects.hashCode(object);
        }
    
        @Override
        public Object deepCopy(Object value) throws HibernateException {
            return value;
        }
    
        @Override
        public boolean isMutable() {
            return false;
        }
    
        @Override
        public Serializable disassemble(Object value) throws HibernateException {
            return (Serializable) value;
        }
    
        @Override
        public Object assemble(Serializable cached, Object value) throws HibernateException {
            return cached;
        }
    
        @Override
        public Object replace(Object original, Object target, Object owner) throws HibernateException {
            return original;
        }
    }

使用时，继承`DefaultUserType`类，主要完成`sqlTypes`, `returnedClass`, `nullSafeGet`和`nullSafeSet`等方法即可；

**举例：自定义`LocalDateTime`类型**
    
    public class LocalDateTimeType extends DefaultUserType {
        private static final int[] SQL_TYPES = new int[]{Types.TIMESTAMP};

        @Override
        public int[] sqlTypes() {
            return SQL_TYPES;
        }
    
        @Override
        public Class returnedClass() {
            return LocalDateTime.class;
        }
    
        @Override
        public Object nullSafeGet(ResultSet resultSet, String[] names, SessionImplementor session, Object owner)
                throws HibernateException, SQLException {
            Object value = StandardBasicTypes.TIMESTAMP.nullSafeGet(resultSet, names, session, owner);
            if (value == null) {
                return null;
            }
            Timestamp timestamp = (Timestamp) value;
            Instant instant = Instant.ofEpochMilli(timestamp.getTime());
            return LocalDateTime.ofInstant(instant, ZoneOffset.UTC);
        }
    
        @Override
        public void nullSafeSet(PreparedStatement preparedStatement, Object value, int index, SessionImplementor session)
                throws HibernateException, SQLException {
            if (value == null) {
                StandardBasicTypes.TIMESTAMP.nullSafeSet(preparedStatement, null, index, session);
            } else {
                Timestamp ts = Timestamp.from(((LocalDateTime)value).toInstant(ZoneOffset.UTC));
                StandardBasicTypes.TIMESTAMP.nullSafeSet(preparedStatement, ts, index, session);
            }
        }
    }

更复杂的自定义类型可以将若干Java类型转换为指定的SQL类型，例如将不同类型的对象和一个JSON字符串相互转换，此时需要通过指定的参数告诉自定义类型确切的转换规格。这一点是通过`ParameterizedType`接口来设置。

**举例：自定义 JSON 转换类型**

    public class JsonSerializeType extends DefaultUserType implements ParameterizedType {
        private Class<?> javaType;
    
        private int sqlType = Types.CLOB;
    
        private ObjectMapper objectMapper = ObjectMapperBuilder.create();
    
        @Override
        public int[] sqlTypes() {
            return new int[]{sqlType};
        }
    
        @Override
        public Class returnedClass() {
            return javaType;
        }
    
        @Override
        public Object assemble(Serializable cached, Object value) throws HibernateException {
            if (cached == null) {
                return null;
            }
            try {
                return objectMapper.readValue((String) cached, javaType);
            } catch (IOException e) {
                throw new HibernateException(e);
            }
        }
    
        @Override
        public Serializable disassemble(Object value) throws HibernateException {
            if (value == null) {
                return null;
            }
            try {
                return objectMapper.writeValueAsString(value);
            } catch (JsonProcessingException e) {
                throw new HibernateException(e);
            }
        }
    
        @Override
        public Object deepCopy(Object value) throws HibernateException {
            if (value == null) {
                return null;
            }
            try {
                return objectMapper.readValue(objectMapper.writeValueAsString(value), javaType);
            } catch (IOException e) {
                throw new HibernateException(e);
            }
        }
    
        @Override
        public Object nullSafeGet(ResultSet rs, String[] names, SessionImplementor session, Object owner) throws HibernateException, SQLException {
            Object obj = null;
            if (!rs.wasNull()) {
                try {
                    if (sqlType == Types.VARBINARY || sqlType == Types.BLOB) {
                        byte[] bytes = rs.getBytes(names[0]);
                        if (bytes != null) {
                            obj = objectMapper.readValue(bytes, javaType);
                        }
                    } else {
                        obj = objectMapper.readValue(rs.getString(names[0]), javaType);
                    }
                } catch (IOException e) {
                    throw new HibernateException(e);
                }
            }
            return obj;
        }
    
        @Override
        public void nullSafeSet(PreparedStatement st, Object value, int index, SessionImplementor session) throws HibernateException, SQLException {
            if (value == null) {
                st.setNull(index, this.sqlType);
            } else {
                try {
                    if (sqlType == Types.VARBINARY || sqlType == Types.BLOB) {
                        st.setBytes(index, objectMapper.writeValueAsBytes(value));
                    } else {
                        st.setString(index, objectMapper.writeValueAsString(value));
                    }
                } catch (IOException e) {
                    throw new HibernateException(e);
                }
            }
        }
    
        @Override
        public void setParameterValues(Properties parameters) {
            String className = parameters.getProperty("class");
            try {
                this.javaType = ReflectHelper.classForName(className, this.getClass());
            } catch (ClassNotFoundException ex) {
                throw new HibernateException(ex);
            }
            String type = parameters.getProperty("type");
            if (type != null) {
                this.sqlType = Integer.decode(type);
            }
        }
    
        @Override
        public boolean isMutable() {
            return true;
        }
    }
    
### 使用自定义类型###

#### 自定义类型无需参数时####

    @Entity
    @Table(name = "user_")
    public class User {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private int id;
        private String name;
        private String password;
    
        @TypeDef(defaultForType = LocalDateTime.class,
                 typeClass = LocalDateTimeType.class)
        private LocalDateTime lastLoginTime;
        ....
    }
    
#### 自定义类型需要参数时####

    @Entity
    @Table(name = "message_")
    public class Message {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private int id;
        
        @Type(defaultForType = JsonSerializable.class,
              typeClass = JsonSerializeType.class, 
              parameters = {@Parameter(name="class", value = "jpa.typedef.models.MessageContent")}
        )
        private MessageContent content;
        private LocalDateTime createdAt;
        ....
    }
    
代码中的`MessageContent`类定义如下：
    
    public class MessageContent implements JsonSerializable {
        private String title;
        private String content;
        ....
    }
    
代码中的`JsonSerializable`接口定义如下：
    
    public interface JsonSerializable extends Serializable {
    }

### 使用`package-info.java`为指定包定义全局自定义类型###

    @TypeDefs({
            @TypeDef(defaultForType = LocalDateTime.class,
                    typeClass = LocalDateTimeType.class),
            @TypeDef(name = "jsonType",
                    defaultForType = JsonSerializable.class,
                    typeClass = JsonSerializeType.class)
    }) package jpa.typedef;
    
此时，在指定包下，可以直接使用定义的类型，例如：

    @Entity
    @Table(name = "message_")
    public class Message {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private int id;
        @Type(type = "jsonType", 
              parameters = {@Parameter(name="class", value = "jpa.typedef.models.MessageContent")}
        )
        private MessageContent content;
        private LocalDateTime createdAt;
        ....
    }
    
其中：

- `createdAt`属性自动使用`LocalDateTimeType`自定义类型；
- `content`属性则通过名称使用`JsonSerializeType`自定义类型。

### 注意事项###

- 如果针对一个实体类型的某个属性，同时存在“类型转换器”和“自定义类型定义”，则“类型转换器”优先起作用。
