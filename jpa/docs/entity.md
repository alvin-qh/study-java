# 实体以及实体关系#

## 实体，实体属性及其注解##

### 实体及其注解###

实体即可以被JPA持久化的类对象，一般而言，实体一定和一张数据表产生对应关系——一个实体即数据表的一行。

JPA通过如下注解定义实体：

- `@Entity`：标注一个实体
- `@Table`：设置实体所对应的数据表 

例如：

    @Entity
    @Table(name = "person_")
    public class Person {
        ....
    }
    
### 实体属性及其注解###

定义实体类后，就需要依据数据表的字段定义实体的属性，JPA 通过如下注解定义实体属性：

- `@Id`：设置类属性表示主键；
- `@GeneratedValue`：设置主键生成规则，包含如下参数：
    - strategy：定义主键的生成规则，这些规则由JPA内建：
        - IDENTITY：利用数据表的字段自增规则产生主键
        - SEQUENCE：利用数据库的自增序列对象产生主键
        - TABLE：利用另一张数据表的字段值产生主键
        - AUTO：主键生成规则由数据库维护，JPA不关心
    - generator：设置一个主键生成器的名字，当JPA内置的主键生成规则无法满足需求是，可以定义一个主键生成器，并在这里进行关联
- `@Basic`：实体属性的默认注解，表示实体属性和数据表产生对应，具有如下参数：
    - fetch: 获取该属性的方式，包含如下值：
        - FetchType.EAGER：默认值，立即模式，当加载实体时，同时加载该属性；
        - FetchType.LAZY：懒惰模式，当第一次使用该属性时才会从数据库中加载；
    - optional：布尔值，表示该属性是否可选，true表示该属性是必填值
    
- `@Column`在`@Basic`规则之上，设置更多的实体字段和数据表字段的对应法则：
    - `name`：设置数据表字段名称，当实体属性名称无法和数据表字段名称通过默认规则对应时，可以指定属性所对应的数据表字段名称；
    - `insertable`/`updateable`：设置字段是否可以被插入和更新；
    - `length`：设置字段的长度（字符串类型）；
    - `unique`：设置字段的唯一约束；
    - `nullable`：设置字段的非空约束；
    - `precision`/`scale`：设置decimal类型字段的精度和小数位；
- `@Transient`：瞬时字段，表示实体的指定属性无需进行持久化；
- `@Lob`：大字段，对应数据表`CLOB`, `BLOB`, `TEXT`等类型；
- `@Enumerated`：枚举类型映射规则，表示实体中的枚举类型属性和数据表字段的映射规则，居有如下参数：
    - value: 映射规则，预设值如下：
        - EnumType.STRING：将枚举值转为字符串；
        - EnumType.ORDINAL：使用枚举值的原始类型；
        
**范例：实体定义**
    
    @Entity
    @Table(name = "person_")
    public class Person {
        public enum Gender {
            M, F
        }
    
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private int id;
        
        @Basic
        private String name;
        
        @Column(name = "gender")
        @Enumerated(EnumType.STRING)
        private Gender gender;
        ....
    }


有些时候，一个实体可能会对应多个数据表，这些表之间具备**主键到主键**的一对一关系，此时可通过如下方法将多个表映射在一个实体类上：

- 实体的`@SecondaryTable`注解：额外的`@SecondaryTable`注解表示将通过额外的数据表共同存储指定的实体，其`name`参数用来指定数据表的名称；
- 实体属性`@Column`注解的`table`参数：用于指定属性对应的数据表；

**范例：实体对应多个表的**

    @Entity
    @Table(name = "student_")
    @SecondaryTable(name = "student_detail_")
    public class Student {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Integer id;
        private String sno;
        private String name;
        private String gender;
        private String telephone;
    
        @Column(table = "student_detail_")
        private LocalDate birthday;
        @Column(table = "student_detail_")
        private String address;
        @Column(table = "student_detail_")
        private String email;
        @Column(table = "student_detail_")
        private String qq;
        ....
    }

## 实体状态即状态转换##

### 实体状态###

JPA会将实体对象在以下四个状态中转换，一个实体对象在一瞬间必然处于下述四个状态之一。
在JPA事务结束前，所有“受控态”对象会被同步到数据表；所有“删除态”对象会从数据表中删除。

- new(新建态)：通过Java的new操作符创建的对象，该对象暂无主键；
- managed(受控态)：受JPA实体容器管理的对象，该对象拥有主键；
- detached(游离态)：拥有主键但不受JPA容器管理(前一个事务获取的对象或通过编程设置的主键值)；
- removed(删除态)：对象已经从JPA容器中删除；

### 实体基本操作###

- `persist(Object entity)`方法：将new/removed状态的对象转为 managed状态(立即执行数据表insert操作)；
- `merge(Object entity)`方法：将new/detached状态的对象转为managed状态(立即执行数据表insert操作)；对于managed状态执行flush derty操作(在需要时执行数据表update操作)；
- `remove(Object entity)`方法：将manage状态的对象转为removed 状态(立即执行数据表delete操作)；
- `find(Serializable key)`方法：通过主键值获取一个managed状态的实体对象；
- `flush(Object entity)`方法：提交事物中所有实体的状态；

## 实体关系##

### 一对一关系###

对于A、B两个实体，如果其互相引用一次，则表现为一对一关系，对应到数据表，则B表需引用A表的主键字段作为外键。一对一关系的注解如下：

- `@OneToOne`：表示一个实体属性(主键)和另一个实体对应属性(外键)具备“一对一关系”，居有如下参数：
    - `mappedBy`：定义在主键持有方，表示在外键持有方中表示外键关系的属性；
- `@JoinColumn`：定义在外键持有方，表示外键对应的表字段。

**范例：一对一关系**

**主键方：**

    @Entity
    @Table(name = "user_")
    public class User {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private int id;
    
        @OneToOne(mappedBy = "user")
        private UserInfo userInfo;
        ....
    }
    
**外键方：**

    @Entity
    @Table(name = "user_info_")
    public class UserInfo {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private int id;
    
        @OneToOne
        @JoinColumn(name = "user_id")
        private User user;
        ....
    }

### 一对多/多对一关系###

对于A、B两个实体，如果A实体持有B实体的集合引用，且B实体持有A实体的单一引用，则称为一对多关系，B表需许引用A表的主键字段作为外键

- `@OneToMany`：定义在主键持有方，为一个List或Set对象，表示主键和外键的一对多关系，具备如下参数：
    - `mappedBy`：表示外键持有方中外键和主键对应的属性；
- `@ManyToOne`：定义在外键持有方的外键属性上，表示外键和主键的多对一关系；
- `@JoinColumn`：定义在外键持有方的外键属性上，表示外键在数据表中对应的字段；

**范例：一对多关系的**

**主键方：**

    @Entity
    @Table(name = "group_")
    public class Group {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private int id;
    
        @OneToMany(mappedBy = "group")
        private List<User> users = new ArrayList<>();
        ....
    }
    
**外键方：**

    @Entity
    @Table(name = "user_")
    public class User {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private int id;
    
        @ManyToOne
        @JoinColumn(name = "group_id")
        private Group group;
        ....
    }

### 多对多关系

对于A、B两个实体，如果其相互应用多个对方实体对象，则这两个实体组成多对多关系。多对多关系必须通过额外的中间表来表示对应关系。

- `@ManyToMany`：为一个List或Set对象，定义在多对多的两方；
    - `mappedBy`：定义在多对多的某一方，表示和另一方指定属性的对应；
- `@JoinTable`：定义在多对多的另一方，用于说明中间表和两方的对应关系，具备如下属性：
    - name：中间表名称；
    - joinColumns：`@JoinColumn`注解，表示当前方主键和中间表的外键的对应关系；
    - inverseJoinColumns：`@JoinColumn`注解，表示另一方主键和中间表外键的对应关系

**范例：一对多关系**

**一方：**

    @Entity
    @Table(name = "user_")
    public class User {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private int id;
    
        @ManyToMany(mappedBy = "users")
        private List<Interest> interests = new ArrayList<>();
        ....
    }
    
**另一方：**

    @Entity
    @Table(name = "interest_")
    public class Interest {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private int id;
    
        @ManyToMany
        @JoinTable(name = "user_interest_",
                joinColumns = {@JoinColumn(name = "interest_id")},
                inverseJoinColumns = {@JoinColumn(name = "user_id")})
        private List<User> users = new ArrayList<>();
        ....
    }

### 自连接###

自连接表示当前实体对象之间的一对一或一对多关系；

**范例：自连接**

    @Entity
    @Table(name = "user_")
    public class User {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private int id;
    
        @OneToMany(mappedBy = "manager")
        private List<User> employees = new ArrayList<>();
            
        @ManyToOne
        @JoinColumn(name = "manager_id")
        private User manager;
        ....
    }

### 级联操作###

- 对于一对一或者一对多关系，级联操作定义在主键方，表示有持有主键的实体完成控制，持有外键的实体作为被控方；
- 对于多对多关系，级联操作定义在某一方，表示主控方，则另一方自动表示被控方；

级联操作的注解如下：

- `@OneToOne`, `@OneToMany`和`@ManyToMany`注解可以定义级联操作，定义级联操作的属性应该具备这些注解的`mappedBy`属性，
而不应该具备`@JoinColumn`注解。级联参数包括：
    - cascade：级联方式，可以为：
        - CascadeType.ALL：在所有操作时进行级联操作；
        - CascadeType.PERSIST：在持久化对象时进行级联操作；
        - CascadeType.MERGE：在持久化对象或更新对象时执行级联操作；
        - CascadeType.REMOVE：在删除对象时进行级联操作；
        - CascadeType.REFRESH：在刷新对象时进行级联操作；
        - CascadeType.DETACH：在分离对象时进行级联操作
    - orphanRemoval：是否删除孤立节点(相当于级联删除)

**范例：级联操作**

    @Entity
    @Table(name = "group_")
    public class Group {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private int id;
    
        @OneToMany(mappedBy = "group", cascade = {CascadeType.ALL}, orphanRemoval = true, fetch = FetchType.LAZY)
        private List<User> users = new ArrayList<>();
        ....
    }

### n + 1问题###

当实体之间产生“一对多”或者“多对多”关系后，如何获取实体并获取与之对应的实体集合就有两种方式：

- JOIN：通过SQL的inner join或outer join同时获取主键对象和与之对应的外键对象；
- SELECT：先通过一条SQL语句获取一个实体对象，在通过额外的一条SQL语句获取与之对应的外键对象集合；

对于SELECT方式，如果通过SQL获取的不是一个对象，而是一个对象集合，则会产生n + 1问题，即通过一条SQL获取n个实体对象，
在通过n条SQL获取与之对应的外键对象集合，解决n + 1问题的方法包括：

- 利用Lazy Fetch：在主键的一方设置集合属性的获取方式为LAZY，则可以避免在获取对象时自动获取对应的集合；
- 利用JPQL的join fetch查询；
- 利用JPA的二级缓存