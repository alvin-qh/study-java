# JAP事务

## 事务的传播性和隔离级别

### JPA事务传播性：

- JPA要求：
    - insert/update/delete操作必须受事务管理(Mandatory)
    - select操作可以有也可以没有事务(Support)，但如果要使用锁，则应该在事务中(Required)
    - 事务必须具备原子性
- 一般情况下JPA建议以线程为单位隔离事务，在同一个线程中，事务会发生合并，即后发生的事务会合并到之前发生的事务中，子事务会合并到父事务中（使用不同的框架，对事务隔离级别的处理也会略有不同，例如Spring允许自定义事务隔离级别，而Guice则自动处理这些问题）

### JPA事务隔离级别：

- 问题：对于A、B两个并发事务
    - 脏读：A事务读取了B事务尚未提交的数据；
    - 不可重复读：由于B事务的update操作，A事务无法重复读取相同的数据；
    - 幻读：A事务无法确定读取到的数据；
- 隔离级别：
    - 未提交读：最低级别的事务隔离，保证不会发生读取错误；
    - 已提交读：无法读取其它未提交事务产生的数据，避免了脏读，一般是数据库的默认隔离级别；
    - 重复读：避免脏读和不可重复读，即无法更新另一个事务尚未提交的数据；
    - 序列化：避免幻读，即在另一个事务操作结束前无法操作相同的资源；

## 锁

### 乐观锁

乐观锁并不真正的对数据表加锁，只是通过版本控制和事务回滚来保证不会同时有两个事务更新同一条数据。要使用乐观锁，需要两个前提：

- 数据表增加版本字段，如version字段；
- 实体对应的属性由`@Version`注解修饰；

**范例：使用乐观锁**

**SQL**

```sql
create table app.optimistic_lock_(
    id integer not null auto_increment,
    total_count int not null,
    left_count int not null,
    version int not null default 1,
    primary key (id)
);
```
    
**Java Model**

```java
@Entity
@Table(name = "optimistic_lock_")
public class OptimisticLock {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer totalCount;
    private Integer leftCount;

    @Version
    private Integer version = 1;
    ....
}
```

**JPA Query**

```java
EntityManager em = ...;

em.getTransaction().begin();
try {
    OptimisticLock lock = em.find(OptimisticLock.class, 1);   // 查找id为1的实体
    lock.setLeftCount(lock.getLeftCount() - 1);
    em.getTransaction().commit();
} finally {
    em.getTransaction().rollback();
}
```

### 悲观锁

利用数据库本身的功能加锁(例如select…from…where…for update)，悲观锁会真正锁定数据表，直到锁被解除或者事务被提交/回滚。悲观锁包括：
    
- PESSIMISTIC_READ：共享锁，对于同一实体，不能再施加排它锁；
- PESSIMISTIC_WRITE：排它锁，对于同一实体，不能再施加共享锁或排它锁。
    
**范例：使用悲观锁**

**SQL**

```java
create table app.pessimistic_lock_(
    id integer not null auto_increment,
    total_count int not null,
    left_count int not null,
    primary key (id)
);
```
    
**Java Model**

```java
@Entity
@Table(name = "pessimistic_lock_")
public class PessimisticLock {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer totalCount;
    private Integer leftCount;
    ....
}
```

**JPA Query**

```java
EntityManager em = ...;

em.getTransaction().begin();
try {
    PessimisticLock lock = em.em.find(entityClass, 1, LockModeType.PESSIMISTIC_WRITE);   // 查找id为1的实体
    lock.setLeftCount(lock.getLeftCount() - 1);
    em.getTransaction().commit();
} finally {
    em.getTransaction().rollback();
}
```
    
除了在使用`find`方法时进行加锁外，JPA对实体加锁的途径还有：

- `EntityManager::lock(Object entity, LockModeType model)`：锁定一个持久态的实体对象；
- `EntityManager::refresh(Object entity, LockModeType model)`：刷新实体对象并进行锁定；

**范例：解除锁定**
    
```java
EntityManager em = ...;
    
em.getTransaction().begin();
try {
    PessimisticLock lock = em.em.find(entityClass, 1, LockModeType.PESSIMISTIC_WRITE);   // 查找id为1的实体
    lock.setLeftCount(lock.getLeftCount() - 1);
    
    em.lock(lock, LockModeType.NONE);
    em.getTransaction().commit();
} finally {
    em.getTransaction().rollback();
}
```
