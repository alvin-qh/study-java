
@TypeDefs({
        @TypeDef(defaultForType = LocalDateTime.class,
                typeClass = LocalDateTimeType.class),
        @TypeDef(defaultForType = LocalDate.class,
                typeClass = LocalDateType.class),
        @TypeDef(name = "jsonType",
                defaultForType = JsonSerializable.class,
                typeClass = JsonSerializeType.class)
})
package alvin.jpa.typedef;      // SUPPRESS

import alvin.jpa.core.json.JsonSerializable;
import alvin.jpa.typedef.types.LocalDateTimeType;
import alvin.jpa.typedef.types.JsonSerializeType;
import alvin.jpa.typedef.types.LocalDateType;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import java.time.LocalDate;
import java.time.LocalDateTime;
