package alvin.jpa.typedef.types;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.StandardBasicTypes;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class LocalDateType extends DefaultUserType {

    private static final int[] SQL_TYPES = new int[]{Types.DATE};

    @Override
    public int[] sqlTypes() {
        return SQL_TYPES;
    }

    @Override
    public Class returnedClass() {
        return LocalDate.class;
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner)
            throws HibernateException, SQLException {

        Object value = StandardBasicTypes.DATE.nullSafeGet(rs, names, session, owner);
        if (value == null) {
            return null;
        }
        Date date = (Date) value;
        Instant instant = Instant.ofEpochMilli(date.getTime());
        return LocalDateTime.ofInstant(instant, ZoneOffset.UTC).toLocalDate();
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session)
            throws HibernateException, SQLException {
        if (value == null) {
            StandardBasicTypes.DATE.nullSafeSet(st, null, index, session);
        } else {
            LocalDate localDate = (LocalDate) value;
            Instant instant = localDate.atStartOfDay().toInstant(ZoneOffset.UTC);
            Date date = new Date(Date.from(instant).getTime());
            StandardBasicTypes.DATE.nullSafeSet(st, date, index, session);
        }
    }
}
