package alvin.jpa.typedef.types;

import alvin.jpa.core.json.ObjectMapperBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.internal.util.ReflectHelper;
import org.hibernate.usertype.ParameterizedType;

import java.io.IOException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;

public class JsonSerializeType extends DefaultUserType implements ParameterizedType {
    private Class<?> javaType;

    private int sqlType = Types.CLOB;

    private ObjectMapper objectMapper = ObjectMapperBuilder.create();

    @Override
    public int[] sqlTypes() {
        return new int[]{sqlType};
    }

    @Override
    public Class returnedClass() {
        return javaType;
    }

    @Override
    public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner)
            throws HibernateException, SQLException {

        Object obj = null;
        if (!rs.wasNull()) {
            try {
                if (sqlType == Types.VARBINARY || sqlType == Types.BLOB) {
                    byte[] bytes = rs.getBytes(names[0]);
                    if (bytes != null) {
                        obj = objectMapper.readValue(bytes, javaType);
                    }
                } else {
                    obj = objectMapper.readValue(rs.getString(names[0]), javaType);
                }
            } catch (IOException e) {
                throw new HibernateException(e);
            }
        }
        return obj;
    }

    @Override
    public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session)
            throws HibernateException, SQLException {

        if (value == null) {
            st.setNull(index, this.sqlType);
        } else {
            try {
                if (sqlType == Types.VARBINARY || sqlType == Types.BLOB) {
                    st.setBytes(index, objectMapper.writeValueAsBytes(value));
                } else {
                    st.setString(index, objectMapper.writeValueAsString(value));
                }
            } catch (IOException e) {
                throw new HibernateException(e);
            }
        }
    }

    @Override
    public Object assemble(Serializable cached, Object value) throws HibernateException {
        if (cached == null) {
            return null;
        }
        try {
            return objectMapper.readValue((String) cached, javaType);
        } catch (IOException e) {
            throw new HibernateException(e);
        }
    }

    @Override
    public Serializable disassemble(Object value) throws HibernateException {
        if (value == null) {
            return null;
        }
        try {
            return objectMapper.writeValueAsString(value);
        } catch (JsonProcessingException e) {
            throw new HibernateException(e);
        }
    }

    @Override
    public Object deepCopy(Object value) throws HibernateException {
        if (value == null) {
            return null;
        }
        try {
            return objectMapper.readValue(objectMapper.writeValueAsString(value), javaType);
        } catch (IOException e) {
            throw new HibernateException(e);
        }
    }

    @Override
    public void setParameterValues(Properties parameters) {
        String className = parameters.getProperty("class");
        try {
            this.javaType = ReflectHelper.classForName(className, this.getClass());
        } catch (ClassNotFoundException ex) {
            throw new HibernateException(ex);
        }
        String type = parameters.getProperty("type");
        if (type != null) {
            this.sqlType = Integer.decode(type);
        }
    }

    @Override
    public boolean isMutable() {
        return true;
    }
}
