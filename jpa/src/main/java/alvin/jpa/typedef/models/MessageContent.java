package alvin.jpa.typedef.models;

import alvin.jpa.core.json.JsonSerializable;
import com.google.common.base.Objects;

public class MessageContent implements JsonSerializable {
    private String title;
    private String content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MessageContent that = (MessageContent) o;
        return Objects.equal(title, that.title) &&
                Objects.equal(content, that.content);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(title, content);
    }
}
