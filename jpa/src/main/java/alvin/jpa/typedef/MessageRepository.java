package alvin.jpa.typedef;

import alvin.jpa.core.repositories.BaseRepository;
import alvin.jpa.typedef.models.Message;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;

@Singleton
public class MessageRepository extends BaseRepository<Message> {

    @Inject
    public MessageRepository(Provider<EntityManager> emProvider) {
        super(emProvider);
    }
}
