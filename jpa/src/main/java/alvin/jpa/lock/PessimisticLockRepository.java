package alvin.jpa.lock;

import alvin.jpa.core.repositories.BaseRepository;
import alvin.jpa.lock.models.PessimisticLock;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;

@Singleton
public class PessimisticLockRepository extends BaseRepository<PessimisticLock> {

    @Inject
    public PessimisticLockRepository(Provider<EntityManager> emProvider) {
        super(emProvider);
    }
}
