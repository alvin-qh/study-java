package alvin.jpa.lock;

import alvin.jpa.core.repositories.BaseRepository;
import alvin.jpa.lock.models.OptimisticLock;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;

@Singleton
public class OptimisticLockRepository extends BaseRepository<OptimisticLock> {

    @Inject
    public OptimisticLockRepository(Provider<EntityManager> emProvider) {
        super(emProvider);
    }
}
