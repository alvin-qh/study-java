package alvin.jpa.relationship;

import alvin.jpa.core.repositories.BaseRepository;
import alvin.jpa.relationship.models.User;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Singleton
public class UserRepository extends BaseRepository<User> {

    @Inject
    public UserRepository(Provider<EntityManager> emProvider) {
        super(emProvider);
    }

    public Optional<User> findByName(String name) {
        return findBy("from User where name=:name order by name",
                q -> q.setParameter("name", name));
    }

    public Optional<User> findAndFetchInfo(Integer id) {
        return findBy("from User u join fetch u.userInfo where u.id=:id",
                q -> q.setParameter("id", id));
    }

    public Optional<User> findAndJoinInfo(Integer id) {
        return findBy("select u from User u join u.userInfo ui where u.id=:id",
                q -> q.setParameter("id", id));
    }

    public List<User> findByFullName(String fillName) {
        return list("select u from User u join fetch u.userInfo ui where ui.fullName=:fullName",
                q -> q.setParameter("fullName", fillName));
    }

    public List<User> findByInterestName(String interestName) {
        return list("select u from User u join u.interests i on i.name=:name",
                q -> q.setParameter("name", interestName));
    }

    public List<User> findByGroupName(String groupName) {
        return list("select u from User u join u.group g on g.name=:name order by u.id",
                q -> q.setParameter("name", groupName));
    }
}
