package alvin.jpa.em;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.matcher.Matchers;
import com.google.inject.persist.Transactional;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class EntityManagerModule extends AbstractModule {

    // 本地线程存储对象
    private static final ThreadLocal<EntityManager> ENTITY_MANAGER_CACHE = new ThreadLocal<>();

    // 创建EntityManager工厂对象
    private static final EntityManagerFactory ENTITY_MANAGER_FACTORY =
            Persistence.createEntityManagerFactory("default");

    @Override
    protected void configure() {
        /* 创建一个方法拦截器用于管理事务 */

        MethodInterceptor transactionInterceptor = new TransactionInterceptor();
        // 将拦截器实例与Transactional注解进行绑定
        bindInterceptor(Matchers.any(), Matchers.annotatedWith(Transactional.class), transactionInterceptor);
        // 注入拦截器
        requestInjection(transactionInterceptor);
    }

    /**
     * 获取EntityManager对象的Provider.
     *
     * @return EntityManager对象
     */
    @Provides
    @Inject
    public EntityManager provideEntityManager() {
        /* 判断本地线程存储中是否具备EntityManager，如果没有，则创建并存入本地线程存储中 */
        
        EntityManager em = ENTITY_MANAGER_CACHE.get();
        if (em == null) {
            em = ENTITY_MANAGER_FACTORY.createEntityManager();
            ENTITY_MANAGER_CACHE.set(em);
        }
        return em;
    }

    /**
     * 拦截器类.
     */
    static class TransactionInterceptor implements MethodInterceptor {

        // EntityManager对象的Provider
        @Inject
        private Provider<EntityManager> emProvides;

        /**
         * 执行拦截器.
         */
        @Override
        public Object invoke(MethodInvocation invocation) throws Throwable {
            EntityManager em = emProvides.get();
            EntityTransaction tx = em.getTransaction();
            boolean active = tx.isActive();
            if (!active) {
                tx.begin();
            }
            try {
                Object result = invocation.proceed();
                if (!active) {
                    tx.commit();
                }
                return result;
            } catch (Exception e) {
                if (!active) {
                    tx.rollback();
                }
                throw e;
            }
        }
    }
}
