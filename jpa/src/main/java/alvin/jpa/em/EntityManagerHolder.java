package alvin.jpa.em;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class EntityManagerHolder {
    private static final ThreadLocal<EntityManager> ENTITY_MANAGER_HOLDER = new ThreadLocal<>();
    private static final EntityManagerFactory EM_FACTORY = Persistence.createEntityManagerFactory("default");

    EntityManager getEntityManager() {
        /* 判断本地线程存储中是否具备EntityManager，如果没有，则创建并存入本地线程存储中 */

        EntityManager em = ENTITY_MANAGER_HOLDER.get();
        if (em == null) {
            em = EM_FACTORY.createEntityManager();
            ENTITY_MANAGER_HOLDER.set(em);
        }
        return em;
    }

    public void clear() {
        EntityManager em = ENTITY_MANAGER_HOLDER.get();
        if (em != null && em.isOpen()) {
            em.close();
        }
        ENTITY_MANAGER_HOLDER.remove();
    }
}
