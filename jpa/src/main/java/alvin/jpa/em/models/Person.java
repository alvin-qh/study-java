package alvin.jpa.em.models;

import alvin.jpa.core.conversion.LocalDateConverter;

import javax.persistence.Cacheable;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.LockModeType;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "em_person")
@Cacheable
@NamedQuery(name = "find by name", query = "from Person where name=:name", lockMode = LockModeType.NONE)
public class Person implements Serializable {

    public enum Gender {
        M, F
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String name;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @Convert(converter = LocalDateConverter.class)
    private LocalDate birthday;
    private String telephone;
    private String email;

    public Person() {
    }

    public Person(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return String.format("id:%d, name:%s, gender:%s, birthday:%s, telephone:%s, email:%s",
                id, name, gender, birthday, telephone, email);
    }
}
