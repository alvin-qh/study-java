package alvin.jpa.mptt.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Range;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;

public final class Node {
    private int id;
    private String name;

    @JsonIgnore
    private Node parent;

    @JsonIgnore
    private Range<Integer> childRange;

    private List<Node> children = new ArrayList<>();

    private Node(int id, String name, int left, int right) {
        this.id = id;
        this.name = name;
        this.childRange = Range.openClosed(left, right);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Node getParent() {
        return parent;
    }

    public List<Node> getChildren() {
        return Collections.unmodifiableList(children);
    }

    public void setParent(Node parent) {
        this.parent = parent;
        if (parent != null) {
            parent.children.add(this);
        }
    }

    public Range<Integer> getChildRange() {
        return childRange;
    }

    public static Node from(MPTT mptt) {
        return new Node(mptt.getId(), mptt.getName(), mptt.getLeft(), mptt.getRight());
    }

    public static Node from(List<MPTT> nodes) {
        if (nodes == null || nodes.isEmpty()) {
            return null;
        }

        Deque<Node> stack = new ArrayDeque<>();

        Node root = from(nodes.get(0));
        stack.push(root);

        for (int i = 1; i < nodes.size(); i++) {
            Node node = from(nodes.get(i));
            Node parent = findParent(stack, node);

            node.setParent(parent);
            stack.push(node);
        }
        return root;
    }

    private static Node findParent(Deque<Node> stack, Node node) {
        while (!stack.isEmpty()) {
            Node parent = stack.peek();
            if (parent.childRange.encloses(node.childRange)) {
                return parent;
            }
            stack.pop();
        }

        return null;
    }
}
