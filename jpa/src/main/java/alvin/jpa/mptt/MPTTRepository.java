package alvin.jpa.mptt;

import alvin.jpa.core.repositories.BaseRepository;
import alvin.jpa.mptt.models.MPTT;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import java.util.List;

@Singleton
public class MPTTRepository extends BaseRepository<MPTT> {

    @Inject
    public MPTTRepository(Provider<EntityManager> emProvider) {
        super(emProvider);
    }

    public void addNode(MPTT parent, MPTT child) {
        em().refresh(parent);

        child.setLeft(parent.getRight());
        child.setRight(parent.getRight() + 1);

        execute("update MPTT m set m.left = m.left + 2 where m.left >= :value",
                query -> query.setParameter("value", parent.getRight()));

        execute("update MPTT m set m.right = m.right + 2 where m.right >= :value",
                query -> query.setParameter("value", parent.getRight()));

        save(child);
    }

    public List<MPTT> findAll() {
        return list("from MPTT m order by m.left");
    }
}
