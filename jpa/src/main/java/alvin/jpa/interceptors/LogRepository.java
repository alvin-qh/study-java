package alvin.jpa.interceptors;

import alvin.jpa.core.repositories.BaseRepository;
import alvin.jpa.interceptors.models.Log;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;

@Singleton
public class LogRepository extends BaseRepository<Log> {

    @Inject
    public LogRepository(Provider<EntityManager> emProvider) {
        super(emProvider);
    }
}
