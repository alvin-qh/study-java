package alvin.jpa.interceptors;

import alvin.jpa.interceptors.models.Audible;
import alvin.jpa.interceptors.models.AuthUser;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

import javax.inject.Inject;
import javax.inject.Provider;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * 要使用JPA Interceptor，需要在 'persistence.xml' 配置文件中声明如下配置.
 *
 * {@link Audible} 接口可以用来标记一个类是否需要被Interceptor处理
 *
 * <property name="hibernate.ejb.interceptor" value="alvin.interceptors.AuditInterceptor"/>
 */
public class AuditInterceptor extends EmptyInterceptor {

    @Inject
    private Provider<AuthUser> currentUserProvider;

    @Override
    public boolean onSave(Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
        boolean modified = false;
        if (entity instanceof Audible) {
            auditSave(state, propertyNames);
            modified = true;
        }
        return modified;
    }

    private void auditSave(Object[] state, String[] propertyNames) {
        for (int i = 0; i < propertyNames.length; i++) {
            if (state[i] == null) {
                String property = propertyNames[i];
                switch (property) {
                case "createdBy":
                    state[i] = currentUserProvider.get();
                    break;
                case "createdAt":
                    state[i] = LocalDateTime.now(ZoneOffset.UTC);
                    break;
                case "updatedBy":
                    state[i] = currentUserProvider.get();
                    break;
                case "updatedAt":
                    state[i] = LocalDateTime.now(ZoneOffset.UTC);
                    break;
                default:
                    break;
                }
            }
        }
    }

    @Override
    public boolean onFlushDirty(Object entity, Serializable id, Object[] state, Object[] previousState,
                                String[] propertyNames, Type[] types) {
        boolean modified = false;
        if (entity instanceof Audible) {
            auditUpdate(state, propertyNames);
            modified = true;
        }
        return modified;
    }

    private void auditUpdate(Object[] state, String[] propertyNames) {
        for (int i = 0; i < propertyNames.length; i++) {
            if (state[i] == null) {
                String property = propertyNames[i];
                switch (property) {
                case "updatedBy":
                    state[i] = currentUserProvider.get();
                    break;
                case "updatedAt":
                    state[i] = LocalDateTime.now(ZoneOffset.UTC);
                    break;
                default:
                    break;
                }
            }
        }
    }
}
