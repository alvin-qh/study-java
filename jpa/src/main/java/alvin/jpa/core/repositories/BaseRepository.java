package alvin.jpa.core.repositories;

import org.hibernate.Filter;
import org.hibernate.Session;

import javax.inject.Provider;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public abstract class BaseRepository<T> implements Repository<T> {
    private Provider<EntityManager> emProvider;
    private Class<T> entityClass;

    public BaseRepository(Provider<EntityManager> emProvider) {
        this.emProvider = emProvider;

        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        //noinspection unchecked
        entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
    }

    protected <E> Optional<E> first(List<E> list) {
        return list.isEmpty() ? Optional.empty() : Optional.of(list.get(0));
    }

    protected Optional<T> findBy(String ql, Statement statement) {
        return first(list(ql, statement));
    }

    protected Optional<T> findBy(String ql) {
        return findBy(ql, null);
    }

    protected List<T> list(String ql, Statement statement) {
        TypedQuery<T> query = em().createQuery(ql, entityClass);
        if (statement != null) {
            statement.prepare(query);
        }
        return query.getResultList();
    }

    protected List<T> list(String ql) {
        return list(ql, null);
    }

    protected long count(String ql, Statement statement) {
        Query query = em().createQuery(ql);
        if (statement != null) {
            statement.prepare(query);
        }
        return (Long) query.getSingleResult();
    }

    protected long count(String ql) {
        return count(ql, null);
    }

    protected int execute(String ql, Statement statement) {
        Query query = em().createQuery(ql);
        if (statement != null) {
            statement.prepare(query);
        }
        return query.executeUpdate();
    }

    protected int execute(String ql) {
        return execute(ql, null);
    }

    public Optional<T> find(Object key) {
        return Optional.ofNullable(em().find(entityClass, key));
    }

    public Optional<T> find(Object key, LockModeType lockModeType) {
        return Optional.ofNullable(em().find(entityClass, key, lockModeType));
    }

    public void save(T model) {
        em().persist(model);
    }

    public void delete(T model) {
        em().remove(model);
    }

    public void lock(T object, LockModeType lockModeType) {
        em().lock(object, lockModeType);
    }

    public EntityManager em() {
        return emProvider.get();
    }

    public void enableFilter(String filterName, Map<String, Object> arguments) {
        Filter filter = em().unwrap(Session.class)
                .enableFilter(filterName);
        if (arguments != null) {
            arguments.forEach(filter::setParameter);
        }
    }

    public void disableFilter(String filterName) {
        em().unwrap(Session.class)
                .disableFilter(filterName);
    }
}
