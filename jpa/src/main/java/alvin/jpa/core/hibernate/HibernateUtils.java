package alvin.jpa.core.hibernate;

import org.hibernate.Cache;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public final class HibernateUtils {

    private HibernateUtils() {
    }

    public static Session getSession(EntityManager em) {
        return em.unwrap(Session.class);
    }

    public static SessionFactory getSessionFactory(EntityManager em) {
        return getSessionFactory(em.getEntityManagerFactory());
    }

    public static SessionFactory getSessionFactory(EntityManagerFactory emFactory) {
        return emFactory.unwrap(SessionFactory.class);
    }

    public static Cache getCache(EntityManager em) {
        return getSessionFactory(em).getCache();
    }

    public static Cache getCache(EntityManagerFactory emFactory) {
        return getSessionFactory(emFactory).getCache();
    }
}

