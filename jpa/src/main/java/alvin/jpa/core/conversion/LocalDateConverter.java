package alvin.jpa.core.conversion;

import javax.persistence.AttributeConverter;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

//@Converter(autoApply = true)
public class LocalDateConverter implements AttributeConverter<LocalDate, Date> {
    @Override
    public Date convertToDatabaseColumn(LocalDate attribute) {
        if (attribute == null) {
            return null;
        }
        java.util.Date data = Date.from(attribute.atStartOfDay().atZone(ZoneOffset.UTC).toInstant());
        return new java.sql.Date(data.getTime());
    }

    @Override
    public LocalDate convertToEntityAttribute(Date dbData) {
        if (dbData == null) {
            return null;
        }
        Instant instant = new java.util.Date(dbData.getTime()).toInstant();
        return LocalDateTime.ofInstant(instant, ZoneOffset.UTC).toLocalDate();
    }
}
