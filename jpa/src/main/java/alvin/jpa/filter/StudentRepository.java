package alvin.jpa.filter;

import alvin.jpa.core.repositories.BaseRepository;
import alvin.jpa.filter.models.Student;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;

@Singleton
public class StudentRepository extends BaseRepository<Student> {

    @Inject
    public StudentRepository(Provider<EntityManager> emProvider) {
        super(emProvider);
    }

    public List<Student> findAll() {
        return list("from Student s join fetch s.school order by s.studentNo");
    }

    public Optional<Student> findByStudentNo(String studentNo) {
        return findBy("from Student s join fetch s.school where s.studentNo=:studentNo",
                q -> q.setParameter("studentNo", studentNo));
    }
}
