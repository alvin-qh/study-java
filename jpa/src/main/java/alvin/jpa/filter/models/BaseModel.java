package alvin.jpa.filter.models;


import alvin.jpa.core.repositories.BaseRepository;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Filters;
import org.hibernate.annotations.ParamDef;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Map;

/**
 * Supper class with @Filter defined.
 *
 * This class defined a filter, so the flowing facts were determined:
 * 1. Every entity inherited by would has school attribute, with school_id column;
 * 2. Any query by inherited entity would be add 'school_id=?' constraint in where sentence;
 * 3. If Hibernate, use Session::enableFilter method to enable filter
 *
 *    {@link BaseRepository#enableFilter(String, Map)}
 *    {@link BaseRepository#disableFilter(String)}
 */
@MappedSuperclass
@FilterDef(name = "school",
        parameters = {@ParamDef(name = "schoolId", type = "int")},
        defaultCondition = "school_id=:schoolId")
@Filters({@Filter(name = "school")})
public class BaseModel implements Serializable {

    @ManyToOne
    @JoinColumn(name = "school_id")
    private School school;

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }
}
