package alvin.jpa.filter.providers;

import alvin.jpa.filter.models.School;

import javax.inject.Provider;
import javax.inject.Singleton;

@Singleton
public class SchoolProvider implements Provider<School> {
    private static final ThreadLocal<School> SCHOOL_THREAD_LOCAL = new ThreadLocal<>();

    @Override
    public School get() {
        return SCHOOL_THREAD_LOCAL.get();
    }

    public void set(School school) {
        if (SCHOOL_THREAD_LOCAL.get() != null) {
            throw new SchoolProviderException("object already exists");
        }
        SCHOOL_THREAD_LOCAL.set(school);
    }

    public void clear() {
        SCHOOL_THREAD_LOCAL.remove();
    }
}
