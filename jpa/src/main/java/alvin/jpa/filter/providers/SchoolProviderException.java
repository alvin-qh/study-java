package alvin.jpa.filter.providers;

public class SchoolProviderException extends RuntimeException {
    public SchoolProviderException(String msg) {
        super(msg);
    }
}
