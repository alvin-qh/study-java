package alvin.jpa.filter;

import alvin.jpa.core.repositories.BaseRepository;
import alvin.jpa.filter.models.AdminClass;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import java.util.Optional;

@Singleton
public class ClassRepository extends BaseRepository<AdminClass> {

    @Inject
    public ClassRepository(Provider<EntityManager> emProvider) {
        super(emProvider);
    }

    public Optional<AdminClass> findByName(String name) {
        return findBy("from AdminClass c join fetch c.school where c.name=:name",
                q -> q.setParameter("name", name));
    }
}
