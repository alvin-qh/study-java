package alvin.jpa.secondtable;

import alvin.jpa.core.repositories.BaseRepository;
import alvin.jpa.secondtable.models.Student;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;

@Singleton
public class StudentRepository extends BaseRepository<Student> {

    @Inject
    public StudentRepository(Provider<EntityManager> emProvider) {
        super(emProvider);
    }
}
