package alvin.jpa.secondtable;

import alvin.jpa.core.repositories.BaseRepository;
import alvin.jpa.secondtable.models.StudentBasic;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;

@Singleton
public class StudentBasicRepository extends BaseRepository<StudentBasic> {

    @Inject
    public StudentBasicRepository(Provider<EntityManager> emProvider) {
        super(emProvider);
    }
}
