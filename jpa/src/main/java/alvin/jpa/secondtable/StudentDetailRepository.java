package alvin.jpa.secondtable;

import alvin.jpa.core.repositories.BaseRepository;
import alvin.jpa.secondtable.models.StudentDetail;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;

@Singleton
public class StudentDetailRepository extends BaseRepository<StudentDetail> {

    @Inject
    public StudentDetailRepository(Provider<EntityManager> emProvider) {
        super(emProvider);
    }
}
