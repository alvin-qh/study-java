package alvin.jpa.secondtable.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "secondary_student_detail")
public class StudentDetail implements Serializable {
    @Id
    private Integer id;
    private LocalDate birthday;
    private String address;
    private String email;
    private String qq;

    public StudentDetail() {
    }

    public StudentDetail(int id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }
}
