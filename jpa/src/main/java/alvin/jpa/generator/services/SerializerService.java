package alvin.jpa.generator.services;

import alvin.jpa.generator.repositories.IdentityRepository;
import com.google.inject.persist.Transactional;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class SerializerService {
    private IdentityRepository identityRepository;

    @Inject
    public SerializerService(IdentityRepository identityRepository) {
        this.identityRepository = identityRepository;
    }

    @Transactional
    public Integer nextValue(String business, int step) {
        return identityRepository.nextValue(business, step);
    }
}
