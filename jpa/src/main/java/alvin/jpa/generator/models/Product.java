package alvin.jpa.generator.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "generator_product")
@TableGenerator(name = "productGenerator",
        table = "generator_identity",
        pkColumnName = "business",
        valueColumnName = "value",
        pkColumnValue = "PRODUCT",
        allocationSize = 1)
public class Product implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "productGenerator")
    private Integer id;
    private String name;
    private String serializeCode;
    private BigDecimal price;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerializeCode() {
        return serializeCode;
    }

    public void setSerializeCode(String serializeCode) {
        this.serializeCode = serializeCode;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }
}
