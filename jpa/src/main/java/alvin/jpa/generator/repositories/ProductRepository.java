package alvin.jpa.generator.repositories;

import alvin.jpa.core.repositories.BaseRepository;
import alvin.jpa.generator.models.Product;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import java.util.Optional;

@Singleton
public class ProductRepository extends BaseRepository<Product> {

    @Inject
    public ProductRepository(Provider<EntityManager> emProvider) {
        super(emProvider);
    }

    public Optional<Product> findBySerializeCode(String serializeCode) {
        return findBy("from Product where serializeCode=:serializeCode",
                q -> q.setParameter("serializeCode", serializeCode));
    }
}
