package alvin.jpa.generator.repositories;

import alvin.jpa.core.repositories.BaseRepository;
import alvin.jpa.generator.models.Identity;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;

@Singleton
public class IdentityRepository extends BaseRepository<Identity> {

    private static final int MAX_LOCK_TIMEOUT = 5000;

    @Inject
    public IdentityRepository(Provider<EntityManager> emProvider) {
        super(emProvider);
    }

    public Integer nextValue(String business, int step) {
        Integer value = (Integer) em().createQuery("select value from Identity " +
                "where business=:business")
                .setParameter("business", business)
                .setHint("javax.persistence.lock.timeout", MAX_LOCK_TIMEOUT)
                .setLockMode(LockModeType.PESSIMISTIC_WRITE)
                .getSingleResult();
        em().createQuery("update Identity set value=:newValue " +
                "where value=:oldValue and business=:business")
                .setParameter("newValue", value + step)
                .setParameter("oldValue", value)
                .setParameter("business", business)
                .executeUpdate();
        return value;
    }
}
