package alvin.jpa.generator.repositories;

import alvin.jpa.core.repositories.BaseRepository;
import alvin.jpa.generator.models.Category;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import java.util.List;

@Singleton
public class CategoryRepository extends BaseRepository<Category> {

    @Inject
    public CategoryRepository(Provider<EntityManager> emProvider) {
        super(emProvider);
    }

    public List<Category> findAllRoots() {
        return list("select distinct c from Category c join fetch c.children where c.parent is null");
    }
}
