package alvin.jpa.discriminator;

import alvin.jpa.core.repositories.BaseRepository;
import alvin.jpa.discriminator.models.ItemA;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import java.util.List;

@Singleton
public class ItemARepository extends BaseRepository<ItemA> {

    @Inject
    public ItemARepository(Provider<EntityManager> emProvider) {
        super(emProvider);
    }

    public List<ItemA> findAll() {
        return list("from ItemA order by id");
    }
}
