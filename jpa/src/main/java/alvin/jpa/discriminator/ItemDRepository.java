package alvin.jpa.discriminator;

import alvin.jpa.core.repositories.BaseRepository;
import alvin.jpa.discriminator.models.ItemD;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import java.util.List;

@Singleton
public class ItemDRepository extends BaseRepository<ItemD> {

    @Inject
    public ItemDRepository(Provider<EntityManager> emProvider) {
        super(emProvider);
    }

    public List<ItemD> findAll() {
        List<ItemD> items = list("from MagazineD order by id");
        items.addAll(list("from PhoneD order by id"));
        return items;
    }
}
