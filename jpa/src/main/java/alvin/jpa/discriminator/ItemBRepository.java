package alvin.jpa.discriminator;

import alvin.jpa.core.repositories.BaseRepository;
import alvin.jpa.discriminator.models.ItemB;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import java.util.List;

@Singleton
public class ItemBRepository extends BaseRepository<ItemB> {

    @Inject
    public ItemBRepository(Provider<EntityManager> emProvider) {
        super(emProvider);
    }

    public List<ItemB> findAll() {
        return list("from ItemB order by id");
    }
}
