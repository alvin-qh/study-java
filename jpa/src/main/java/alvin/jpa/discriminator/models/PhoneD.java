package alvin.jpa.discriminator.models;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "discriminator_d_phone")
public class PhoneD extends ItemD {
    private String factory;
    private Float durationTime;

    public String getFactory() {
        return factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public Float getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(Float durationTime) {
        this.durationTime = durationTime;
    }
}
