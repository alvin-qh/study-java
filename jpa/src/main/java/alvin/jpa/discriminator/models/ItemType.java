package alvin.jpa.discriminator.models;

public interface ItemType {
    String ITEM = "ITEM",
            MAGAZINE = "MAGAZINE",
            PHONE = "PHONE";
}
