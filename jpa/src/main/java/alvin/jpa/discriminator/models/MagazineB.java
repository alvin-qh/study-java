package alvin.jpa.discriminator.models;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Extends by join.
 *
 * MagazineB extends from ItemB with one to one join,
 * So table `discri_b_magazine` must be have column ID and fill with same
 * value as ID column of table `discri_b_item` by each related row
 */
@Entity
@Table(name = "discriminator_b_magazine")
@DiscriminatorValue(ItemType.MAGAZINE)
public class MagazineB extends ItemB {
    private String isbn;
    private String publisher;

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
}
