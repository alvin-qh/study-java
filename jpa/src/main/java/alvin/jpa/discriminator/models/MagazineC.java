package alvin.jpa.discriminator.models;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "discriminator_c_magazine")
@DiscriminatorValue(ItemType.MAGAZINE)
public class MagazineC extends ItemC {
    private String isbn;
    private String publisher;

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
}
