package alvin.jpa.discriminator.models;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(ItemType.PHONE)
public class PhoneA extends ItemA {
    private String factory;
    private Float durationTime;

    public String getFactory() {
        return factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public Float getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(Float durationTime) {
        this.durationTime = durationTime;
    }
}
