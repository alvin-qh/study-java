package alvin.jpa.discriminator.models;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "discriminator_d_magazine")
public class MagazineD extends ItemD {
    private String isbn;
    private String publisher;

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
}
