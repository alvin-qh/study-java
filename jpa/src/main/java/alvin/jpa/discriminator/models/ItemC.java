package alvin.jpa.discriminator.models;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "discriminator_c_item")
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue(ItemType.ITEM)
@TableGenerator(name = "discriminatorGenerator",
        table = "generator_identity",
        pkColumnName = "business",
        valueColumnName = "value",
        pkColumnValue = "DISCRIMINATOR",
        allocationSize = 1)
public class ItemC implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "discriminatorGenerator")
    private Integer id;
    private String title;
    private BigDecimal price;
    private String description;

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
