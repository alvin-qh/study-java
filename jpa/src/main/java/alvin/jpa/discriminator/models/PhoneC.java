package alvin.jpa.discriminator.models;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "discriminator_c_phone")
@DiscriminatorValue(ItemType.PHONE)
public class PhoneC extends ItemC {
    private String factory;
    private Float durationTime;

    public String getFactory() {
        return factory;
    }

    public void setFactory(String factory) {
        this.factory = factory;
    }

    public Float getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(Float durationTime) {
        this.durationTime = durationTime;
    }
}
