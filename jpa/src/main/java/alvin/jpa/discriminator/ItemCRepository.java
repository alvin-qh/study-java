package alvin.jpa.discriminator;

import alvin.jpa.core.repositories.BaseRepository;
import alvin.jpa.discriminator.models.ItemC;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import java.util.List;

@Singleton
public class ItemCRepository extends BaseRepository<ItemC> {

    @Inject
    public ItemCRepository(Provider<EntityManager> emProvider) {
        super(emProvider);
    }

    public List<ItemC> findAll() {
        return list("from ItemC order by id");
    }
}
