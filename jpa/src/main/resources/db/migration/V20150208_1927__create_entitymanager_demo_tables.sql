CREATE TABLE em_person (
    id        BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    name      VARCHAR(30) NOT NULL,
    birthday  DATE        NOT NULL,
    gender    CHAR(1)     NOT NULL,
    telephone VARCHAR(50),
    email     VARCHAR(50),
    PRIMARY KEY (id)
);
