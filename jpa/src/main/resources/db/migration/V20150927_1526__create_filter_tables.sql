CREATE TABLE filter_school (
    id   BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(50)     NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE filter_student (
    id         BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    school_id  BIGINT          NOT NULL,
    student_no VARCHAR(50)     NOT NULL,
    name       VARCHAR(50)     NOT NULL,
    gender     CHAR(1)         NOT NULL,
    telephone  VARCHAR(50),
    PRIMARY KEY (id)
);

CREATE TABLE filter_class (
    id        BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    school_id BIGINT          NOT NULL,
    name      VARCHAR(50)     NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE filter_class_student (
    id         BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    class_id   BIGINT          NOT NULL,
    student_id BIGINT          NOT NULL,
    PRIMARY KEY (id)
);
