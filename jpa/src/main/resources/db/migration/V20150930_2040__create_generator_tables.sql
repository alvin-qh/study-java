CREATE TABLE generator_identity (
    id       BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    business VARCHAR(50)     NOT NULL,
    value    INT             NOT NULL,
    PRIMARY KEY (id)
);

CREATE UNIQUE INDEX ix_business
    ON generator_identity (business);

INSERT INTO generator_identity (business, value)
VALUES ('CATEGORY', 1);
INSERT INTO generator_identity (business, value)
VALUES ('PRODUCT', 1);
INSERT INTO generator_identity (business, value)
VALUES ('SERIALIZE_ID', 1);

CREATE TABLE generator_category (
    id        BIGINT UNSIGNED NOT NULL,
    name      VARCHAR(20)     NOT NULL,
    parent_id BIGINT DEFAULT 0,
    PRIMARY KEY (id)
);

CREATE TABLE generator_product (
    id             BIGINT UNSIGNED NOT NULL,
    category_id    BIGINT          NOT NULL,
    name           VARCHAR(20)     NOT NULL,
    serialize_code VARCHAR(11)     NOT NULL,
    price          DECIMAL(10, 2)  NOT NULL,
    PRIMARY KEY (id)
);
