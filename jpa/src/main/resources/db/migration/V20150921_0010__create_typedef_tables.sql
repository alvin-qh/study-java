CREATE TABLE typedef_message (
    id         BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    content    TEXT            NOT NULL,
    created_at TIMESTAMP       NOT NULL DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id)
);
