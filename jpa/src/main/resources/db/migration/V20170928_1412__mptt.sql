CREATE TABLE mptt (
    id   BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(50),
    lft  INT,
    rht  INT,
    PRIMARY KEY (id)
);

CREATE INDEX `ix_left`
    ON mptt (lft);

CREATE INDEX `ix_right`
    ON mptt (rht);
