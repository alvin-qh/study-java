CREATE TABLE relationship_user (
    id              BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    name            VARCHAR(30)     NOT NULL,
    password        VARCHAR(50)     NOT NULL,
    last_login_time TIMESTAMP       NOT NULL,
    manager_id      BIGINT,
    group_id        BIGINT,
    PRIMARY KEY (id)
);

CREATE TABLE relationship_user_info (
    id        BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    user_id   BIGINT          NOT NULL,
    full_name VARCHAR(30)     NOT NULL,
    gender    CHAR(1)         NOT NULL,
    birthday  DATE            NOT NULL,
    telephone VARCHAR(50),
    email     VARCHAR(50),
    remark    VARCHAR(500),
    PRIMARY KEY (id)
);

CREATE TABLE relationship_interest (
    id   BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(50)     NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE relationship_user_interest (
    user_id     BIGINT UNSIGNED NOT NULL,
    interest_id BIGINT UNSIGNED NOT NULL,
    PRIMARY KEY (user_id, interest_id)
);

CREATE TABLE relationship_group (
    id   BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    name VARCHAR(50)     NOT NULL,
    PRIMARY KEY (id)
);
