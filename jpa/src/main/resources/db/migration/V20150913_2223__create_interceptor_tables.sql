CREATE TABLE inter_auth_user (
    id       BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    name     VARCHAR(50)     NOT NULL,
    password VARCHAR(50)     NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE inter_log (
    id         BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    message    VARCHAR(1000)   NOT NULL,
    created_at TIMESTAMP       NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP       NOT NULL DEFAULT CURRENT_TIMESTAMP,
    created_by BIGINT          NOT NULL,
    updated_by BIGINT          NOT NULL,
    PRIMARY KEY (id)
);
