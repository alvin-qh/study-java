CREATE TABLE discriminator_a_item (
    id            BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    type          VARCHAR(50),
    title         VARCHAR(50),
    price         DECIMAL(11, 2),
    description   TEXT,
    duration_time FLOAT,
    factory       VARCHAR(50),
    ISBN          VARCHAR(50),
    publisher     VARCHAR(100),
    PRIMARY KEY (id)
);

CREATE TABLE discriminator_b_item (
    id    BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    type  VARCHAR(50),
    title VARCHAR(50),
    price DECIMAL(11, 2),
    description TEXT,
    PRIMARY KEY (id)
);

CREATE TABLE discriminator_b_magazine (
    id        BIGINT UNSIGNED NOT NULL,
    ISBN      VARCHAR(50),
    publisher VARCHAR(100),
    PRIMARY KEY (id)
);

CREATE TABLE discriminator_b_phone (
    id            BIGINT UNSIGNED NOT NULL,
    duration_time FLOAT,
    factory       VARCHAR(50),
    PRIMARY KEY (id)
);

CREATE TABLE discriminator_c_item (
    id          BIGINT UNSIGNED NOT NULL,
    type        VARCHAR(50),
    title       VARCHAR(50),
    price       DECIMAL(11, 2),
    description TEXT,
    PRIMARY KEY (id)
);

CREATE TABLE discriminator_c_magazine (
    id          BIGINT UNSIGNED NOT NULL,
    type        VARCHAR(50),
    title       VARCHAR(50),
    price       DECIMAL(11, 2),
    description TEXT,
    ISBN        VARCHAR(50),
    publisher   VARCHAR(100),
    PRIMARY KEY (id)
);

CREATE TABLE discriminator_c_phone (
    id            BIGINT UNSIGNED NOT NULL,
    type          VARCHAR(50),
    title         VARCHAR(50),
    price         DECIMAL(11, 2),
    description   TEXT,
    duration_time FLOAT,
    factory       VARCHAR(50),
    PRIMARY KEY (id)
);

INSERT INTO generator_identity (business, value)
VALUES ('DISCRIMINATOR', 1);

CREATE TABLE discriminator_d_magazine (
    id          BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    type        VARCHAR(50),
    title       VARCHAR(50),
    price       DECIMAL(11, 2),
    description TEXT,
    ISBN        VARCHAR(50),
    publisher   VARCHAR(100),
    PRIMARY KEY (id)
);

CREATE TABLE discriminator_d_phone (
    id            BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    type          VARCHAR(50),
    title         VARCHAR(50),
    price         DECIMAL(11, 2),
    description   TEXT,
    duration_time FLOAT,
    factory       VARCHAR(50),
    PRIMARY KEY (id)
);
