CREATE TABLE secondary_student (
    id        BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    sno       VARCHAR(50)     NOT NULL,
    name      VARCHAR(30)     NOT NULL,
    gender    CHAR(1)         NOT NULL,
    telephone VARCHAR(50),
    PRIMARY KEY (id)
);

CREATE TABLE secondary_student_detail (
    id       BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    birthday DATE,
    address  VARCHAR(200),
    email    VARCHAR(100),
    qq       VARCHAR(50),
    PRIMARY KEY (id)
);
