CREATE TABLE lock_optimistic_lock (
    id          BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    total_count INT             NOT NULL,
    left_count  INT             NOT NULL,
    version     INT             NOT NULL DEFAULT 1,
    PRIMARY KEY (id)
);

CREATE TABLE lock_pessimistic_lock (
    id          BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    total_count INT             NOT NULL,
    left_count  INT             NOT NULL,
    PRIMARY KEY (id)
);
