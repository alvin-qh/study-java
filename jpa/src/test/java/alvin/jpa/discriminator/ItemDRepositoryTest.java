package alvin.jpa.discriminator;

import alvin.jpa.core.BaseTest;
import alvin.jpa.discriminator.builders.MagazineDBuilder;
import alvin.jpa.discriminator.builders.PhoneDBuilder;
import alvin.jpa.discriminator.models.ItemD;
import alvin.jpa.discriminator.models.MagazineD;
import alvin.jpa.discriminator.models.PhoneD;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.typeCompatibleWith;

class ItemDRepositoryTest extends BaseTest {

    @Inject
    private ItemDRepository itemDRepository;

    @BeforeEach
    void seedData() {
        try (Transaction ignore = beginTransaction()) {
            withBuilder(MagazineDBuilder.class).create();
            withBuilder(PhoneDBuilder.class).create();
        }
    }

    @Test
    void test_find_different_item() {
        List<ItemD> items = itemDRepository.findAll();
        assertThat(items.size(), is(2));

        assertThat(items.get(0).getClass(), typeCompatibleWith(MagazineD.class));
        assertThat(items.get(0).getTitle(), is("Science"));
        assertThat(items.get(0).getPrice().doubleValue(), is(19.9));
        assertThat(items.get(0).getDescription(), is("Published in Xi'an"));
        assertThat(((MagazineD) items.get(0)).getIsbn(), is("333-221-123"));
        assertThat(((MagazineD) items.get(0)).getPublisher(), is("IBM"));

        assertThat(items.get(1).getClass(), typeCompatibleWith(PhoneD.class));
        assertThat(items.get(1).getTitle(), is("iPhone 6s"));
        assertThat(items.get(1).getPrice().doubleValue(), is(5288.0));
        assertThat(items.get(1).getDescription(), is("Newest Apple phone"));
        assertThat(((PhoneD) items.get(1)).getFactory(), is("Apple"));
        assertThat(((PhoneD) items.get(1)).getDurationTime(), is(200f));
    }
}
