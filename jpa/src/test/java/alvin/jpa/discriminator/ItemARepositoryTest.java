package alvin.jpa.discriminator;

import alvin.jpa.core.BaseTest;
import alvin.jpa.discriminator.builders.ItemABuilder;
import alvin.jpa.discriminator.builders.MagazineABuilder;
import alvin.jpa.discriminator.builders.PhoneABuilder;
import alvin.jpa.discriminator.models.ItemA;
import alvin.jpa.discriminator.models.MagazineA;
import alvin.jpa.discriminator.models.PhoneA;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.typeCompatibleWith;

class ItemARepositoryTest extends BaseTest {

    @Inject
    private ItemARepository itemARepository;

    @BeforeEach
    void seedData() {
        try (Transaction ignore = beginTransaction()) {
            withBuilder(ItemABuilder.class).create();
            withBuilder(MagazineABuilder.class).create();
            withBuilder(PhoneABuilder.class).create();
        }
    }

    @Test
    void test_find_different_item() {
        List<ItemA> items = itemARepository.findAll();
        assertThat(items.size(), is(3));
        assertThat(items.get(0).getClass(), typeCompatibleWith(ItemA.class));
        assertThat(items.get(0).getTitle(), is("Computer"));
        assertThat(items.get(0).getPrice().doubleValue(), is(5000.0));
        assertThat(items.get(0).getDescription(), is("Made by IBM"));

        assertThat(items.get(1).getClass(), typeCompatibleWith(MagazineA.class));
        assertThat(items.get(1).getTitle(), is("Science"));
        assertThat(items.get(1).getPrice().doubleValue(), is(19.9));
        assertThat(items.get(1).getDescription(), is("Published in Xi'an"));
        assertThat(((MagazineA) items.get(1)).getIsbn(), is("333-221-123"));
        assertThat(((MagazineA) items.get(1)).getPublisher(), is("IBM"));

        assertThat(items.get(2).getClass(), typeCompatibleWith(PhoneA.class));
        assertThat(items.get(2).getTitle(), is("iPhone 6s"));
        assertThat(items.get(2).getPrice().doubleValue(), is(5288.0));
        assertThat(items.get(2).getDescription(), is("Newest Apple phone"));
        assertThat(((PhoneA) items.get(2)).getFactory(), is("Apple"));
        assertThat(((PhoneA) items.get(2)).getDurationTime(), is(200f));
    }
}
