package alvin.jpa.discriminator;

import alvin.jpa.core.BaseTest;
import alvin.jpa.discriminator.builders.ItemBBuilder;
import alvin.jpa.discriminator.builders.MagazineBBuilder;
import alvin.jpa.discriminator.builders.PhoneBBuilder;
import alvin.jpa.discriminator.models.ItemB;
import alvin.jpa.discriminator.models.MagazineB;
import alvin.jpa.discriminator.models.PhoneB;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.typeCompatibleWith;

class ItemBRepositoryTest extends BaseTest {

    @Inject
    private ItemBRepository itemBRepository;

    @BeforeEach
    void seedData() {
        try (Transaction ignore = beginTransaction()) {
            withBuilder(ItemBBuilder.class).create();
            withBuilder(MagazineBBuilder.class).create();
            withBuilder(PhoneBBuilder.class).create();
        }
    }

    @Test
    void test_find_different_item() {
        List<ItemB> items = itemBRepository.findAll();
        assertThat(items.size(), is(3));
        assertThat(items.get(0).getClass(), typeCompatibleWith(ItemB.class));
        assertThat(items.get(0).getTitle(), is("Computer"));
        assertThat(items.get(0).getPrice().doubleValue(), is(5000.0));
        assertThat(items.get(0).getDescription(), is("Made by IBM"));

        assertThat(items.get(1).getClass(), typeCompatibleWith(MagazineB.class));
        assertThat(items.get(1).getTitle(), is("Science"));
        assertThat(items.get(1).getPrice().doubleValue(), is(19.9));
        assertThat(items.get(1).getDescription(), is("Published in Xi'an"));
        assertThat(((MagazineB) items.get(1)).getIsbn(), is("333-221-123"));
        assertThat(((MagazineB) items.get(1)).getPublisher(), is("IBM"));

        assertThat(items.get(2).getClass(), typeCompatibleWith(PhoneB.class));
        assertThat(items.get(2).getTitle(), is("iPhone 6s"));
        assertThat(items.get(2).getPrice().doubleValue(), is(5288.0));
        assertThat(items.get(2).getDescription(), is("Newest Apple phone"));
        assertThat(((PhoneB) items.get(2)).getFactory(), is("Apple"));
        assertThat(((PhoneB) items.get(2)).getDurationTime(), is(200f));
    }
}
