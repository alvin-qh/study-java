package alvin.jpa.discriminator;

import alvin.jpa.core.BaseTest;
import alvin.jpa.discriminator.builders.ItemCBuilder;
import alvin.jpa.discriminator.builders.MagazineCBuilder;
import alvin.jpa.discriminator.builders.PhoneCBuilder;
import alvin.jpa.discriminator.models.ItemC;
import alvin.jpa.discriminator.models.MagazineC;
import alvin.jpa.discriminator.models.PhoneC;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.typeCompatibleWith;

class ItemCRepositoryTest extends BaseTest {

    @Inject
    private ItemCRepository itemCRepository;

    @BeforeEach
    void seedData() {
        try (Transaction ignore = beginTransaction()) {
            withBuilder(ItemCBuilder.class).create();
            withBuilder(MagazineCBuilder.class).create();
            withBuilder(PhoneCBuilder.class).create();
        }
    }

    @Test
    void test_find_different_item() {
        List<ItemC> items = itemCRepository.findAll();
        assertThat(items.size(), is(3));
        assertThat(items.get(0).getClass(), typeCompatibleWith(ItemC.class));
        assertThat(items.get(0).getTitle(), is("Computer"));
        assertThat(items.get(0).getPrice().doubleValue(), is(5000.0));
        assertThat(items.get(0).getDescription(), is("Made by IBM"));

        assertThat(items.get(1).getClass(), typeCompatibleWith(MagazineC.class));
        assertThat(items.get(1).getTitle(), is("Science"));
        assertThat(items.get(1).getPrice().doubleValue(), is(19.9));
        assertThat(items.get(1).getDescription(), is("Published in Xi'an"));
        assertThat(((MagazineC) items.get(1)).getIsbn(), is("333-221-123"));
        assertThat(((MagazineC) items.get(1)).getPublisher(), is("IBM"));

        assertThat(items.get(2).getClass(), typeCompatibleWith(PhoneC.class));
        assertThat(items.get(2).getTitle(), is("iPhone 6s"));
        assertThat(items.get(2).getPrice().doubleValue(), is(5288.0));
        assertThat(items.get(2).getDescription(), is("Newest Apple phone"));
        assertThat(((PhoneC) items.get(2)).getFactory(), is("Apple"));
        assertThat(((PhoneC) items.get(2)).getDurationTime(), is(200f));
    }
}
