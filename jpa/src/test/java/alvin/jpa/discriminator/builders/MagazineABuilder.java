package alvin.jpa.discriminator.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.discriminator.models.MagazineA;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;

public class MagazineABuilder extends BaseItemBuilder<MagazineABuilder> implements Builder<MagazineA> {
    private String isbn = "333-221-123";
    private String publisher = "IBM";

    @Inject
    public MagazineABuilder(EntityManager em) {
        super(em);
        title = "Science";
        price = BigDecimal.valueOf(19.9);
        description = "Published in Xi'an";
    }

    public MagazineABuilder isbn(String isbn) {
        this.isbn = isbn;
        return this;
    }

    public MagazineABuilder publisher(String publisher) {
        this.publisher = publisher;
        return this;
    }

    @Override
    public MagazineA build() {
        MagazineA magazine = new MagazineA();
        magazine.setTitle(title);
        magazine.setDescription(description);
        magazine.setPrice(price);
        magazine.setIsbn(isbn);
        magazine.setPublisher(publisher);
        return magazine;
    }

    @Override
    public MagazineA create() {
        MagazineA magazine = build();
        em.persist(magazine);
        return magazine;
    }
}
