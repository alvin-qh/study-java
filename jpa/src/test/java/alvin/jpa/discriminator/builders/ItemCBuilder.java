package alvin.jpa.discriminator.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.discriminator.models.ItemC;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;

public class ItemCBuilder extends BaseItemBuilder<ItemCBuilder> implements Builder<ItemC> {

    @Inject
    public ItemCBuilder(EntityManager em) {
        super(em);
        title = "Computer";
        price = BigDecimal.valueOf(5000.0);
        description = "Made by IBM";
    }

    @Override
    public ItemC build() {
        ItemC item = new ItemC();
        item.setTitle(title);
        item.setDescription(description);
        item.setPrice(price);
        return item;
    }

    @Override
    public ItemC create() {
        ItemC item = build();
        em.persist(item);
        return item;
    }
}
