package alvin.jpa.discriminator.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.discriminator.models.PhoneA;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;

public class PhoneABuilder extends BaseItemBuilder<PhoneABuilder> implements Builder<PhoneA> {
    private String factory = "Apple";
    private Float durationTime = 200f;

    @Inject
    public PhoneABuilder(EntityManager em) {
        super(em);
        title = "iPhone 6s";
        price = BigDecimal.valueOf(5288.0);
        description = "Newest Apple phone";
    }

    public PhoneABuilder factory(String factory) {
        this.factory = factory;
        return this;
    }

    public PhoneABuilder durationTime(Float durationTime) {
        this.durationTime = durationTime;
        return this;
    }

    @Override
    public PhoneA build() {
        PhoneA phone = new PhoneA();
        phone.setTitle(title);
        phone.setDescription(description);
        phone.setPrice(price);
        phone.setFactory(factory);
        phone.setDurationTime(durationTime);
        return phone;
    }

    @Override
    public PhoneA create() {
        PhoneA phone = build();
        em.persist(phone);
        return phone;
    }
}
