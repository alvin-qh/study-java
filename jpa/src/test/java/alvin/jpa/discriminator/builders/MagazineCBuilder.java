package alvin.jpa.discriminator.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.discriminator.models.MagazineC;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;

public class MagazineCBuilder extends BaseItemBuilder<MagazineCBuilder> implements Builder<MagazineC> {
    private String isbn = "333-221-123";
    private String publisher = "IBM";

    @Inject
    public MagazineCBuilder(EntityManager em) {
        super(em);
        title = "Science";
        price = BigDecimal.valueOf(19.9);
        description = "Published in Xi'an";
    }

    public MagazineCBuilder isbn(String isbn) {
        this.isbn = isbn;
        return this;
    }

    public MagazineCBuilder publisher(String publisher) {
        this.publisher = publisher;
        return this;
    }

    @Override
    public MagazineC build() {
        MagazineC magazine = new MagazineC();
        magazine.setTitle(title);
        magazine.setDescription(description);
        magazine.setPrice(price);
        magazine.setIsbn(isbn);
        magazine.setPublisher(publisher);
        return magazine;
    }

    @Override
    public MagazineC create() {
        MagazineC magazine = build();
        em.persist(magazine);
        return magazine;
    }
}
