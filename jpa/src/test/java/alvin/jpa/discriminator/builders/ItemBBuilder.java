package alvin.jpa.discriminator.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.discriminator.models.ItemB;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;

public class ItemBBuilder extends BaseItemBuilder<ItemBBuilder> implements Builder<ItemB> {

    @Inject
    public ItemBBuilder(EntityManager em) {
        super(em);
        title = "Computer";
        price = BigDecimal.valueOf(5000.0);
        description = "Made by IBM";
    }

    @Override
    public ItemB build() {
        ItemB item = new ItemB();
        item.setTitle(title);
        item.setDescription(description);
        item.setPrice(price);
        return item;
    }

    @Override
    public ItemB create() {
        ItemB item = build();
        em.persist(item);
        return item;
    }
}
