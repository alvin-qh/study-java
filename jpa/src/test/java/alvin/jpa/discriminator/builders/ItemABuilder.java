package alvin.jpa.discriminator.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.discriminator.models.ItemA;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;

public class ItemABuilder extends BaseItemBuilder<ItemABuilder> implements Builder<ItemA> {

    @Inject
    public ItemABuilder(EntityManager em) {
        super(em);
        title = "Computer";
        price = BigDecimal.valueOf(5000.0);
        description = "Made by IBM";
    }

    @Override
    public ItemA build() {
        ItemA item = new ItemA();
        item.setTitle(title);
        item.setDescription(description);
        item.setPrice(price);
        return item;
    }

    @Override
    public ItemA create() {
        ItemA item = build();
        em.persist(item);
        return item;
    }
}
