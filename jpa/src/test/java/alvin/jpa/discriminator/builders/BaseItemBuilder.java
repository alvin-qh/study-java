package alvin.jpa.discriminator.builders;

import javax.persistence.EntityManager;
import java.math.BigDecimal;

@SuppressWarnings("unchecked")
public abstract class BaseItemBuilder<T extends BaseItemBuilder> {
    protected EntityManager em;
    protected String title;
    protected BigDecimal price;
    protected String description;

    public BaseItemBuilder(EntityManager em) {
        this.em = em;
    }

    public T title(String title) {
        this.title = title;
        return (T) this;
    }

    public T price(BigDecimal price) {
        this.price = price;
        return (T) this;
    }

    public T description(String description) {
        this.description = description;
        return (T) this;
    }
}
