package alvin.jpa.discriminator.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.discriminator.models.PhoneB;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;

public class PhoneBBuilder extends BaseItemBuilder<PhoneBBuilder> implements Builder<PhoneB> {
    private String factory = "Apple";
    private Float durationTime = 200f;

    @Inject
    public PhoneBBuilder(EntityManager em) {
        super(em);
        title = "iPhone 6s";
        price = BigDecimal.valueOf(5288.0);
        description = "Newest Apple phone";
    }

    public PhoneBBuilder factory(String factory) {
        this.factory = factory;
        return this;
    }

    public PhoneBBuilder durationTime(Float durationTime) {
        this.durationTime = durationTime;
        return this;
    }

    @Override
    public PhoneB build() {
        PhoneB phone = new PhoneB();
        phone.setTitle(title);
        phone.setDescription(description);
        phone.setPrice(price);
        phone.setFactory(factory);
        phone.setDurationTime(durationTime);
        return phone;
    }

    @Override
    public PhoneB create() {
        PhoneB phone = build();
        em.persist(phone);
        return phone;
    }
}
