package alvin.jpa.discriminator.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.discriminator.models.PhoneC;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;

public class PhoneCBuilder extends BaseItemBuilder<PhoneCBuilder> implements Builder<PhoneC> {
    private String factory = "Apple";
    private Float durationTime = 200f;

    @Inject
    public PhoneCBuilder(EntityManager em) {
        super(em);
        title = "iPhone 6s";
        price = BigDecimal.valueOf(5288.0);
        description = "Newest Apple phone";
    }

    public PhoneCBuilder factory(String factory) {
        this.factory = factory;
        return this;
    }

    public PhoneCBuilder durationTime(Float durationTime) {
        this.durationTime = durationTime;
        return this;
    }

    @Override
    public PhoneC build() {
        PhoneC phone = new PhoneC();
        phone.setTitle(title);
        phone.setDescription(description);
        phone.setPrice(price);
        phone.setFactory(factory);
        phone.setDurationTime(durationTime);
        return phone;
    }

    @Override
    public PhoneC create() {
        PhoneC phone = build();
        em.persist(phone);
        return phone;
    }
}
