package alvin.jpa.discriminator.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.discriminator.models.PhoneD;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;

public class PhoneDBuilder extends BaseItemBuilder<PhoneDBuilder> implements Builder<PhoneD> {
    private String factory = "Apple";
    private Float durationTime = 200f;

    @Inject
    public PhoneDBuilder(EntityManager em) {
        super(em);
        title = "iPhone 6s";
        price = BigDecimal.valueOf(5288.0);
        description = "Newest Apple phone";
    }

    public PhoneDBuilder factory(String factory) {
        this.factory = factory;
        return this;
    }

    public PhoneDBuilder durationTime(Float durationTime) {
        this.durationTime = durationTime;
        return this;
    }

    @Override
    public PhoneD build() {
        PhoneD phone = new PhoneD();
        phone.setTitle(title);
        phone.setDescription(description);
        phone.setPrice(price);
        phone.setFactory(factory);
        phone.setDurationTime(durationTime);
        return phone;
    }

    @Override
    public PhoneD create() {
        PhoneD phone = build();
        em.persist(phone);
        return phone;
    }
}
