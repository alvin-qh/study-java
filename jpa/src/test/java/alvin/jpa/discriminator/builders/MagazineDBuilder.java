package alvin.jpa.discriminator.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.discriminator.models.MagazineD;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;

public class MagazineDBuilder extends BaseItemBuilder<MagazineDBuilder> implements Builder<MagazineD> {
    private String isbn = "333-221-123";
    private String publisher = "IBM";

    @Inject
    public MagazineDBuilder(EntityManager em) {
        super(em);
        title = "Science";
        price = BigDecimal.valueOf(19.9);
        description = "Published in Xi'an";
    }

    public MagazineDBuilder isbn(String isbn) {
        this.isbn = isbn;
        return this;
    }

    public MagazineDBuilder publisher(String publisher) {
        this.publisher = publisher;
        return this;
    }

    @Override
    public MagazineD build() {
        MagazineD magazine = new MagazineD();
        magazine.setTitle(title);
        magazine.setDescription(description);
        magazine.setPrice(price);
        magazine.setIsbn(isbn);
        magazine.setPublisher(publisher);
        return magazine;
    }

    @Override
    public MagazineD create() {
        MagazineD magazine = build();
        em.persist(magazine);
        return magazine;
    }
}
