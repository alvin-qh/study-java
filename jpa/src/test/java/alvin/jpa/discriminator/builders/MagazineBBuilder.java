package alvin.jpa.discriminator.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.discriminator.models.MagazineB;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;

public class MagazineBBuilder extends BaseItemBuilder<MagazineBBuilder> implements Builder<MagazineB> {
    private String isbn = "333-221-123";
    private String publisher = "IBM";

    @Inject
    public MagazineBBuilder(EntityManager em) {
        super(em);
        title = "Science";
        price = BigDecimal.valueOf(19.9);
        description = "Published in Xi'an";
    }

    public MagazineBBuilder isbn(String isbn) {
        this.isbn = isbn;
        return this;
    }

    public MagazineBBuilder publisher(String publisher) {
        this.publisher = publisher;
        return this;
    }

    @Override
    public MagazineB build() {
        MagazineB magazine = new MagazineB();
        magazine.setTitle(title);
        magazine.setDescription(description);
        magazine.setPrice(price);
        magazine.setIsbn(isbn);
        magazine.setPublisher(publisher);
        return magazine;
    }

    @Override
    public MagazineB create() {
        MagazineB magazine = build();
        em.persist(magazine);
        return magazine;
    }
}
