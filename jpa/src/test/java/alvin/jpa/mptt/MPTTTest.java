package alvin.jpa.mptt;

import alvin.jpa.core.BaseTest;
import alvin.jpa.core.json.ObjectMapperBuilder;
import alvin.jpa.mptt.builder.MPTTBuilder;
import alvin.jpa.mptt.models.MPTT;
import alvin.jpa.mptt.models.Node;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;

class MPTTTest extends BaseTest {

    @Inject
    private MPTTRepository mpttRepository;

    @Test
    void test_create_mptt_tree() throws Exception {
        try (Transaction ignore = beginTransaction()) {
            MPTT node1 = withBuilder(MPTTBuilder.class).name("n1").create();

            MPTT node2 = withBuilder(MPTTBuilder.class).name("n2").build();
            mpttRepository.addNode(node1, node2);

            MPTT node5 = withBuilder(MPTTBuilder.class).name("n5").build();
            mpttRepository.addNode(node2, node5);

            MPTT node3 = withBuilder(MPTTBuilder.class).name("n3").build();
            mpttRepository.addNode(node1, node3);

            MPTT node6 = withBuilder(MPTTBuilder.class).name("n6").build();
            mpttRepository.addNode(node1, node6);

            MPTT node4 = withBuilder(MPTTBuilder.class).name("n4").build();
            mpttRepository.addNode(node3, node4);
        }
        em().clear();

        List<MPTT> nodes = mpttRepository.findAll();
        Node root = Node.from(nodes);

        ObjectMapper objectMapper = ObjectMapperBuilder.create();
        String json = objectMapper.writeValueAsString(root);
        System.out.println(json);
    }
}
