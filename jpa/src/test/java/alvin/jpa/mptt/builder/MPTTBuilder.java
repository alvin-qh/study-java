package alvin.jpa.mptt.builder;

import alvin.jpa.core.Builder;
import alvin.jpa.mptt.models.MPTT;

import javax.inject.Inject;
import javax.persistence.EntityManager;

public class MPTTBuilder implements Builder<MPTT> {

    @Inject
    private EntityManager em;

    private String name = "ROOT";
    private int left = 1;
    private int right = 2;

    @Override
    public MPTT build() {
        MPTT mptt = new MPTT();
        mptt.setName(name);
        mptt.setLeft(left);
        mptt.setRight(right);
        return mptt;
    }

    @Override
    public MPTT create() {
        MPTT mptt = build();
        em.persist(mptt);
        return mptt;
    }

    public MPTTBuilder name(String name) {
        this.name = name;
        return this;
    }

    public MPTTBuilder left(int left) {
        this.left = left;
        return this;
    }

    public MPTTBuilder right(int right) {
        this.right = right;
        return this;
    }
}
