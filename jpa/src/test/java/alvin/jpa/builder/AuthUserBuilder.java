package alvin.jpa.builder;

import alvin.jpa.core.Builder;
import alvin.jpa.interceptors.models.AuthUser;

import javax.inject.Inject;
import javax.persistence.EntityManager;

public class AuthUserBuilder implements Builder<AuthUser> {
    private String name = "admin";
    private String password = "password";

    private EntityManager em;

    @Inject
    public AuthUserBuilder(EntityManager em) {
        this.em = em;
    }

    @Override
    public AuthUser build() {
        AuthUser au = new AuthUser();
        au.setName(name);
        au.setPassword(password);
        return au;
    }

    @Override
    public AuthUser create() {
        AuthUser au = build();
        em.persist(au);
        return au;
    }

    public AuthUserBuilder name(String name) {
        this.name = name;
        return this;
    }

    public AuthUserBuilder password(String password) {
        this.password = password;
        return this;
    }
}
