package alvin.jpa.typedef;

import alvin.jpa.core.BaseTest;
import alvin.jpa.typedef.models.Message;
import alvin.jpa.typedef.models.MessageContent;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class MessageRepositoryTest extends BaseTest {

    @Inject
    private MessageRepository messageRepository;

    @Test
    void test_save_user() {
        int id;
        LocalDateTime savedTime = LocalDateTime.now(ZoneOffset.UTC);
        try (Transaction ignore = beginTransaction()) {
            MessageContent mc = new MessageContent();
            mc.setTitle("Test Title");
            mc.setContent("Test Content");

            Message message = new Message();
            message.setCreatedAt(savedTime);
            message.setContent(mc);
            messageRepository.save(message);
            id = message.getId();
        }

        Optional<Message> mayMessage = messageRepository.find(id);
        assertThat(mayMessage.isPresent(), is(true));

        LocalDateTime loginTime = mayMessage.get().getCreatedAt();
        assertThat(loginTime, is(savedTime));
    }
}
