package alvin.jpa.generator;

import alvin.jpa.core.BaseTest;
import alvin.jpa.generator.builders.CategoryBuilder;
import alvin.jpa.generator.models.Category;
import alvin.jpa.generator.repositories.CategoryRepository;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class CategoryRepositoryTest extends BaseTest {

    @Inject
    private CategoryRepository categoryRepository;

    @Test
    void test_find_parent() {
        try (Transaction ignore = beginTransaction()) {
            Category root = withBuilder(CategoryBuilder.class).name("R1").create();
            withBuilder(CategoryBuilder.class).name("C01").parent(root).create();
            withBuilder(CategoryBuilder.class).name("C02").parent(root).create();

            root = withBuilder(CategoryBuilder.class).name("R2").create();
            withBuilder(CategoryBuilder.class).name("C11").parent(root).create();
            withBuilder(CategoryBuilder.class).name("C12").parent(root).create();
        }
        clear();

        List<Category> roots = categoryRepository.findAllRoots();
        assertThat(roots.size(), is(2));

        Category r1 = roots.get(0);
        assertThat(r1.getName(), is("R1"));
        assertThat(r1.getChildren().size(), is(2));
        assertThat(r1.getChildren().get(0).getName(), is("C01"));
        assertThat(r1.getChildren().get(0).getParent(), is(r1));
        assertThat(r1.getChildren().get(1).getName(), is("C02"));
        assertThat(r1.getChildren().get(1).getParent(), is(r1));

        Category r2 = roots.get(1);
        assertThat(r2.getName(), is("R2"));
        assertThat(r2.getChildren().size(), is(2));
        assertThat(r2.getChildren().get(0).getName(), is("C11"));
        assertThat(r2.getChildren().get(0).getParent(), is(r2));
        assertThat(r2.getChildren().get(1).getName(), is("C12"));
        assertThat(r2.getChildren().get(1).getParent(), is(r2));
    }
}
