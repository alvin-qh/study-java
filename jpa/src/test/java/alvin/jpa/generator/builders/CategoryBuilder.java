package alvin.jpa.generator.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.generator.models.Category;
import alvin.jpa.generator.models.Product;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class CategoryBuilder implements Builder<Category> {

    private EntityManager em;

    private String name = "category-001";
    private Category parent = null;
    private List<Product> products = new ArrayList<>();

    @Inject
    public CategoryBuilder(EntityManager em) {
        this.em = em;
    }

    public CategoryBuilder name(String name) {
        this.name = name;
        return this;
    }

    public CategoryBuilder parent(Category parent) {
        this.parent = parent;
        return this;
    }

    public CategoryBuilder addProducts(Product product) {
        products.add(product);
        return this;
    }

    @Override
    public Category build() {
        Category category = new Category();
        category.setName(name);
        category.setParent(parent);
        category.setProducts(products);
        return category;
    }

    @Override
    public Category create() {
        Category category = build();
        em.persist(category);
        return category;
    }
}
