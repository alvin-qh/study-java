package alvin.jpa.generator.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.generator.models.Category;
import alvin.jpa.generator.models.Product;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;

public class ProductBuilder implements Builder<Product> {

    private EntityManager em;

    private String name = "Apple iphone 6s";
    private BigDecimal price = BigDecimal.valueOf(5288d);
    private String serializeCode;
    private Category category;

    @Inject
    public ProductBuilder(EntityManager em) {
        this.em = em;
    }

    public ProductBuilder name(String name) {
        this.name = name;
        return this;
    }

    public ProductBuilder price(BigDecimal price) {
        this.price = price;
        return this;
    }

    public ProductBuilder serializeCode(String serializeCode) {
        this.serializeCode = serializeCode;
        return this;
    }

    public ProductBuilder category(Category category) {
        this.category = category;
        return this;
    }

    @Override
    public Product build() {
        Product product = new Product();
        product.setName(name);
        product.setPrice(price);
        product.setCategory(category);
        product.setSerializeCode(serializeCode);
        return product;
    }

    @Override
    public Product create() {
        Product product = build();
        em.persist(product);
        return product;
    }
}
