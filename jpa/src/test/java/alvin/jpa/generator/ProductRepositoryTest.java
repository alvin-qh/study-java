package alvin.jpa.generator;

import alvin.jpa.core.BaseTest;
import alvin.jpa.generator.builders.CategoryBuilder;
import alvin.jpa.generator.builders.ProductBuilder;
import alvin.jpa.generator.models.Product;
import alvin.jpa.generator.repositories.ProductRepository;
import alvin.jpa.generator.services.SerializerService;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;

class ProductRepositoryTest extends BaseTest {

    @Inject
    private SerializerService serializerService;

    @Inject
    private ProductRepository productRepository;

    @Test
    void test_add_new_product() {
        String serializeCode = String.format("%010d", serializerService.nextValue("PRODUCT", 1));
        try (Transaction ignore = beginTransaction()) {
            withBuilder(ProductBuilder.class)
                    .category(withBuilder(CategoryBuilder.class).create())
                    .serializeCode(serializeCode)
                    .create();
        }
        clear();

        Optional<Product> mayProduct = productRepository.findBySerializeCode(serializeCode);
        assertThat(mayProduct.isPresent(), is(true));

        Product product = mayProduct.get();
        assertThat(product.getCategory().getParent(), is(nullValue()));
        assertThat(product.getCategory().getName(), is("category-001"));
        assertThat(product.getCategory().getProducts().size(), is(1));
        assertThat(product.getCategory().getProducts().get(0), is(product));

        assertThat(product.getName(), is("Apple iphone 6s"));
        assertThat(product.getPrice().doubleValue(), is(5288d));
    }
}
