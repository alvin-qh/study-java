package alvin.jpa.lock;

import alvin.jpa.core.BaseTest;
import alvin.jpa.lock.models.OptimisticLock;
import alvin.jpa.lock.models.PessimisticLock;
import com.google.inject.persist.UnitOfWork;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.persistence.LockModeType;
import javax.persistence.OptimisticLockException;
import javax.persistence.RollbackException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class LockRepositoryTest extends BaseTest {

    @Inject
    private OptimisticLockRepository optimisticLockRepository;

    @Inject
    private PessimisticLockRepository pessimisticLockRepository;

    @Inject
    private UnitOfWork unitOfWork;

    @Test
    void test_version() {
        int id;
        try (Transaction ignore = beginTransaction()) {
            OptimisticLock lock = new OptimisticLock();
            lock.setTotalCount(100);
            lock.setLeftCount(100);

            optimisticLockRepository.save(lock);
            id = lock.getId();
        }

        try (Transaction ignore = beginTransaction()) {
            Optional<OptimisticLock> mayLock = optimisticLockRepository.find(id);
            assertThat(mayLock.isPresent(), is(true));

            OptimisticLock lock = mayLock.get();
            lock.setLeftCount(lock.getLeftCount() - 1);
        }

        clear();
        Optional<OptimisticLock> mayLock = optimisticLockRepository.find(id);
        assertThat(mayLock.isPresent(), is(true));

        OptimisticLock lock = mayLock.get();
        assertThat(lock.getLeftCount(), is(99));
        assertThat(lock.getVersion(), is(2));
    }

    @Test
    void test_optimistic_lock() throws Exception {
        int id;
        try (Transaction ignore = beginTransaction()) {
            OptimisticLock lock = new OptimisticLock();
            lock.setTotalCount(100);
            lock.setLeftCount(100);

            optimisticLockRepository.save(lock);
            id = lock.getId();
        }

        final AtomicInteger atom = new AtomicInteger(0);
        final boolean[] exceptRaised = {false};

        Thread t = new Thread(() -> {
            unitOfWork.begin();
            try (Transaction ignore = beginTransaction()) {
                Optional<OptimisticLock> mayLock = optimisticLockRepository.find(id);
                assertThat(mayLock.isPresent(), is(true));
                if (atom.incrementAndGet() == 2) {
                    doNotify();
                } else {
                    doWait();
                }
                OptimisticLock lock = mayLock.get();
                lock.setLeftCount(lock.getLeftCount() - 1);
            } catch (RollbackException e) {
                if (e.getCause() instanceof OptimisticLockException) {
                    exceptRaised[0] = true;
                }
            } finally {
                unitOfWork.end();
            }
        });
        t.start();

        try (Transaction ignore = beginTransaction()) {
            Optional<OptimisticLock> mayLock = optimisticLockRepository.find(id);
            assertThat(mayLock.isPresent(), is(true));
            if (atom.incrementAndGet() == 2) {
                doNotify();
            } else {
                doWait();
            }
            OptimisticLock lock = mayLock.get();
            lock.setLeftCount(lock.getLeftCount() - 1);
        } catch (RollbackException e) {
            if (e.getCause() instanceof OptimisticLockException) {
                exceptRaised[0] = true;
            }
        }
        t.join();

        clear();
        assertThat(exceptRaised[0], is(true));

        Optional<OptimisticLock> mayLock = optimisticLockRepository.find(id);
        assertThat(mayLock.isPresent(), is(true));
        assertThat(mayLock.get().getLeftCount(), is(99));
    }

    private synchronized void doWait() {
        try {
            this.wait();
        } catch (InterruptedException ignore) {
        }
    }

    private synchronized void doNotify() {
        this.notify();
    }

    @Test
    void test_pessimistic_lock() throws Exception {
        int id;
        try (Transaction ignore = beginTransaction()) {
            PessimisticLock lock = new PessimisticLock();
            lock.setTotalCount(100);
            lock.setLeftCount(100);

            pessimisticLockRepository.save(lock);
            id = lock.getId();
        }
        clear();

        Thread t = new Thread(() -> {
            unitOfWork.begin();
            try (Transaction ignore = beginTransaction()) {
                Optional<PessimisticLock> mayLock = pessimisticLockRepository.find(id, LockModeType.PESSIMISTIC_WRITE);
                assertThat(mayLock.isPresent(), is(true));

                PessimisticLock lock = mayLock.get();
                System.out.println(lock.getLeftCount());
                lock.setLeftCount(lock.getLeftCount() - 1);
            } finally {
                unitOfWork.end();
            }
        });
        t.start();

        try (Transaction ignore = beginTransaction()) {
            Optional<PessimisticLock> mayLock = pessimisticLockRepository.find(id, LockModeType.PESSIMISTIC_WRITE);
            assertThat(mayLock.isPresent(), is(true));

            PessimisticLock lock = mayLock.get();
            System.out.println(lock.getLeftCount());
            lock.setLeftCount(lock.getLeftCount() - 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
        t.join();

        clear();
        Optional<PessimisticLock> mayLock = pessimisticLockRepository.find(id);
        assertThat(mayLock.isPresent(), is(true));
        assertThat(mayLock.get().getLeftCount(), is(98));
    }
}
