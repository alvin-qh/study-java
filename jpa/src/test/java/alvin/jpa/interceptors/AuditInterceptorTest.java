package alvin.jpa.interceptors;

import alvin.jpa.core.BaseTest;
import alvin.jpa.interceptors.builders.LogBuilder;
import alvin.jpa.interceptors.models.AuthUser;
import alvin.jpa.interceptors.models.Log;
import alvin.jpa.interceptors.providers.AuthUserProvider;
import com.google.inject.Injector;
import com.google.inject.Scopes;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManagerFactory;
import java.time.LocalDateTime;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;

class AuditInterceptorTest extends BaseTest {

    @Inject
    private Injector injector;

    @Inject
    private LogRepository logRepository;

    @Inject
    private Provider<AuthUser> authUserProvider;

    @BeforeAll
    public static void beforeClass() {
        setUpWithJpaUnitName("interceptor", binder ->
                binder.bind(AuthUser.class).toProvider(AuthUserProvider.class).in(Scopes.SINGLETON));
    }

    @BeforeEach
    public void setUp() {
        super.setUp();
        EntityManagerFactory entityManagerFactory = injector.getInstance(EntityManagerFactory.class);
        SessionFactory sessionFactory = entityManagerFactory.unwrap(SessionFactory.class);
        injector.injectMembers(sessionFactory.getSessionFactoryOptions().getInterceptor());
    }

    @Test
    void test_save_log() {
        int id;
        try (Transaction ignore = beginTransaction()) {
            id = withBuilder(LogBuilder.class).create().getId();
        }
        clear();

        Optional<Log> mayLog = logRepository.find(id);
        assertThat(mayLog.isPresent(), is(true));

        Log log = mayLog.get();
        assertThat(log.getMessage(), is("test message"));
        assertThat(log.getCreatedAt(), lessThan(LocalDateTime.now()));
        assertThat(log.getUpdatedAt(), greaterThanOrEqualTo(log.getCreatedAt()));

        AuthUser authUser = authUserProvider.get();
        assertThat(log.getCreatedBy().getId(), is(authUser.getId()));
        assertThat(log.getCreatedBy(), is(log.getUpdatedBy()));
    }
}
