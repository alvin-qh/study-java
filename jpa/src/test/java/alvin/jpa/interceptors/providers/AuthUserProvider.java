package alvin.jpa.interceptors.providers;

import alvin.jpa.builder.AuthUserBuilder;
import alvin.jpa.interceptors.models.AuthUser;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

/**
 * 提供AuthUser对象的Provider类，用于测试.
 */
@Singleton
public class AuthUserProvider implements Provider<AuthUser> {

    private AuthUser authUser;

    @Inject
    public AuthUserProvider(AuthUserBuilder authUserBuilder) {
        authUser = authUserBuilder.create();
    }

    @Override
    public AuthUser get() {
        return authUser;
    }
}
