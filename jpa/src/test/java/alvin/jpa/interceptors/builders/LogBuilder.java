package alvin.jpa.interceptors.builders;

import alvin.jpa.interceptors.models.Log;
import alvin.jpa.core.Builder;

import javax.inject.Inject;
import javax.persistence.EntityManager;

public class LogBuilder implements Builder<Log> {
    private String message = "test message";

    private EntityManager em;

    @Inject
    public LogBuilder(EntityManager em) {
        this.em = em;
    }

    @Override
    public Log build() {
        Log log = new Log();
        log.setMessage(message);
        return log;
    }

    @Override
    public Log create() {
        Log log = build();
        em.persist(log);
        return log;
    }

    public LogBuilder message(String message) {
        this.message = message;
        return this;
    }
}
