package alvin.jpa.relationship.builders;

import alvin.jpa.relationship.models.User;
import alvin.jpa.core.Builder;
import alvin.jpa.relationship.models.UserInfo;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;

public class UserInfoBuilder implements Builder<UserInfo> {

    private EntityManager em;

    private String fullName = "Alvin Qu";
    private String gender = "M";
    private LocalDate birthday = LocalDate.of(1981, 3, 17);
    private String email = "alivn@fake.com";
    private String telephone = "13991320111";
    private String remark = "test";
    private User user;

    @Inject
    public UserInfoBuilder(EntityManager em) {
        this.em = em;
    }

    @Override
    public UserInfo build() {
        UserInfo userInfo = new UserInfo();
        userInfo.setFullName(fullName);
        userInfo.setGender(gender);
        userInfo.setBirthday(birthday);
        userInfo.setEmail(email);
        userInfo.setTelephone(telephone);
        userInfo.setRemark(remark);
        userInfo.setUser(user);
        return userInfo;
    }

    @Override
    public UserInfo create() {
        UserInfo userInfo = build();
        em.persist(userInfo);
        return userInfo;
    }

    public UserInfoBuilder fullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public UserInfoBuilder gender(String gender) {
        this.gender = gender;
        return this;
    }

    public UserInfoBuilder birthday(LocalDate birthday) {
        this.birthday = birthday;
        return this;
    }

    public UserInfoBuilder email(String email) {
        this.email = email;
        return this;
    }

    public UserInfoBuilder telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public UserInfoBuilder remark(String remark) {
        this.remark = remark;
        return this;
    }

    public UserInfoBuilder user(User user) {
        this.user = user;
        return this;
    }
}
