package alvin.jpa.relationship.builders;

import alvin.jpa.relationship.models.User;
import alvin.jpa.core.Builder;
import alvin.jpa.relationship.models.Group;
import alvin.jpa.relationship.models.UserInfo;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class UserBuilder implements Builder<User> {

    private EntityManager em;

    private String name = "Alvin";
    private String password = "123";
    private LocalDateTime lastLoginTime = LocalDateTime.now(ZoneOffset.UTC);
    private UserInfo userInfo;
    private Group group;

    @Inject
    public UserBuilder(EntityManager em) {
        this.em = em;
    }

    @Override
    public User build() {
        User user = new User();
        user.setName(name);
        user.setPassword(password);
        user.setLastLoginTime(lastLoginTime);
        user.setUserInfo(userInfo);
        user.setGroup(group);
        return user;
    }

    @Override
    public User create() {
        User user = build();
        if (user.getUserInfo() != null) {
            user.getUserInfo().setUser(user);
        }
        em.persist(user);
        return user;
    }

    public UserBuilder name(String name) {
        this.name = name;
        return this;
    }

    public UserBuilder password(String password) {
        this.password = password;
        return this;
    }

    public UserBuilder lastLoginTime(LocalDateTime lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
        return this;
    }

    public UserBuilder userInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
        return this;
    }

    public UserBuilder group(Group group) {
        this.group = group;
        return this;
    }
}
