package alvin.jpa.relationship.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.relationship.models.Interest;
import alvin.jpa.relationship.models.User;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class InterestBuilder implements Builder<Interest> {

    private String name = "Football";
    private List<User> users = new ArrayList<>();

    private EntityManager em;

    @Inject
    public InterestBuilder(EntityManager em) {
        this.em = em;
    }

    public InterestBuilder name(String interest) {
        this.name = interest;
        return this;
    }

    public InterestBuilder users(List<User> users) {
        this.users = users;
        return this;
    }

    public InterestBuilder addUser(User user) {
        users.add(user);
        return this;
    }

    @Override
    public Interest build() {
        Interest interest = new Interest();
        interest.setName(name);
        interest.setUsers(users);
        return interest;
    }

    @Override
    public Interest create() {
        Interest interest = build();
        if (interest.getUsers() != null) {
            interest.getUsers().forEach(u -> u.getInterests().add(interest));
        }
        em.persist(interest);
        return interest;
    }
}
