package alvin.jpa.relationship.builders;

import alvin.jpa.relationship.models.User;
import alvin.jpa.core.Builder;
import alvin.jpa.relationship.models.Group;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class GroupBuilder implements Builder<Group> {
    private String name;
    private List<User> users = new ArrayList<>();

    @Inject
    private EntityManager em;

    @Override
    public Group build() {
        Group group = new Group();
        group.setName(name);
        group.setUsers(users);
        return group;
    }

    @Override
    public Group create() {
        Group group = build();
        if (group.getUsers() != null) {
            group.getUsers().forEach(u -> u.setGroup(group));
        }
        em.persist(group);
        return group;
    }

    public GroupBuilder name(String name) {
        this.name = name;
        return this;
    }

    public GroupBuilder users(List<User> users) {
        this.users = users;
        return this;
    }

    public GroupBuilder addUser(User user) {
        this.users.add(user);
        return this;
    }
}
