package alvin.jpa.relationship;

import alvin.jpa.core.BaseTest;
import alvin.jpa.relationship.builders.GroupBuilder;
import alvin.jpa.relationship.builders.InterestBuilder;
import alvin.jpa.relationship.builders.UserBuilder;
import alvin.jpa.relationship.builders.UserInfoBuilder;
import alvin.jpa.relationship.models.Group;
import alvin.jpa.relationship.models.Interest;
import alvin.jpa.relationship.models.User;
import alvin.jpa.relationship.models.UserInfo;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class UserRepositoryTest extends BaseTest {

    @Inject
    private UserRepository userRepository;

    @Test
    void test_save_user() {
        /*
            持久化一个User对象
         */
        int userId;
        try (Transaction ignore = beginTransaction()) {
            User user = withBuilder(UserBuilder.class).build();
            userRepository.save(user);
            userId = user.getId();
        }
        clear();

        Optional<User> mayUser = userRepository.find(userId);
        assertThat(mayUser.isPresent(), is(true));
        assertThat(mayUser.get().getId(), is(userId));
    }

    @Test
    void test_save_user_by_user_info() {
        /*
            通过保存User对象同时保存一个UserInfo对象
         */
        Integer id;
        try (Transaction ignore = beginTransaction()) {
            // 创建User对象
            User user = withBuilder(UserBuilder.class).build();
            // 创建UserInfo对象并关联User对象
            UserInfo userInfo = withBuilder(UserInfoBuilder.class).user(user).build();
            // 管理User对象和UserInfo对象
            user.setUserInfo(userInfo);

            // 保存User对象，同时会自动保存UserInfo对象
            userRepository.save(user);
            id = user.getId();
        }
        clear();

        // 查询User对象，同时会查询与之关联的UserInfo对象
        Optional<User> mayUser = userRepository.find(id);

        assertThat(mayUser.isPresent(), is(true));
        assertThat(mayUser.get().getName(), is("Alvin"));
        assertThat(mayUser.get().getUserInfo().getFullName(), is("Alvin Qu"));
    }

    @Test
    void test_query_user_info_by_user_id() {
        /*
            测试"nature generator", "generator fetch"和"inner generator...on..."连接查询，根据User id查询User对象和UserInfo对象
         */
        Integer id;
        try (Transaction ignore = beginTransaction()) {
            User user = withBuilder(UserBuilder.class).create();
            withBuilder(UserInfoBuilder.class).user(user).create();
            id = user.getId();
        }
        clear();

        Optional<User> mayUser = userRepository.find(id);
        assertThat(mayUser.isPresent(), is(true));
        assertThat(mayUser.get().getName(), is("Alvin"));
        assertThat(mayUser.get().getUserInfo().getFullName(), is("Alvin Qu"));

        mayUser = userRepository.findAndFetchInfo(id);
        assertThat(mayUser.isPresent(), is(true));
        assertThat(mayUser.get().getName(), is("Alvin"));
        assertThat(mayUser.get().getUserInfo().getFullName(), is("Alvin Qu"));

        mayUser = userRepository.findAndJoinInfo(id);
        assertThat(mayUser.isPresent(), is(true));
        assertThat(mayUser.get().getName(), is("Alvin"));
        assertThat(mayUser.get().getUserInfo().getFullName(), is("Alvin Qu"));
    }

    @Test
    void test_find_by_user_info_full_name() {
        /*
            测试一对一连接查询，根据UserInfo的fullName属性查询对应的User对象
         */
        try (Transaction ignore = beginTransaction()) {
            User user = withBuilder(UserBuilder.class).create();
            withBuilder(UserInfoBuilder.class).user(user).create();
        }
        clear();

        List<User> users = userRepository.findByFullName("Alvin Qu");
        assertThat(users.size(), is(1));
        assertThat(users.get(0).getName(), is("Alvin"));
    }

    @Test
    void test_find_by_group_name() {
        /*
            测试一对多连接查询，根据Group的name属性查询对应的User对象的集合
         */
        try (Transaction ignore = beginTransaction()) {
            Group group = withBuilder(GroupBuilder.class).name("g1").create();
            withBuilder(UserBuilder.class).name("a1").group(group).create();
            withBuilder(UserBuilder.class).name("a2").group(group).create();

            User u1 = withBuilder(UserBuilder.class).name("b1").group(group).create();
            User u2 = withBuilder(UserBuilder.class).name("b2").group(group).create();
            withBuilder(GroupBuilder.class).name("g2").addUser(u1).addUser(u2).create();
        }
        clear();

        List<User> users = userRepository.findByGroupName("g1");
        assertThat(users.size(), is(2));
        assertThat(users.get(0).getName(), is("a1"));
        assertThat(users.get(1).getName(), is("a2"));

        users = userRepository.findByGroupName("g2");
        assertThat(users.size(), is(2));
        assertThat(users.get(0).getName(), is("b1"));
        assertThat(users.get(1).getName(), is("b2"));
    }

    @Test
    void test_find_with_interest() {
        /*
            测试多对多连接查询，根据Interest的name属性查询对应的User对象集合，根据User的name属性查询对应的Interest对象集合
         */
        try (Transaction ignore = beginTransaction()) {
            User user = withBuilder(UserBuilder.class).name("A1").create();
            Interest interest1 = withBuilder(InterestBuilder.class).name("I1").addUser(user).create();
            Interest interest2 = withBuilder(InterestBuilder.class).name("I2").addUser(user).create();

            user = withBuilder(UserBuilder.class).name("A2").create();
            user.addInterest(interest2);

            user = withBuilder(UserBuilder.class).name("A3").create();
            user.addInterest(interest1);
        }
        clear();

        List<User> users = userRepository.findByInterestName("I2");
        assertThat(users.size(), is(2));
        assertThat(users.stream().anyMatch(u -> "A1".equals(u.getName())), is(true));
        assertThat(users.stream().anyMatch(u -> "A2".equals(u.getName())), is(true));
        assertThat(users.stream().anyMatch(u -> "A3".equals(u.getName())), is(false));
        clear();

        Optional<User> mayUser = userRepository.findByName("A1");
        assertThat(mayUser.isPresent(), is(true));

        List<Interest> interests = mayUser.get().getInterests();
        assertThat(interests.stream().anyMatch(i -> "I1".equals(i.getName())), is(true));
        assertThat(interests.stream().anyMatch(i -> "I2".equals(i.getName())), is(true));
        assertThat(interests.stream().anyMatch(i -> "I3".equals(i.getName())), is(false));
    }
}
