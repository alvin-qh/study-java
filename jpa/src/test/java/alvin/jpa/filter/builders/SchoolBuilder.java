package alvin.jpa.filter.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.filter.models.School;

import javax.inject.Inject;
import javax.persistence.EntityManager;

public class SchoolBuilder implements Builder<School> {
    @Inject
    private EntityManager em;
    private String name = "No.1 High School";

    public SchoolBuilder name(String name) {
        this.name = name;
        return this;
    }

    @Override
    public School build() {
        School school = new School();
        school.setName(name);
        return school;
    }

    @Override
    public School create() {
        School school = build();
        em.persist(school);
        return school;
    }
}
