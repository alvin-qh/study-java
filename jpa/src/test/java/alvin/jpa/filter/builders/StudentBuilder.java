package alvin.jpa.filter.builders;

import alvin.jpa.filter.models.Student;

import javax.inject.Inject;
import javax.persistence.EntityManager;

public class StudentBuilder extends BuilderWithSchool<Student, StudentBuilder> {

    @Inject
    private EntityManager em;

    private String studentNo = "001";
    private String name = "Alvin";
    private String gender = "M";
    private String telephone = "13991300000";

    @Override
    public Student build() {
        Student student = new Student();
        student.setStudentNo(studentNo);
        student.setName(name);
        student.setGender(gender);
        student.setTelephone(telephone);
        student.setSchool(school);
        return student;
    }

    @Override
    public Student create() {
        Student student = build();
        em.persist(student);
        return student;
    }

    public StudentBuilder studentNo(String studentNo) {
        this.studentNo = studentNo;
        return this;
    }

    public StudentBuilder name(String name) {
        this.name = name;
        return this;
    }

    public StudentBuilder gender(String gender) {
        this.gender = gender;
        return this;
    }

    public StudentBuilder telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }
}
