package alvin.jpa.filter.builders;

import alvin.jpa.filter.models.AdminClass;
import alvin.jpa.filter.models.Student;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class AdminClassBuilder extends BuilderWithSchool<AdminClass, AdminClassBuilder> {
    @Inject
    private EntityManager em;

    private String name = "AdminClass 1";
    private List<Student> students = new ArrayList<>();

    public AdminClassBuilder name(String name) {
        this.name = name;
        return this;
    }

    public AdminClassBuilder addStudent(Student student) {
        students.add(student);
        return this;
    }

    @Override
    public AdminClass build() {
        AdminClass adminClass = new AdminClass();
        adminClass.setName(name);
        adminClass.setStudents(students);
        adminClass.setSchool(school);
        return adminClass;
    }

    @Override
    public AdminClass create() {
        AdminClass adminClass = build();
        em.persist(adminClass);
        return adminClass;
    }
}
