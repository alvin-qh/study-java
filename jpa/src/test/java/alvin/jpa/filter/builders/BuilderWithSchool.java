package alvin.jpa.filter.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.filter.models.School;

public abstract class BuilderWithSchool<T, E extends Builder<T>> implements Builder<T> {

    protected School school;

    public E school(School school) {
        this.school = school;
        return (E) this;
    }
}
