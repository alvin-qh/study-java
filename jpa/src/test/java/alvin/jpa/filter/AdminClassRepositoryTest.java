package alvin.jpa.filter;

import alvin.jpa.core.BaseTest;
import alvin.jpa.filter.builders.AdminClassBuilder;
import alvin.jpa.filter.builders.SchoolBuilder;
import alvin.jpa.filter.builders.StudentBuilder;
import alvin.jpa.filter.models.AdminClass;
import alvin.jpa.filter.models.School;
import alvin.jpa.filter.models.Student;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class AdminClassRepositoryTest extends BaseTest {

    @Inject
    private ClassRepository classRepository;

    private School school;

    @BeforeEach
    void prepareData() {
        Student student;
        try (Transaction ignore = beginTransaction()) {
            School school1 = withBuilder(SchoolBuilder.class).name("No.1 School").create();
            School school2 = withBuilder(SchoolBuilder.class).name("No.2 School").create();

            Student s1 = withBuilder(StudentBuilder.class).school(school1).studentNo("001").name("AA").create();
            Student s2 = withBuilder(StudentBuilder.class).school(school1).studentNo("002").name("AB").create();
            Student s3 = withBuilder(StudentBuilder.class).school(school1).studentNo("003").name("AC").create();
            Student s4 = withBuilder(StudentBuilder.class).school(school2).studentNo("004").name("BA").create();
            Student s5 = withBuilder(StudentBuilder.class).school(school2).studentNo("005").name("BB").create();
            Student s6 = withBuilder(StudentBuilder.class).school(school2).studentNo("006").name("BC").create();

            withBuilder(AdminClassBuilder.class).school(school1).name("AdminClass 1")
                    .addStudent(s1)
                    .addStudent(s2)
                    .create();
            withBuilder(AdminClassBuilder.class).school(school1).name("AdminClass 2")
                    .addStudent(s3)
                    .create();
            withBuilder(AdminClassBuilder.class).school(school2).name("AdminClass 3")
                    .addStudent(s4)
                    .create();
            withBuilder(AdminClassBuilder.class).school(school2).name("AdminClass 4")
                    .addStudent(s5)
                    .addStudent(s6)
                    .create();

            school = school1;
            student = s1;
        }
        clear();

        classRepository.enableFilter("school", ImmutableMap.of("schoolId", school.getId()));
        classRepository.enableFilter("student", ImmutableMap.of("studentId", student.getId()));
    }

    @Test
    void test_find_class_by_name_with_wrong_school() {
        Optional<AdminClass> mayClass = classRepository.findByName("AdminClass 3");
        assertThat(mayClass.isPresent(), is(false));
    }

    @Test
    void test_enable_and_disable_filter() {
        Optional<AdminClass> mayClass = classRepository.findByName("AdminClass 1");

        assertThat(mayClass.isPresent(), is(true));
        assertThat(mayClass.get().getSchool().getId(), is(school.getId()));
        assertThat(mayClass.get().getStudents().size(), is(1));
        assertThat(mayClass.get().getStudents().get(0).getStudentNo(), is("002"));

        clear();
        classRepository.disableFilter("student");

        mayClass = classRepository.findByName("AdminClass 1");
        assertThat(mayClass.isPresent(), is(true));
        assertThat(mayClass.get().getSchool().getId(), is(school.getId()));
        assertThat(mayClass.get().getStudents().size(), is(2));
        assertThat(mayClass.get().getStudents().get(0).getStudentNo(), is("001"));
        assertThat(mayClass.get().getStudents().get(1).getStudentNo(), is("002"));
    }
}
