package alvin.jpa.filter;

import alvin.jpa.core.BaseTest;
import alvin.jpa.filter.builders.SchoolBuilder;
import alvin.jpa.filter.builders.StudentBuilder;
import alvin.jpa.filter.models.School;
import alvin.jpa.filter.models.Student;
import alvin.jpa.filter.providers.SchoolProvider;
import com.google.common.collect.ImmutableMap;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

class StudentRepositoryTest extends BaseTest {

    @Inject
    private SchoolProvider schoolProvider;

    @Inject
    private StudentRepository studentRepository;

    @BeforeEach
    void prepareData() {
        try (Transaction ignore = beginTransaction()) {
            School school1 = withBuilder(SchoolBuilder.class).name("No.1 School").create();
            School school2 = withBuilder(SchoolBuilder.class).name("No.2 School").create();

            withBuilder(StudentBuilder.class).school(school1).studentNo("001").name("AA").create().getId();
            withBuilder(StudentBuilder.class).school(school1).studentNo("002").name("AB").create().getId();
            withBuilder(StudentBuilder.class).school(school1).studentNo("003").name("AC").create().getId();
            withBuilder(StudentBuilder.class).school(school2).studentNo("004").name("BA").create().getId();
            withBuilder(StudentBuilder.class).school(school2).studentNo("005").name("BB").create().getId();
            withBuilder(StudentBuilder.class).school(school2).studentNo("006").name("BC").create().getId();

            schoolProvider.set(school1);
        }
        studentRepository.enableFilter("school", ImmutableMap.of("schoolId", schoolProvider.get().getId()));
        clear();
    }

    @AfterEach
    void clean() {
        schoolProvider.clear();
    }

    @Test
    void test_find_student() {
        List<Student> students = studentRepository.findAll();
        assertThat(students.size(), is(3));

        students.forEach(s -> assertThat(s.getSchool(), is(students.get(0).getSchool())));
        assertThat(students.get(0).getStudentNo(), is("001"));
        assertThat(students.get(1).getStudentNo(), is("002"));
        assertThat(students.get(2).getStudentNo(), is("003"));
    }

    @Test
    void test_find_by_student_no() {
        Optional<Student> mayStudent = studentRepository.findByStudentNo("001");
        assertThat(mayStudent.isPresent(), is(true));
    }

    @Test
    void test_find_by_student_no_with_wrong_school() {
        Optional<Student> mayStudent = studentRepository.findByStudentNo("005");
        assertThat(mayStudent.isPresent(), is(false));
    }
}
