package alvin.jpa.core;

import net.sf.ehcache.CacheManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;

import javax.persistence.EntityManager;

public abstract class TestWithTransaction {

    private static final Bootstrap BOOTSTRAP = new Bootstrap();

    @BeforeEach
    public void setUp() {
        BOOTSTRAP.clearTables(em(), "study_java_jpa",
                "schema_versions",
                "generator_identity");
    }

    @AfterAll
    public static void afterClass() {
        CacheManager.getInstance().shutdown();
    }

    protected Transaction beginTransaction() {
        em().getTransaction().begin();
        return new Transaction(em());
    }

    protected void clear() {
        em().clear();
    }

    protected abstract EntityManager em();

    public static class Transaction implements AutoCloseable {
        private EntityManager em;

        public Transaction(EntityManager em) {
            this.em = em;
        }

        @Override
        public void close() {
            em.getTransaction().commit();
        }
    }
}
