package alvin.jpa.core;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.persist.PersistService;
import com.google.inject.persist.UnitOfWork;
import com.google.inject.persist.jpa.JpaPersistModule;
import net.sf.ehcache.CacheManager;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.persistence.EntityManager;
import java.util.Arrays;

public abstract class BaseTest extends TestWithTransaction {

    private static Injector injector;

    @Inject
    private UnitOfWork unitOfWork;

    @Inject
    private Provider<EntityManager> emProvider;

    @BeforeAll
    public static void beforeClass() {
        setUpWithJpaUnitName("default");
    }

    protected static void setUpWithJpaUnitName(String name, Module... modules) {
        injector = Guice.createInjector(binder -> {
            binder.install(new JpaPersistModule(name));
            Arrays.stream(modules).forEach(binder::install);
        });
        injector.getInstance(PersistService.class).start();
    }

    @BeforeEach
    public void setUp() {
        injector.injectMembers(this);
        unitOfWork.begin();
        super.setUp();
    }

    @Override
    protected EntityManager em() {
        return emProvider.get();
    }

    @AfterEach
    public void tearDown() {
        unitOfWork.end();
    }

    @AfterAll
    public static void afterClass() {
        CacheManager.getInstance().shutdown();
        injector.getInstance(PersistService.class).stop();
    }

    protected <T extends Builder> T withBuilder(Class<T> type) {
        T build = injector.getInstance(type);
        injector.injectMembers(build);
        return build;
    }
}
