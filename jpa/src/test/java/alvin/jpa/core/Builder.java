package alvin.jpa.core;

public interface Builder<T> {
    T build();

    T create();
}
