package alvin.jpa.core;

import alvin.common.db.Migration;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

final class Bootstrap {
    private static final String CONNECTION_URL = "jdbc:mysql://localhost:3306/study_java_jpa?useLegacyDatetimeCode=false&characterEncoding=UTF-8";

    static {
        Migration migration = new Migration.Builder(CONNECTION_URL)
                .account("root")
                .password("")
                .build();
        migration.migrate();
    }

    @SuppressWarnings({"unchecked", "SqlResolve"})
    private List<String> listAllTables(EntityManager em, String schema) {
        return em.createNativeQuery("SELECT `table_name` FROM information_schema.tables " +
                "WHERE `table_schema`=:schema AND `table_type`='base table'")
                .setParameter("schema", schema)
                .getResultList();
    }

    void clearTables(EntityManager em, String schema, String...except) {
        Set<String> exceptSet = new HashSet<>();
        Stream.of(except).forEach(ex -> {
            exceptSet.add(ex.toLowerCase());
            exceptSet.add(ex.toUpperCase());
        });

        List<String> tables = listAllTables(em, schema);
        EntityTransaction tx = em.getTransaction();
        try {
            tx.begin();
            em.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0").executeUpdate();
            tables.stream()
                    .filter(t -> !exceptSet.contains(t))
                    .forEach(t ->
                            em.createNativeQuery("TRUNCATE TABLE " + t).executeUpdate());
            em.createNativeQuery("SET FOREIGN_KEY_CHECKS = 1").executeUpdate();
            tx.commit();
        } catch (Exception e) {
            tx.rollback();
            throw e;
        }
    }
}
