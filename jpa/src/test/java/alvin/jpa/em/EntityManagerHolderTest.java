package alvin.jpa.em;

import alvin.jpa.core.TestWithTransaction;
import alvin.jpa.core.hibernate.HibernateUtils;
import alvin.jpa.em.builders.PersonBuilder;
import alvin.jpa.em.models.Person;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

@SuppressWarnings("JpaQueryApiInspection")
@Slf4j
class EntityManagerHolderTest extends TestWithTransaction {
    private EntityManagerHolder entityManagerHolder = new EntityManagerHolder();

    @AfterEach
    void tearDown() {
        entityManagerHolder.clear();
    }

    @Override
    protected EntityManager em() {
        return entityManagerHolder.getEntityManager();
    }

    @Test
    void test_persist() {
        /*
            EntityManager的persist用于持久化一个对象，包括：
            1. 对象是“新”态，则对象转为“受控”态，且保存到数据库；
            2. 对象是“游离”态，则对象转为“受控”态，且更新到数据库；
            3. 对象是“受控”态，则状态不变；
         */
        Person expectedPerson = new PersonBuilder(null).build();
        log.info("before persist: {}", expectedPerson);

        try (Transaction ignore = beginTransaction()) {
            em().persist(expectedPerson);
        }

        assertThat(expectedPerson.getId(), is(notNullValue()));
        log.info("tearDown persist: {}", expectedPerson);
        clear();

        Person actualPerson = em().find(Person.class, expectedPerson.getId());
        log.info("tearDown load: {}", actualPerson);

        assertThat(expectedPerson.toString(), is(actualPerson.toString()));
    }

    @Test
    void test_update() {
        /*
            对于“受控”态的对象，更改其属性会导致该对象被更新到数据表
         */
        int id;
        try (Transaction ignore = beginTransaction()) {
            id = new PersonBuilder(em()).create().getId();
        }
        log.info("tearDown persist: id={}", id);
        clear();

        try (Transaction ignore = beginTransaction()) {
            Person person = em().find(Person.class, id);
            person.setName("New Alvin");
        }

        Person person = em().find(Person.class, id);
        assertThat(person.getName(), is("New Alvin"));
    }

    @Test
    void test_merge() {
        /*
            EntityManager的merge方法用于持久化一个对象，包括：
            1. 对象是“新”态，则根据对象是否有PK，将对象保存/更新到数据库，且返回“受控”态对象；
            2. 对象是“游离”态，则将对象更新到数据库，且返回“受控”态对象；；
            3. 对象是“受控”态，则状态不变；
         */
        int id;
        try (Transaction ignore = beginTransaction()) {
            Person person = new PersonBuilder(null).build();
            em().persist(person);
            id = person.getId();
        }

        Person person = new Person(id);
        person.setName("AAA");
        person.setBirthday(LocalDate.of(1981, 3, 17));
        person.setEmail("aaa@bbb.com");
        person.setTelephone("123123123");
        person.setGender(Person.Gender.M);
        try (Transaction ignore = beginTransaction()) {
            Person managePerson = em().merge(person);
            assertThat(managePerson == person, is(false));
        }

        try (Transaction ignore = beginTransaction()) {
            person = em().find(Person.class, id);
            person.setName("BBB");
            Person managePerson = em().merge(person);
            assertThat(managePerson == person, is(true));
        }
    }

    @Test
    void test_delete() {
        int id;
        try (Transaction ignore = beginTransaction()) {
            Person person = new PersonBuilder(null).build();
            em().persist(person);
            id = person.getId();
        }

        Person person = em().find(Person.class, id);
        try (Transaction ignore = beginTransaction()) {
            em().remove(person);
        }

        person = em().find(Person.class, id);
        assertThat(person, is(nullValue()));
    }

    @Test
    void test_fresh() throws InterruptedException {
        /*
            EntityManager的refresh方法用于同步一个持久化对象状态，将数据库的数据重新加载到对象中
         */
        int id;
        try (Transaction ignore = beginTransaction()) {
            Person person = new PersonBuilder(em()).create();
            id = person.getId();
        }

        // 在线程1中获取Person对象
        Person person = em().find(Person.class, id);

        // 在线程2中重新获取Person对象并对齐进行修改
        Thread thread = new Thread(() -> {
            em().getTransaction().begin();
            try {
                Person person1 = em().find(Person.class, id);
                person1.setName("New");
                em().getTransaction().commit();
            } catch (Exception ignore) {
                em().getTransaction().rollback();
            }
        });
        thread.start();
        thread.join();

        // 查看线程1中的Person对象，此时状态未更新
        assertThat(person.getName(), not("New"));

        // 更新线程1中的Person对象状态
        em().refresh(person);
        assertThat(person.getName(), is("New"));
    }

    @Test
    void test_jpql_query() {
        /*
            测试通过JPQL进行查询
         */
        String name;
        try (Transaction ignore = beginTransaction()) {
            name = new PersonBuilder(em()).create().getName();
        }
        clear();

        List<Person> persons = em().createQuery("from Person where name=:name", Person.class)
                .setParameter("name", name).getResultList();
        assertThat(persons.size(), is(1));
        assertThat(persons.get(0).getName(), is(name));
    }

    @Test
    void test_criteria_query() {
        /*
            测试通过Criteria Query进行查询
         */
        String name;
        try (Transaction ignore = beginTransaction()) {
            name = new PersonBuilder(em()).create().getName();
        }
        clear();

        CriteriaBuilder cb = em().getCriteriaBuilder();
        CriteriaQuery<Person> query = cb.createQuery(Person.class);
        Root<Person> from = query.from(Person.class);
        ParameterExpression<String> parameter = cb.parameter(String.class);
        query.select(from).where(cb.equal(from.get("name"), parameter));

        List<Person> persons = em().createQuery(query).setParameter(parameter, "Alvin").getResultList();
        assertThat(persons.size(), is(1));
        assertThat(persons.get(0).getName(), is(name));
    }

    @Test
    void test_named_query() {
        /*
            测试Named query
         */
        String name;
        try (Transaction ignore = beginTransaction()) {
            name = new PersonBuilder(em()).create().getName();
        }
        clear();

        TypedQuery<Person> query = em().createNamedQuery("find by name", Person.class);

        List<Person> persons = query.setParameter("name", name).getResultList();
        assertThat(persons.size(), is(1));
        assertThat(persons.get(0).getName(), is(name));
    }

    @Test
    @SuppressWarnings("unchecked")
    void test_native_query() {
        /*
            测试通过Native query进行查询
         */
        String name;
        try (Transaction ignore = beginTransaction()) {
            name = new PersonBuilder(em()).create().getName();
        }
        clear();

        List<Person> persons = em().createNativeQuery("SELECT id,name,gender,birthday,telephone,email FROM em_person " +
                "WHERE name=:name", Person.class).setParameter("name", name).getResultList();
        assertThat(persons.size(), is(1));
        assertThat(persons.get(0).getName(), is(name));
    }

    @Test
    void test_find_list() throws Exception {
        /*
            测试查询列表结果
         */
        try (Transaction ignore = beginTransaction()) {
            new PersonBuilder(em()).name("a1").create();
            new PersonBuilder(em()).name("b1").create();
            new PersonBuilder(em()).name("b2").create();
        }
        clear();

        List<Person> persons = em()
                .createQuery("from Person where name like :name order by name", Person.class)
                .setParameter("name", "b%")
                .getResultList();
        assertThat(persons.size(), is(2));
        assertThat(persons.get(0).getName(), is("b1"));
        assertThat(persons.get(1).getName(), is("b2"));
    }

    @Test
    void test_use_cache() throws Exception {
        /*
            测试二级缓存
         */
        // final Map<String, Object> hint = ImmutableMap.of("org.hibernate.cacheable", false);
        int id;
        try (Transaction ignore = beginTransaction()) {
            id = new PersonBuilder(em()).create().getId();
        }
        entityManagerHolder.clear();

        for (int i = 0; i < 10; i++) {
            assertThat(em().find(Person.class, id/*, hint*/), is(notNullValue()));

            // 验证对象是否已被缓冲
            assertThat(HibernateUtils.getCache(em()).containsEntity(Person.class, id), is(true));

            // 关闭当前连接
            entityManagerHolder.clear();
        }
    }
}
