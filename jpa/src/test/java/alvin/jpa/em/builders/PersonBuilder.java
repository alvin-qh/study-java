package alvin.jpa.em.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.em.models.Person;

import javax.persistence.EntityManager;
import java.time.LocalDate;

import static alvin.jpa.em.models.Person.Gender;

public class PersonBuilder implements Builder<Person> {
    private String name = "Alvin";

    private LocalDate birthday = LocalDate.of(1981, 3, 17);

    private Gender gender = Gender.M;

    private String telephone = "13999999999";

    private String email = "alvin@fakeaddr.com";

    private EntityManager em;

    public PersonBuilder(EntityManager em) {
        this.em = em;
    }

    public PersonBuilder name(String name) {
        this.name = name;
        return this;
    }

    public PersonBuilder birthday(LocalDate birthday) {
        this.birthday = birthday;
        return this;
    }

    public PersonBuilder gender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public PersonBuilder telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public PersonBuilder email(String email) {
        this.email = email;
        return this;
    }

    @Override
    public Person build() {
        Person person = new Person();
        person.setName(name);
        person.setBirthday(birthday);
        person.setEmail(email);
        person.setTelephone(telephone);
        person.setGender(gender);
        return person;
    }

    @Override
    public Person create() {
        Person person = build();
        em.persist(person);
        return person;
    }
}
