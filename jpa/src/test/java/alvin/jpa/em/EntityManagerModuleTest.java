package alvin.jpa.em;

import alvin.jpa.core.TestWithTransaction;
import alvin.jpa.em.builders.PersonBuilder;
import alvin.jpa.em.models.Person;
import com.google.inject.Guice;
import com.google.inject.persist.Transactional;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;
import javax.persistence.EntityManager;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

@Slf4j
class EntityManagerModuleTest extends TestWithTransaction {

    @Inject
    private Repository repository;

    @Inject
    private EntityManager em;

    @BeforeEach
    public void setUp() {
        Guice.createInjector(new EntityManagerModule()).injectMembers(this);
        super.setUp();
    }

    @Override
    protected EntityManager em() {
        return em;
    }

    @Test
    void test_save_person() {
        repository.savePerson();
    }

    @Test
    void test_save_and_raise_exception() {
        int id = 0;
        try {
            repository.savePersonAndRaiseException();
        } catch (Throwable t) {
            id = Integer.parseInt(t.getMessage());
            log.info("person id={} is rollback", id);
        }
        clear();

        Person person = em.find(Person.class, id);
        assertThat(person, is(nullValue()));
    }

    /**
     * 测试Repository类，用于测试事务注解.
     */
    @SuppressWarnings("WeakerAccess")
    @Singleton
    static class Repository {

        @Inject
        private Provider<EntityManager> emProvider;

        @Transactional
        void savePerson() {
            /*
                测试保存用户并提交事务
             */
            Person expectedPerson = new PersonBuilder(null).build();
            log.info("before persist: {}", expectedPerson);

            emProvider.get().persist(expectedPerson);

            assertThat(expectedPerson.getId(), is(notNullValue()));
            log.info("tearDown persist: {}", expectedPerson);

            emProvider.get().clear();

            Person actualPerson = emProvider.get().find(Person.class, expectedPerson.getId());
            log.info("tearDown load: {}", actualPerson);

            assertThat(expectedPerson.toString(), is(actualPerson.toString()));
        }

        @Transactional
        void savePersonAndRaiseException() {
            /*
                测试保存用户并回滚事务
             */
            Person person = new PersonBuilder(emProvider.get()).create();
            log.info("tearDown persist: {}", person);

            throw new RuntimeException(String.valueOf(person.getId()));
        }
    }
}
