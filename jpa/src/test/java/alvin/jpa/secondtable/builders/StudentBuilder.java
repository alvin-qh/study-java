package alvin.jpa.secondtable.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.secondtable.models.Student;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;

public class StudentBuilder implements Builder<Student> {
    private String sno = "001";
    private String name = "Alvin";
    private String gender = "M";
    private String telephone = "13900000001";
    private LocalDate birthday = LocalDate.of(1981, 3, 17);
    private String address = "Xi'an China";
    private String email = "test@fake.com";
    private String qq = "1111";

    private EntityManager em;

    @Inject
    public StudentBuilder(EntityManager em) {
        this.em = em;
    }

    public StudentBuilder sno(String sno) {
        this.sno = sno;
        return this;
    }

    public StudentBuilder name(String name) {
        this.name = name;
        return this;
    }

    public StudentBuilder gender(String gender) {
        this.gender = gender;
        return this;
    }

    public StudentBuilder telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    public StudentBuilder birthday(LocalDate birthday) {
        this.birthday = birthday;
        return this;
    }

    public StudentBuilder address(String address) {
        this.address = address;
        return this;
    }

    public StudentBuilder email(String email) {
        this.email = email;
        return this;
    }

    public StudentBuilder qq(String qq) {
        this.qq = qq;
        return this;
    }

    @Override
    public Student build() {
        Student student = new Student();
        student.setName(name);
        student.setEmail(email);
        student.setAddress(address);
        student.setBirthday(birthday);
        student.setGender(gender);
        student.setQq(qq);
        student.setSno(sno);
        student.setTelephone(telephone);
        return student;
    }

    @Override
    public Student create() {
        Student student = build();
        em.persist(student);
        return student;
    }
}
