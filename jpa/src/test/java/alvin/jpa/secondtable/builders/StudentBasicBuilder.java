package alvin.jpa.secondtable.builders;

import alvin.jpa.core.Builder;
import alvin.jpa.secondtable.models.StudentBasic;

import javax.inject.Inject;
import javax.persistence.EntityManager;

public class StudentBasicBuilder implements Builder<StudentBasic> {
    private String sno = "001";
    private String name = "Alvin";
    private String gender = "M";
    private String telephone = "13900000001";

    private EntityManager em;

    @Inject
    public StudentBasicBuilder(EntityManager em) {
        this.em = em;
    }

    public StudentBasicBuilder sno(String sno) {
        this.sno = sno;
        return this;
    }

    public StudentBasicBuilder name(String name) {
        this.name = name;
        return this;
    }

    public StudentBasicBuilder gender(String gender) {
        this.gender = gender;
        return this;
    }

    public StudentBasicBuilder telephone(String telephone) {
        this.telephone = telephone;
        return this;
    }

    @Override
    public StudentBasic build() {
        StudentBasic studentBasic = new StudentBasic();
        studentBasic.setName(name);
        studentBasic.setGender(gender);
        studentBasic.setSno(sno);
        studentBasic.setTelephone(telephone);
        return studentBasic;
    }

    @Override
    public StudentBasic create() {
        StudentBasic studentBasic = build();
        em.persist(studentBasic);
        return studentBasic;
    }
}
