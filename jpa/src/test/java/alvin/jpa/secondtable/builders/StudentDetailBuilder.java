package alvin.jpa.secondtable.builders;

import alvin.jpa.secondtable.models.StudentDetail;
import alvin.jpa.core.Builder;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.time.LocalDate;

public class StudentDetailBuilder implements Builder<StudentDetail> {
    private int id;
    private LocalDate birthday = LocalDate.of(1981, 3, 17);
    private String address = "Xi'an China";
    private String email = "test@fake.com";
    private String qq = "1111";

    private EntityManager em;

    @Inject
    public StudentDetailBuilder(EntityManager em) {
        this.em = em;
    }

    public StudentDetailBuilder id(int id) {
        this.id = id;
        return this;
    }

    public StudentDetailBuilder birthday(LocalDate birthday) {
        this.birthday = birthday;
        return this;
    }

    public StudentDetailBuilder address(String address) {
        this.address = address;
        return this;
    }

    public StudentDetailBuilder email(String email) {
        this.email = email;
        return this;
    }

    public StudentDetailBuilder qq(String qq) {
        this.qq = qq;
        return this;
    }

    @Override
    public StudentDetail build() {
        StudentDetail studentDetail = new StudentDetail(id);
        studentDetail.setEmail(email);
        studentDetail.setAddress(address);
        studentDetail.setBirthday(birthday);
        studentDetail.setQq(qq);
        return studentDetail;
    }

    @Override
    public StudentDetail create() {
        StudentDetail studentDetail = build();
        em.persist(studentDetail);
        return studentDetail;
    }
}
