package alvin.jpa.secondtable;

import alvin.jpa.core.BaseTest;
import alvin.jpa.secondtable.builders.StudentBasicBuilder;
import alvin.jpa.secondtable.builders.StudentBuilder;
import alvin.jpa.secondtable.builders.StudentDetailBuilder;
import alvin.jpa.secondtable.models.Student;
import alvin.jpa.secondtable.models.StudentBasic;
import alvin.jpa.secondtable.models.StudentDetail;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SuppressWarnings("ResultOfMethodCallIgnored")
class SecondTableTest extends BaseTest {

    @Inject
    private StudentBasicRepository studentBasicRepository;

    @Inject
    private StudentDetailRepository studentDetailRepository;

    @Inject
    private StudentRepository studentRepository;

    @Test
    void test_add_student_into_two_tables() throws Exception {
        int id;
        try (Transaction ignore = beginTransaction()) {
            id = withBuilder(StudentBuilder.class).create().getId();
        }

        Optional<StudentBasic> mayStudentBasic = studentBasicRepository.find(id);
        assertThat(mayStudentBasic.isPresent(), is(true));
        assertThat(mayStudentBasic.get().getSno(), is("001"));

        Optional<StudentDetail> mayStudentDetail = studentDetailRepository.find(id);
        assertThat(mayStudentDetail.isPresent(), is(true));
        assertThat(mayStudentDetail.get().getId(), is(mayStudentBasic.get().getId()));
        assertThat(mayStudentDetail.get().getQq(), is("1111"));
    }

    @Test
    void test_find_student_from_two_table() {
        int id;
        try (Transaction ignore = beginTransaction()) {
            id = withBuilder(StudentBasicBuilder.class).create().getId();
            withBuilder(StudentDetailBuilder.class).id(id).create().getId();
        }

        Optional<Student> mayStudent = studentRepository.find(id);
        assertThat(mayStudent.isPresent(), is(true));
        assertThat(mayStudent.get().getSno(), is("001"));
        assertThat(mayStudent.get().getQq(), is("1111"));
    }
}
