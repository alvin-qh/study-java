package alvin.algorithms;

import org.apache.commons.codec.binary.Base64;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class RSAKeyFactory {

    private static final String PUB_KEY_START = "-----BEGIN PUBLIC KEY-----";
    private static final String PUB_KEY_END = "-----END PUBLIC KEY-----";

    private static final String PRI_KEY_START = "-----BEGIN PRIVATE KEY-----";
    private static final String PRI_KEY_END = "-----END PRIVATE KEY-----";

    private static final Pattern BLANK_REG = Pattern.compile("[\r\n\t ]", Pattern.MULTILINE);

    private RSAKeyFactory() {
    }

    public static PublicKey createPublicKey(byte[] keyBytes, String algorithm) throws InvalidKeySpecException {
        try {
            return KeyFactory.getInstance(algorithm).generatePublic(new X509EncodedKeySpec(keyBytes));
        } catch (NoSuchAlgorithmException ignore) {
            return null;
        }
    }

    public static PublicKey createPublicKey(String key, String algorithm) throws InvalidKeySpecException {
        return createPublicKey(convertPublicKey(key), algorithm);
    }

    public static PrivateKey createPrivateKey(byte[] keyBytes, String algorithm) throws InvalidKeySpecException {
        try {
            /* Add PKCS#8 formatting */
//            ASN1EncodableVector v2 = new ASN1EncodableVector();
//            v2.add(new ASN1ObjectIdentifier(PKCSObjectIdentifiers.rsaEncryption.getId()));
//            v2.add(DERNull.INSTANCE);
//
//            ASN1EncodableVector v1 = new ASN1EncodableVector();
//            v1.add(new ASN1Integer(0));
//            v1.add(new DERSequence(v2));
//            v1.add(new DEROctetString(keyBytes));
//            ASN1Sequence seq = new DERSequence(v1);
//            keyBytes = seq.getEncoded("DER");
//            System.out.println(Base64.encodeBase64String(keyBytes));

            return KeyFactory.getInstance(algorithm).generatePrivate(new PKCS8EncodedKeySpec(keyBytes));
        } catch (NoSuchAlgorithmException /* | IOException */ ignore) {
            return null;
        }
    }

    public static PrivateKey createPrivateKey(String key, String algorithm) throws InvalidKeySpecException {
        return createPrivateKey(convertPrivateKey(key), algorithm);
    }

    static byte[] convertPublicKey(String key) {
        return Base64.decodeBase64(fixKey(key, PUB_KEY_START, PUB_KEY_END));
    }

    static byte[] convertPrivateKey(String key) {
        return Base64.decodeBase64(fixKey(key, PRI_KEY_START, PRI_KEY_END));
    }

    private static String fixKey(String key, final String beginLine, final String endLine) {
        int start = key.indexOf(beginLine);
        if (start < 0) {
            start = 0;
        } else {
            start += beginLine.length();
        }

        int end = key.lastIndexOf(endLine);
        if (end < 0) {
            end = key.length() - 1;
        }

        Matcher matcher = BLANK_REG.matcher(key.substring(start, end));
        return matcher.replaceAll("");
    }
}
