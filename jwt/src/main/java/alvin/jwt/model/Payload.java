package alvin.jwt.model;


/*
    Test case:
    {
        "iss": "alvin",
        "aud": "third-part",
        "sub": "u_b989c9ea-e07b-4263-ae98-11d6b1b0e327",
        "sub_org_code": "o_a4ef30",
        "sub_user_type": "employee",
        "iat": 1502939170,
        "exp": 9999999999
    }
 */

import java.time.LocalDateTime;

public interface Payload {
    String getIssuer();

    String getAudience();

    String getSubject();

    String getSubjectOrgCode();

    UserType getSubjectUserType();

    LocalDateTime getIssuedAt();

    LocalDateTime getExpires();
}
