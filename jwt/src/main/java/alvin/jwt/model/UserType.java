package alvin.jwt.model;

public enum UserType {
    EMPLOYEE("employee"), MANAGER("manager");

    private final String value;

    UserType(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    public static UserType maches(String value) {
        for (UserType type : values()) {
            if (type.value.equals(value)) {
                return type;
            }
        }
        throw new IllegalArgumentException("Invalid value of UserType");
    }
}
