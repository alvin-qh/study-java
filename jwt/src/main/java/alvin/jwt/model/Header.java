package alvin.jwt.model;


/*
    Test case:
    {
        "alg": "RS256",
        "typ": "JWT"
    }
 */

public interface Header {
    String getType();

    String getAlgorithm();
}
