package alvin.jwt;

import alvin.jwt.model.Header;
import alvin.jwt.model.Payload;
import alvin.jwt.model.UserType;
import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

public final class Decoder implements Header, Payload {
    private final DecodedJWT decode;

    private Decoder(String token) {
        this.decode = JWT.decode(token);
    }

    public static Decoder create(String token) {
        return new Decoder(token);
    }

    @Override
    public String getType() {
        return decode.getType();
    }

    @Override
    public String getAlgorithm() {
        return decode.getAlgorithm();
    }

    @Override
    public String getIssuer() {
        return decode.getIssuer();
    }

    @Override
    public String getAudience() {
        return decode.getAudience().get(0);
    }

    @Override
    public String getSubject() {
        return decode.getSubject();
    }

    @Override
    public String getSubjectOrgCode() {
        return decode.getClaim("sub_org_code").asString();
    }

    @Override
    public UserType getSubjectUserType() {
        return UserType.maches(decode.getClaim("sub_user_type").asString());
    }

    @Override
    public LocalDateTime getIssuedAt() {
        return LocalDateTime.ofInstant(decode.getIssuedAt().toInstant(), ZoneOffset.UTC);
    }

    @Override
    public LocalDateTime getExpires() {
        return LocalDateTime.ofInstant(decode.getExpiresAt().toInstant(), ZoneOffset.UTC);
    }
}
