package alvin.jwt;

import alvin.algorithms.RSAKeyFactory;
import alvin.jwt.model.Payload;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;

import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.time.ZoneOffset;
import java.util.Date;

public final class Encoder {
    private final JWTCreator.Builder builder;

    private Encoder(JWTCreator.Builder builder) {
        this.builder = builder;
    }

    public static Encoder create(Payload payload) {
        JWTCreator.Builder builder = JWT.create()
                .withIssuer(payload.getIssuer())
                .withAudience(payload.getAudience())
                .withSubject(payload.getSubject())
                .withClaim("sub_org_code", payload.getSubjectOrgCode())
                .withClaim("sub_user_type", payload.getSubjectUserType().value())
                .withIssuedAt(Date.from(payload.getIssuedAt().toInstant(ZoneOffset.UTC)))
                .withExpiresAt(Date.from(payload.getExpires().toInstant(ZoneOffset.UTC)));
        return new Encoder(builder);
    }

    public String make(String privateKey) throws InvalidKeySpecException {
        PrivateKey rsa = RSAKeyFactory.createPrivateKey(privateKey, "RSA");
        final Algorithm algorithm = Algorithm.RSA256(null, (RSAPrivateKey) rsa);
        return builder.sign(algorithm);
    }
}
