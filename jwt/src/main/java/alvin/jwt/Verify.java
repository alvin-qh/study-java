package alvin.jwt;

import alvin.algorithms.RSAKeyFactory;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.security.PublicKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;

public final class Verify {
    private final JWTVerifier verifier;

    public static Verify build(String publicKey) throws InvalidKeySpecException {
        final PublicKey key = RSAKeyFactory.createPublicKey(publicKey, "RSA");
        final Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) key, null);
        return new Verify(algorithm);
    }

    private Verify(Algorithm algorithm) {
        verifier = JWT.require(algorithm)
                .withIssuer("alvin")
                .withAudience("third-part")
                .build();
    }

    public DecodedJWT verify(String token) throws JWTVerificationException {
        return verifier.verify(token);
    }
}
