package alvin.util;

import com.google.common.io.ByteStreams;

import java.io.IOException;
import java.io.InputStream;

import static java.nio.charset.StandardCharsets.UTF_8;

@SuppressWarnings("UnstableApiUsage")
public final class KeyReader {

    private KeyReader() {
    }

    public static String readPublicKey() throws IOException {
        InputStream stream = KeyReader.class.getResourceAsStream("/key.pub");
        return new String(ByteStreams.toByteArray(stream), UTF_8);
    }

    public static String readPrivateKey() throws IOException {
        InputStream stream = KeyReader.class.getResourceAsStream("/key");
        return new String(ByteStreams.toByteArray(stream), UTF_8);
    }

    public static String readJWT() throws IOException {
        InputStream stream = KeyReader.class.getResourceAsStream("/token");
        return new String(ByteStreams.toByteArray(stream), UTF_8);
    }
}
