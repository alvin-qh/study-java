package alvin.jwt;

import alvin.jwt.model.UserType;
import alvin.util.KeyReader;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class DecoderTest {

    @Test
    void test_decode() throws Exception {
        String token = KeyReader.readJWT();

        Decoder decoder = Decoder.create(token);

        assertThat(decoder.getAlgorithm(), is("RS256"));
        assertThat(decoder.getType(), is("JWT"));

        assertThat(decoder.getIssuer(), is("alvin"));
        assertThat(decoder.getAudience(), is("third-part"));
        assertThat(decoder.getSubject(), is("u_b989c9ea-e07b-4263-ae98-11d6b1b0e327"));
        assertThat(decoder.getSubjectOrgCode(), is("o_a4ef30"));
        assertThat(decoder.getSubjectUserType(), is(UserType.EMPLOYEE));
        assertThat(decoder.getIssuedAt().toString(), is("2017-08-17T03:06:10"));
        assertThat(decoder.getExpires().toString(), is("2286-11-20T17:46:39"));
    }
}
