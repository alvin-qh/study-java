package alvin.jwt;

import alvin.util.KeyReader;
import com.auth0.jwt.exceptions.JWTDecodeException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import static org.junit.jupiter.api.Assertions.assertThrows;

class VerifyTest {

    @Test
    void test_verify() throws Exception {
        Verify verify = Verify.build(KeyReader.readPublicKey());
        verify.verify(KeyReader.readJWT());
    }

    @Test
    void test_verify_failed() {
        Executable executable = () -> {
            Verify verify = Verify.build(KeyReader.readPublicKey());
            verify.verify("abc.def.xyz");
        };
        assertThrows(JWTDecodeException.class, executable);
    }
}
