package alvin.jwt;

import alvin.jwt.model.Payload;
import alvin.jwt.model.UserType;
import alvin.util.KeyReader;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

class EncoderTest {

    @Test
    void test_make_token() throws Exception {

        Encoder encoder = Encoder.create(new Payload() {
            @Override
            public String getIssuer() {
                return "alvin";
            }

            @Override
            public String getAudience() {
                return "third-part";
            }

            @Override
            public String getSubject() {
                return "u_b989c9ea-e07b-4263-ae98-11d6b1b0e327";
            }

            @Override
            public String getSubjectOrgCode() {
                return "o_a4ef30";
            }

            @Override
            public UserType getSubjectUserType() {
                return UserType.EMPLOYEE;
            }

            @Override
            public LocalDateTime getIssuedAt() {
                return LocalDateTime.ofEpochSecond(1502939170, 0, ZoneOffset.UTC);
            }

            @Override
            public LocalDateTime getExpires() {
                return LocalDateTime.ofEpochSecond(9999999999L, 0, ZoneOffset.UTC);
            }
        });

        String token = encoder.make(KeyReader.readPrivateKey());

        Decoder.create(token);

        assertThat(token, is(KeyReader.readJWT().replaceAll("\n", "")));
    }
}
