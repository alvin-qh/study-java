package alvin.security;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

class DigestTest {

    private byte[] makeData() {
        byte[] data = new byte[100];
        for (int i = 0; i < data.length; i++) {
            data[i] = (byte) i;
        }
        return data;
    }

    @Test
    void test_md5_with_byte() throws Exception {
        Digest digest = new Digest("MD5");

        byte[] data = makeData();

        assertThat(digest.toString(data), is("7acedd1a84a4cfcb6e7a16003242945e"));

        data[50]++;
        assertThat(digest.toString(data), is("e17805819b3512436b9f9269f6aa512b"));
    }

    @Test
    void test_md5_with_string() throws Exception {
        Digest digest = new Digest("MD5");

        assertThat(digest.toString("hello", "utf-8"), is("5d41402abc4b2a76b9719d911017c592"));
        assertThat(digest.toString("Hello", "utf-8"), is("8b1a9953c4611296a827abf8c47804d7"));
    }

    @Test
    void test_md5_with_stream() throws Exception {
        Digest digest = new Digest("MD5");

        byte[] data1 = makeData();
        String result1 = digest.toString(new ByteArrayInputStream(data1));
        assertThat(result1, is("7acedd1a84a4cfcb6e7a16003242945e"));

        byte[] data2 = makeData();
        String result2 = digest.toString(new ByteArrayInputStream(data2));
        assertThat(result2, is("7acedd1a84a4cfcb6e7a16003242945e"));
    }

    @Test
    void test_md5_with_stream_not_equal() throws Exception {
        Digest digest = new Digest("MD5");

        byte[] data1 = makeData();
        String result1 = digest.toString(new ByteArrayInputStream(data1));
        assertThat(result1, is("7acedd1a84a4cfcb6e7a16003242945e"));

        byte[] data2 = makeData();
        data2[3] = (byte) ~data2[3];
        String result2 = digest.toString(new ByteArrayInputStream(data2));
        assertThat(result2, not("7acedd1a84a4cfcb6e7a16003242945e"));
    }

    @Test
    void test_sha1_with_string() throws Exception {
        Digest digest = new Digest("SHA1");

        assertThat(digest.toString("hello", "utf-8"), is("aaf4c61ddcc5e8a2dabede0f3b482cd9aea9434d"));
        assertThat(digest.toString("Hello", "utf-8"), is("f7ff9e8b7bb2e09b70935a5d785e0cc5d9d0abf0"));
    }

    @Test
    void test_sha512_with_stream() throws Exception {
        Digest digest = new Digest("SHA-512");

        byte[] data1 = makeData();
        String result1 = digest.toString(new ByteArrayInputStream(data1));
        assertThat(result1, is("af216a7122d29d6a7dc7b89c8b41c111e7c9a00781d4a867a1d75110b48a5a9c92a15d1dc2aeabb" +
                "53b83bcffc50f44cfdcae29dc9984c8c84febd0189322be25"));

        byte[] data2 = makeData();
        String result2 = digest.toString(new ByteArrayInputStream(data2));
        assertThat(result2, is("af216a7122d29d6a7dc7b89c8b41c111e7c9a00781d4a867a1d75110b48a5a9c92a15d1dc2aeabb" +
                "53b83bcffc50f44cfdcae29dc9984c8c84febd0189322be25"));
    }
}
