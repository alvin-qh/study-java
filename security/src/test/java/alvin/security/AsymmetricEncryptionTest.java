package alvin.security;

import org.apache.commons.codec.binary.Base64;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class AsymmetricEncryptionTest {

    private static final int DEFAULT_RSA_KEY_SIZE = 1024;
    private static final int DEFAULT_AES_KEY_SIZE = 128;

    @Test
    void test_sign_with_private_key() throws Exception {
        AsymmetricEncryption encrypt = new AsymmetricEncryption("RSA");
        AsymmetricEncryption.KeyPair keyPair = encrypt.makeKey(DEFAULT_RSA_KEY_SIZE);

        byte[] publicKey = keyPair.getPublicKey();
        byte[] privateKey = keyPair.getPrivateKey();

        final byte[] srcData = "Hello World".getBytes(UTF_8);

        byte[] sign = encrypt.sign(privateKey, "SHA1", srcData);
        assertThat(encrypt.verifySign(publicKey, "SHA1", srcData, sign), is(true));

        byte[] srcData2 = srcData.clone();
        srcData2[1]++;
        assertThat(encrypt.verifySign(publicKey, "SHA1", srcData2, sign), is(false));

        sign[0]++;
        assertThat(encrypt.verifySign(publicKey, "SHA1", srcData, sign), is(false));
    }

    @Test
    void test_sign_stream() throws Exception {
        AsymmetricEncryption encrypt = new AsymmetricEncryption("RSA");
        AsymmetricEncryption.KeyPair keyPair = encrypt.makeKey(DEFAULT_RSA_KEY_SIZE);

        byte[] publicKey = keyPair.getPublicKey();
        byte[] privateKey = keyPair.getPrivateKey();

        byte[] data = "Hello World".getBytes(UTF_8);

        byte[] sign;
        try (InputStream in = new ByteArrayInputStream(data)) {
            sign = encrypt.sign(privateKey, "MD5", in);
        }

        try (InputStream in = new ByteArrayInputStream(data)) {
            assertThat(encrypt.verifySign(publicKey, "MD5", in, sign), is(true));
        }
    }

    @Test
    void test_bad_sign_to_file() throws Exception {
        AsymmetricEncryption encrypt = new AsymmetricEncryption("RSA");
        AsymmetricEncryption.KeyPair keyPair = encrypt.makeKey(DEFAULT_RSA_KEY_SIZE);

        byte[] publicKey = keyPair.getPublicKey();
        byte[] privateKey = keyPair.getPrivateKey();

        byte[] data = "Hello World".getBytes(UTF_8);

        byte[] sign;
        try (InputStream in = new ByteArrayInputStream(data)) {
            sign = encrypt.sign(privateKey, "MD5", in);
        }

        data[3] = (byte) ~data[3];

        try (InputStream in = new ByteArrayInputStream(data)) {
            assertThat(encrypt.verifySign(publicKey, "MD5", in, sign), is(false));
        }
    }

    @Test
    void test_rsa_encrypt() throws Exception {
        final String srcString = "Hello World";
        final byte[] srcData = srcString.getBytes(UTF_8);

        AsymmetricEncryption encrypt = new AsymmetricEncryption("RSA");
        AsymmetricEncryption.KeyPair keyPair = encrypt.makeKey(DEFAULT_RSA_KEY_SIZE);

        byte[] publicKey = keyPair.getPublicKey();
        byte[] privateKey = keyPair.getPrivateKey();

        int encBlockSize = keyPair.getEncBlockSize();
        int decBlockSize = keyPair.getDecBlockSize();

        byte[] encData = encrypt.encrypt(publicKey, encBlockSize, srcData);
        assertThat(srcString, not(new String(encData, UTF_8)));

        byte[] decData = encrypt.decrypt(privateKey, decBlockSize, encData);
        assertThat(new String(decData, UTF_8), is(srcString));
    }

    @Test
    void test_full() throws Exception {

        AsymmetricEncryption asymmetricEncryption = new AsymmetricEncryption("RSA");
        SymmetricEncryption symmetricEncryption = new SymmetricEncryption("AES");

        // A生成一对密钥，并与B共享公钥
        AsymmetricEncryption.KeyPair keyPairA = asymmetricEncryption.makeKey(DEFAULT_RSA_KEY_SIZE);

        // B生成一对密钥，并与A共享公钥
        AsymmetricEncryption.KeyPair keyPairB = asymmetricEncryption.makeKey(DEFAULT_RSA_KEY_SIZE);

        // A生成文件加密密钥
        byte[] fileKeyA = symmetricEncryption.makeKey(DEFAULT_AES_KEY_SIZE);

        // A用B的公钥将文件密钥加密
        byte[] encFileKeyA = asymmetricEncryption.encrypt(keyPairB.getPublicKey(),
                keyPairB.getEncBlockSize(), fileKeyA);

        // A用自己的私钥对加密结果签名
        byte[] signA = asymmetricEncryption.sign(keyPairA.getPrivateKey(), "SHA1", encFileKeyA);

        // A将加密的文件密钥及签名发送给B
        String encFileKeyContent = Base64.encodeBase64String(encFileKeyA);
        String signContent = Base64.encodeBase64String(signA);

        byte[] data = "Hello World".getBytes(UTF_8);

        // A使用文件密钥将数据加密, 发送给B
        String encFileContent;
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            try (InputStream in = new ByteArrayInputStream(data)) {
                symmetricEncryption.encrypt(fileKeyA, in, out);
            }
            encFileContent = Base64.encodeBase64String(out.toByteArray());
        }

        // ......

        // B接收到加密的文件密钥和签名，对利用A的公钥对文件密钥进行验签
        byte[] encFileKeyB = Base64.decodeBase64(encFileKeyContent);
        byte[] signB = Base64.decodeBase64(signContent);
        assertTrue(asymmetricEncryption.verifySign(keyPairA.getPublicKey(), "SHA1", encFileKeyB, signB));
        assertArrayEquals(encFileKeyB, encFileKeyA);

        // B利用自己的私钥对加密的文件密钥进行解密，得到文件密钥
        byte[] fileKeyB = asymmetricEncryption.decrypt(keyPairB.getPrivateKey(),
                keyPairB.getDecBlockSize(), encFileKeyB);
        assertArrayEquals(fileKeyB, fileKeyA);

        // B利用得到的文件密码对文件内容进行解密
        byte[] decData = symmetricEncryption.decrypt(fileKeyB, Base64.decodeBase64(encFileContent));
        assertThat(new String(decData, UTF_8), is("Hello World"));
    }


    @Test
    void test_ca() throws Exception {
        Path cerFile = Paths.get(SymmetricEncryptionTest.class.getResource("/client.cer").toURI());
        Path keyStoreFile = Paths.get(SymmetricEncryptionTest.class.getResource("/KeyStore.p12").toURI());

        CA ca = new CA("x509", "PKCS12");

        byte[] publicKey = ca.publicKeyFromCertificateFile(cerFile);
        byte[] privateKey = ca.privateKeyFromKeyStore(keyStoreFile, "root", "urp@wg.com");

        final String srcString = "Hello World";
        final byte[] srcData = srcString.getBytes(UTF_8);

        AsymmetricEncryption asymmetricEncryption = new AsymmetricEncryption("RSA");
        byte[] encData = asymmetricEncryption.encrypt(publicKey, 245, srcData);
        byte[] decData = asymmetricEncryption.decrypt(privateKey, 256, encData);

        assertThat(new String(decData, UTF_8), is(srcString));
    }
}
