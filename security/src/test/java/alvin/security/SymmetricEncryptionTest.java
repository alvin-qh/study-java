package alvin.security;

import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

class SymmetricEncryptionTest {

    private static final int DEFAULT_AES_KEY_SIZE = 128;

    @Test
    void test_des_enc_and_dec() throws Exception {
        final String expectedString = "Hello Java";
        final byte[] expectedData = expectedString.getBytes(UTF_8);

        SymmetricEncryption encrypt = new SymmetricEncryption("DES");

        byte[] key = encrypt.makeKey(56);
        byte[] encData = encrypt.encrypt(key, expectedData);

        assertThat(encData.length, greaterThan(expectedData.length));
        assertThat(new String(encData, UTF_8), not(expectedString));

        byte[] srcData = encrypt.decrypt(key, encData);
        assertThat(srcData.length, is(expectedData.length));
        assertThat(new String(srcData, UTF_8), is(expectedString));
    }

    @Test
    void test_3des_enc_and_dec() throws Exception {
        final String expectedString = "Hello Java";
        final byte[] expectedData = expectedString.getBytes(UTF_8);

        SymmetricEncryption encrypt = new SymmetricEncryption("DESede");

        byte[] key = encrypt.makeKey(168);
        byte[] encData = encrypt.encrypt(key, expectedData);

        assertThat(encData.length, greaterThan(expectedData.length));
        assertThat(new String(encData, UTF_8), not(expectedString));

        byte[] srcData = encrypt.decrypt(key, encData);
        assertThat(srcData.length, is(expectedData.length));
        assertThat(new String(srcData, UTF_8), is(expectedString));
    }

    @Test
    void test_aes_enc_and_dec() throws Exception {
        final String expectedString = "Hello Java";
        final byte[] expectedData = expectedString.getBytes(UTF_8);

        SymmetricEncryption encrypt = new SymmetricEncryption("AES");

        byte[] key = encrypt.makeKey(DEFAULT_AES_KEY_SIZE);
        byte[] encData = encrypt.encrypt(key, expectedData);

        assertThat(encData.length, greaterThan(expectedData.length));
        assertThat(new String(encData, UTF_8), not(expectedString));

        byte[] srcData = encrypt.decrypt(key, encData);
        assertThat(srcData.length, is(expectedData.length));
        assertThat(new String(srcData, UTF_8), is(expectedString));
    }

    @Test
    void test_aes_enc_and_dec_by_stream() throws Exception {
        SymmetricEncryption encrypt = new SymmetricEncryption("AES");
        byte[] key = encrypt.makeKey(DEFAULT_AES_KEY_SIZE);

        byte[] data = "Hello World".getBytes(UTF_8);

        byte[] encData;
        try (InputStream in = new ByteArrayInputStream(data)) {
            try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
                encrypt.encrypt(key, in, out);
                out.flush();
                encData = out.toByteArray();
            }
        }

        byte[] decData;
        try (InputStream in = new ByteArrayInputStream(encData)) {
            try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
                encrypt.decrypt(key, in, out);
                out.flush();
                decData = out.toByteArray();
            }
        }

        assertThat(new String(decData, UTF_8), is("Hello World"));
    }

    @Test
    void test_rc4_enc_and_dec() throws Exception {
        final String expectedString = "Hello Java";
        final byte[] expectedData = expectedString.getBytes(UTF_8);

        SymmetricEncryption encrypt = new SymmetricEncryption("RC4");

        byte[] key = encrypt.makeKey(0);
        byte[] encData = encrypt.encrypt(key, expectedData);

        assertThat(new String(encData, UTF_8), not(expectedString));

        byte[] srcData = encrypt.decrypt(key, encData);
        assertThat(srcData.length, is(expectedData.length));
        assertThat(new String(srcData, UTF_8), is(expectedString));
    }
}
