package alvin.security;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

public class CA {
    private final String certificateName;
    private final String keyStoreName;

    public CA(String certificateName, String keyStoreName) {
        this.certificateName = certificateName;
        this.keyStoreName = keyStoreName;
    }

    public byte[] publicKeyFromCertificateFile(Path certificateFile) throws Exception {
        CertificateFactory certificateFactory = CertificateFactory.getInstance(certificateName);
        try (InputStream in = Files.newInputStream(certificateFile)) {
            Certificate certificate = certificateFactory.generateCertificate(in);
            return certificate.getPublicKey().getEncoded();
        }
    }

    public byte[] privateKeyFromKeyStore(Path keyStoreFile, String aliasName, String keyStorePasswd)
            throws Exception {
        KeyStore keyStore = KeyStore.getInstance(keyStoreName);
        try (InputStream in = Files.newInputStream(keyStoreFile)) {
            keyStore.load(in, keyStorePasswd.toCharArray());
            PrivateKey privateKey = (PrivateKey) (keyStore.getKey(aliasName, keyStorePasswd.toCharArray()));
            return privateKey.getEncoded();
        }
    }
}
