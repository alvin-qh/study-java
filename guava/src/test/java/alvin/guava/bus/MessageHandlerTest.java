package alvin.guava.bus;

import com.google.common.eventbus.Subscribe;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SuppressWarnings("UnstableApiUsage")
public class MessageHandlerTest extends Handler {

    private boolean executed = false;

    @Subscribe
    public void onEvent(MessageEvent event) {
        executed = true;
        assertThat(event.getId(), is(1));
        assertThat(event.getMessage(), is("Hello"));
    }

    @Test
    void test_publish_event() {
        EventHub.getInstance().publish(new MessageEvent(1, "Hello"));
        assertThat(executed, is(true));
    }
}
