package alvin.guava.bus;

@SuppressWarnings("WeakerAccess")
public class MessageEvent implements Event {
    private int id;
    private String message;

    public MessageEvent(int id, String message) {
        this.id = id;
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }
}
