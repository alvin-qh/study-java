package alvin.guava.bus;

import com.google.common.eventbus.EventBus;

@SuppressWarnings("WeakerAccess")
public final class EventHub {
    private static EventHub singleton;

    private final EventBus eventBus;

    private EventHub(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    public void publish(Event event) {
        eventBus.post(event);
    }

    public void register(Handler handler) {
        eventBus.register(handler);
    }

    public static EventHub getInstance() {
        if (singleton == null) {
            singleton = new EventHub(new EventBus());
        }
        return singleton;
    }
}
