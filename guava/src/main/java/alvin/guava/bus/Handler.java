package alvin.guava.bus;

@SuppressWarnings("WeakerAccess")
public abstract class Handler {

    public Handler() {
        EventHub.getInstance().register(this);
    }
}
