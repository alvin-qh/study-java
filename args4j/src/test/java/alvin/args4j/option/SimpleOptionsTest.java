package alvin.args4j.option;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.ParserProperties;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.kohsuke.args4j.OptionHandlerFilter.ALL;

@Slf4j
class SimpleOptionsTest {
    private ParserProperties properties;

    @BeforeEach
    void setUp() {
        properties = ParserProperties.defaults()
                .withUsageWidth(80);
    }

    @Test
    void test_get_option() throws Exception {
        SimpleOptions options = new SimpleOptions();
        CmdLineParser parser = new CmdLineParser(options, properties);

        final String[] arguments = {
                "-n", "Alvin",
                "-a", "32",
                "--save",
                "arg1",
                "arg2"
        };
        parser.parseArgument(arguments);
        log.debug("parsed options are: {}", options);

        assertThat(options.getName(), is("Alvin"));
        assertThat(options.getAge(), is(32));
        assertThat(options.getSave(), is(true));
        assertThat(options.getArguments(), contains("arg1", "arg2"));
    }

    @Test
    void test_example() {
        SimpleOptions options = new SimpleOptions();
        CmdLineParser parser = new CmdLineParser(options, properties);

        String example = parser.printExample(ALL);
        log.debug("example of all arguments are: {}", example);

        assertThat(example, is(" -a (--age) <number> -n (--name) <string> -s (--save)"));
    }

    @Test
    void test_print_usage_require_options_only() throws Exception {
        SimpleOptions options = new SimpleOptions();
        CmdLineParser parser = new CmdLineParser(options, properties);

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            parser.printUsage(out);
            String usage = new String(out.toByteArray());
            log.debug("usage of required arguments are: {}", usage);

            assertThat(usage, is(" arguments            : arg1 arg2 ...\n" +
                    " -n (--name) <string> : Your name\n" +
                    " -s (--save)          : Save change\n"));
        }
    }

    @Test
    void test_print_usage_all_options() throws Exception {
        SimpleOptions options = new SimpleOptions();
        CmdLineParser parser = new CmdLineParser(options, properties);

        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            try (OutputStreamWriter writer = new OutputStreamWriter(out)) {
                parser.printUsage(writer, null, ALL);
                String usage = new String(out.toByteArray());
                log.debug("usage of all arguments are: {}", usage);

                assertThat(usage, is(" arguments            : arg1 arg2 ...\n" +
                        " -a (--age) <number>  : Your age (default: 18)\n" +
                        " -n (--name) <string> : Your name\n" +
                        " -s (--save)          : Save change\n"));
            }
        }
    }
}
