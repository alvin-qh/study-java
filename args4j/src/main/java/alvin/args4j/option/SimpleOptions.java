package alvin.args4j.option;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;

import java.util.List;

@SuppressWarnings("All")
public class SimpleOptions {

    @Option(name = "-n",
            aliases = {"--name"},
            metaVar = "<string>",
            usage = "Your name",
            required = true)
    private String name;

    @Option(name = "-a",
            aliases = {"--age"},
            metaVar = "<number>",
            usage = "Your age",
            hidden = true,
            required = false)
    private int age = 18;   // SUPPRESS

    @Option(name = "-s",
            aliases = {"--save"},
            usage = "Save change")
    private Boolean save;

    @Argument(usage = "arg1 arg2 ...",
            metaVar = "arguments",
            required = true,
            index = 0,
            multiValued = true)
    private List<String> arguments;

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public Boolean getSave() {
        return save;
    }

    public List<String> getArguments() {
        return arguments;
    }

    @Override
    public String toString() {
        return "SimpleOptions{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", save=" + save +
                ", arguments=" + arguments +
                '}';
    }
}
