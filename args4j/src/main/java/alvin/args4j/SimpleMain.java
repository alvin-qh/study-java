package alvin.args4j;

import alvin.args4j.option.SimpleOptions;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;

import static org.kohsuke.args4j.OptionHandlerFilter.ALL;

public final class SimpleMain {

    private SimpleMain() {
    }

    public static void main(String[] args) {
        SimpleOptions options = new SimpleOptions();
        CmdLineParser parser = new CmdLineParser(options);

        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println();

            System.err.println("java SampleMain [options...] arguments...");
            parser.printUsage(System.err);
            System.err.println();

            System.err.println("  Example: java SampleMain" + parser.printExample(ALL));
        }
    }
}
